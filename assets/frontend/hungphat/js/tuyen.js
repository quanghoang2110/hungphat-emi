/**
 * Created by tuyennguyen on 10/22/2016.
 */
function submitContactPage() {
    var hoten = $('#name_user').val();
    var sodienthoai = $('#phone_user').val();
    var email = $('#email_user').val();
    var note = $('#content_user').val();
    var is_check = 1;
    //var mess = 'Xin vui lòng nhập : ';
    var itemErr = 0;
    if (hoten == "" || hoten == null) {
        is_check = 0;
        $('#error_name_user').html('Xin nhập họ tên');

    }
    if (sodienthoai == "" || sodienthoai == null) {
        is_check = 0;
        $('#error_phone_user').html('Xin nhập số điện thoại');
    }

    if (email == "" || email == null) {
        is_check = 0;
        $('#error_email_user').html('Xin nhập email');
    } else {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            is_check = 0;
            //mess += '<p>Xin nhập đúng định dạng email</p>';
            $('#error_email_user').html('Xin nhập đúng định dạng email');
        }
    }

    if (is_check == 0) {
        //showThongBao(mess);
        $('#error_name_user').removeClass('hidden');
        $('#error_phone_user').removeClass('hidden');
        $('#error_email_user').removeClass('hidden');
        setTimeout(function () {
            $('#error_name_user').addClass('hidden');
            $('#error_phone_user').addClass('hidden');
            $('#error_email_user').addClass('hidden');
        }, 3000)
    } else {
        $.ajax({
            type: "POST",
            url: base_url + "frontend/contact/save_contact_page",//trỏ tới file nào
            data: {
                hoten: hoten,
                email: email,
                sodienthoai: sodienthoai,
                note: note
            },
            success: function (data) {
                showThongBao("Gửi  thành công. Chúng tôi sẽ liên hệ sớm với bạn");
                //else
                //    showThongBao('Thất bại,xin vui lòng  gửi lại');
                setTimeout(function () {
                    location.reload();
                }, 2000);
            },
            error: function () {
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }
}
function submitContact() {
    var hoten = $('#hotenContact').val();
    var sodienthoai = $('#sodienthoaiContact').val();
    var email = $('#emailContact').val();
    var note = $('#noteContact').val();
    var is_check = 1;
    //var mess = 'Xin vui lòng nhập : ';
    var itemErr = 0;
    if (hoten == "" || hoten == null) {
        is_check = 0;
        //if(itemErr == 0){
        //    mess += ' họ tên';
        //}else{
        //    mess += ', họ tên';
        //}
        //itemErr++;
        $('.errorhoten').html('Xin nhập họ tên');

    }
    if (sodienthoai == "" || sodienthoai == null) {
        is_check = 0;
        //if(itemErr == 0){
        //    mess += ' số điện thoại';
        //}else{
        //    mess += ',số điện thoại';
        //}
        //itemErr++;
        $('.errorsodienthoai').html('Xin nhập số điện thoại');
    }

    if (email == "" || email == null) {
        is_check = 0;
        //if(itemErr == 0){
        //    mess += ' email';
        //}else{
        //    mess += ',email';
        //}
        //itemErr++;
        $('.erroremail').html('Xin nhập email');
    } else {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            is_check = 0;
            //mess += '<p>Xin nhập đúng định dạng email</p>';
            $('.erroremail').html('Xin nhập đúng định dạng email');
        }
    }

    if (is_check == 0) {
        //showThongBao(mess);
        $('.errorhoten').removeClass('hidden');
        $('.errorsodienthoai').removeClass('hidden');
        $('.erroremail').removeClass('hidden');
        setTimeout(function () {
            $('.errorhoten').addClass('hidden');
            $('.errorsodienthoai').addClass('hidden');
            $('.erroremail').addClass('hidden');
        }, 3000)
    } else {
        $.ajax({
            type: "POST",
            url: base_url + "frontend/contact/save",//trỏ tới file nào
            data: {
                hoten: hoten,
                email: email,
                sodienthoai: sodienthoai,
                note: note
            },
            success: function (data) {
                $('#contact').modal('hide');
                //var rs = JSON.parse(data);
                //if(rs.is_true == 1)
                showThongBao("Gửi  thành công. Chúng tôi sẽ liên hệ sớm với bạn");
                //else
                //    showThongBao('Thất bại,xin vui lòng  gửi lại');
            },
            error: function () {
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }

}
function frm_send_email() {
    var email = $('#email_get_info').val();
    if (email == null || email == "") {
        showThongBao('Bạn chưa nhập email');
    } else {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            showThongBao("Xin nhập đúng định dạng email");
        } else {
            $.ajax({
                type: "POST",
                url: base_url + "fronted/contact_mail/save",//trỏ tới file nào
                data: {
                    email: email
                },
                success: function () {
                    showThongBao("Gửi mail thành công");
                },
                error: function () {
                    showThongBao("Thất bại. xin vui lòng làm lại");
                }
            });
        }

    }
}
function showThongBao(text, action, onactionModal) {
    $("#modal_msg .modal-body p").html(text);
    $("#modal_msg").modal("show");
    if (onactionModal) {
        afterThongBaoModal = onactionModal;
        $(onactionModal).modal('hide');
        afterThongBao();
    }
    if (action == 'home') {
        $('#modal_msg #msgOK').click(function () {
            location.href = '/';
        });
    }
    else if (action == 'reload') {
        $('#modal_msg #msgOK').click(function () {
            location.reload();
        });
    } else if (action == 'login') {zzzz
        $('#modal_msg #msgOK').click(function () {
//                location.href = '<?php //echo ROUTE_ADMIN_LOGIN ?>//';
        });
    }
}
function afterThongBao() {
    if (afterThongBaoModal) {
        $('#modal_msg').on('hidden.bs.modal', function (e) {
            $(afterThongBaoModal).modal("show");
            afterThongBaoModal = "";
        });
    }
}

$(document).ready(function(){
    $('.fb-comments span').css('width','100%');
    $('.fb-comments span .fb_ltr').css('width','100%');
})
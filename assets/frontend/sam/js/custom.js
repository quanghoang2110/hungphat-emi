$(document).ready(function () {
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
    
    //Click event to scroll to top
    $('#back-top').click(function(){
        $('html, body').animate({scrollTop : 0}, 'slow');
        return false;
    });
    
    //?????
    // $(".maxmun-slider-product").owlCarousel({
    //     loop: false,
    //     margin: 10,
    //     nav: true,
    //     navText:['',''],
    //     responsiveClass: true,
    //     responsive: {
    //         0: {
    //             items: 1
    //         },
    //         400: {
    //             items: 2
    //         },
    //         640: {
    //             items: 3,
    //             loop: true
    //         },
    //         1020: {
    //             items: 4,
    //             loop: true
    //         }
    //     }
    // });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href"); // activated tab
        $(target+' > .owl-carousel').addClass('maxmun-slider-product').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            navText:['',''],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 2
                },
                640: {
                    items: 3,
                    loop: true
                },
                1020: {
                    items: 4,
                    loop: true
                }
            }
        });
    });
});

function addCommas(value) {
    return value.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ".");
}

function InitComma(){
    $(".comma").bind('input', function () {
        var textbox = $(this);
        var value = textbox.val();
        value = value.replace(/[^0-9]/g, '');
        value = addCommas(value);
        textbox.val(value);
    });
}

$(function(){
    InitComma();
    $('.comma').each(function(){
        $(this).val(addCommas($(this).val()));
    });

    $('.datepicker').datepicker({
        language : 'vi',
        autoclose : true
    });
})

$('.datepicker').keydown(function(){
    return false;
})

$(document).on('click', '.push-cat', function(e) {
    ShowLoading();
    var redirect = false;
    if($(this).hasClass('product-cat')){
        redirect = true;
    }

    var id = $(this).data('product-id');
    $.post(url_cart_add + '/' + id + '/' + 1, {})
        .done(function (data) {
            if(redirect){
                window.location.href = url_cart;
            }else {
                var data = JSON.parse(data);
                var total = parseInt(data.total);
                if (total > 99) {
                    $('#circle-cart').html('99+');
                } else {
                    $('#circle-cart').html(total);
                }
                InfoCart(function(){HideLoading();});
            }
            HideLoading();
        })
        .fail(function(){
        HideLoading();
    });
})

$(document).on('click', '.icon-del', function() {
    ShowLoading();
    var id = $(this).data('product-id');
    UpdateTotal();
    $.post(url_cart_delete + '/' + id, {})
        .done(function (data) {
            var num = $('#circle-cart').html();
            if(parseInt(num) > 99){
                $('#circle-cart').html('99+');
            }else if(parseInt(num) > 0) {
                $('#circle-cart').html(parseInt(num) - 1);
            }
            InfoCart(function(){
                HideLoading()
            });
        })
        .fail(function(){
            HideLoading();
        });
})

function UpdateTotal(){
    var total = 0;
    var qty = 0;
    var price = 0;
    $('.bg-cart').each(function(){
        qty = parseInt($(this).find('.txt-num').val());
        price = parseInt($(this).find('.price-new').html().replace(' VNĐ', '').replace('.', ''));
        total = total + (qty * price);
    });
    var rs = addCommas(total) + ' VNĐ';
    $('.total-price > span').html(rs);
}
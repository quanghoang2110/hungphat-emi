function menubar(){
    if($('nav.menu').hasClass('open')) {
        $('nav.menu').addClass('effect-menu').css({'left': '-250px'}).removeClass('open');
        $('header .topbar, #page').addClass('effect-menu').css({'margin-left': '0'});
        $('header .topbar').addClass('effect-menu').css({'position':'fixed'});
    }else{
        $('nav.menu').addClass('effect-menu').css({'left': '0'}).addClass('open');
        $('#page').addClass('effect-menu').css({'margin-left': '250px'});
        $('header .topbar, #page').addClass('effect-menu').css({'position':'relative'});
    }
}
$('.lesson').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
});

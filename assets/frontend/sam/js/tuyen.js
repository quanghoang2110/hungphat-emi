/**
 * Created by TuanHai on 7/25/2016.
 */
function exam_ajax() {
    $('.before').hide();
    $('.after').removeClass("hidden");
    $('#send_my_answer').attr('data-show', '0');
}

function not_send() {
    $('.before').show();
    $('.after').addClass("hidden");
    $('#send_my_answer').attr('data-show', '1');
}

// hàm đồng hồ đếm ngược
var m = null; // Phút
var s = null; // Giây
var timeout = null; // Timeout

//tao dong ho dem nguoc thoi gian
function start() {
    if (m === null) {
        m = parseInt($('#time_exam_question').val());
        s = 0;
    }
    if (s === -1) {
        m -= 1;
        s = 59;
    }

    if (m === -1) {
        clearTimeout(timeout);
        return false;
    }
    //show time ra
    document.getElementById('m').innerText = m < 10 ? '0' + m : m.toString();
    document.getElementById('s').innerText = s < 10 ? '0' + s : s.toString();
    document.getElementById('M').innerText = m.toString();
    document.getElementById('S').innerText = s.toString();
    //gọi lại hàm mỗi 1s
    timeout = setTimeout(function () {
        s--;
        start();
    }, 1000);
}

var sum_exam_question = $('#input_sum_exam_question').val();
var sum_page_exam_question = $('#input_sum_page_exam_question').val();
//tuyen: in tong so cau
$(document).ready(function () {
    //hien slide
    $('.sum_exam_question').html($('#input_sum_exam_question').val());
    for (var i = 1; i <= $('#input_sum_exam_question').val(); i++) {
        if (i <= 2) {
            $('.topic_name_' + i).show();
            $('.header_question_' + i).show();
            $('.question_' + i).show();
            $('.answer_' + i).show();
            $('#fill_text_answer_' + i).show();
			$('.height-20-'+i).removeClass('display_none');
        }
        else {
            $('.topic_name_' + i).hide();
            $('.header_question_' + i).hide();
            $('.question_' + i).hide();
            $('.answer_' + i).hide();
            $('#fill_text_answer_' + i).hide();
			$('.height-20-'+i).addClass('display_none');
        }
    }
    //hien phan trang
    if ($('#input_sum_page_exam_question').val() > 4) {
        $('.ba-cham-sau').removeClass("display_none");
        for (i = 5; i <= $('#input_sum_page_exam_question').val(); i++) {
            $('#page_' + i).addClass("display_none");
        }
    }
    //hien nut next neu so trang > 2
    if ($('#input_sum_page_exam_question').val() > 2) {
        $('#next_page').removeClass("display_none");
    }
    $('.content3').addClass("display_none");
    start();
    //sau thoi gian quy dinh cua bai test, nguoi dung chua lam xong thi tu dong show ra ket qua
    var time_do_exam_question = parseInt($('#time_exam_question').val()) * 60000;
    setTimeout(function () {
        if ($('#send_my_answer').attr('data-show') == 1) {
            showThongBao('Hết thời gian làm bài. Đây là kết quả bạn đạt được');
            savetest();
        }
    }, time_do_exam_question);
//		radio_checked();
    $('input:radio').click(function () {
        var name = $(this).attr('name');
        $('input[name=' + name + ']:radio').each(function () {
            $(this).attr('data-checked', 0);
        });
        $(this).attr('data-checked', 1);
    });

    //thay doi so thu tu page se hien thi cau tyuong ung
    $('#stt_page').change(function () {
        show_exam_question($('#stt_page').val());
    });

    //vơi nhap ma code active khoa hoc
    $('#code_active_pack').val('');
})

//tuyen: ham danh sau radio duoc checked
function radio_checked() {
    $('input:radio').click(function () {
        var name = $(this).attr('name');
        $('input[name=' + name + ']:radio').each(function () {
            $(this).attr('data-checked', 0);
        });
        $(this).attr('data-checked', 1);
    });
}
//click vao page
$('.page_click').click(function () {
    var page = $(this).data('page');
    show_exam_question(page);
});
//click next page
$('#next_page').click(function () {
    var page = $('#next_page').attr('data-next-page');
    // alert(page);
    show_exam_question(page);
})
//click prev  page
$('#prev_page').click(function () {
    var page = $('#prev_page').attr('data-prev-page');
    // alert (page);
    show_exam_question(page);
})
//tuyen: tinh diem cho bai test
function savetest() {
    var score_test = 0;
    $('input:radio').each(function () {
        if ($(this).attr('data-checked') == 1 && $(this).attr('data-correct') == 1) {
            score_test += 1;
        }
    })
//		alert(
//			'diem radio ' + score_test
//		);
    // tinh diem cho cac cau dien tu
    for (var i = 1; i <= parseInt($('#input_sum_exam_question_type_text').val()); i++) {
//			kiem tra cau hoi do nguoi dung co tra loi khong. Neu co thi moi bat dau so sanh voi dap an
        var fill_text_answer = $('#fill_text_answer_' + i).val();
        var fillanswer = $.trim(fill_text_answer).toLowerCase();
//			var fillanswer = fill_text_answer;
        if (fillanswer != null && fillanswer != '') {
            //lay tat ca cac dap an cua cau hoi do
            var answer = $('#answer_' + i).val();
            answer = answer.toLowerCase();
            answer = $.trim(answer);
            //Th1: co nhieu dap dan dung. tach cac dap an ra.b1 tim kiem dau /
            if (answer.indexOf("/") != -1) {
                var count = 0;
                var answer_correct = new Array();
                //bat dau cat
                while (answer.indexOf("/") != -1) {
                    var id = answer.indexOf("/");
                    //lay ra cac dap an
                    answer_correct[count] = answer.slice(0, id);
                    var cut = answer_correct[count] + '/';
                    //cat doan vua lay ra
                    answer = answer.replace(cut, '');
                    count += 1;
                }
                //lay dap an cuoi cung
                answer_correct[count] = answer;
                //trim cac dap an vua cat
                for (var item = 0; item <= count; item++) {
                    answer_correct[item] = $.trim(answer_correct[item]);
                    //neu nguoi dung nhap dung dap an thi thoat vong for
                    if (fillanswer == answer_correct[item]) {
                        score_test += 1;
                        item = count;
                    }
                }
            }
            //Th2: chi co 1 dap an dung
            else {
                answer = $.trim(answer);
                if (fillanswer == answer) {
                    score_test += 1;
                }
            }
        }
    }
//		alert (score_test);
    //an danh sach cau hoi
    $('.content2').addClass("display_none")
    $('.after').addClass("display_none")
    //chuyển sang trang hiển thị kết quả
    $('.content3').removeClass("display_none")
    $('.result_exam_test').html(score_test);
    $('#input_correct_answer_exam_question').val(score_test);


}

function show_exam_question(page) {

    var p = $("#test_focus");
    var position = p.position();
    console.log("left: " + position.left + ", top: " + position.top);
    $('body,html').animate({
        scrollTop: 30
    }, 800);

    for (var i = 1; i <= sum_exam_question; i++) {
        if (i >= ((page * 2) - 1) && i <= (page * 2)) {
            $('.topic_name_' + i).show();
            $('.header_question_' + i).show();
            $('.question_' + i).show();
            $('.answer_' + i).show();
            $('#fill_text_answer_' + i).show();
            var stt_question = i;
			$('.height-20-'+i).removeClass('display_none');
        } else {
            $('.topic_name_' + i).hide();
            $('.header_question_' + i).hide();
            $('.question_' + i).hide();
            $('.answer_' + i).hide();
            $('#fill_text_answer_' + i).hide();
			$('.height-20-'+i).addClass('display_none');
        }
    }
    $('#stt_page').val(page);
    //hien thi cac cau dang duoc show
    var stt_question_prev = stt_question - 1;
    $('#stt_question').html(stt_question_prev + ' - ' + stt_question);
    //thay doi data page o nut prev
    var next_page = parseInt(page) + 1;
    $('#next_page').attr('data-next-page', next_page);
    var prev_page = parseInt(page) - 1;
    $('#prev_page').attr('data-prev-page', prev_page);
    //neu page > 1 thi hien thị nut 3 cham truoc.
    if (page != 1) {
        $('.ba-cham-truoc').removeClass('display_none');
        $('#prev_page').removeClass('display_none');
        $('#prev_page').attr('data-prev-page', page - 1);
    } else {
        $('.ba-cham-truoc').addClass('display_none');
        $('#prev_page').addClass('display_none');
    }
    //button phan trang lấy từ page = page_click
    var count_page = 0;
    for (i = 1; i <= sum_page_exam_question; i++) {
        //remove cac nut o cac page truoc do
        if (i < page) {
            $('#page_' + i).addClass('display_none');
        }
        //hien thi cac page tu page vua chon
        else {
//					chi hien thi 3 page tiep theo
            if (count_page < 4) {
                count_page += 1;
                $('#page_' + i).removeClass('display_none');
                //an nut co 3 cham sau
                $('.ba-cham-sau').addClass('display_none');
                $('#next_page').addClass('display_none');
            }
            else {
                $('#page_' + i).addClass('display_none');
                $('.ba-cham-sau').removeClass('display_none');
                $('#next_page').removeClass('display_none');
            }
        }
    }
}
function show_pack_suggest() {
    $('.content3').css('display', 'none');
    $('.content5').removeClass('display_none');
    $.ajax({
        type: "POST",
        url: base_url + "frontend/test/suggest_pack",//trỏ tới file nào
        data: {
            exam_id: ID_BAI_KIEM_TRA_DAU_VAO,
            sum_question: $('#input_sum_exam_question').val(),
            correct_question: $('#input_correct_answer_exam_question').val()
        },
        success: function (data) {
            var rs = JSON.parse(data);
            $('#pack_suggest_name').html(rs.pack_name);
            $('#pack_suggest_description').html(rs.description);
        },
        error: function () {
            showThongBao("Thất bại. xin vui lòng làm lại");
        }
    });
}


function send_change_pass() {
    var fill_old_pass = $('#fill-old-pass').val();
    var fill_new_pass = $('#fill-new-pass').val();
    var error = 0;
    if( fill_old_pass == null || fill_old_pass == ""){
        error = 1;
        $('#no-fill-old-pass').removeClass('hidden');
    }
    if( fill_new_pass == null || fill_new_pass == ""){
        error = 1;
        $('#no-fill-new-pass').removeClass('hidden');
    }
    var pass=$('#pass').val();
    if(fill_old_pass != pass){
        error = 1;
        $('#fill-false-pass').removeClass('hidden');
    }
     if(fill_new_pass.length >0 && fill_new_pass.length < 6){
        error = 1;
         $('#new-pass-false').removeClass('hidden');
     }
    if(error == 1){
        $('.show-error-change-pass').removeClass('hidden');
        setTimeout(function () {
            $('.show-error-change-pass').addClass('hidden');
            $('#no-fill-old-pass').addClass('hidden');
            $('#no-fill-new-pass').addClass('hidden');
            $('#fill-false-pass').addClass('hidden');
            $('#new-pass-false').addClass('hidden');
        },3000)
    }else{
        $.ajax({
            type: "POST",
            url: base_url + "frontend/user/change_pass",//trỏ tới file nào
            data: {
                new_pass: fill_new_pass
            },
            success: function (){
                $('#modalThayMK').modal('hide');
                showThongBao('Thay đổi mật khẩu thành công');
            },
            error: function(){
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }

}

$('#stt_page').change(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
});
//tuyen: js cua bai test (copy code anh Quang sang file nay)
// urlTest_detail = base_url + URL_FRONTEND_TEST_DETAIL;

function test_detail() {
    window.location.href = urlTest_detail;
}
$(function(){
    if(window.location.href.indexOf('quen-mat-khau') !== -1){
        GetPass();
    }
})

function Login() {
    $('#modal_login').modal('show');
    $('#modal_login #tab_login').addClass('active');
    $('#modal_login #tab_register').removeClass('active');
    $('#modal_login #dangnhap').addClass('active');
    $('#modal_login #dangky').removeClass('active');
}

function Register() {
    $('#modal_login').modal('show');
    $('#modal_login #tab_register').addClass('active');
    $('#modal_login #tab_login').removeClass('active');
    $('#modal_login #dangky').addClass('active');
    $('#modal_login #dangnhap').removeClass('active');
}

function ForgotPass(){
    $('#modal_login').modal('hide');
    $('#modal_forgot_pass').modal('show');
}

function GetPass(){
    $('#modal_get_pass').modal('show');
}

function MemberInfo(){
    var id = $('#hdinfoID').val();
    $.ajax({
        type :'GET',
        url: base_url + 'frontend/user/index',
        data:{id:id},
        success: function(data){
            // var result = JSON.parse(data);
            var result = $.parseJSON(data);
            $('#modal_member_info').modal('show');
                $('#member_info_name').val(result.fullname);
                $('#member_info_email').val(result.email);
                $('#member_info_phone').val(result.phone);
                $('#member_info_birthday').val(result.birthday);
                $('#member_info_address').val(result.address);
                $('#member_info_org').val(result.org);
        }
    })
}

function UpdatePass(){
    $('#modal_update_pass').modal('show');
}

function showThongBao(text, callback) {
    $("#modal_msg .modal-body p").html(text);
    $("#modal_msg").modal("show");
    if(typeof (callback) !== 'undefined'){
        $("#modal_msg .btn-callback").click(function(){
            callback();
        });
    }
}

function ShowLoading(){
    $('#modal_loading').modal('show');
}

function HideLoading(){
    $('#modal_loading').modal('hide');
}

var frm_login = $('#frm_login');
// frm_login.submit(function (ev) {
//     // if (!requestSent) {
//     //     $(this).find('.has-error').removeClass('has-error');
//     alert ('1');
//         $.ajax({
//             type: 'POST',
//             url: base_url + 'frontend/login/login',
//             data: new FormData(this),
//             processData: false,
//             contentType: false,
//             success: function (data) {
//                 alert ('1');
//                 var result = JSON.parse(data);
//                 // if (result.b_Check != null) {
// //                     if (!result.b_Check) {
// //                         if (result.msg_error != null) {
// //                             var top = "";
// //                             var index = 0;
// //                             var msg = '';
// //                             // for (var i in result.msg_error) {
// //                                 msg = msg + '<p>' + result.msg_error[i] + '</p>';
// //                                 // index++;
// //                                 if (index == 1) {
// //                                     top = i;
// //                                 }
// //                                 $('#' + i).closest('.form-group').addClass('has-error');
// //                             // }
// //                             ShowMessageError('alert_login', msg);
// //                             $('#alert_login').html(msg).show();
// //                             $("#" + top).focus();
// //                             return false;
// //                         }
// //                     }
// //                 } else {
// //
// //                     if (result.status == 1) {
// // //dang nhap thanh cong
// //                         location.reload();
// //                     } else if (result.status == 2) {
// //                         ShowMessageError('alert_login', 'Tài khoản của bạn đã bị khóa.');
// //                     } else {
// //                         ShowMessageError('alert_login', 'Sai tên tài khoản hoặc mật khẩu.');
// //                     }
// //                 }
// //                 requestSent = false;
//                 if(result.msg_error != 0){
//                     showThongBao('Sai tên tài khoản hoặc mật khẩu');
//                 } else  {
//                     showThongBao('Đăng nhập thành công');
//                     location.reload();
//                 }
//             },
//             error: function (jqXHR, error, errorThrown) {
//                 requestSent = false;
//             }
//         });
//     // }
//     ev.preventDefault();
// });

function showThongBao(text, callback) {
    $('header .topbar').css('z-index','0');
    $("#modal_msg .modal-body p").html(text);
    $("#modal_msg").modal("show");
    if(typeof (callback) !== 'undefined'){
        $("#modal_msg .btn-callback").click(function(){
            callback();
        });
    }
}

$('#submit_login').click(function () {
    $.ajax({
        type: "POST",
        url: base_url + "frontend/login/login",//trỏ tới file nào
        data: {
            username: $("#username").val(),
            password : $('#password').val()
        },
        success: function (data) {
            var rs = $.parseJSON(data)
            if(rs.msg_error == 0){
                location.reload();
            } else {
                showThongBao('Sai email hoặc mật khẩu');
            }
        },
        error: function(){
            showThongBao("Thất bại. xin vui lòng làm lại");
        }
    });
})
var frm_regis = $('#frm_regis');
frm_regis.submit(function (ev) {
    if (!requestSent) {
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/login/register',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                var error ='';
                    if (result.msg != '') {
                        for (var i in result.msg) {
                            error = error + '<p>' + result.msg[i] + '</p>';
                        }
                        ShowMessageError('alert_regis', error);
                        $("#" + top).focus();
                        return false;
                    } else {
                        window.location.reload();
                    }
                // requestSent = false;
            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    }
    ev.preventDefault();
});

var frm_forgot_pass = $('#frm_forgot_pass');
frm_forgot_pass.submit(function (ev) {
    if (!requestSent) {
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/login/forgot_pass',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                                index++;
                                if (index == 1) {
                                    top = i;
                                }
                                $('#forgot_pass_' + i).closest('.form-group').addClass('has-error');
                            }
                            ShowMessageError('alert_forgot_pass', msg);
                            $("#" + top).focus();
                            return false;
                        }
                    }
                } else {

                    if (result.status == 1) {
                        ShowMessageSuccess('alert_forgot_pass', 'Bạn hãy kiểm tra email để lấy lại mật khẩu.');
                    }else{
                        ShowMessageError('alert_forgot_pass', 'Đã có lỗi xảy ra, vui lòng thử lại sau.');
                    }
                }
                requestSent = false;
            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    }
    ev.preventDefault();
});

var frm_get_pass = $('#frm_get_pass');
frm_get_pass.submit(function (ev) {
    if (!requestSent) {
        $(this).find('.has-error').removeClass('has-error');
        var email = getUrlParameter('email');
        var key = getUrlParameter('key');
        var frmData = new FormData(this);
        frmData.append('email', email);
        frmData.append('key', key);
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/login/get_pass',
            data: frmData,
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                                index++;
                                if (index == 1) {
                                    top = i;
                                }
                                $('#get_pass_' + i).closest('.form-group').addClass('has-error');
                            }
                            ShowMessageError('alert_get_pass', msg);
                            $("#" + top).focus();
                            return false;
                        }
                    }
                } else {
                    if (result.status == 1) {
                        ShowMessageSuccess('alert_get_pass', 'Đổi mật khẩu thành công.');
                        $(frm_get_pass).find('input').val('');
                    }else{
                        ShowMessageError('alert_get_pass', 'Đã có lỗi xảy ra, vui lòng thử lại sau.');
                    }
                }
                requestSent = false;
            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    }
    ev.preventDefault();
});

function ShowMessageError(id, msg){
    $('#' + id).html(msg).removeClass().addClass('alert alert-danger').show();
}

function ShowMessageSuccess(id, msg){
    $('#' + id).html(msg).removeClass().addClass('alert alert-success').show();
}

var frm_update_pass = $('#frm_update_pass');
frm_update_pass.submit(function (ev) {
    if (!requestSent) {
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/login/update_pass',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                                index++;
                                if (index == 1) {
                                    top = i;
                                }
                                $('#update_pass_' + i).closest('.form-group').addClass('has-error');
                            }
                            ShowMessageError('alert_update_pass', msg);
                            $("#" + top).focus();
                            return false;
                        }
                    }
                } else {
                    if (result.status == 1) {
                        ShowMessageSuccess('alert_update_pass', 'Đổi mật khẩu thành công.');
                        $(frm_update_pass).find('input').val('');
                    }else{
                        ShowMessageError('alert_update_pass', 'Đã có lỗi xảy ra, vui lòng thử lại sau.');
                    }
                }
                requestSent = false;
            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    }
    ev.preventDefault();
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var frm_member_info = $('#frm_member_info');
frm_member_info.submit(function (ev) {
    if (!requestSent) {
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/user/change_info',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                                index++;
                                if (index == 1) {
                                    top = i;
                                }
                                $('#member_info_' + i).closest('.form-group').addClass('has-error');
                            }
                            ShowMessageError('alert_member_info', msg);
                            $("#" + top).focus();
                            return false;
                        }
                    }
                } else {
                    if (result.status == 1) {
                        showThongBao('Thay đổi thông tin thành công',function(){
                            location.reload();
                        })
                        
                    } else {
                        ShowMessageError('alert_member_info', 'Đã có lỗi xảy ra, vui lòng thử lại sau.');
                    }
                }
                requestSent = false;
            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    }
    ev.preventDefault();
});

function InfoCart(callback){
    ShowLoading();
    $('#cart-table').html('');
    $.post(url_cart_ajax, {})
        .done(function (data) {
            HideLoading();
            var data = JSON.parse(data);
            $('#modal_cart_content').html(data.html);
            $('#modal_cart').modal('show');
            if(typeof (callback) != 'undefined'){
                callback();
            }
        })
        .fail(function(){
            HideLoading();
        });
}
$('.datepicker').keydown(function(){
    return false;
});

// tuyen mua khoa hoc
$('#buy_pack').click(function () {
    var session = $(this).data('session');
    if ( session == 'none-session'){
        showThongBao('Bạn phải đăng nhập vào hệ thống');
    } else {
        var pack_id = $(this).data('pack-id');
        // var pack_name = $(this).data('pack-name');
        // var image = $(this).data('image');
        // var money = $(this).data('money');
        // var time = $(this).data('time');
        // $('#modal_buy_pack').modal('show');
        // $('#image_pack_buy').html("<img src='"+base_url+image+"'/>");
        // $('#name_pack_buy').html(' <b>'+pack_name+'</b>');
        // $('#time_pack_buy').html(' <b>'+time+' (ngày)</b>');
        // $('#money_pack_buy').html(' <b>'+money+'.000 vnđ</b>');
        // $('#input_money_pack_buy_have_code').val(money);
        // $('#input_money_pack_buy').val(money);
        // $('#input_id_pack_buy').val(pack_id);
        // $('#input_time_pack_buy').val(time);
        window.location.href= url_fill_code_active_pack+'-'+pack_id+'.html';
    }

})
//tuyen: mua khoa hoc
function fill_code_active_pack() {
    if($('#code_active_pack').val() == null || $('#code_active_pack').val() == ""){
        showThongBao('Bạn chưa nhập mã code');
    } else {
        var pack = $('#pack_id').val();
        $.ajax({
            type: "POST",
            url: base_url + "frontend/pack/check_code_active_pack",//trỏ tới file nào
            data: {
                code: $("#code_active_pack").val(),
                pack:$('#pack_id').val()
            },
            success: function (data) {
                var rs = JSON.parse(data);
                if(rs.msg == 1){
                    showThongBao('Kích hoạt thành công');
                    window.location.href = base_url+'pack/bai-hoc-chinh-'+pack;
                } else {
                    showThongBao(rs.err);
                }
            },
            error: function(){
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }
}
//tuyen: nhap ma code khuyen mai
function check_code_pack_buy() {
    var code = $('#code_pack_buy').val();
    $.ajax({
        type: "POST",
        url: base_url + "frontend/pack/check_code",//trỏ tới file nào
        data: {
           code: code
        },
        success: function (data) {
            var rs = JSON.parse(data);
            var msg = rs.msg;
            if (parseInt(rs.count_pack) > 0){
                if(msg !=""){
                    $('#error_code').removeClass('hidden').addClass('show');
                    $('#error_code').html(msg);
                    // setTimeout(function () {
                    //     $('#modal_msg').modal('hide');
                    // },2000)
                } else {
                    var discount = parseInt(rs.discount);
                    $('#discount_pack_buy').html(discount+' %');
                    $('#div_discount_pack_buy_have_code').removeClass('hidden').addClass('show');
                    $('#input_discount_pack_buy').val(discount);
                    var money_pack_buy_have_code = (100 - discount)*(parseInt($('#input_money_pack_buy').val()))/100;
                    $('#money_pack_buy_have_code').html(money_pack_buy_have_code+'.000 vnđ');
                    $('#div_money_pack_buy_have_code').removeClass('hidden').addClass('show');
                    $('#input_money_pack_buy_have_code').val(money_pack_buy_have_code);
                }

            } else {
                $('#error_code').removeClass('hidden').addClass('show');
                $('#error_code').html('Mã code nhập sai');
                setTimeout(function () {
                    $('#error_code').addClass('hidden');
                })
            }
        },
        error: function(){
            showThongBao('Đã có lỗi. Xin vui lòng thao tác lại');
        }
    });
}
//tuyen: mua khoa hoc
function modal_buy_pack() {
    $.ajax({
        type: "POST",
        url: base_url + "frontend/pack/save_user_pack",//trỏ tới file nào
        data: {
            pack:  $('#input_id_pack_buy').val(),
            time :  $('#input_time_pack_buy').val()
        },
        success: function () {
            //dy den danh sach bai hoc
            $('#modal_buy_pack').modal('hide');
            showThongBao('Đăng kí thành công');
        },
        error: function(){
            showThongBao('Đã có lỗi. Xin vui lòng thao tác lại');
        }
    });
}

//tuyen: lien he
$('#submit_contact').click(function () {
    var fullname =$.trim($('#fullname').val());
    var email = $.trim($('#email_contact').val());
    var phone = $.trim($('#phone').val());
    var address = $.trim($('#address').val());
    var code = $.trim($('#code').val());
    var code_correct = $('#code_correct').val();
    var content_contact = $.trim($('#content_contact').val());
    var status_contact = 1;
    var msg = '';
    if( fullname == "" ){
        status_contact = 0;
        msg += '<p>Bạn chưa nhập họ tên</p>';
    }
    if (email == ""){
        status_contact = 0;
        msg += '<p>Bạn chưa nhập email</p>';
    }
    if(code == ""){
        status_contact = 0;
        msg += '<p>Bạn chưa nhập mã bảo vệ</p>';
    }
    if(content_contact == "" ){
        status_contact = 0;
        msg += '<p>Bạn chưa nhập nội dung muốn chia sẻ với Wow</p>';
    }
    if(status_contact == 1){
        $.ajax({
            type: "POST",
            url: base_url + "frontend/contact/add_contact",//trỏ tới file nào
            data: {
                'fullname': fullname,
                'email':email,
                'phone':phone,
                'address':address,
                'content':content_contact,
                // 'code':code
            },
            success: function (data){
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check || result.b_Check == false) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                            }
                            showThongBao(msg);
                        }
                    }
                }else{
                    showThongBao('Yêu cầu của bạn đã được gửi thành công!.');
                }
            },
            error: function(){
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    } else {
        showThongBao(msg);
    }
})

//tuyen:cap nhat thong tin
function updateInfo() {
    var msg='';
    if($.trim($('#first_name').val()) ==""){
        msg += '<p>Bạn chưa nhập họ</p>';
    }
    if($.trim($('#last_name').val()) ==""){
        msg += '<p>Bạn chưa nhập tên</p>';
    }
    if(msg == ''){
        $.ajax({
            type: 'POST',
            url: base_url + 'frontend/contact/update_info',
            // data: new FormData(this),
            data:{
              'first_name':  $.trim($('#first_name').val()),
                'last_name' : $.trim($('#last_name').val()),
                'birth_day' : $.trim($('#birthday').val()),
                'gender' : $('#gender').val(),
                'identify_code' :$('#identify_code').val(),
                'address':$.trim($('#address').val()),
                'job':$.trim($('#job').val())
            },
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check || result.b_Check == false) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            var msg = '';
                            for (var i in result.msg_error) {
                                msg = msg + '<p>' + result.msg_error[i] + '</p>';
                            }
                            showThongBao(msg);
                        }
                    }
                }else{
                    showThongBao('Bạn đã cập nhật thành công');
                }

            },
            error: function (jqXHR, error, errorThrown) {
                requestSent = false;
            }
        });
    } else {
        showThongBao(msg);
    }

}


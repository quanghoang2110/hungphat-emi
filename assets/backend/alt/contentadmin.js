function ShowAlertMessage(id,class_name, message){
    $('#' + id).removeClass();
    $('#' + id).addClass('callout');
    $('#' + id).addClass(class_name);
    $('#' + id).html(message);
    $('#' + id).show();
}

function HideAlertMessage(id){
    $('#' + id).removeClass();
    $('#' + id).addClass('callout');
    $('#' + id).html('');
    $('#' + id).hide();
}

function ShowPopupDelete(controller, event){
    var data = {event_click:event};
    var func = "ajax_show_confirm_delete";
    AjaxShowPopup(data,controller,func);
}

function AjaxShowPopup(data,controller,func,w,h){
    $.ajax({
        type:"POST",
        url:base_url + "backend/" + controller + "/" + func,
        data:data,
        success:function(data){
            var result = JSON.parse(data);
            if(typeof (w) !== 'undefined'){
                $("#popupDiv").css("width",w);
            }
            if(typeof (w) !== 'undefined'){
                $("#popupDiv").css("height",h);
            }

            $("#popupDiv > .modal-dialog").html(result.html);
            $('#popupDiv').modal('show');
        }
    });
}

function message(div,result,type){
    $(div).removeClass('hidden-me');
    $(div).addClass(type);
    $(div).html(result.msg);
    $(div).slideDown();
    setTimeout(function(){
        $(div).hide();
        $('#myModal').html('');
        $("#myModal").modal("hide");
        location.reload();
    },2000);
}

$(document).on("change","#img_file",function(){
    readURL(this,'image');
});
function readURL(input,imgname) {
    var file_name = "Vui lòng chọn ảnh";
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var max_size = parseInt("5");
        var size = input.files[0]["size"];
        var file_id = $(input).attr("id");
        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(input.files[0]["name"])) {
            alert("File ảnh của bạn không đúng định dạng");
            return false;
        }else if(size/(1024*1024) > max_size){
            alert("File ảnh của bạn phải nhỏ hơn 5MB");
            return false;
        }else{
            reader.onload = function (e) {
                $('#'+imgname).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            file_name = input.files[0]["name"];
        }
    }
    return file_name;
}

function show_textarea(tab_name,width,height){
    try {
        tinymce.remove('#'+tab_name);
    } catch (e) {}
    if(typeof width === 'undefined'){
        width = 700;
    }
    if(typeof height === 'undefined'){
        height = 300;
    }

    tinymce.init({
        elements: tab_name,
        mode: "exact",
        theme: "modern",
        width: width,
        height: height,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        // content_css: '//www.tinymce.com/css/codepen.min.css',
        // relative_urls: false,
        // remove_script_host: false,
        // remove_linebreaks : false,
        // invalid_elements: "form",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        // plugins: [
        //     "advlist autolink link image lists charmap print preview hr anchor pagebreak paste",
        //     "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
        //     "table contextmenu directionality emoticons paste textcolor responsivefilemanager","lineheight"
        // ],
        // toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | sizeselect",
        // toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code  | fontselect |  fontsizeselect | lineheightselect",
        // image_advtab: true ,
        // style_formats: [
        //     {title: 'Image Left', selector: 'img', styles: {
        //         'float' : 'left',
        //         'width' : '100%',
        //         'height' : 'auto'
        //     }},
        //     {title: 'Image Center', selector: 'img', styles: {
        //         'float' : 'center',
        //         'width' : '100%',
        //         'height' : 'auto'
        //     }},
        //     {title: 'Image Right', selector: 'img', styles: {
        //         'float' : 'right',
        //         'width' : '100%',
        //         'height' : 'auto'
        //     }}
        // ],
        //
        // fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 36pt",
        // lineheight_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 36pt",
        external_filemanager_path:base_url + "filemanager/",
        filemanager_title:"Quản lý tệp tin" ,
        external_plugins: { "filemanager" : base_url + "filemanager/plugin.min.js"}
    });
}

var rfm_obj;

$(document).on('click','.file-manager', function(){
    $('#modalRFM').modal('show');
    rfm_obj = $(this);
    var rfm_link_id = rfm_obj.children('.rfm-link').attr('id');
    $('#rfm-iframe').attr('src', base_url + 'filemanager/dialog.php?type=1&field_id=' + rfm_link_id);
});

function responsive_filemanager_callback(field_id){
    var url=$('#'+field_id).val();
    if(typeof (url) !== 'undefined'){
        rfm_obj.children('.rfm-image').attr('src', url);
    }
}

function addCommas(value) {
    return value.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
}

function InitComma(){
    $(".comma").bind('input', function () {
        var textbox = $(this);
        var value = textbox.val();
        value = value.replace(/[^0-9]/g, '');
        value = addCommas(value);
        textbox.val(value);
    });
}

$(function(){
    InitComma();
    $('.comma').each(function(){
        $(this).val(addCommas($(this).val()));
    });

    $('.datepicker').datepicker({
        language : 'vi',
        autoclose : true
    });
})

$('.datepicker').keydown(function(){
    return false;
})
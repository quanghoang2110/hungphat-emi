<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {
    function __construct($config = array())
    {
        parent::__construct($config);
        $this->CI->lang->load('form_validation');
    }
    
    /**
     * count number
     *
     * @access  public
     * @param   string
     * @return  bool
     */
    function is_count($str)
    {
        $this->CI->form_validation->set_message('is_count', $this->CI->lang->line('is_count'));
        return ( ! preg_match("/^((?:\d{1,3}[,\.]?)+\d*)$/", $str)) ? FALSE : TRUE;

    }
}

// END Form Validation Extension Class

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
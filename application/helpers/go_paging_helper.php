<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        bangvl
 */
if (!function_exists('get_paging')) {
    function get_paging($url, $total, $limit)
    {
        $ci =& get_instance();
        $ci->load->library('pagination');
        $config['base_url'] = $url;
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;

        $config['full_tag_open'] = '<ul class="pagination pull-right" style="margin-top: 0">';
        $config['full_tag_close'] = '</ul>';
        $page_tag_open = "<li>";
        $page_tag_close = "</li>";
        $config['num_tag_open'] = $page_tag_open;
        $config['num_tag_close'] = $page_tag_close;
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_link'] = ' ‹‹ ';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = $page_tag_close;
        $config['next_link'] = ' ›› ';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = $page_tag_close;
        $config['num_links'] = 2;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;

        $config['first_link'] = 'Trang đầu';
        $config['first_tag_open'] = '<li class="prev">';
        $config['first_tag_close'] = $page_tag_close;
        $config['last_link'] = 'Trang cuối';
        $config['last_tag_open'] = '<li class="prev">';
        $config['last_tag_close'] = $page_tag_close;

        $ci->pagination->initialize($config);
        return $ci->pagination->create_links();
    }
}

if (!function_exists('get_public_paging')) {
    function get_public_paging($url, $total, $limit)
    {
        $ci =& get_instance();
        $ci->load->library('pagination');
        $config['base_url'] = $url;
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;

        $config['full_tag_open'] = '<ul class="pagination-list">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li><span class="page hover-focus-color">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li><a href="#" class="page current customBgColor">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_link'] = '← Trang trước';
        $config['prev_tag_open'] = '<li><span class="prev hover-focus-color">';
        $config['prev_tag_close'] = '</span></li>';
        $config['next_link'] = 'Trang sau →';
        $config['next_tag_open'] = '<li><span class="next hover-focus-color">';
        $config['next_tag_close'] = '</span></li>';
        $config['num_links'] = 2;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;

        $ci->pagination->initialize($config);
        return $ci->pagination->create_links();
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * ss sendmail Helpers
 *
 * @package        application
 * @subpackage    Helpers
 * @category    Helpers
 * @link
 */
function send_email_common($subject, $view, $to_email, $data, $to_email_cc = null)
{
    $ci =& get_instance();

    $ci->config->load('email');
    $from_email = $ci->config->item('useragent');
    $user_agent = $ci->config->item('smtp_user');

    $ci->load->library("email");
    $ci->email->from($from_email, $user_agent);
    $ci->email->to($to_email);
    $ci->email->subject($subject);
    if ($to_email_cc) {
        $ci->email->cc($to_email_cc);
    }
    $message = $ci->load->view($view, $data, TRUE);
    $ci->email->message($message);

    if ($ci->email->send()) {
        return TRUE;
    } else {
        return FALSE;
    }
}
/* End of file go2_sendemail_helper.php */
/* Location: ./application/helpers/ssacount_helper.php */
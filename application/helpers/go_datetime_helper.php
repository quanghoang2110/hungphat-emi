<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * for only datetime function!
 */

/**
 * tinh bai viet duoc tao cach hien tai bao lau
 * @author hungnm
 */
function calculate_time($time)
{
    $now = time();
    $your_date = strtotime($time);
    $datediff = $now - $your_date;
    $date = (int)($datediff / (60 * 60 * 24));
    if ($date == 0) {
        $time = "";
        if (date('A', strtotime($time)) == 'PM') $time = "Chiều";
        else $time = "Sáng";
        $rs = date('h:i', strtotime($time)) . ' ' . $time;
    } else {
        if ($date >= 365) {
            $rs = (int)($date / 365) . ' năm trước';
        } else if ($date >= 30) {
            $rs = (int)($date / 30) . ' tháng trước';
        } else if ($date >= 7) {
            $rs = (int)($date / 7) . ' tuần trước';
        } else {
            $rs = (int)$date . ' ngày trước';
        }
    }
    return $rs;
}

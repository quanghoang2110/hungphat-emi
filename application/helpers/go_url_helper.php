<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 */

/**
 * check url is valid - kiem tra url co ton tai
 */
function valid_url($url)
{
    $url = str_replace("http://", "", $url);
    if (strstr($url, "/")) {
        $url = explode("/", $url, 2);
        $url[1] = "/" . $url[1];
    } else {
        $url = array($url, "/");
    }

    $fh = fsockopen($url[0], 80);
    if ($fh) {
        fputs($fh, "GET " . $url[1] . " HTTP/1.1\nHost:" . $url[0] . "\n\n");
        if (fread($fh, 22) == "HTTP/1.1 404 Not Found") {
            return FALSE;
        } else {
            return TRUE;
        }

    } else {
        return FALSE;
    }
}

function get_category_name($name)
{
    return url_title(loc_dau_tv(strtolower($name), "-"), "-", true);
}

/**
 * @param $name
 * @param string $id
 * @param string $dot
 * @return string
 */
function get_urltitle($name, $id = "", $dot = '')
{
    if ($dot != NO_SUFFIX) {
        $ci = &get_instance();
        $dot = $ci->config->item('custom_url_suffix');
    }
    $dot = $dot == NO_SUFFIX ? '' : $dot;
    if ($id)
        return url_title(loc_dau_tv(strtolower($name), "-") . "-" . $id, "-", true) . $dot;
    else
        return url_title(loc_dau_tv(strtolower($name), "-"), "-", true);
}

/*
 * get category url
 */
function get_category_url($route = '', $parent_slug, $sub_slug = "")
{
    if ($route) {
        return "/{$route}/{$parent_slug}/$sub_slug";
    } else if (!$parent_slug && !$sub_slug) {
        return "#";
    } else {
        return "/{$parent_slug}/$sub_slug";
    }
}

/**
 * for: get category tag
 * @param $_name
 * @param string $route
 * @param $parent_slug
 * @param string $sub_slug
 * @return string
 */
function get_category_tag($_name, $route = '', $parent_slug, $sub_slug = "")
{
    return "<a href='" . get_category_url($route, $parent_slug, $sub_slug) . "'>" . $_name . "</a>";
}

/**
 * for: get detail url
 * @param string $route
 * @param $name
 * @param $id
 * @return string
 */
function get_detail_url($route = '', $name, $id, $dot = "")
{
    if ($route) {
        return "/{$route}/" . get_urltitle($name, $id, $dot);
    } else {
        return "/" . get_urltitle($name, $id, $dot);
    }
}

/**
 * for: get detail tag (include tag a)
 * @param string $route
 * @param $name
 * @param $id
 * @param string $class
 * @return string
 */
function get_detail_tag($route = '', $name, $id, $class = '')
{
    return "<a href='" . get_detail_url($route, $name, $id) . "' title='{$name}' class='{$class}'>{$name}</a>";
}

/**
 * for: get image link
 * @param $img_link
 * @param string $img_default
 * @return string
 */
function get_img_url($img_link, $img_default = "")
{
    if (strpos($img_link, 'http://') !== FALSE || strpos($img_link, 'https://') !== FALSE) {
        return $file_link = $img_link;
    } else {
        $dot = substr($img_link, 0, 1) == "/" ? "." : "./";
//        $file_link = $dot . str_replace(array("%20", "%2C"), array(" ", ","), $img_link);
        $file_link = iconv('utf-8', 'cp1252', $img_link);
    }
//    echo $file_link;
//    if(file_exists($file_link)){
//        echo "OK";
//    }else echo "FALSE";
    if($img_default) {
        return $img_link && file_exists($file_link) ? base_url($img_link) : $img_default;
    }else{
        return base_url($img_link);
    }
}

/**
 * for: get image tag
 * @param $img_link
 * @param $img_name
 * @param string $class
 * @param string $img_default
 * @return string
 */
function get_img_tag($img_link, $img_name, $class = '', $img_default = '')
{
    return '<img src="' . get_img_url($img_link, $img_default) . '" alt="' . loc_dau_tv($img_name) . '" title="' . $img_name . '" class="' . $class . '" />';
}

/**
 * for: get link image tag
 * @param $link
 * @param string $target
 * @param $image_link
 * @param string $image_name
 * @param string $image_class
 * @return string
 */
function get_img_href_tag($link, $target = '', $image_link, $image_name = '', $image_class = '')
{
    return '<a href="' . $link . '" target="' . $target . '">' . get_img_tag($image_link, $image_name, $image_class, base_url('images/default/image.png')) . '</a>';
}

/**
 * @param string $route
 * @param $name
 * @param $id
 * @param $image_link
 * @param string $image_class
 * @param string $link_target
 * @return string
 */
function get_img_link_tag($route = '', $name, $id, $image_link, $image_class = '', $link_target = '')
{
    return '<a href="' . get_detail_url($route, $name, $id) . '" target="' . $link_target . '">' . get_img_tag($image_link, $name, $image_class, base_url('images/default/image.png')) . '</a>';
}

if (!function_exists('loc_dau_tv')) {
    function loc_dau_tv($str, $separator = '_')
    {

        if ($separator == 'dash')
            $separator = '-';

        $marTViet = array(" ", "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

        $marKoDau = array($separator, "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
        return str_replace($marTViet, $marKoDau, $str);
    }
}

function get_id_from_uri()
{
    $ci = &get_instance();
    $segs = $ci->uri->segment_array();
    $last = str_replace($ci->config->item('custom_url_suffix'), '', $segs[count($segs)]);
    $last_arr = explode('-', $last);
    return $last_arr[count($last_arr) - 1];
}

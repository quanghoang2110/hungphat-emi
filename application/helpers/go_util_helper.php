<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        bangvl
 */

/**
 * encryption pass
 */
function encryption_password($str)
{
    return sha1(md5($str));
}

if (!function_exists('format_phone')) {
    function format_phone($phone)
    {
        //format +84 to 0
        if (substr($phone, 0, 3) == "+84")
            $phone = str_replace("+84", "0", $phone);
        //format 84xxx to 0xxx
        else if (substr($phone, 0, 2) == "84") {
            //echo "1: ".$phone;
            $phone = "0" . substr($phone, 2);
        } //format 9xxx to 09xxx
        else if (substr($phone, 0, 1) != "0") {
            //echo "2: ".$phone;
            $phone = "0" . $phone;
        }
        return $phone;
    }
}

// lay key xac thuc
if (!function_exists('get_forget_password_key')) {
    function get_forget_password_key($length)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

function get_real_ip()
{
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}


//create captcha image
if (!function_exists('docreateCaptcha')) {
    function docreateCaptcha()
    {
        $ci =& get_instance();
        $ci->load->helper('captcha');
        $ip_address = $ci->input->ip_address();
        $RandomStr = md5(microtime()) . $ip_address;
        $ResultStr = strtoupper(substr($RandomStr, 0, 5));
        $vals = array(
            'word' => $ResultStr,
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'font_path' => './fonts/VHANTIQ.TTF',
            'img_width' => 150,
            'img_height' => 42,
            'expiration' => 30,
        );

        $cap = create_captcha($vals);

//        print_r($cap);
        $word = $cap['word'];
        $ci->session->set_userdata(CAPTCHA_WORD, $word);
        return $cap['image'];
    }
}

if (!function_exists('remove_special_characters')) {
    function remove_special_characters($str, $needed)
    {
        $str = str_replace(array("'", '"', '`'), array($needed, $needed, $needed), $str);
        return $str;
    }
}

function loc_dau_tv($str, $separator = '_')
{

    if ($separator == 'dash')
        $separator = '-';

    $marTViet = array(" ", "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

    $marKoDau = array($separator, "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
    return str_replace($marTViet, $marKoDau, $str);

}

if (!function_exists('encode_TV')) {
    function encode_TV($str)
    {

        $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

        $marchange = array("&agrave;", "&aacute;", "ạ", "ả", "&atilde;", "&acirc;", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "&egrave;", "&eacute;", "ẹ", "ẻ", "ẽ", "&ecirc;", "ề", "ế", "ệ", "ể", "ễ", "&igrave;", "&iacute;", "ị", "ỉ", "ĩ", "&ograve;", "&oacute;", "ọ", "ỏ", "&otilde;", "&ocirc;", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "&ugrave;", "&uacute;", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "&yacute;", "ỵ", "ỷ", "ỹ", "đ", "&Agrave;", "&Aacute;", "Ạ", "Ả", "&Atilde;", "&Acirc;", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "&Egrave;", "&Eacute;", "Ẹ", "Ẻ", "Ẽ", "&Ecirc;", "Ề", "Ế", "Ệ", "Ể", "Ễ", "&Igrave;", "&Iacute;", "Ị", "Ỉ", "Ĩ", "&Ograve;", "&Oacute;", "Ọ", "Ỏ", "&Otilde;", "&Ocirc;", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "&Ugrave;", "&Uacute;", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "&Yacute;", "Ỵ", "Ỷ", "Ỹ", "Đ");
        return str_replace($marTViet, $marchange, $str);

    }
}

if (!function_exists('get_numeric_from_str')) {
    function get_numeric_from_str($str)
    {
        preg_match_all('/\d+/', $str, $matches);
        return $matches[0];
    }
}

function get_price($price, $dot = ' VNĐ')
{
    return number_format($price, 0, ',', '.') . $dot;
}

function get_discount_price($price_1, $price_2, $dot = '%')
{
    if($price_1 > 0) {
        return ceil(($price_2 - $price_1) * 100 / $price_1) . $dot;
    }else{
        return 0;
    }
}

function get_text_area_content($content)
{
    return str_replace("\n", "<br/>", $content);
}

if (! function_exists ( 'get_id_from_slug' )) {
    function get_id_from_slug($slug, $dot='.html'){
        $slug_arr = explode ( "-", str_replace ( $dot, "", $slug ) );
        $id = $slug_arr ? $slug_arr [count ( $slug_arr ) - 1] : "";

        return $id;
    }
}

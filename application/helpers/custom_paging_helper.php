<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        bangvl
 */

function get_thebox_paging($url, $total, $limit)
{
    $ci =& get_instance();
    $ci->load->library('pagination');

    $config['total_rows'] = $total;
    $config['base_url'] = $url;
    $config['per_page'] = $limit;

    $html_p = '<ul class="pagination category-pagination pull-right">';

    $config['full_tag_open'] = $html_p;
    $config['full_tag_close'] = '</ul>';
    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
    $config['prev_tag_open'] = '<li class="last">';
    $config['prev_tag_close'] = '</li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
    $config['next_tag_open'] = '<li class="last">';
    $config['next_tag_close'] = '</span></li>';
    $config['num_links'] = 3;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;

    $config['first_link'] = 'Trang đầu';
    $config['first_tag_open'] = '<li class="last">';
    $config['first_tag_close'] = "";
    $config['last_link'] = 'Trang cuối';
    $config['last_tag_open'] = '<li class="last">';
    $config['last_tag_close'] = "";

    $ci->pagination->initialize($config);
    return $ci->pagination->create_links();
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Gallery
 * @author quanghv
 * @description
 */
class Gallery extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('gallery_content');
    }

    function index($slug = '')
    {
        $arr = explode("-", $slug);
        $id = $arr[count($arr) - 1];

        if (!$id) {
            $this->_load_data_from('gallery');
            $first = $this->_model->_get_top_one(array('valid' => 1), array('id' => 'asc'));
            $id = $first ? $first->id : 0;
        }

        $this->_content = 'gallery/index';

        $vars['current_menu'] = ROUTE_GALLERY;
        $this->_load_data_from('gallery_content');
        $vars['list'] = $id ? $this->_model->_get_limit(array('gallery_id' => $id), '', 1, 12) : array();

        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    /**
     * gallery content
     * @param $slug
     */
    function detail($slug)
    {
        $vars['current_menu'] = ROUTE_GALLERY;
        $arr = explode("-", str_replace(URL_SUFFIX, "", $slug));
        $id = $arr[count($arr) - 1];

        $this->_content = 'gallery/detail';

        $vars['news'] = $content = $this->_model->_get_row(array('id' => $id));
        $vars['list'] = array();
        $imgs = array();
        if ($content) {
            if ($content->content_type == 'image') {
                foreach (glob("./albums/{$content->id}/*.*") as $filename) {
                    $imgs[] = $filename;
                }
            }
            $vars['list'] = $this->_model->_get_limit(array('id !=' => $id, 'gallery_id' => $content->gallery_id, 'valid' => 1), array('position' => 'desc', 'id' => 'desc'), 1, 3);
        }
        $vars['imgs'] = $imgs;
        $vars['slug'] = "thu-vien/{$slug}";
        //tang 1 views vao luot xem
        $query = $this->_model->_get_row(array('id' => $id));
        $old_view = $query ? $query->total_views : '';
        $new_view = $old_view + 1;
        $this->_model->_update(array('total_views' => $new_view), array('id' => $id));
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

}
<?php

class Contact_mail extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
    }

    function save()
    {
        $this->_load_data_from('Contact_mail');
        $this->_model->_insert(array('email' => $this->input->post('email')));
    }
}
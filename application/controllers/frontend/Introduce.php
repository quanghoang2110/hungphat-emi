<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author tuyen
 */
class Introduce extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->_load_data_from('page_introduce');
        $vars['current_menu'] = "introduce";
        $this->_content = 'introduce';

        $seo = array(
            'title' => 'Giới thiệu',
            'desc' => "",
            "img" => ""
        );
        $vars['seo'] = $seo;
        $vars['info'] = $this->_model->_get();
        $this->_load_data_from('news');
        $vars['list'] = $this->_model->_get_limit(array('valid' => 1), array('position' => 'desc', 'id' => 'desc'), 1, 3);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author tuyen
 */
class Product_publisher extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->_load_data_from('product_publisher');
        $vars['current_menu'] = "product_publisher";
        $this->_content = 'product_publisher';

        $seo = array(
            'title' => 'Nhà cung cấp',
            'desc' => "",
            "img" => ""
        );
        $vars['seo'] = $seo;
        $vars['list'] = $this->_model->_get();
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

}
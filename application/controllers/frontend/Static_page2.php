<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Static_page
 * @author quanghv
 * @description load static page content (about us,...)
 */
class Static_page2 extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('static_page');
    }

//    function index()
//    {
//        $vars['current_menu'] = $slug = $this->uri->segment(1);
//        $this->_content = 'static_page';
//
//        $this->load->model('model_staticpage');
//        $vars['list'] = $this->model_staticpage->get_static_pages_by_menu($slug, 1, 6);
//
//        $this->_module_vars = $vars;
//        $this->_load_tmp();
//    }

//    function detail()
//    {
//        $segs = $this->uri->segment_array();
//        if ($segs) {
//            $slug = $segs[count($segs)];
//            $arr = explode("-", str_replace(URL_SUFFIX, "", $slug));
//            $id = $arr[count($arr) - 1];
//
//            $this->_content = 'static_page_detail';
//            $vars['news'] = $news = $this->_model->_get_row(array('id' => $id), '');
//            $vars['list'] = array();
//            if ($news) {
////                $menu_id = $news->menu_id;
//                $vars['list'] = $this->_model->_get(
//                    array(
//                        'id!=' => $id,
////                        'menu_id' => $menu_id,
//                        'valid' => 1
//                    ),
//                    array(
////                        'position' => 'desc',
//                        'id' => 'asc'
//                    ), '*');
////                $this->_load_data_from('menu');
////                $menu = $this->_model->_get_row(array('id' => $menu_id));
////                $vars['current_menu'] = $menu ? $menu->slug : '';
//            }
//            $vars['slug'] = $slug;
//            $this->_module_vars = $vars;
//            $this->_load_tmp();
//        }
//    }
    function detail($slug)
    {
        $vars['current_menu'] = "";
        $arr = explode("-", str_replace(URL_SUFFIX, "", $slug));
        $id = $arr[count($arr) - 1];

        $this->_content = 'static_page2/detail';

        $vars['info'] = $this->_model->_get_row(array('id' => $id));
        $vars['list'] = $this->_model->_get_limit(array('id !=' => $id, 'valid' => 1), array('position' => 'desc', 'id' => 'desc'), 1, 3);
        $vars['slug'] = "tin-tuc/{$slug}";
        $vars['no_top_banner'] = 1;
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends GoFrontendController
{

    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('product');
        $this->load->model('model_product');
    }

    function index()
    {
        $id = get_id_from_uri();

        if ($id) {

            $this->_init_page();
            $this->_limit = 12;

            $this->_load_data_from('product_type');
            $vars['info'] = $info = $this->_model->_get_row(array('id' => $id));
            if ($info) {

                $vars['list'] = $this->model_product->get_in_category($id, $this->_page, $this->_limit);
                $total = $this->model_product->count_in_category($id);
                $url = get_detail_url(ROUTE_PRODUCT, $info->name, $info->id);
                $vars['paging'] = get_paging($url, $total, $this->_limit);

                $vars['menu_current'] = "product";
                $this->_content = 'product';
                $this->load->vars($vars);
                $this->_load_tmp();
                return;
            }
        }
        redirect(ERROR_404);
    }

    function search()
    {
        $this->_init_page();
        $this->_limit = 16;
        $this->_search = $this->input->get('q');

        $this->_load_data_from('product');
        $vars['list'] = $this->_model->_get_search_limit(array('valid' => 1), $this->_search, "", $this->_page, $this->_limit);
        $total = $this->_model->_count_search(array('valid' => 1), $this->_search);

        $url = "/" . ROUTE_SEARCH . "?q=" . $this->_search;
        $vars['paging'] = get_paging($url, $total, $this->_limit);

        $vars['menu_current'] = "product";
        $this->_content = 'search';
        $this->load->vars($vars);
        $this->_load_tmp();
        return;
    }

    function detail($id)
    {
        $segment = $this->uri->segment(1);
        //get product_detail
        if ($id > 0) {
            $vars['info'] = $info = $this->_model->_get_row(array('id' => $id));
            if ($info) {
                $this->_load_data_from('product_img');
                $vars['imgs'] = $this->_model->_get(array('product_id' => $id));

                $this->_load_data_from('product');
                $vars['info_hot'] = $this->_model->_get(array('id' => $id, 'is_hot' => 1));

                $this->_seo_title_detail = ellipsize($info->head_title, 65);
                $this->_seo_url_detail = base_url().$segment;
                $this->_seo_description_detail = ellipsize($info->head_description, 165);
                $this->_seo_keyword_detail = $info->head_keyword ? $info->head_keyword : "";
                $this->_seo_image_detail = base_url($info->avatar);

                $this->_load_data_from('banner');
                $vars['ads'] = $this->_model->_get_limit(array('valid' => 1, 'type' => BANNER_VUONG), array('position' => 'asc'), 1, 2);


                $vars['menu_current'] = "product";
                $this->_content = 'product_detail';
                $this->load->vars($vars);
                $this->_load_tmp();
            } else redirect(ERROR_404);
        } else redirect(ERROR_404);
    }

    function increase_viewed()
    {
        $id = $this->input->post('id');
        $this->_model->_increase_field('viewed', 'viewed+1', array('id' => $id));
    }

    function buy_cart()
    {
        $id = $this->input->post('id');
        $soluong = $this->input->post('soluong');
        if ($id > 0 && $soluong > 0) {
            $vars['info'] = $info = $this->_model->_get_row(array('id' => $id));
            if ($info) {
                $this->load->helper('cookie');
                $ids = get_cookie('cart_product');
                $so_sanpham = 0;
                $id_arr = '';
                $pos = strpos($ids, ',');
                if ($pos !== false) {
                    $id_arr = explode(',', $ids);
                    $so_sanpham = count($id_arr);
                }
                if (!$ids) {
                    $cookie_sp = array(
                        'name' => 'cart_product',
                        'value' => $id,
                        'expire' => time() + 86500
                    );
                    set_cookie($cookie_sp);
                    $cookie_sl = array(
                        'name' => 'cart_product_sl',
                        'value' => $soluong,
                        'expire' => time() + 86500
                    );
                    set_cookie($cookie_sl);
                    $so_sanpham = 1;
                } elseif ($id_arr && !in_array($id, $id_arr) || (!$id_arr && $ids != $id)) {
                    $cookie_sp = array(
                        'name' => 'cart_product',
                        'value' => $ids . ',' . $id,
                        'expire' => time() + 86500,
                    );
                    set_cookie($cookie_sp);
                    $sls = get_cookie('cart_product_sl');
                    $cookie_sl = array(
                        'name' => 'cart_product_sl',
                        'value' => $sls . ',' . $soluong,
                        'expire' => time() + 86500,
                    );
                    set_cookie($cookie_sl);
                    $so_sanpham = count($id_arr) + 1;
                } elseif ($id_arr && in_array($id, $id_arr)) {
                    $sls = get_cookie('cart_product_sl');
                    $sl_arr = explode(',', $sls);
                    foreach ($id_arr as $key => $val) {
                        if ($val == $id) {
                            $sl_arr[$key] = $soluong;
                        }
                    }
                    $sls = implode($sl_arr, ',');
                    $cookie_sl = array(
                        'name' => 'cart_product_sl',
                        'value' => $sls,
                        'expire' => time() + 86500,
                    );
                    set_cookie($cookie_sl);
                }
                echo $so_sanpham;
            } else redirect(ERROR_404);
        } else redirect(ERROR_404);
    }

    function get_product_cart()
    {
        $data = $this->get_list_cart();
        $html = $this->load->view('frontend/hungphat/cart.php', $data, true);
        echo json_encode(array('html' => $html));
    }

    function get_list_cart()
    {
        $this->load->helper('cookie');
        $ids = get_cookie('cart_product');
        $vars['list'] = $data['list'] = array();
        if ($ids) {
            $sls = get_cookie('cart_product_sl');
            $id_arr = '';
            $pos = strpos($ids, ',');
            if ($pos !== false) {
                $id_arr = explode(',', $ids);
                $sl_arr = explode(',', $sls);

                $sls = $this->sapxep($id_arr, $sl_arr);
                $ids = implode($id_arr, ',');
            }
            $where_query = "id IN (" . $ids . ")";
            $vars['list'] = $this->_model->_get_query($where_query, array('id' => 'asc'));
            $vars['sl'] = $sls;
        }
        return $vars;
    }

    function sapxep($arr, $arr_sl)
    {
        $n = count($arr);
        for ($i = 0; $i < $n; $i++) {
            for ($j = $n - 1; $j > 0; $j--) {
                if ($arr[$j] < $arr[$j - 1]) {
                    $temp = $arr_sl[$j];
                    $arr_sl[$j] = $arr_sl[$j - 1];
                    $arr_sl[$j - 1] = $temp;
                }
            }
        }
        return $arr_sl;
    }

    function delete_product_cart()
    {
        $id = $this->input->post('id');
        if ($id > 0) {
            $this->load->helper('cookie');
            $ids = get_cookie('cart_product');
            $sls = get_cookie('cart_product_sl');
            $id_arr = '';
            $pos = strpos($ids, ',');
            $so_sanpham = '';
            if ($pos !== false) {
                $id_arr = explode(',', $ids);
                $sl_arr = explode(',', $sls);
                foreach ($id_arr as $key => $val) {
                    if ($val == $id) {
                        unset($id_arr[$key]);
                        unset($sl_arr[$key]);
                    }
                }
                $ids = implode($id_arr, ",");
                $sls = implode($sl_arr, ",");
                $cookie_sp = array(
                    'name' => 'cart_product',
                    'value' => $ids,
                    'expire' => time() + 86500,
                );
                set_cookie($cookie_sp);
                $sls = get_cookie('cart_product_sl');
                $cookie_sl = array(
                    'name' => 'cart_product_sl',
                    'value' => $sls,
                    'expire' => time() + 86500,
                );
                set_cookie($cookie_sl);
                $so_sanpham = count($id_arr);
            } else if ($id == $ids) {
                delete_cookie('cart_product');
                delete_cookie('cart_product_sl');
                $so_sanpham = 0;
            }
            echo $so_sanpham;
        }
    }

    function add_number_product_cart()
    {
        $id = $this->input->post('id');
        $soluong = $this->input->post('soluong');
        if ($id > 0) {
            $this->load->helper('cookie');
            $ids = get_cookie('cart_product');
            $sls = get_cookie('cart_product_sl');
            $id_arr = '';
            $pos = strpos($ids, ',');
            if ($pos !== false) {
                $sls = get_cookie('cart_product_sl');
                $sl_arr = explode(',', $sls);
                $id_arr = explode(',', $ids);
                foreach ($id_arr as $key => $val) {
                    if ($val == $id) {
                        $sl_arr[$key] = $soluong;
                    }
                }
                $sls = implode($sl_arr, ',');
                $cookie_sl = array(
                    'name' => 'cart_product_sl',
                    'value' => $sls,
                    'expire' => time() + 86500,
                );
                set_cookie($cookie_sl);
            } else {
                $sls = $soluong;
            }
            $cookie_sl = array(
                'name' => 'cart_product_sl',
                'value' => $sls,
                'expire' => time() + 86500,
            );
            set_cookie($cookie_sl);
        }
    }

    function form_payment()
    {
        $vars = $this->get_list_cart();
        $this->_content = 'form_payment';
        $this->load->vars($vars);
        $this->_load_tmp();
    }


    function process_payment()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "fullname",
                "show_name" => "Họ và tên",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "phone",
                "show_name" => "Số điện thoại",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "address",
                "show_name" => "Địa chỉ",
                "request" => "required"
            )
        );
        if (!$this->_validation($config)) {
            echo json_encode(array('error' => 1, 'msg' => 'Vui lòng nhập đầy đủ thông tin để thanh toán.'));
        } else {
            $this->_process_payment();
        }
    }

    function _process_payment()
    {
        $fullname = $this->input->post('fullname');
        $phone = $this->input->post('phone');
        $address = $this->input->post('address');
        $note = $this->input->post('note');
        $this->_load_data_from('account_order');
        $order_info_id = $this->_model->_insert(
            array(
                'account_id' => 14,
                'code' => 12,
                'name' => $fullname,
                'phone' => $phone,
                'address' => $address,
                'note' => $note,
                'status' => 0,
                'update_time' => date('Y-m-d H:i:s')
            )
        );

        $this->load->helper('cookie');
        $ids = get_cookie('cart_product');
        $sls = get_cookie('cart_product_sl');
        $id_arr = '';
        $pos = strpos($ids, ',');
        if ($pos !== false) {
            $sls = get_cookie('cart_product_sl');
            $sl_arr = explode(',', $sls);
            $id_arr = explode(',', $ids);
            foreach ($id_arr as $key => $val) {
                $this->_load_data_from('product');
                $info = $this->_model->_get_row(array('id' => $val));

                $this->_load_data_from('account_order_product');
                $this->_model->_insert(
                    array(
                        'order_info_id' => $order_info_id,
                        'product_id' => $val,
                        'number' => $sl_arr[$key],
                        'price_per_one' => ($info->price_2) ? $info->price_2 : $info->price_1,
                        'update_time' => date('Y-m-d H:i:s')
                    )
                );
            }
        } else {
            $this->_load_data_from('product');
            $info = $this->_model->_get_row(array('id' => $ids));

            $this->_load_data_from('account_order_product');
            $this->_model->_insert(
                array(
                    'order_info_id' => $order_info_id,
                    'product_id' => $ids,
                    'number' => $sls,
                    'price_per_one' => ($info->price_2) ? $info->price_2 : $info->price_1,
                    'update_time' => date('Y-m-d H:i:s')
                )
            );
        }
        delete_cookie('cart_product');
        delete_cookie('cart_product_sl');
        echo json_encode(array('error' => 0, 'msg' => 'Thanh toán thành công!'));
    }

    //san pham hot
    function product_hot($str)
    {
        $this->_init_page();
        $this->_limit = 16;
        $this->_load_data_from('product');

        if($str == 'moi-nhat') {
            $data = array(
                'valid'=>1,
                'is_new'=>1
            );
            $vars['title'] = 'SẢN PHẨM MỚI NHẤT';
        }elseif($str == 'noi-bat') {
            $data = array(
                'valid'=>1,
                'is_hot'=>1
            );
            $vars['title'] = 'SẢN PHẨM NỔI BẬT';
        }
        if($str == 'moi-nhat' || $str == 'noi-bat') {
            $vars['list'] = $this->_model->_get_search_limit($data, $this->_search, "", $this->_page, $this->_limit);
            $total = $this->_model->_count_search($data, $this->_search);

            $url = "/" . ROUTE_PRODUCT_HOT . $str.'.html';
            $vars['paging'] = get_paging($url, $total, $this->_limit);

        }

        $vars['menu_current'] = "product";
        $this->_content = 'search';
        $this->load->vars($vars);
        $this->_load_tmp();
    }
}
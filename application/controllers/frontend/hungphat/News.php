<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('news');
    }

    public function index()
    {
        $segment = str_replace(URL_SUFFIX, "", $this->uri->segment(1));
        switch ($segment) {
            case ROUTE_NEWS_SALE:
                $breadcum = "Khuyến mại";
                $news_type = TIN_KHUYEN_MAI;
                $current_menu = "news_sale";
                $this->_content = 'news';
                break;
            case ROUTE_NEWS_RECRUITMENT:
                $breadcum = "Tuyển dụng";
                $news_type = TIN_TUYEN_DUNG;
                $current_menu = "news_recruitment";
                $this->_content = 'news_recruitment';
                break;
            default:
                $breadcum = "Tin tức";
                $news_type = TIN_TUC;
                $current_menu = "news";
                $this->_content = 'news';
                break;
        }

        $vars['current_menu'] = $current_menu;

        $where = array('valid' => 1, 'news_type' => $news_type);

        $this->_init_page();
        $total = $this->_model->_count($where);
        $url = base_url($segment);
        $vars['list'] = '';
        //lay tin tuc
        $vars['list'] = $this->_model->_get_limit($where, array('id' => 'desc'), $this->_page, $this->_limit);

        $vars['paging'] = get_paging($url, $total, $this->_limit);

        //lay tin tuc moi
        $vars['list_new'] = $this->_model->_get_limit($where, array('id' => 'desc'), 1, 5);

        //get banner
        $this->_load_data_from('banner');
        $vars['banner'] = $this->_model->_get_top_one(array('valid' => 1, 'type' => BANNER_NGANG), array('id' => 'desc'));


//        $seo = array(
//            'title' => 'Tin tức',
//            'desc' => "",
//            "img" => ""
//        );
        $vars['breadcum'] = $breadcum;
        $vars['route'] = $segment;

        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function detail($slug = '')
    {

        $segment = $this->uri->segment(1);
        switch ($segment) {
            case ROUTE_NEWS_SALE:
                $breadcum = "Khuyến mại";
                $news_type = TIN_KHUYEN_MAI;
                $current_menu = "news_sale";
                break;
            case ROUTE_NEWS_RECRUITMENT:
                $breadcum = "Tuyển dụng";
                $news_type = TIN_TUYEN_DUNG;
                $current_menu = "news_recruitment";
                break;
            default:
                $breadcum = "Tin tức";
                $news_type = TIN_TUC;
                $current_menu = "news";
                break;
        }
        $vars['current_menu'] = $current_menu;
        $vars['breadcum'] = $breadcum;

        $vars['route'] = str_replace(URL_SUFFIX, "", $segment);

        $arr = explode("-", str_replace(URL_SUFFIX, "", $slug));
        $id = $arr[count($arr) - 1];

        $this->_content = 'news_detail';

        $vars['info'] = $info = $this->_model->_get_row(array('id' => $id));
        $vars['list'] = $this->_model->_get_limit(array('id !=' => $id, 'valid' => 1), array('position' => 'desc', 'id' => 'desc'), 1, 3);
        $vars['slug'] = "tin-tuc/{$slug}";
        $vars['seo_title'] = $info->head_title;
        $vars['_seo_title'] = $info->head_title;

        $this->_seo_title_detail = ellipsize($info->head_title, 65);
        $this->_seo_url_detail = base_url().$segment.'/'.$slug.URL_SUFFIX;
        $this->_seo_description_detail = ellipsize($info->head_description, 165);
        $this->_seo_keyword_detail = $info->head_keyword ? $info->head_keyword : "";
        $this->_seo_image_detail = base_url($info->img);

        $this->_module_vars = $vars;
        $this->_load_tmp();
    }
}



<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $vars['menu_current'] = "home";
        $this->_content = 'home';
        $this->load->vars($vars);
        $this->_load_tmp();
    }
}



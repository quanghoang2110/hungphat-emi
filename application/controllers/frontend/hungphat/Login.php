<?php
class Login extends  GoFrontendController{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('account');
    }
    function index(){
        $erro = 0; $msg = '';
        $email = trim($this->input->post('email'));
        $password = trim($this->input->post('password'));
        $user = $this->_model->_get_row(array('email'=>$email,'password'=>encryption_password($password)));
        if($user){
            if($user->valid) {
                //session
                $session_arr = $this->config->item('session');
                $this->session->set_userdata(
                    array(
                        USERNAME_FRONTEND_SESSION => $user->fullname,
                        ID_FRONTEND_SESSION => $user->id
                    )
                );
                $this->_sess_userid = $user->id;
                $msg = 'Đăng nhập thành công';
            }else{
                $erro = 1;
                $msg = 'Tài khoản của bạn đã bị khóa.';
            }
        }else{
            $erro = 1;
            $msg = 'Tài khoản hoặc mật khẩu không chính xác.';
        }
        echo json_encode(array('msg'=>$msg,'err'=>$erro));
    }
    function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function check_register_user(){
        $config = array(
            array(
                "message_name" => "required",
                "name" => "fullname",
                "show_name" => "Họ và tên",
                "request" => "required|trim"
            ),
            array(
                "message_name" => "required",
                "name" => "email",
                "show_name" => "email",
                "request" => "required|valid_email|callback_check_email"
            ),
            array(
                "message_name" => "required",
                "name" => "password",
                "show_name" => "mật khẩu",
                "request" => "required|trim"
            )
            ,array(
                "message_name" => "required",
                "name" => "repassword",
                "show_name" => "mật khẩu",
                "request" => "required|trim|matches[password]"
            )
        );
        if($this->input->post('phone')){
            $config[]= array(
                "message_name" => "required",
                "name" => "phone",
                "show_name" => "số điện thoại",
                "request" => "required|numeric|max_length[15]"
            );
        }
        $flag = $this->_validation($config);
        if (!$flag) {
            $data['error'] = 1;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_register();
        }
    }

    function check_email()
    {
        $email = $this->input->post('email');
        $check = $this->_model->is_exists_check(array('email'=>$email));
        if ($check) {
            $this->form_validation->set_message('check_email', 'Email đã tồn tại');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _register(){
        $error = 0; $msg = '';
        $email = trim($this->input->post('email'));
        $password = $this->input->post('password');
        $hoten = $this->input->post('fullname');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');
        $id = $this->_model->_insert(
            array(
                'password' => encryption_password($password),
                'email'=>$email,
                'fullname'=>$hoten,
                'address'=>$address,
                'phone'=>$phone
            )
        );
        $msg = 'Đăng ký thành công.';
        $session_arr = $this->config->item('session');
        $this->session->set_userdata(
            array(
                USERNAME_FRONTEND_SESSION => $hoten,
                ID_FRONTEND_SESSION => $id
            )
        );
        $this->_sess_userid = $id;
        echo json_encode(array('msg'=>$msg,'err'=>$error));
    }


    function get_token_change_pass(){
        $erro = 0; $msg = '';
        $tokenKey = $this->input->get('key');
        $email = $this->input->get('email');
        $count = $this->_model->_count($where_arr = array(
            'email'=>trim($email),
            'tokenKey'=>$tokenKey
        ));
        if($count > 0) {
            $this->_model->_update(array('password'=>encryption_password(123456)),array('email'=>$email,'tokenKey'=>$tokenKey));
            $this->load->library('email');
            $this->email->from($this->config->item('smtp_user'), 'hungphat');
            $this->email->to($email);
            $this->email->subject('Xác nhận mật khẩu');
            $mess = "Mật khẩu mới của bạn là 123456";
            $this->email->message($mess);
            //Send mail
            if($this->email->send()) {
                echo  "Lấy lại mật khẩu thành công.Xin vui lòng vào email của bạn để xác nhận mật khẩu.";
            }else {
                $erro = 1;
                echo "Lỗi gửi email.";
            }
        }else{
            $erro = 1;
            echo  "Lỗi key or email của bạn.";
        }
        echo json_encode(array('msg'=>$msg,'err'=>$erro));
    }

    function login_facebook_a(){
        $email = trim($this->input->post('email'));
        $name = trim($this->input->post('name'));
        $id  = trim($this->input->post('id'));
        $erro = 0; $msg = '';
        $user = $this->_model->_get_row(array('facebook_id'=>$id));
        if($user){
            if($user->valid) {
                //session
                $session_arr = $this->config->item('session');
                $this->session->set_userdata(
                    array(
                        USERNAME_FRONTEND_SESSION => $user->fullname,
                        ID_FRONTEND_SESSION => $user->id
                    )
                );
                $this->_sess_userid = $user->id;
                $msg = 'Đăng nhập thành công';
            }else{
                $erro = 1;
                $msg = 'Tài khoản của bạn đã bị khóa.';
            }
        }else{
            $id = $this->_model->_insert(
                array(
                    'facebook_id' => $id,
                    'email'=>$email,
                    'fullname'=>$name
                )
            );
            $this->session->set_userdata(
                array(
                    USERNAME_FRONTEND_SESSION =>$name,
                    ID_FRONTEND_SESSION => $id
                )
            );
            $this->_sess_userid = $id;
        }
        echo json_encode(array('msg'=>$msg,'err'=>$erro));
    }

    function get_pass_before(){
        $err = 0;
        $code = md5(uniqid(rand()));
        $to_email = $this->input->post('email');
        $count = $this->_model->_count($where_arr = array(
            'email'=>trim($to_email)
        ));
        if($count > 0) {
            $this->_model->_update(array('tokenKey'=>$code),array('email'=>$to_email));
            $data = array('key' => $code, 'fullname' => 'Bạn', 'email' => $to_email);
            $mess = $this->load->view('mail/forgot_pass', $data, true);
            $this->load->library('email');
            $this->email->from($this->config->item('smtp_user'), 'hungphat');
            $this->email->to($to_email);
            $this->email->subject('Quên mật khẩu');
            $this->email->message($mess);
            //Send mail
            if($this->email->send()) {
                $msg = "Vui lòng vào email của bạn để xác nhận mật khẩu.";
            }else {
                $err = 1;
                $msg = "Lỗi gửi email.";
            }
        }else{
            $err = 1;
            $msg = 'Email chưa đăng kí';
        }
        $data = array('msg'=>$msg,'err'=>$err);
        echo json_encode($data);
    }

}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Static_page2 extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('static_page');
    }

//    public function index()
//    {
//        $vars['current_menu'] = "";
//        $this->_content = 'static_page2';
//
//        //lay static_page
//        $vars['list'] = $this->_model->_get(array('valid'=>1));
//
//        //get banner quang cao trang chu
//        $this->_load_data_from('banner');
//        $vars['banner'] = $this->_model->_get_row(array('valid' => 1));
//
//
//        $seo = array(
//            'title' => 'Tin tức',
//            'desc' => "",
//            "img" => ""
//        );
//        $vars['seo'] = $seo;
//
//        $this->_module_vars = $vars;
//        $this->_load_tmp();
//    }

    function detail($slug = '')
    {
//        echo '1';
//        die;
        $vars['current_menu'] = "";
        $arr = explode("-", str_replace(URL_SUFFIX, "", $slug));
        $id = $arr[count($arr) - 1];

        $this->_content = 'static_page2_detail';

        $vars['info'] = $this->_model->_get_row(array('id' => $id));
        $this->_load_data_from('banner');
        $vars['list'] = $this->_model->_get(array('position' => BANNER_QUANG_CAO_TRANG_CHU, 'valid' => 1), array('position' => 'desc', 'id' => 'desc'));
        $vars['slug'] = "trang-tinh/{$slug}";
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }
}



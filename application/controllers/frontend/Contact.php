<?php

class Contact extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
//        $this->load->model('model_contact');
    }

    function index()
    {
        $input = $this->input->post();
        if ($input) {

            $fullname = $this->input->post('fullname');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $address = $this->input->post('address');
            $content = $this->input->post('content');

            $this->_load_data_from('contact');
            if ($this->input->post('code') == $this->session->userdata(CAPTCHA_WORD)) {
                if ($this->validate()) {
                    $this->_model->_insert(
                        array(
                            'fullname' => $fullname,
                            'email' => $email,
                            'phone' => $phone,
                            'address' => $address,
                            'content' => $content,
                            'created_at' => date('Y-m-d H:i:s'),
                            'valid' => '0'
                        )
                    );
                    $vars['err_msg'] = "<p class='text-success'>Đã gửi thông tin liên hệ thành công.</p>";
                    $input = array();
                } else {
                    $vars['err_msg'] = "<p class='text-danger'>Vui lòng điền đầy đủ thông tin</p>";
                }
            } else {
                echo '2';
                $vars['err_msg'] = "<p class='text-danger'>Mã xác nhận không hợp lệ</p>";
            }
        }

        $vars['input_data'] = $input;
        $this->_content = 'contact';
        $vars['current_menu'] = 'lien-he';
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function validate()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "fullname",
                "show_name" => "Họ và tên",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "email",
                "show_name" => "Email",
                "request" => "trim|required|valid_email"
            ),
            array(
                "message_name" => "required",
                "name" => "content",
                "show_name" => "Nội dung",
                "request" => "trim|required|max_length[255]"
            )
        );

        if (!$this->_run_form_validation($config)) {
            return false;
        } else {
            return true;
        }
    }

    //tuyen
    function save()
    {
        $this->_load_data_from('contact');
        $id = $this->_model->_insert(
            array(
                'name' => $this->input->post('hoten'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'content' => $this->input->post('note')
            )
        );
        if ($id)
            $data = array('is_true' => 1);
        else
            $data = array('is_true' => 1);
        echo json_encode($data);
    }

    //tuyen
    function contact_page()
    {
        $this->_content = 'contact_page';
        $vars['current_menu'] = 'lien-he';
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    //tuyen
    function save_contact_page()
    {
        $this->_load_data_from('contact');
        $id = $this->_model->_insert(
            array(
                'name' => $this->input->post('hoten'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'content' => $this->input->post('note')
            )
        );
        if ($id)
            $data = array('is_true' => 1);
        else
            $data = array('is_true' => 1);
        echo json_encode($data);
    }
}
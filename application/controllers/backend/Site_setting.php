<?php

class Site_setting extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
        $this->_permission_controller_name = "thông tin website";
        parent::__construct();
        $this->_load_data_from('site_setting');
    }


    function index()
    {
        $this->_init_page();

        if ($this->input->post()) {
            $this->_update_general();
        }

        $this->_content = 'site_setting/index';
        $vars['module_search'] = " Nhập nội dung mô tả ...";
        $this->_module_title = "Cấu hình thông tin website";
        $this->_content = 'site_setting/index';
        $vars['list'] = $this->_model->_get();
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function _update_general()
    {
        echo '???';
        $description = $this->input->post('description', true);
        $facebook = $this->input->post('facebook', true);
        $data = array(
            'logo' => $this->input->post('image'),
            'name' => $this->input->post('site_name', true),
            'company_name' => $this->input->post('company_name', true),
            'company_address' => $this->input->post('company_address', true),
            'company_email' => $this->input->post('company_email', true),
            'company_phone' => $this->input->post('company_phone', true),
            'company_time' => $this->input->post('company_time', true),
            'company_info' => $description,
            'youtube' => $this->input->post('youtube', true),
            'google' => $this->input->post('google', true),
            'gg_analytics_id' => $this->input->post('gg_analytics_id', true),
            'fb_app_id' => $this->input->post('fb_app_id', true),
            'fb_fanpage' => $facebook,
            'meta_title' => $this->input->post('meta_title', true),
            'meta_desc' => $this->input->post('meta_desc', true)
        );
        print_r($data);
        $this->_load_data_from('site_setting');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            print_r($data);
            $this->_model->_update($data, array('id' => $id));
        }
    }

}
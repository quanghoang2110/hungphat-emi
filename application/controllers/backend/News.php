<?php

class News extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
//        $this->_permission_controller_name = "tin tức";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý tin tức";
        $vars['module_search'] = " Nhập tiêu đề tin tức ...";
        $this->_content = 'news/index';

        $this->_load_data_from('news');
        $query_arr = array();
        if($this->input->get('news_type'))
        $query_arr = array(
            'news_type'=> $this->input->get('news_type')
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count($query_arr);

        $order_arr = array(
            "id" => "desc"
        );
        $vars['news_list'] = array();
        if ($total > 0)
            $vars['news_list'] = $this->_model->_get_limit($query_arr, $order_arr, $this->_page, $this->_limit, '*');

        $url = base_url(QUAN_TRI_TIN_TUC) . "?per_size=" . $this->_limit;

        $this->_load_data_from('news_type');
        $vars['news_type'] = $this->_model->_get();

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_search_enable = true;
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'news/update';
            $id = $this->input->get('id');

            if (trim($id) > 0) {
                $this->_load_data_from('news');
                $vars['data'] = $this->_model->_get_row(array('id' => $id));
                $this->_module_title = 'Cập nhật tin tức';

            } else {
                $this->_module_title = "Thêm mới tin tức";
            }
            $this->_load_data_from('news_type');
            $vars['news_type'] = $this->_model->_get();
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "img",
                "show_name" => "Ảnh",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tiêu đề",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "content",
                "show_name" => "Nội dung tin tức",
                "request" => "required"
            )

        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $this->_load_data_from('news');
        $err = 0;
        $msg = '';
        $name = $this->input->post('name');
        $content = $this->input->post('content');
        $valid = $this->input->post('valid');
        $img = $this->input->post('img');
        $desc = $this->input->post('desc');
        $news_type = $this->input->post('news_type');
        $head_title = $this->input->post('head_title');
        $head_description = $this->input->post('head_description');
        if ($news_type == -1)
            $news_type = null;
        $data = array(
            'name' => $name,
            'content' => $content,
            'desc' => $desc,
            'valid' => $valid,
            'img' => $img,
            'news_type' => $news_type,
            'head_title' => $head_title,
            'head_description' => $head_description
        );
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('news');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('news');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }
}
<?php
class User extends Go_BackendController{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_user';
        $this->_load_model($this->_model);
        $this->_folder = 'user';
        
        $this->_module = ADMIN_MENU_USER;
        $this->_permission = unserialize(ADMIN_PERMISSION_USER);
        $this->_permission_view = ADMIN_PERMISSION_USER_VIEW;
        $this->_permission_edit = ADMIN_PERMISSION_USER_EDIT;
        $this->_permission_delete = ADMIN_PERMISSION_USER_DELETE;
    }
    /**
     * Category news
     **/
    function index()
    {
        if(!check_permissions($this->_permission)) {
            permission_denied();
        }else {
            $this->_init_page();
            $this->_module_title = 'Trang chủ';
            $this->_module_desc = "Quản lý người dùng";
            $this->_content = $this->_folder . '/index';
            $row_search = "username";


            $total = $this->_model->_count_search("", $this->_search, $row_search);
            if ($this->_page > ceil($total / $this->_limit))
                $this->_page = $this->_page--;


            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('id' => 'desc'), $this->_page, $this->_limit, "*", $row_search);

            $url = base_url(ROUTE_ADMIN_USER) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function update(){
        if(!check_permissions($this->_permission)) {
            permission_denied();
        }else {
            $this->_module_title = 'Trang chủ';

            $this->_content = $this->_folder . '/update';

            $id = $this->input->get('id');
            $vars = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $vars['data'] = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật người dùng";
            } else {
                $this->_module_desc = "Thêm người dùng";
            }
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_update(){
        $config = array(
            array(
                "message_name" => "required",
                "name" => "username",
                "show_name" => "Tài khoản",
                "request" => "trim|required|min_length[6]|max_length[32]|callback_check_account"
            ),
            array(
                "message_name" => "required",
                "name" => "email",
                "show_name" => "Email",
                "request" => "trim|required|valid_email|callback_check_email"
            )
        );
        if(!$this->input->post('hdID')){
            $config[] = array(
                "message_name" => "required",
                "name" => "password",
                "show_name" => "Mật khẩu",
                "request" => "required|min_length[6]|max_length[20]"
            );
            $config[] = array(
                "message_name" => "required",
                "name" => "repassword",
                "show_name" => "Mật khẩu xác nhận",
                "request" => "required|matches[password]"
            );
        }
        if($this->input->post('phone')){
            $config[] = array(
                "message_name" => "required",
                "name" => "phone",
                "show_name" => "Số điện thoại",
                "request" => "trim|required|is_natural|min_length[9]|max_length[15]"
            );
        }
        $flag = $this->_ValidateFormCustom($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function check_email(){
        $check = $this->_model->check_exist_email($this->input->post('email'), $this->input->post('hdID'));
        if($check){
            $this->form_validation->set_message('check_email', 'Email đã tồn tại');
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    function check_account(){
        $check = $this->_model->check_exist_account($this->input->post('username'), $this->input->post('hdID'));
        if($check){
            $this->form_validation->set_message('check_account', 'Tài khoản đã tồn tại.');
            return FALSE;
        }else{
            return TRUE;
        }
    }

    function _update_general(){
        if (!check_permissions($this->_permission_edit) && !check_permissions($this->_permission_delete)) {
            permission_denied();
        } else {
            $err = 0;
            $msg = '';
            $valid = $this->input->post('valid');
            $username = trim($this->input->post('username'));
            $fullname = trim($this->input->post('fullname'));
            $gender = $this->input->post('gender');
            $birthday = format_date_dmy($this->input->post('birthday'));
            $address = trim($this->input->post('address'));
            $phone = trim($this->input->post('phone'));
            $email = trim($this->input->post('email'));
            $org = trim($this->input->post('org'));
            $data = array(
                'valid' => $valid,
                'username' => $username,
                'fullname' => $fullname,
                'gender' => $gender,
                'birthday' => $birthday,
                'address' => $address,
                'phone' => $phone,
                'org' => $org,
                'email' => $email,
            );
            $now = date('Y-m-d H:i:s');
            if (!$this->input->post('hdID')) {
                $data['password'] = encryption_password($this->input->post('password'));
                $data['created_at'] = $now;
                $id = $this->_model->_insert($data);
            } else {
                $data['updated_at'] = $now;
                $id = $this->input->post('hdID');
                $this->_model->_update($data, array('id' => $id));
            }
            if ($id) {
                $err = 0;
                $msg = 'Đã có lỗi xảy ra';
            }
            $result = array(
                'err' => $err,
                'msg' => $msg,
            );
            echo json_encode($result);
        }
    }

    function update_pass(){
        if (!check_permissions($this->_permission_edit) && !check_permissions($this->_permission_delete)) {
            permission_denied();
        } else {
            if ($this->input->post('hdUpdatePassID')) {
                $now = date('Y-m-d H:i:s');
                $id = $this->input->post('hdUpdatePassID');
                $key = get_forget_password_key(FORGOT_PASS_LEN);
                $this->_model->_update(array('reset_password_code' => $key, 'time_change_password' => $now, 'updated_at' => $now), array('id' => $id));
                $data = $this->_model->_get_top_one(array('id'=>$id));
                if($data) {
                    $data['key'] = $key;
                    send_email_common('Khôi phục mật khẩu', "mail/forgot_pass", $data->email, $data);
                }
            }
        }
    }

}
?>
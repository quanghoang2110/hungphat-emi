<?php

class Setting extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
//        $this->_permission_controller_name = "tin tức";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý thông tin website";
        $vars['module_search'] = " Nhập tiêu đề tin tức ...";
        $this->_content = 'site_setting/index';

        $this->_load_data_from('setting');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'username'
        );

        $vars['setting'] = $this->_model->_get();
        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;
        $vars['search_disable'] = true;
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        $err = 0;
        $msg = '';
        $facebook = $this->input->post('facebook', true);
        $data = array(
            'logo' => $this->input->post('logo'),
            'name' => $this->input->post('name', true),
            'company_name' => $this->input->post('company_name', true),
            'company_address' => $this->input->post('company_address'),
            'company_email' => $this->input->post('company_email', true),
            'company_phone' => $this->input->post('company_phone', true),
            'company_time' => $this->input->post('company_time', true),
            'company_info' => $this->input->post('company_info', true),
            'gg_plus_id' => $this->input->post('gg_plus_id', true),
            'gg_analytics_id' => $this->input->post('gg_analytics_id', true),
            'fb_fanpage' => $facebook,
            'title' => $this->input->post('meta_title', true),
            'description' => $this->input->post('description', true),
            'company_address2' => $this->input->post('company_address2'),
            'google_map' => $this->input->post('google_map', true),
            'company_skype' => $this->input->post('company_skype', true),
        );

//        print_r($data);die();

        $this->_load_data_from('setting');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if ($id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
            'id' => $id
        );
        echo json_encode($result);
    }


}
<?php

class Comment extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
//        $this->_permission_controller_name = "tin tức";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý cảm nhận khách hàng̣";
        $vars['module_search'] = " Nhập tên khách hàng ...";
        $this->_content = 'comment/index';

        $this->_load_data_from('comment');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'from'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        $order_arr = array(
            "id" => "desc"
        );
        $vars['news_list'] = array();
        if ($total > 0)
            $vars['news_list'] = $this->_model->_get_limit('', $order_arr, $this->_page, $this->_limit, '*');

        $url = base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'comment/update';
            $id = $this->input->get('id');

            if (trim($id) > 0) {
                $this->_load_data_from('comment');
                $vars['data'] = $this->_model->_get_row(array('id' => $id));
                $this->_module_title = 'Cập nhật cảm nhận khách hàng̣';
                $this->_module_vars = $vars;
            } else {
                $this->_module_title = "Thêm mới cảm nhận khách hàng̣";
            }
            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "avartar",
                "show_name" => "Ảnh đại diện",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "from",
                "show_name" => "Tên khách hàng",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "content",
                "show_name" => "Cảm nhận ",
                "request" => "required"
            )
        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $this->_load_data_from('comment');
        $err = 0;
        $msg = '';
        $from = $this->input->post('from');
        $content = $this->input->post('content');
        $valid = $this->input->post('valid');
        $avartar = $this->input->post('avartar');
        $data = array(
            'from' => $from,
            'content' => $content,
            'valid' => $valid,
            'avartar' => $avartar
        );
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('comment');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('comment');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }
}
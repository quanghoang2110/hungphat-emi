<?php
/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 4/1/2016
 * Time: 5:30 PM
 */
class Home extends Go_BackendController{

    function index(){

        $this->_module = ADMIN_MENU_1;
        $this->_module_title = "Trang chủ";
        $this->_module_desc = "trang chủ hệ thống";
        $this->_content = "home";
        $this->_load_tmp();
    }
}
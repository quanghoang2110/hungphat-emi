<?php

/**
 * Created by PhpStorm.
 * User: Sonnn
 * Date: 7/1/2016
 * Time: 9:23 AM
 */
class Slide extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
        parent::__construct();
        $this->_load_data_from('slider');
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý slide";
        $vars['module_search'] = " Nhập tên slide ...";
        $this->_content = 'slide/index.php';

        $this->_load_data_from('slider');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        $order_arr = array(
            "id" => "desc"
        );
        $vars['menu_list'] = array();
        if ($total > 0)
            $vars['menu_list'] = $this->_model->_get_limit('', $order_arr, $this->_page, $this->_limit);
        $url = base_url(QUAN_TRI_SLIDER) . "?per_size=" . $this->_limit;
        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        $this->_content = 'slide/update';

        if ($this->input->post()) {
            $this->_update_general();
        }

        $id = $this->input->get('id');
        $vars = array();
        if (trim($id) > 0) {
            $vars['data'] = $this->_model->_get_top_one($where_arr = array('id' => trim($id)), $order_arr = "", $select = "*");
            $this->_module_title = "Cập nhật slide";
        } else {
            $this->_module_title = "Thêm mới slide";
        }
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function submit_update()
    {
        $err = 0;
        $msg = '';
        $image = $this->input->post('image');
        $position = $this->input->post('position');
        $name = trim($this->input->post('name'));
        $valid = trim($this->input->post('valid'));
        $link = trim($this->input->post('link'));

        $data = array(
            'name' => $name,
            'img' => $image,
            'valid' => $valid,
            'position' => $position,
            'link' => $link,
        );
//            print_r($data);
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }

        if ($id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('slider');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('slider');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }
}
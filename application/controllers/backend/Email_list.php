<?php
class Email_list extends Go_BackendController{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_email_list';
        $this->_load_model($this->_model);
        $this->_folder = 'email_list';
        $this->_module = ADMIN_MENU_EMAIL_LIST;
        $this->_init_page();
        $this->_permission = unserialize(ADMIN_PERMISSION_EMAIL_LIST);
        $this->_permission_view = 0;
        $this->_permission_edit = 0;
        $this->_permission_delete = ADMIN_PERMISSION_EMAIL_LIST_DELETE;
    }
    /**
     * Category news
     **/
    function index()
    {
        if(!check_permissions($this->_permission)) {
            permission_denied();
        }else {
            $this->_module_title = 'Trang chủ';
            $this->_module_desc = "Quản lý danh sách email";
            $this->_content = $this->_folder . '/index';
            $row_search = "link";


            $total = $this->_model->_count_search("", $this->_search, $row_search);
            if ($this->_page > ceil($total / $this->_limit))
                $this->_page = $this->_page--;


            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('id' => 'desc'), $this->_page, $this->_limit, "*", $row_search);

            $url = base_url(ROUTE_ADMIN_EMAIL_LIST) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

}
?>
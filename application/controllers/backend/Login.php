<?php

class Login extends GoBackendController
{

    function __construct()
    {
        $this->_sess_required = false;
        parent::__construct();
    }

    function index()
    {
        $back_url = $this->session->flashdata("back_url");

        if ($this->_sess_userid) {
            redirect($back_url ? $back_url : QUAN_TRI);
        }

        if ($this->input->post()) {
            //check username and pass
            $this->_load_data_from('admin');
            $user = $this->_model->_get_row(
                array(
                    "username" => $this->input->post("username"),
                    "password" => encryption_password($this->input->post("password"))
                )
            );

            if ($user) {
                if ($user->valid) {
                    //TODO set session for backend

                    //set session for permission check
                    $this->_set_permission_sesson($user->id, $user->group_id);

                    $this->session->set_userdata('username', $user->username);

                    redirect($back_url ? $back_url : QUAN_TRI);

                } else {
                    //TODO show message error
                    $msg = "<p style='color: red'>Tài khoản đang bị khóa</p>";
                }
            } else {
                //TODO show message error
                $msg = "<p style='color: red'>Tài khoản hoặc mật khẩu không hợp lệ</p>";
            }
            $this->load->vars(array('msg' => $msg));
        }

        $this->load->view('backend/alt/login');
//        $this->_content = 'login';
//        $this->_load_tmp();
    }

    function change_pass()
    {

        $this->_load_data_from('admin');
        $current_pass = $this->input->post("cur_pass");
        $status = -1;
        $msg = "Vui lòng đăng nhập";
        if ($this->_sess_userid) {
            if ($this->_model->_count(
                array(
                    'id' => $this->_sess_userid,
                    'password' => encryption_password($current_pass)
                )
            )
            ) {
                $new_pass = $this->input->post("new_pass");
                $this->_model->_update(
                    array('password' => encryption_password($new_pass)), array("id" => $this->_sess_userid)
                );
                $status = 1;
                $msg = "Bạn đã thay đổi mật khẩu thành công!";
            } else {
                $status = 0;
                $msg = "Mật khẩu hiện tại không chính xác!";
            }
        }

        echo json_encode(array(
            'status' => $status,
            'msg' => $msg
        ));
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(QUAN_TRI);
    }

    function error_403()
    {
        $this->load->view('backend/template/error_403');
    }

}
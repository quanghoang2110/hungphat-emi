<?php
/**
 * @copyright Go Solution JSC
 * Created by PHPStorm.
 * User: Tran Duc
 * Date: 4/18/2016
 * Time: 11:06 AM
 */
class Category_product extends Go_BackendController{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_category_product';
        $this->_load_model($this->_model);
        $this->_folder = 'cate_product';
    }
    /**
     * Category product
     **/
    function index()
    {
            $this->_init_page();
            $this->_module = ADMIN_MENU_CATEGORY;
            $this->_module_sub = ADMIN_MENUSUB_CAT_PRODUCT;
            $this->_module_title = $this->lang->line('title_admin_cat_product');
            $this->_module_desc = "";
            $this->_content = $this->_folder . '/index';
            $row_search = "name";

            $total = $this->_model->_count_search("", $this->_search, $row_search);
            if ($this->_page > ceil($total / $this->_limit))
                $this->_page = $this->_page--;


            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('order'=>'asc'), $this->_page, $this->_limit, "*", $row_search);

            $url = base_url(ROUTE_ADMIN_CAT_PRODUCT) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);
            $this->_module_vars = $vars;
            $this->_load_tmp();
    }

    function check_data(){
        $config = array(
            array(
                "message_name" => "required",
                "name" => "image",
                "show_name" => "Ảnh",
                "request" => "required"
            ),
            array(
                "message_name" => "name",
                "name" => "name",
                "show_name" => "Tên danh mục",
                "request" => "trim|required|max_length[255]"
            ),
            array(
                "message_name" => "required",
                "name" => "order",
                "show_name" => "Vị trí",
                "request" => "required|numeric|max_length[3]"
            )
        );
        $flag = $this->_ValidateFormCustom($config);
        if (!$flag) {
            $error = $this->form_validation->error_array();
            echo json_encode(array('error'=>'1','data'=>$error,'type'=>1));
            return;
        } else{
            $this->update_news();
        }
    }

    function upload_image_check(){
        $file_name = "img_file";
        if(empty($_FILES[$file_name]["name"])){
            $image_old  = html_escape($this->input->post('image_old'));
            if($image_old ==''){
                $this->form_validation->set_message("upload_image_check",$this->lang->line('image'));
                return FALSE;
            }else{
                return TRUE;
            }
        }else{
            return check_image_upload_valid($file_name,'upload_image_check');
        }
        return TRUE;
    }

    function db_exists_check(){

        $check=0;
        $id = (int)$this->input->post('HD_Id');
        $where_id = ($id>0) ? "id !=".$id : '';
        if($this->input->post('order') && $this->input->post('order') != ''){
            $check = $this->_model->is_exists_check(array('order'=>$this->input->post('order')),$where_id);
        }
        if($check>0){
            $this->form_validation->set_message('db_exists_check',$this->lang->line('is_exists'));
            return FALSE;
        }else{
            return TRUE;
        }
    }

    function update_news(){
        if(!check_permissions($this->_permission_edit) && !check_permissions($this->_permission_delete)) {
            permission_denied();
        }else {
            $id = (int)$this->input->post('hdID');
            $image = $this->input->post('image');
            $msg = "";
            $err = 0;
            //Du lieu lay tu form
            $name = trim($this->input->post('name'));
            $order = (int)$this->input->post('order');
            $valid = (int)$this->input->post('valid');
            $show_home = (int)$this->input->post('show_home_page');
            $data_member = array(
                "name" => $name,
                "image" => get_image_url($image),
                "order" => $order,
                "valid" => $valid,
                "show_home_page" => $show_home
            );

            if ($id != '' && $id > 0) {
                $id = $this->input->post('hdID');
                $this->_model->_update($data_member, array('id' => $id));
                $msg = $this->lang->line('update_success');
            } else {
                $id = $this->_model->_insert($data_member);
                $msg = $this->lang->line('insert_success');
            }
            if (!$id) {
                $msg = $this->lang->line('error');
                $err = 1;
            }

            $result = array(
                "msg" => $msg,
                "err" => $err
            );
            echo json_encode($result);
        }
    }

    function ajax_show_home_page(){
        if (!check_permissions($this->_permission_edit) && !check_permissions($this->_permission_delete)) {
            permission_denied();
        } else {
            $id = $this->input->post("id");
            $val = $this->input->post("show_home_page");
            $val = $val == 1 ? 0 : 1;
            $this->_model->_update(array('show_home_page' => $val), array('id' => $id));
            return $val;
        }
    }

    function update()
    {
        if (!check_permissions($this->_permission)) {
            permission_denied();
        } else {
            $this->_module = ADMIN_MENU_CATEGORY;
            $this->_module_sub = ADMIN_MENUSUB_CAT_PRODUCT;
            $this->_module_title = $this->lang->line('title_admin_cat_product');

            $this->_content = $this->_folder . '/update';

            $vars = array();

            if ($this->input->get('id')) {
                $this->_module_desc = $this->lang->line('title_edit');
                $id = $this->input->get('id');
                $vars['data'] = $this->_model->_get_top_one(array('id' => $id));
            } else {
                $this->_module_desc = $this->lang->line('title_add');
            }
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }

    }
}
?>
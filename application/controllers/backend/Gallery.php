<?php

/**
 * Class Gallery - danh muc thu vien
 */
class Gallery extends GoBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = 'danh mục thư viện';
        parent::__construct();
        $this->_load_data_from('gallery');
    }

    function index()
    {
        $this->_module_title = "Quản lý danh mục thư viện";
        $vars['module_search'] = " Nhập tên thư viện ...";
        $this->_content = 'gallery/index';

        $order_arr = array(
            "id" => "desc"
        );
        $vars['library_list'] = $this->_model->_get('', $order_arr);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'gallery/update';
            $id = $this->input->get('id');

            if (trim($id) > 0) {
                $vars['data'] = $this->_model->_get_row(array('id' => $id));
                $this->_module_title = "Cập nhật danh mục thư viện";
                $this->_module_vars = $vars;

            } else {
                $this->_module_title = "Thêm mới danh mục thư viện";
            }

            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên danh mục",
                "request" => "required"
            )
        );
        $flag = $this->_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = 'Bạn chưa nhập tên danh mục';
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $name = $this->input->post('name');
        $valid = $this->input->post('valid');
        $data = array(
            'name' => $name,
            'valid' => $valid
        );
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }


}
<?php

class Contact extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
//        $this->_permission_controller_name = "tin tức";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý liên hệ";
        $vars['module_search'] = " Nhập tên khách hàng ...";
        $this->_content = 'contact/index';

        $this->_load_data_from('contact');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        $order_arr = array(
            "id" => "desc"
        );
        $vars['news_list'] = array();
        if ($total > 0)
            $vars['news_list'] = $this->_model->_get_limit('', $order_arr = array('valid' => 'asc', 'id' => 'asc'), $this->_page, $this->_limit, '*');

        $url = base_url(QUAN_TRI_LIEN_HE) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'contact/update';
            $id = $this->input->get('id');

            if (trim($id) > 0) {
                $this->_load_data_from('contact');
                $vars['data'] = $this->_model->_get_row(array('id' => $id));
                $this->_module_title = 'Cập nhật liên hệ';
                $this->_module_vars = $vars;
            } else {
                $this->_module_title = "Thêm mới liên hệ";
            }

            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên khách hàng",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "email",
                "show_name" => "Email",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "phone",
                "show_name" => "Số điện thoại",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "titlè",
                "show_name" => "Tiêu đề",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "content",
                "show_name" => "Nội dung",
                "request" => "required"
            )

        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $this->_load_data_from('contact');
        $err = 0;
        $msg = '';
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $valid = $this->input->post('valid');
        $data = array(
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'title' => $title,
            'content' => $content,
            'valid' => $valid,
        );
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('contact');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('contact');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }

    function ajaxValidContact()
    {
        $this->_load_data_from('contact');
        $this->_model->_update(array('valid' => $this->input->post('valid')), array('id' => $this->input->post('id')));
    }
}
<?php

/**
 * Class Gallery content - danh muc thu vien
 */
class Gallery_content extends GoBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = 'nội dung thư viện';
        parent::__construct();
        $this->_load_data_from('gallery_content');
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý tin tức";
        $vars['module_search'] = " Nhập tên bài viết ...";
        $this->_content = 'gallery_content/index';
        $this->_load_data_from('news');

        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'username'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        if ($total > 0) {
            $this->load->model('model_gallery_content');
            $vars['list'] = $this->model_gallery_content->get_list_content($this->_search, $this->_page, $this->_limit);
        }

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'gallery_content/update';
            $this->_module_title = 'Quản lý nội dung thư viện';
            $id = $this->input->get('id');

            $this->_load_data_from('gallery');
            $vars['list_menu'] = $this->_model->_get('', array('id' => 'desc'));

            if (trim($id) > 0) {
                $this->_load_data_from('gallery_content');
                $vars['data'] = $this->_model->_get_row(array('id' => $id));
                $this->_module_title = "Cập nhật nội dung thư viện";
            } else {
                $this->_module_title = "Thêm mới nội dung thư viện";
            }

            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_upload_file()
    {
        require(APPPATH . "/third_party/UploadHandler.php");
        $album = $this->input->get('id');
        new UploadHandler(base_url(ROUTE_ADMIN_GALLERY_CONTENT_UPLOAD), $album);
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "image",
                "show_name" => "Ảnh",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "title",
                "show_name" => "Tiêu đề",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "desc",
                "show_name" => "Giới thiệu",
                "request" => "required"
            )
//            array(
//                "message_name" => "required",
//                "name" => "content",
//                "show_name" => "Nội dung bài viết",
//                "request" => "required"
//            ),
        );
        $flag = $this->_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $title = $this->input->post('title');
        $menu_id = $this->input->post('menu_id');
        $content = $this->input->post('content');
        $valid = $this->input->post('valid');
        $image = $this->input->post('image');
        $desc = $this->input->post('desc');
        $content_type = $this->input->post('content_type');
        $position = $this->input->post('position');
        $data = array(
            'title' => $title,
            'gallery_id' => $menu_id,
            'content' => $content,
            'description' => $desc,
            'valid' => $valid,
            'image' => $image,
            'content_type' => $content_type,
            'position' => $position
        );
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);

            //rename default album name
            if ($content_type == 'image') {
                rename('./albums/_undefined', './albums/' . $id);
            }
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

}
<?php

class Permission_group extends GoBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = "nhóm người dùng";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module = '';
        $this->_module_title = "Nhóm người dùng";
        $this->_module_desc = "nhóm quyền tài khoản quản trị";
        $this->_content = 'permission_group/index';

//        $row_search = 'username';

        $this->_load_data_from('permission_group');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'username'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();
        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $order_arr = array(
            "id" => "asc"
        );
        $vars['list'] = array();
        if ($total > 0)
            $vars['list'] = $this->_model->_get_limit('', $order_arr, $this->_page, $this->_limit, "*");

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function ajax_view_permission()
    {
        $data['group_id'] = $group_id = $this->input->post('group_id');
        $data['group_name'] = $this->input->post('group_name');

        $data['funcs'] = $this->_model->get_permission_func();

        $data['theme_assets'] = $this->_theme_assets;

        //get group
        $this->load->model('model_permission_map');
        $data['list'] = $this->model_permission_map->get_group_permission($group_id);
//        echo $this->_last_query();

        $this->load->view('backend/alt/permission_group/permission', $data);
    }

    function ajax_update_permission()
    {
        $permission = $this->input->post('permission[]');
        $group_id = $this->input->post('group_id');
        $this->_load_data_from('permission_map');
        $this->_model->_delete(array('group_id' => $group_id));
        $insert = array();
        foreach ($permission as $item) {
            $insert[] = array(
                'group_id' => $group_id,
                'permission_id' => $item
            );
        }
        $this->_model->_insert_batch($insert);
    }
}
<?php

class Provider extends GoBackendController
{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_provider';
        $this->_load_model($this->_model);
        $this->_folder = 'provider';
    }
    /**
     * Category news
     **/
    function index()
    {
        $this->load->model('model_provider');
            $this->_init_page();
        $this->_module = QUAN_TRI_NHA_CUNG_CAP;
        $this->_module_title = 'Quản lý nhà cung cấp';
        $this->_module_desc = "";
            $this->_content = $this->_folder . '/index';
            $row_search = "name";
        $this->_load_data_from('product_publisher');
            $total = $this->_model->_count_search("", loc_dau_tv($this->_search), $row_search);
            if ($this->_page > ceil($total / $this->_limit))
                $this->_page = $this->_page--;

            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('id'=>'desc'), $this->_page, $this->_limit, "*", $row_search);

        $url = base_url(QUAN_TRI_NHA_CUNG_CAP) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->load->vars($vars);
        $this->_load_tmp();
    }

    function update()
    {
        $this->_module = QUAN_TRI_NHA_CUNG_CAP;
        $this->_module_title = 'Nhà cung cấp';

            $this->_content = $this->_folder . '/update';

            $id = $this->input->get('id');
        $this->_load_data_from('product_publisher');
            //get list
            $vars = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $vars['list'] = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }
            $this->_module_vars = $vars;
            $this->_load_tmp();
    }

    function ajax_update(){
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên nhà cung cấp",
                "request" => "required|trim|max_length[255]"
            ),
            array(
                "message_name" => "required",
                "name" => "image",
                "show_name" => "Ảnh",
                "request" => "required"
            ), array(
                "message_name" => "required",
                "name" => "file_pdf",
                "show_name" => "Link Pdf",
                "request" => "required"
            )

        );
        $flag = $this->_ValidateFormCustom($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general(){
            $err = 0;
            $msg = '';
        $this->_load_data_from('product_publisher');
            $name = trim($this->input->post('name'));
            $valid = $this->input->post('valid');
            $image = $this->input->post('image');
        $image = str_replace(base_url(), "", $image);
        $file_pdf = trim($this->input->post('file_pdf'));
            $data = array(
                'name' => $name,
                'valid' => $valid,
                'site' => $this->input->post('site', true),
                'img' => $image,
                'file_pdf' => $file_pdf,
            );
            if (!$this->input->post('hdID')) {
                $id = $this->_model->_insert($data);
            } else {
                $id = $this->input->post('hdID');
                $this->_model->_update($data, array('id' => $id));
            }
            if ($id) {
                $err = 0;
                $msg = 'Đã có lỗi xảy ra';
            }
            $result = array(
                'err' => $err,
                'msg' => $msg,
            );
            echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('product_publisher');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('product_publisher');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }
}
?>
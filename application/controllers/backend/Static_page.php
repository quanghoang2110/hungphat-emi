<?php

class Static_page extends GoBackendController
{

    function __construct()
    {
        //ten chuc nang
        $this->_permission_controller_name = "nội dung tĩnh";
        parent::__construct();
    }

    /**
     * Category news
     **/
    function index()
    {
        $this->_init_page();
        $this->_module_title = 'Trang chủ';
        $this->_module_desc = "Quản lý trang tĩnh";
        $this->_content = 'staticpage/index';

        $this->_load_data_from('staticpage');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'username'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page = $this->_page--;

        $this->load->model('model_staticpage');
        $vars['list'] = $this->model_staticpage->get_search_limit("", $this->_search, array('position' => 'desc'), $this->_page, $this->_limit);
        //echo $this->_last_query();

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_module_title = 'Trang chủ';

            $this->_content = 'staticpage/update';

            $id = $this->input->get('id');
            $vars = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $this->_load_data_from('staticpage');
                $vars['data'] = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật trang tĩnh";
            } else {
                $this->_module_desc = "Thêm trang tĩnh";
            }
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "title",
                "show_name" => "Tiêu đề",
                "request" => "trim|required|max_length[255]"
            ),
            array(
                "message_name" => "required",
                "name" => "title2",
                "show_name" => "Tiêu đề 2",
                "request" => "trim|required|max_length[255]"
            ),
            array(
                "message_name" => "required",
                "name" => "order",
                "show_name" => "Vị trí",
                "request" => "required|numeric|max_length[3]"
            )

        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $valid = $this->input->post('valid');
        $title = trim($this->input->post('title'));
        $order = $this->input->post('order');
        $content = trim($this->input->post('content'));
        $data = array(
            'valid' => $valid,
            'title' => $title,
            'sub_title' => $this->input->post('title2', true),
            'image' => $this->input->post('image'),
            'position' => $order,
            'description' => trim($this->input->post('desc', true)),
            'content' => $content
        );
        $this->_load_data_from('staticpage');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');

            $this->_model->_update($data, array('id' => $id));
        }
        if ($id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

}

?>
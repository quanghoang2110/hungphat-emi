<?php

class Dashboard extends GoBackendController
{
    function index()
    {
        $this->_module_title = "Trang chủ";
        $this->_module_desc = "trang chủ hệ thống";
        $this->_content = "dashboard";
        $this->_load_tmp();
    }
}
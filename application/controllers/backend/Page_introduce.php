<?php

class Page_introduce extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
//        $this->_permission_controller_name = "tin tức";
        parent::__construct();
    }

    function index()
    {
        $this->_init_page();
        $this->_module_title = "Quản lý trang giới thiệu";
        $vars['module_search'] = "";
        $this->_content = 'page_introduce/index';

        $this->_load_data_from('page_introduce');
        $vars['introduce'] = $this->_model->_get();
        $vars['search_disable'] = true;
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        $err = 0;
        $msg = '';
        $data = array(
            'value' => $this->input->post('value'),
//            'mission' => $this->input->post('mission', true),
//            'prize' => $this->input->post('prize', true)
        );

        $this->_load_data_from('page_introduce');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array());
        }
        if (!$id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
            'id' => $id
        );
        echo json_encode($result);
    }
}
<?php

class Menu extends GoBackendController
{
    function __construct()
    {
        //ten chuc nang
        $this->_permission_controller_name = "menu";
        parent::__construct();
        $this->_load_data_from('menu');
    }

    function index()
    {
        $this->_search_enable = false;
        $this->_module_title = "Quản lý danh mục menu";
        $vars['module_search'] = " Nhập tên menu ...";
        $this->_content = 'menu/index.php';
        $vars['menu_list'] = array();

        $this->_load_data_from('menu');
        $parents = $this->_model->_get(array('parent_id' => null));
        foreach ($parents as $index => $item) {
            $parents[$index]->sub = $this->_model->_get(array("parent_id" => $item->id));
        }
        $vars['menu_list'] = $parents;
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_content = 'menu/update';
            $this->_module_title = 'Quản lý menu';
            $id = $this->input->get('id');

            $this->_load_data_from('menu');
            $vars['parents'] = $this->_model->_get(array('parent_id' => null));
            $this->_load_data_from('staticpage');
            $vars['trangtinh'] = $this->_model->_get_limit('', array('title' => 'asc'), 1, 20);
//            $this->load->model("model_pack");
//            $vars['khoahoc'] = $this->model_pack->_get_limit('', array('money' => 'asc', 'pack_name' => 'asc'), 1, 20);
            $this->_load_data_from('gallery');
            $vars['thuvien'] = $this->_model->_get_limit('', array('id' => 'asc', 'name' => 'asc'), 1, 20);

            $vars['data'] = "";
            if (trim($id) > 0) {
                $this->_load_data_from('menu');
                $vars['data'] = $this->_model->_get_top_one(array('id' => $id));
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }

            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên menu",
                "request" => "required"
            )
        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = 'bạn chưa nhập đầy đủ';
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $name = $this->input->post('name');
        $valid = $this->input->post('valid');
        $menu_parent = $this->input->post('parent_menu');
        $menu_url = $this->input->post('menu_url');
        $data = array(
            'name' => $name,
            'valid' => $valid,
            'parent_id' => $menu_parent,
            'url' => $menu_url
        );
        $this->_load_data_from('menu');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if (!$id) {
            $err = 1;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }


}
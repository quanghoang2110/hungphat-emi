<?php

class Admin extends GoBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = "tài khoản quản trị";
        parent::__construct();
    }

    function index()
    {

        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_init_page();
            $this->_module = "";
            $this->_module_title = "Trang chủ";
            $this->_module_desc = "trang chủ hệ thống";
            $this->_content = 'admin/index';

            $this->_load_data_from('admin');
            $query_arr = array(
                'keyword' => $this->_search,
                'search_row' => 'username'
            );
            $this->_set_query($query_arr);
            $total = $this->_model->_count();

            if ($this->_page > ceil($total / $this->_limit))
                $this->_page--;

            $order_arr = array(
                "id" => "desc"
            );
            $vars['list'] = array();
            if ($total > 0)
                $vars['list'] = $this->_model->_get_limit('', $order_arr, $this->_page, $this->_limit, "*, (select name from tbl_permission_group where group_id=id) group_name");

            $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);

            //permission
            $vars['permission_edit'] = $this->check_permission($this->_permission_edit);
            $vars['permission_delete'] = $this->check_permission($this->_permission_delete);

            $vars['user_id'] = $this->_sess_userid;

            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function logout()
    {
        $this->session->sess_destroy();

        redirect('/');
    }

    function update()
    {
        $data['admin_group_data'] = array();
        $id = $this->input->post('id');

        $data['permission_view'] = $this->check_permission($this->_permission_view) ? true : false;
        $data['permission_edit'] = $this->check_permission($this->_permission_edit) ? true : false;
        $data['permission_delete'] = $this->check_permission($this->_permission_delete) ? true : false;

        $data['admin_data'] = array();

        if (trim($id) > 0) {
            $where = array(
                'id' => $id
            );
            $this->_load_data_from('admin');
            $data['admin_data'] = $info = $this->_model->_get_row($where);
            $data['title'] = "Cập nhật tài khoản quản trị";
        } else {
            $data['title'] = "Thêm tài khoản quản trị";
        }

        $this->_load_data_from('permission_group');
        $data['groups'] = $this->_model->_get(array('valid' => 1));

        $this->load->view('backend/alt/admin/update', $data);
    }


    function check_account()
    {
        $admin_id = $this->input->post('hdID');
        $username = $this->input->post('username');
        $where['username'] = $username;
        if ($admin_id) {
            $where['id !='] = $admin_id;
        }
        $this->_load_data_from('admin');
        if ($this->_model->_count($where)) {
            $this->form_validation->set_message('check_account', 'Tài khoản đã tồn tại.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function ajax_update()
    {

        $config = array(
            array(
                "message_name" => "required",
                "name" => "username",
                "show_name" => "Tên tài khoản",
                "request" => "required|trim|callback_check_account"
            ),
            array(
                "message_name" => "required",
                "name" => "fullname",
                "show_name" => "Họ tên",
                "request" => "required|trim|max_length[50]"
            )

        );
        $flag = $this->_validation($config);

        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {

            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $username = trim($this->input->post('username'));
        $valid = trim($this->input->post('valid'));
        $group = $this->input->post('group');
        $fullname = $this->input->post('fullname');
        $data = array(
            'username' => $username,
            'valid' => $valid,
            'group_id' => $group,
            'fullname' => $fullname
        );
        $this->_load_data_from('admin');
        if (!$this->input->post('hdID')) {
            $data['password'] = encryption_password('123456');
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if ($id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
            'id' => $id
        );
        echo json_encode($result);
    }

    /**
     * doi mat khau
     * @author hungnm
     */
    function update_pass()
    {
        if (!$this->input->post('hdUpdatePassID')) {
            permission_denied();
        } else {
            $id = $this->input->post('hdUpdatePassID');
            if ($id != $this->session->userdata(ADMIN_SESSION_USERID)) {
                //reset pass
                $this->_model->_update(array('password' => encryption_password(DEFAULT_PASSWORD)), array('id' => $id));
            } else {
                // validation update pass
                $config = array(
                    array(
                        "message_name" => "required",
                        "name" => "password_old",
                        "show_name" => "mật khẩu cũ",
                        "request" => "required|callback_check_pass"
                    ),
                    array(
                        "message_name" => "required",
                        "name" => "password_new",
                        "show_name" => "mật khẩu mới",
                        "request" => "required|min_length[6]|max_length[20]"
                    ),
                    array(
                        "message_name" => "required",
                        "name" => "repassword",
                        "show_name" => "xác nhận mật khẩu",
                        "request" => "required|matches[password_new]"
                    ),

                );
                $flag = $this->_ValidateFormCustom($config);
                if (!$flag) {
                    $data['b_Check'] = $flag;
                    $data['msg_error'] = $this->form_validation->error_array();
                    echo json_encode($data);
                    return;
                } else {
                    $this->_update_pass();
                }
            }
        }
    }

    function check_pass()
    {
        $pass = $this->input->post('password_old');
        $id = $this->input->post('hdUpdatePassID');
        $check = 1;
        if ($id) {
            $check = $this->_model->_count(array('id' => $id, 'password' => encryption_password($pass)));
        }

        if ($check > 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_pass', 'Mật khẩu cũ không chính xác');
            return FALSE;
        }
    }

    function _update_pass()
    {
        $err = 0;
        $msg = '';
        $id = $this->input->post('hdUpdatePassID');
        $password = trim($this->input->post('password_new'));
        if ($id == $this->_sess_userid) {
            $this->_model->_update(array('password' => encryption_password($password)), array('id' => $id));
        } else {
            $err = 1;
            $msg = 'Không thể đổi mật khẩu tài khoản này';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }
}
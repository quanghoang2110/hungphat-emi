<?php

class Product_type extends GoBackendController
{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_product_type';
        $this->_load_model($this->_model);
        $this->_folder = 'product_type';
    }

    /**
     * Category news
     **/
    function index()
    {
        $this->load->model('model_product_type');
        $this->_init_page();
        $this->_module = QUAN_TRI_SAN_PHAM;
        $this->_module_title = 'Quản lý nhóm sản phẩm';
        $this->_module_desc = "";
        $this->_content = $this->_folder . '/index';
//        $row_search = "name";
        $this->_load_data_from('product_type');
//        $total = $this->_model->_count_search("", loc_dau_tv($this->_search), $row_search);
//        if ($this->_page > ceil($total / $this->_limit))
//            $this->_page = $this->_page--;

//        $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('id' => 'desc'), $this->_page, $this->_limit, "", $row_search);
//
//        $url = base_url(QUAN_TRI_SAN_PHAM) . "?per_size=" . $this->_limit;
        $vars['list'] = $this->_model->_get("", array('parent_id' => 'asc', 'id' => 'asc'), '*');
//        $url = base_url(QUAN_TRI_SAN_PHAM);
//        $url = base_url(QUAN_TRI_SAN_PHAM) . "?per_size=" . $this->_limit;
//        $vars['paging_info'] = $this->_get_page_info($total);
//        $vars['paging'] = get_paging($url, $total, $this->_limit);
//        echo'<pre>';print_r($vars['list']);die;
        $vars['list_parent'] = $this->_model->_get(array('parent_id' => null));
        $this->load->vars($vars);
        $this->_load_tmp();
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('product_type');
        $id = (int)($this->input->post("id"));
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            $this->load->helper('url');
            redirect('backend/provider/index', 'refresh');
            echo '1';
        }
    }

    function update()
    {
        $this->_load_data_from('product_type');
        //them moi
        if ($this->input->post('id') == -1) {
            $data = array(
                'name' => $this->input->post('name')
            );
            if ($this->input->post('parent') != -1)
                $data['parent_id'] = $this->input->post('parent');
            else
                $data['parent_id'] = null;
            $this->_model->_insert($data);
        } //cap nhat
        else {
            $data = array(
                'name' => $this->input->post('name')
            );
            if ($this->input->post('parent') != -1)
                $data['parent_id'] = $this->input->post('parent');
            else
                $data['parent_id'] = null;
            $this->_model->_update($data, array('id' => $this->input->post('id')));

        }
    }


    function delete()
    {
        $this->_load_data_from('product_type');
        $this->_model->_delete(array('id' => $this->input->post('id')));
//        $this->_model->_update(array('parent_id'=>null), array('parent_id'=>$this->input->post('id')));
    }
//    function ajax_update()
//    {
//        $config = array(
//            array(
//                "message_name" => "required",
//                "name" => "name",
//                "show_name" => "Tên sản phẩm",
//                "request" => "required|trim|max_length[255]"
//            ),
//            array(
//                "message_name" => "required",
//                "name" => "avatar",
//                "show_name" => "Ảnh",
//                "request" => "required"
//            )
//
//        );
//        $flag = $this->_ValidateFormCustom($config);
//        if (!$flag) {
//            $data['b_Check'] = $flag;
//            $data['msg_error'] = $this->form_validation->error_array();
//            echo json_encode($data);
//            return;
//        } else {
//            $this->_update_general();
//        }
//    }
//
//    function _update_general()
//    {
//        $err = 0;
//        $msg = '';
//        $this->_load_data_from('product');
//        $name = trim($this->input->post('name'));
//        $valid = $this->input->post('valid');
//        $is_hot = $this->input->post('is_hot');
//        $avatar = $this->input->post('avatar');
//        $avatar = str_replace(base_url(), "", $avatar);
//        $publisher = trim($this->input->post('publisher'));
//        $type = trim($this->input->post('type'));
//        $form = trim($this->input->post('from'));
//        $price_1 = trim($this->input->post('price_1'));
//        $price_2 = trim($this->input->post('price_2'));
//        $pice = trim($this->input->post('pice'));
//        $head_title = trim($this->input->post('head_title'));
//        $head_description = trim($this->input->post('head_description'));
//        $data = array(
//            'name' => $name,
//            'valid' => $valid,
//            'is_hot' => $is_hot,
//            'avatar' => $avatar,
//            'from' => $form,
//            'price_1' => $price_1,
//            'price_2' => $price_2,
//            'guide' => $this->input->post('guide'),
//            'content' => $this->input->post('content'),
//            'pice' => $pice,
//            'head_title' => $head_title,
//            'head_description' => $head_description,
//        );
//
////        print_r($data);
//
//        if ($publisher != -1)
//            $data['product_publisher'] = $publisher;
//        if ($type != -1)
//            $data['product_type'] = $type;
//        if (!$this->input->post('hdID')) {
//            $this->_model->_insert($data);
//            $qr_id = $this->_model->_get_row($data);
//            $id = $qr_id->id;
//        } else {
//            $id = $this->input->post('hdID');
//            $this->_model->_update($data, array('id' => $id));
//            //xoa nhung anh product cu
//            $this->_load_data_from('product_img');
//            $this->_model->_delete(array('product_id' => $id));
//        }
//        //them nhung anh moi
//        $this->_load_data_from('product_img');
//        for ($i = 0; $i < count($this->input->post('image_product')); $i++) {
//            if ($this->input->post('image_product') != DEFAULT_IMG) {
//                $this->_model->_insert(
//                    array(
//                        'product_id' => $id,
//                        'img' => $this->input->post('image_product[' . $i . ']')
//                    )
//                );
//            }
//        }
//        if ($id) {
//            $err = 0;
//            $msg = 'Đã có lỗi xảy ra';
//        }
//        $result = array(
//            'err' => $err,
//            'msg' => $msg,
//        );
//        echo json_encode($result);
//    }

    function ajax_delete()
    {
        $this->_load_data_from('product_type');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }


}

?>
<?php

class Static_page2 extends GoBackendController
{

    function __construct()
    {
        //ten chuc nang
        $this->_permission_controller_name = "nội dung tĩnh";
        parent::__construct();
    }

    /**
     * Category news
     **/
    function index()
    {
        $this->_init_page();
        $this->_module_title = 'Trang tĩnh';
        $this->_module_desc = "Quản lý trang tĩnh";
        $this->_content = 'static_page2/index';

        $this->_load_data_from('static_page');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page = $this->_page--;
        if ($total > 0)
            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('id' => 'asc'), $this->_page, $this->_limit);
        //echo $this->_last_query();

        $url = base_url(QUAN_TRI_TRANG_TINH) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $this->_module_title = 'Trang tĩnh';

            $this->_content = 'static_page2/update';

            $id = $this->input->get('id');
            $vars = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $this->_load_data_from('static_page');
                $vars['data'] = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật trang tĩnh";
            } else {
                $this->_module_desc = "Thêm trang tĩnh";
            }
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên trang tĩnh",
                "request" => "trim|required|max_length[255]"
            ),
            array(
                "message_name" => "required",
                "name" => "content",
                "show_name" => "Nội dung trang",
                "request" => "trim|required"
            )
        );
        $flag = $this->_run_form_validation($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $name = $this->input->post('name');
        $content = $this->input->post('content');
        $data = array(
            'name' => $name,
            'content' => $content
        );

        echo '<pre>';
        print_r($content);
        echo '</pre>'; die();

        $this->_load_data_from('static_page');
        if (!$this->input->post('hdID')) {
            $id = $this->_model->_insert($data);
        } else {
            $id = $this->input->post('hdID');
            $this->_model->_update($data, array('id' => $id));
        }
        if ($id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

}

?>
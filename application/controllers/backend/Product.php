<?php

class Product extends GoBackendController
{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_product';
        $this->_load_model($this->_model);
        $this->_folder = 'product';
    }

    /**
     * Category news
     **/
    function index()
    {
        $this->load->model('model_product');
        $this->_init_page();
        $this->_module = QUAN_TRI_SAN_PHAM;
        $this->_module_title = 'Quản lý sản phẩm';
        $this->_module_desc = "";
        $this->_content = $this->_folder . '/index';
        $row_search = "product.name";
        $this->_load_data_from('product');
        $total = $this->_model->_count_search("", loc_dau_tv($this->_search), $row_search);
        if ($this->_page > ceil($total / $this->_limit))
            $this->_page = $this->_page--;

        $vars['list'] = $this->model_product->_get_search_limit("", $this->_search, array('id' => 'desc'), $this->_page, $this->_limit, "", $row_search);

        $url = base_url(QUAN_TRI_SAN_PHAM) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_search_enable = true;
        $this->load->vars($vars);
        $this->_load_tmp();
    }

    function update()
    {
        $this->_module = QUAN_TRI_SAN_PHAM;
        $this->_module_title = 'Sản phẩm';

        $this->_content = $this->_folder . '/update';

        $id = $this->input->get('id');
        $this->_load_data_from('product');
        //get list
        $vars = array();
        if (trim($id) > 0) {
            $where = array(
                'id' => $id
            );
            $vars['list'] = $this->_model->_get_top_one($where);
            $this->_load_data_from('product_img');
            $vars['list_image'] = $this->_model->_get(array('product_id' => $id));
            $this->_module_desc = "Cập nhật";
        } else {
            $this->_module_desc = "Thêm mới";
        }
        $this->_load_data_from('product_publisher');
        $vars['publisher'] = $this->_model->_get();
        $this->_load_data_from('product_type');
        $vars['type'] = $this->_model->_get();
        $this->_module_vars = $vars;
        $this->_load_tmp();
    }

    function ajax_update()
    {
        $config = array(
            array(
                "message_name" => "required",
                "name" => "name",
                "show_name" => "Tên sản phẩm",
                "request" => "required|trim|max_length[255]"
            )

        );
        $flag = $this->_ValidateFormCustom($config);
        if (!$flag) {
            $data['b_Check'] = $flag;
            $data['msg_error'] = $this->form_validation->error_array();
            echo json_encode($data);
            return;
        } elseif (($this->input->post(('image_product[0]')) == DEFAULT_IMG) && ($this->input->post(('image_product[1]')) == DEFAULT_IMG) && ($this->input->post(('image_product[2]')) == DEFAULT_IMG) && ($this->input->post(('image_product[3]')) == DEFAULT_IMG)) {
            $data['b_Check'] = false;
            $data['msg_error'] = array('Vui lòng nhập ảnh');
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $msg = '';
        $this->_load_data_from('product');
        $name = trim($this->input->post('name'));
        $valid = $this->input->post('valid');
        $is_hot = $this->input->post('is_hot');
//        $avatar = $this->input->post('avatar');
//        if ($this->input->post('image_product[0]')) {
//            $avatar = $this->input->post('image_product[0]');
//        } else
//            $avatar = DEFAULT_IMG;
//        echo $avatar;die();
        $publisher = trim($this->input->post('publisher'));
        $type = trim($this->input->post('type'));
        $form = trim($this->input->post('from'));
        $price_1 = trim($this->input->post('price_1'));
        $price_2 = trim($this->input->post('price_2'));
        $pice = trim($this->input->post('pice'));
        $head_title = trim($this->input->post('head_title'));
        $total_vote = trim($this->input->post('total_vote'));
        $head_description = trim($this->input->post('head_description'));
        $data = array(
            'name' => $name,
            'valid' => $valid,
            'is_hot' => $is_hot,
            'from' => $form,
            'total_vote' => $total_vote,
            'price_1' => $price_1,
            'price_2' => $price_2,
            'guide' => $this->input->post('guide'),
            'content' => $this->input->post('content'),
            'pice' => $pice,
            'head_title' => $head_title,
            'head_description' => $head_description,
        );

//        print_r($data);

        if ($publisher != -1)
            $data['product_publisher'] = $publisher;
        if ($type != -1)
            $data['product_type'] = $type;
        if (!$this->input->post('hdID')) {
            $this->_model->_insert($data);
            $qr_id = $this->_model->_get_row($data);
            $id = $qr_id->id;
        } else {
            $id = $this->input->post('hdID');
            //xoa nhung anh product cu
            $this->_load_data_from('product_img');
            $this->_model->_delete(array('product_id' => $id));
            $this->_load_data_from('product');

            $this->_model->_update($data, array('id' => $id));

        }
        //them nhung anh moi
        $this->_load_data_from('product_img');
        for ($i = 0; $i < count($this->input->post('image_product')); $i++) {
            if ($this->input->post('image_product[' . $i . ']') != DEFAULT_IMG) {
                $this->_model->_insert(
                    array(
                        'product_id' => $id,
                        'img' => $this->input->post('image_product[' . $i . ']'),
                        'order' => ($i + 1)
                    )
                );
            }
        }
        if ($id) {
            $err = 0;
            $msg = 'Thành công';
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
        );
        echo json_encode($result);
    }

    function ajax_delete()
    {
        $this->_load_data_from('product');
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    function ajax_valid_old()
    {
        $this->_load_data_from('product');
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }

    function ajax_hot()
    {
        $this->_load_data_from('product');
        $id = $this->input->post("id");
        $hot = $this->input->post("hot");
        $hot = $hot == 1 ? 0 : 1;
        $data = array('is_hot' => $hot);
        if ($id > 0) {
            $this->_model->_update($data, $where_arr = array('id' => $id));
            die();
        }
    }


}

?>
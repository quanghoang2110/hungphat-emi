<?php
/**
 * @copyright Go Solution JSC
 * Created by PHPStorm.
 * User: Tran Duc
 * Date: 4/18/2016
 * Time: 11:06 AM
 */
class Category_news extends Go_BackendController{

    function __construct()
    {
        parent::__construct();
        $this->_model = 'model_news_category';
        $this->_load_model($this->_model);
        $this->_folder = 'cat_news';
        $this->_permission = unserialize(ADMIN_PERMISSION_NEWS_CATEGORY);
        $this->_permission_view = ADMIN_PERMISSION_NEWS_CATEGORY_VIEW;
        $this->_permission_edit = ADMIN_PERMISSION_NEWS_CATEGORY_EDIT;
        $this->_permission_delete = ADMIN_PERMISSION_NEWS_CATEGORY_DELETE;
    }
    /**
    * Category news
    **/
    function index()
    {
        if(!check_permissions($this->_permission)) {
            permission_denied();
        }else {
            $this->_init_page();
            $this->_module = ADMIN_MENU_CATEGORY;
            $this->_module_sub = ADMIN_MENUSUB_CAT_NEWS;
            $this->_module_title = "Danh mục tin tức";
            $this->_module_desc = "";
            $this->_content = $this->_folder . '/index';
            $row_search = "name";

            $total = $this->_model->_count_search("", $this->_search, $row_search);
            if ($this->_page > ceil($total / $this->_limit))
                $this->_page = $this->_page--;


            $vars['list'] = $this->_model->_get_search_limit("", $this->_search, array('order'=>'asc'), $this->_page, $this->_limit, "*", $row_search);

            $url = base_url(ROUTE_ADMIN_CAT_NEWS) . "?per_size=" . $this->_limit;

            $vars['paging_info'] = $this->_get_page_info($total);
            $vars['paging'] = get_paging($url, $total, $this->_limit);
            $this->_module_vars = $vars;
            $this->_load_tmp();
        }
    }
    
    function check_data(){
        $config = array(
            array(
                "message_name" => "name",
                "name" => "name",
                "show_name" => "tên danh mục tin tức",
                "request" => "required"
            ),
            array(
                "message_name" => "required",
                "name" => "order",
                "show_name" => "Vị trí",
                "request" => "required|numeric|callback_db_exists_check"
            ),
            array(
                "message_name" => "required",
                "name" => "optvalid",
                "show_name" => "trạng thái",
                "request" => "required|numeric"
            )
        );
        $flag = $this->_ValidateFormCustom($config);
        if (!$flag) {
            $error = $this->form_validation->error_array();
            echo json_encode(array('error'=>'1','data'=>$error,'type'=>1));
            return;
        } else{
            $this->update_news();
        }
    }

    function db_exists_check(){

        $check=0;
        $id = (int)$this->input->post('HD_Id');
        $where_id = ($id>0) ? "id !=".$id : '';
        if($this->input->post('order') && $this->input->post('order') != ''){
            $check = $this->_model->is_exists_check(array('order'=>$this->input->post('order')),$where_id);
        }
        if($check>0){
            $this->form_validation->set_message('db_exists_check',$this->lang->line('is_exists'));
            return FALSE;
        }else{
            return TRUE;
        }
    }

    function update_news(){
        if(!check_permissions($this->_permission_edit) && !check_permissions($this->_permission_delete)) {
            permission_denied();
        }else {
            $msg = "";
            $err = 0;
            //Du lieu lay tu form
            $name = trim($this->input->post('name'));
            $order = (int)$this->input->post('order');
            $status = (int)$this->input->post('optvalid');
            $data_member = array(
                "name" => $name,
                "order" => $order,
                "valid" => $status
            );

            if ($this->input->post('HD_Id')) {
                $id = $this->input->post('HD_Id');
                $this->_model->_update($data_member, array('id' => $id));
                $msg = $this->lang->line('update_success');
            } else {
                $id = $this->_model->_insert($data_member);
                $msg = $this->lang->line('insert_success');
                $id = 1;
            }
            if (!$id) {
                $msg = $this->lang->line('error');
                $err = 1;
            }

            $result = array(
                "msg" => $msg,
                "err" => $err
            );
            echo json_encode($result);
        }
    }
}
?>
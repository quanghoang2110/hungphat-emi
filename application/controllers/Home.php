<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();

//        $this->_load_data_from('category');
//        $this->_module_vars['menu'] = $this->_model->_get(array('valid' => 1));

    }

    public function index()
    {
        $vars['menu_current'] = "home";
        //slider
        $this->_load_data_from('slider');
        $vars['list_slider'] = $this->_model->_get(
            array(
                'valid' => 1
            ),
            array('position' => 'asc'),
            $select = "*");
        $this->_load_data_from('product');
        $where = array(
            'valid' => 1,
            'is_new' => 1
        );
        $order = array(
            'position' => 'desc',
            'id' => 'desc'
        );
        $vars['product_new'] = $this->_model->_get_limit($where, $order, 1, 16);

        $where = array(
            'valid' => 1,
            'is_hot' => 1
        );
        $vars['product_hot'] = $this->_model->_get_limit($where, array('position' => 'desc', 'id' => 'desc', 'is_hot' => 1), 1, 16);

        $this->_load_data_from('news');
        $vars['news'] = $this->_model->_get_limit(array('valid' => 1), array('position' => 'desc', 'id' => 'desc'), 1, 4);

        $this->_load_data_from('banner');
        $vars['banner'] = $this->_model->_get_limit(array('valid' => 1, 'type' => BANNER_VUONG), array('position' => 'desc'), 1, 4);

        $vars['banner_top'] = $this->_model->_get_top_one(array('valid' => 1, 'type' => BANNER_TOP), array('position' => 'desc'));

        $this->_load_data_from('product_publisher');
        $vars['publisher'] = $this->_model->_get_limit(array('valid' => 1), array(), 1, 15);

        //cam nhan khach hang
        $this->_load_data_from('comment');
        $vars['list_comment'] = $this->_model->_get(array('valid' => 1));
        $this->_content = 'home';
        $this->load->vars($vars);
        $this->_load_tmp();
    }
}



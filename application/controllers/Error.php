<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Max Mun
 * Date: 9/1/2015
 * Time: 17:04
 */
class Error extends GoFrontendController
{
    function __construct()
    {
        parent::__construct();
    }

    function index($type = "404")
    {
        if ($this->config->item('error_404'))
            redirect('404');
        else {
            $data['heading'] = "PAGE NOT FOUND";
            $data['message'] = "<p>Liên kết không tồn tại hoặc đã bị thay đổi</p><p><a href='/'>Quay lại trang chủ</a></p>";
            $this->load->vars($data);
            $this->load->view('errors/html/error_404');
        }

    }
}
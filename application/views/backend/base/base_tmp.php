<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title><?php echo $module_title ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

<!--    <link rel="shortcut icon" href="--><?php //echo $favicon; ?><!--" type="image/x-icon">-->
<!--    <link rel="icon" href="--><?php //echo $favicon; ?><!--" type="image/x-icon">-->

    <?php $this->load->view('backend/base/base_js'); ?>

    <!-- load head tag -->
    <?php
    if (file_exists(APPPATH . "views/{$theme_template}/head.php"))
        $this->load->view("{$theme_template}/head");
    ?>

</head>

<body>
<!--load content-->
<?php
//echo $theme_template;
$go_base_content = $theme_template . 'content';
if (file_exists(APPPATH . "views/{$go_base_content}.php"))
    $this->load->view($go_base_content) ?>

<!--load footer-->
<?php
$go_base_footer = $theme_template . 'footer';
if (file_exists(APPPATH . "views/{$go_base_footer}.php"))
    $this->load->view($go_base_footer);
?>

<!--base tmp-->
<?php //$this->load->view('backend/base/modal_tmp'); ?>

<div id="for_show_thongbao_only" style="display: none"></div>

</body>

</html>
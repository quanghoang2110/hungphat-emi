<link rel="stylesheet" href="/assets/base/bootstrap/css/bootstrap.min.css">
<script src="/assets/base/js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="/assets/base/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $("#modal_thongbaogap").modal("show");

        $('#modal_thongbaogap').on('hidden.bs.modal', function () {
            location.href = '<?php echo $url_redirect ?>';
        })
    });
</script>

<div class="modal" id="modal_thongbaogap">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $msg ?></p>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url()?>" class="btn btn-default" id="msgOK">Trang chủ</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="msgOK">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
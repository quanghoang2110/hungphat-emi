<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }
</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->

        <div class="row">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="title">Tên khách hàng <span class="require">(*)</span></label></br>
                        <input id="title" class="form-control" type="text" name="name"
                               value="<?php if (isset($data)) echo $data->name; ?>"/>
                    </div>
                    <div class="col-sm-4">
                        <label>Email</label>
                        <input class="form-control" value="<?php if (isset($data->email)) echo $data->email; ?>"
                               name="email">
                    </div>
                    <div class="col-sm-4">
                        <label>Số điện thoại</label>
                        <input class="form-control" value="<?php if (isset($data->phone)) echo $data->phone; ?>"
                               name="phone">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="valid">Trang thái:</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                value="1">Mở
                            </option>
                            <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                value="0">Khóa
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Tiêu đề</label>
                <textarea id="title" name="title" class="form-control" maxlength="450"
                          placeholder="Mô tả nội dung...(max 450 ký tự)"><?php if (isset($data)) echo $data->title; ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Nội dung liên hệ <span class="require">(*)</span></label>
                <textarea id="content" name="content"
                          placeholder="Nội dung ..."><?php if (isset($data)) echo $data->content; ?></textarea>
            </div>
        </div>


        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
    <div class="box-footer">
        <!--        <button type="button" class="btn btn-default"-->
        <!--                onclick="backUrl()">-->
        <!--            Hủy-->
        <!--        </button>-->
        <a href='<?php echo base_url(QUAN_TRI_LIEN_HE); ?>' type='button' class='btn btn-default'>
            Hủy </a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<style>
    textarea {
        min-height: 80px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $(function () {
        show_textarea('content', '900');
    });
    urlUpdate = "<?php echo base_url(QUAN_TRI_LIEN_HE_UPDATE);?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    urlBack = "<?php echo base_url(QUAN_TRI_LIEN_HE);?>?per_size="+per_size+"&per_page="+per_page;
    var frm_id = 'form_update';
    SubmitForm(frm_id,'',urlBack);
</script>
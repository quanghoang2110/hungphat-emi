<div class="box-body">
    <div class="col-sm-6 pull-right">
        <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_250">tên khách hàng</th>
            <th class="width_300">Thông tin</th>
            <th>Liên hệ</th>
            <th class="width_150">Trạng thái</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($news_list) { ?>
            <?php foreach ($news_list as $index => $news) { ?>
                <tr id="item-<?php echo $news->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($news->name); ?></td>
                    <td>
                        <p>Email: <?php if (isset($news->email)) echo $news->email; ?></p>

                        <p>Phone: <?php if (isset($news->phone)) echo $news->phone; ?></p>

                        <p>Ngày
                            gửi: <?php if (isset($news->update_time)) echo date('d-m-Y H:i:s', strtotime($news->update_time)); ?></p>
                    </td>
                    <td class="text-muted">
                        <!--                        <p>Tiêu đề: -->
                        <?php //if (isset($news->title)) echo $news->title; ?><!--</p>-->

                        <!--                        <p>Nội dung̀:</p>-->
                        <?php if (isset($news->content)) echo $news->content; ?>
                    </td>
                    <td align="center">
                        <select onchange="ajaxValidContact(<?php echo $news->id; ?>);" class="form-control"
                                id="contact_<?php echo $news->id; ?>">
                            <option value="0" <?php if ($news->valid == 0) echo 'selected'; ?>>Chưa liên hệ</option>
                            <option value="1" <?php if ($news->valid == 1) echo 'selected'; ?>>Đã liên hệ</option>
                        </select>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>

                        <button class="btn btn-flat" title="Xóa" title="Xóa"
                                onclick="deleteAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>

                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_LIEN_HE_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_LIEN_HE_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_LIEN_HE_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
        function updateAjax(id) {
            if (typeof (id) !== 'underfined') {
                window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
            } else {
                window.location.href = urlForm;
            }
        }
    function ajaxValidContact(id) {
        $.ajax({
            type: "POST",
            url: base_url + "backend/contact/ajaxValidContact",//trỏ tới file nào
            data: {
                id: id,
                valid: $('#contact_' + id).val()
            },
            success: function () {
                showThongBao("Cập nhật trạng thái thành công");
            },
            error: function () {
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }
</script>


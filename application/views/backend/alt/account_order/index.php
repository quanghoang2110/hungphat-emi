<div class="box-body">
    <!--        <div class="col-sm-6 pull-right">-->
    <!--            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>-->
    <!--        </div>-->
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th style="width: 200px">Mã đơn hàng</th>
            <th>Khách hàng</th>
            <th style="width: 150px">Tổng tiền</th>
            <th class="width_200">Trạng thái</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($news_list) { ?>
            <?php foreach ($news_list as $index => $news) { ?>
                <tr id="item-<?php echo $news->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($news->code); ?></td>
                    <td class="text-muted">
                        <p><b>Tài khoản: </b><?php if (isset($news->username)) echo $news->username; ?></p>

                        <p><b>Họ tên: </b><?php if (isset($news->name)) echo $news->name; ?></p>

                        <p><b>Số điện thoại: </b> <?php if (isset($news->phone)) echo $news->phone; ?></p>

                        <p><b>Địa chỉ: </b><?php if (isset($news->address)) echo $news->address; ?></p>

                        <p><b>Ngày đặt
                                mua: </b><?php if (isset($news->update_time)) echo date('H:i:s d-m-y', strtotime($news->update_time)); ?>
                        </p>

                        <p><b>Ghí chú: </b><?php if (isset($news->note)) echo $news->note; ?></p>
                    </td>
                    <td>
                        <?php if (isset($news->total_price)) echo $news->total_price . ' vnđ'; ?>
                    </td>
                    <td align="center">
                        <select id="order_<?php echo $news->id; ?>" class="form-control"
                                onchange="ajaxStatusOrder(<?php echo $news->id ?>);">
                            <option value="-1" <?php if ($news->status == CHUA_DIEN_THONG_TIN) echo 'selected'; ?>>Chưa
                                điền thông tin
                            </option>
                            <option value="0" <?php if ($news->status == CHUA_GIAO_HANG) echo 'selected'; ?>>Chưa giao
                                hàng
                            </option>
                            <option value="1" <?php if ($news->status == HOAN_THANH) echo 'selected'; ?>>Hoàn thành
                            </option>
                        </select>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Xem chi tiết"
                                onclick="updateAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-eye"></i>
                        </button>

                        <button class="btn btn-flat" title="Xóa" title="Xóa"
                                onclick="deleteAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_DON_HANG_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_DON_HANG_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_DON_HANG_FORM) ?>";

    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id;
        } else {
            window.location.href = urlForm;
        }
    }
    function ajaxStatusOrder(id) {
        $.ajax({
            type: "POST",
            url: base_url + "backend/account_order/ajaxStatus",//trỏ tới file nào
            data: {
                id: id,
                valid: $('#order_' + id).val()
            },
            success: function () {
                showThongBao("Cập nhật trạng thái thành công");
            },
            error: function () {
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }
</script>


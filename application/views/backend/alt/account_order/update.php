<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }
</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->
        <div class="col-sm-12 padding_bottom_20">
            <b>Mã đơn hàng:</b> <?php if (isset($data->code)) echo $data->code; ?>
        </div>

        <div class="col-sm-12 padding_left_15">
            <h4 class="">Thông tin khách hàng</h4>
        </div>
        <div class="col-sm-12 div_child padding_bottom_20">
            <p><span class="width_200">Tài khoản: </span> <?php if (isset($data->username)) echo $data->username; ?>
            </p>

            <p><span class="width_200">Họ và tên: </span> <?php if (isset($data->name)) echo $data->name; ?></p>

            <p><span class="width_200">Số điện thoại: </span> <?php if (isset($data->phone)) echo $data->phone; ?>
            </p>

            <p><span class="width_200">Địa chỉ: </span> <?php if (isset($data->address)) echo $data->address; ?></p>
        </div>
        <div class="col-sm-12 padding_left_15 ">
            <h4 class="">Chi tiết đơn hàng</h4>
        </div>

        <div class="col-sm-12 div_child min_height_120">
            <div class="col-sm-6 border_bottom"><b>Sản phẩm</b></div>
            <div class="col-sm-2 border_bottom"><b>Số lượng</b></div>
            <div class="col-sm-3 border_bottom"><b>Giá/1 sản phẩm (vnđ)</b></div>
            <?php $item = -1; ?>
            <?php foreach ($list as $key => $val): ?>
                <?php $item++; ?>
                <div class="col-sm-6 border_bottom">
                    <?php foreach ($product as $k => $v): ?>
                        <?php if ($val->id == $v->id) echo $v->name; ?>
                    <?php endforeach; ?>
                </div>
                <div class="col-sm-2 border_bottom">
                    <?php if (isset($val->number)) echo $val->number; ?>
                </div>
                <div class="col-sm-3 border_bottom">
                    <?php if (isset($val->price_per_one)) echo $val->price_per_one; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-sm-12 ">

        </div>
        <div class="col-sm-12">
            <h4>Tổng tiền(đơn vị vnđ)</h4>

            <div class="col-sm-2">
                <input disabled value="<?php if (isset($data->total_price)) echo $data->total_price; else echo '0' ?>"
                       name="total_price" class="form-control" id="total_price">
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href='<?php echo base_url(QUAN_TRI_DON_HANG); ?>' type='button' class='btn btn-default'>
            Quay lại </a>
        <!--        <button type="button" id="submit_button" class="btn btn-primary">Lưu</button>-->
        <!--        <button type="submit" id="submit_button_ok" class="btn btn-primary hidden">OK</button>-->
        <!--        <input type="hidden" id="hdID" name="hdID" value="-->
        <?php //if (isset($data)) echo $data->id; ?><!--">-->
    </div>
</form>
<style>
    .min_height_120 {
        min-height: 120px;
    }

    .border_bottom {
        border-bottom: 1px solid #cccccc;
        padding-top: 3px;
        padding-bottom: 3px;
    }

    .width_200 {
        width: 200px;
    }
    textarea {
        min-height: 80px;
    }

    .padding_left_15 {
        padding-left: 15px;
    }

    .div_child {
        padding-left: 10%;
    }

    h4 {
        border-top: 1px solid #cccccc;
        padding-top: 10px;
        font-weight: bold;
    }

    .padding_bottom_20 {
        padding-bottom: 20px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    urlUpdate = "<?php echo base_url(QUAN_TRI_DON_HANG_UPDATE);?>";
    var frm_id = 'form_update';
    function changeTotalPrice() {
        var item = parseInt($('#item').val()) + 1;
        var total_price = 0;
        for (var i = 0; i < item; i++) {
            total_price += parseInt($('#number_' + i).val()) * parseInt($('#price_per_one_' + i).val());
        }
        $('#total_price').val(total_price);
    }
    $('#submit_button').click(function () {
        changeTotalPrice();
        $('#submit_button_ok').click();
    });
    SubmitForm(frm_id);
</script>
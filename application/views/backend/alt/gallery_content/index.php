<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_120">Hình ảnh</th>
            <th class="width_200">Tên bài viết</th>
            <th class="">Mô tả</th>
            <th class="width_200">Danh mục menu</th>
            <th class="width_150">Type</th>
            <th class="width_80">Lượt xem</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $index => $news) { ?>
            <tr id="item-<?php echo $news->id; ?>">
                <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                <td align="center"><?php echo get_img_tag($news->image, '', 'table-image', base_url('images/default/image.png')); ?></td>
                <td><?php echo($news->title); ?></td>
                <td><?php echo $news->description; ?></td>
                <td><?php echo($news->gallery_name); ?></td>
                <td>
                    <?php
                    switch ($news->content_type) {
                        case 'image':
                            echo 'Hình ảnh';
                            break;
                        case 'video':
                            echo 'Video';
                            break;
                        default:
                            echo 'Văn bản';
                            break;
                    }
                    ?>
                </td>
                <td><span style="float: right"><?php echo($news->total_views); ?></span></td>
                <td align="center">
                    <button class="item_valid btn-valid btn btn-flat"
                            title="<?php if ($news->valid == 1) echo 'Mở bài viết'; else echo 'Khóa bài viết'; ?>"
                            onclick="validAjax(<?php echo $news->id ?>, <?php echo $news->valid ?>)">
                        <i class="fa <?php if ($news->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                        <span
                            class="text-show-valid"><?php if ($news->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                    </button>
                </td>
                <td align="center">
                    <button class="btn-repair-question btn btn-flat" title="Sửa"
                            onclick="updateAjax(<?php echo $news->id ?>)">
                        <i class="fa fa-edit"></i>
                    </button>
                    <?php if ($permission_delete) { ?>
                        <button class="btn btn-flat" title="Xóa" onclick="deleteAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>

<?php $this->load->view($theme_template . '/script'); ?>

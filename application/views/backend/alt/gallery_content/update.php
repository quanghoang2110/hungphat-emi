<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->

        <div class="row">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="form-group col-xs-3">
                        <label for="group_name">Ảnh <span class="require">(*)</span></label>
                        <div id="show_img">
                            <a class="file-manager">
                                <?php echo get_img_tag((isset($data->image) && $data->image != '') ? $data->image : '', '', 'rfm-image table-image', base_url('images/default/image.png')); ?>
                                <input type="text" id="image" name="image" class="rfm-link hidden"
                                       value="<?php if (isset($data->image)) echo $data->image; ?>">
                            </a>
                        </div>
                        <span id="error_image" class="error"></span>
                    </div>
                    <div class="form-group col-xs-6">
                        <label for="title">Tên bài viết <span class="require">(*)</span></label></br>
                        <input id="title" class="form-control" type="text" name="title"
                               value="<?php if (isset($data)) echo $data->title; ?>"/>
                    </div>
                    <div class="form-group col-xs-3">
                        <label for="total_view">Lượt xem</label></br>
                        <input id="total_view" class="form-control" disabled type="text" name="total_view"
                               value="<?php if (isset($data)) echo $data->total_views; ?>"/>
                    </div>
                    <div class="form-group col-xs-3">
                        <label for="menu">Danh mục</label></br>
                        <select class="form-control" id="menu_id" name="menu_id">
                            <?php foreach ($list_menu as $index => $item) { ?>
                                <option <?php if (isset($data) && $data->gallery_id == $item->id) echo 'selected' ?>
                                    value="<?php echo $item->id; ?>">
                                    <?php echo $item->name ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-3">
                        <label for="valid">Trạng thái:</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                value="<?php echo 1; ?>">Mở
                            </option>
                            <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                value="<?php echo 0; ?>">Khóa
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-2 hidden">
                        <label for="valid">Vị trí:</label>
                        <input type="number" name="position" class="form-control"
                               value="<?php echo isset($data) ? $data->position : '1' ?>"/>
                    </div>
                    <div class="form-group col-xs-3">
                        <label for="valid">Dạng bài viết: </label>
                        <?php $content_type = isset($data) ? $data->content_type : 'text' ?>
                        <select class="form-control" id="content_type" name="content_type"
                                onchange="javascript:changeContentType($(this))">
                            <option value="text" <?php echo $content_type == 'text' ? 'selected' : '' ?>>Dạng văn bản
                            </option>
                            <option value="image" <?php echo $content_type == 'image' ? 'selected' : '' ?>>Dạng ảnh
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Giới thiệu <span class="require">(*)</span></label>
                <textarea id="desc" name="desc" class="form-control" maxlength="450"
                          placeholder="Mô tả nội dung...(max 450 ký tự)"><?php if (isset($data)) echo $data->description; ?></textarea>
            </div>
        </div>
        <!--        <div class="row">-->
        <!--            -->
        <!--        </div>-->
        <div class="row">
            <div class="form-group col-xs-12">
                <div class="content-hidden" id="content-text">
                    <label for="link_download">Nội dung tin tức <span class="require">(*)</span></label>
                    <textarea id="content" name="content"
                              placeholder="Nội dung ..."><?php if (isset($data)) echo $data->content; ?></textarea>
                </div>
            </div>
        </div>

        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
</form>
<div class="box-body">
    <div class="row">
        <div class="form-group col-xs-12">
            <div class="content-hidden" id="content-image">
                <label for="link_download">Thêm hình ảnh</label>
                <div class="col-sm-12">
                    <?php $var['id'] = isset($data) ? $data->id : "_undefined"; ?>
                    <?php $this->load->view($theme_view.'/gallery_content/file_upload', $var); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="box-footer">
    <button type="submit" onclick="javascript:$('#form_update').submit();" class="btn btn-primary pull-right"
            style="margin-left: 10px">Lưu
    </button>
    <a class="btn btn-default pull-right"
       href="#cancel" onclick="backUrl()">
        Hủy
    </a>
</div>

<style>
    textarea {
        min-height: 80px;
    }

    .content-hidden {
        display: none;
    }
</style>
<?php $this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $("#content_type").change();

    $(function () {
        show_textarea('content', '900');
    });

    function changeContentType(contentType) {
        $(".content-hidden").hide();
        switch (contentType.val()) {
            case 'video':
                $("#content-video").fadeIn();
                break;
            case 'image':
                $("#content-image").fadeIn();
                break;
            default:
                $("#content-text").fadeIn();
                break;
        }
    }

    SubmitForm();

</script>
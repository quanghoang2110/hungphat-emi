<div class="box-body">
    <!--        <div class="col-sm-6 pull-right">-->
    <!--            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>-->
    <!--        </div>-->
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_30">Tên trang</th>
            <th>Mô tả</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($list) { ?>
            <?php foreach ($list as $index => $news) { ?>
                <tr id="item-<?php echo $news->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($news->name); ?></td>
                    <td class="text-muted"><?php echo word_limiter($news->content, 120); ?></td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    //    urlDelete = "<?php //echo base_url(QUAN_TRI_TIN_TUC_DELETE); ?>//";
    //    urlValid = "<?php //echo base_url(QUAN_TRI_TIN_TUC_AJAX_VALID); ?>//";
    urlForm = "<?php echo base_url(QUAN_TRI_TRANG_TINH_FORM) ?>";

    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id;
        } else {
            window.location.href = urlForm;
        }
    }
</script>


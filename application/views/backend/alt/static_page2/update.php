<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }
</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->


        <div class="form-group col-xs-12">
            <label for="title">Tên bài viết <span class="require">(*)</span></label></br>
            <input id="title" class="form-control" type="text" name="name"
                   value="<?php if (isset($data)) echo $data->name; ?>"/>
        </div>

        <div class="form-group col-xs-12">
            <label for="link_download">Nội dung <span class="require">(*)</span></label>
            <textarea id="content" name="content"
                      placeholder="Nội dung ..."><?php if (isset($data)) echo $data->content; ?></textarea>
        </div>


        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
    <div class="box-footer">
        <a href='<?php echo base_url(QUAN_TRI_TRANG_TINH); ?>' type='button' class='btn btn-default'>
            Hủy </a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<style>
    textarea {
        min-height: 80px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $(function () {
        show_textarea('content', '900');
    });
    urlBack = "<?php echo base_url(QUAN_TRI_TRANG_TINH); ?>";
    urlUpdate = "<?php echo base_url(QUAN_TRI_TRANG_TINH_UPDATE);?>";
    var frm_id = 'form_update';
    SubmitForm(frm_id,'',urlBack);
</script>
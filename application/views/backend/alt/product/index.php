<div class="modal" id="modal_group_update"></div>
<div class="box-body">
    <div class="col-sm-6 pull-right">
        <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_100">Ảnh đại diện</th>
            <th class="width_250">Sản phẩm</th>
            <th>Thông tin</th>
            <th class="width_100">Sản phẩm nổi bật</th>
            <th class="width_100">Trạng thái</th>
            <th class="width_150">Thao tác</th>
        </tr>

        </thead>
        <tbody>
        <?php if ($list) {
            foreach ($list as $index => $val) {
                ?>
                <tr id="item-<?php echo $val->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <!--                    <td><img-->
                    <!--                            src="-->
                    <?php //if ($val && $val->img_avatar != null) echo $val->img_avatar; else echo base_url(DEFAULT_IMG) ?><!--"-->
                    <!--                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12"/></td>  -->
                    <td><img
                            src="<?php if (isset($val) && ($val->avatar != null)) echo $val->avatar; else echo base_url(DEFAULT_IMG) ?>"
                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12"/></td>
                    <td><?php if ($val && $val->name != null) echo $val->name ?></td>
                    <td>
                        <p><b>Nhà cung
                                cấp:</b> <?php if ($val && isset($val->name_publisher)) echo $val->name_publisher; ?>
                        </p>

                        <p><b>Nhóm :</b> <?php if ($val && isset($val->name_type)) echo $val->name_type; ?></p>
                    </td>
                    <td align="center">
                        <button class="item_hot btn-hot btn btn-flat"
                                title="<?php if ($val->is_hot == 1) echo 'Nôt bật'; else echo 'Bình thường'; ?>"
                                onclick="hotAjax(<?php echo $val->id ?>, <?php echo $val->is_hot ?>)">
                            <i class="fa <?php if ($val->is_hot == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                        </button>
                    </td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($val->valid == 1) echo 'Mở'; else echo 'Khóa'; ?>"
                                onclick="validAjax(<?php echo $val->id ?>, <?php echo $val->valid ?>)">
                            <i class="fa <?php if ($val->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i> <span
                                class="text-show-valid"><?php if ($val->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>

                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $val->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-flat" title="Xóa" title="Xóa"
                                onclick="deleteAjax(<?php echo $val->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>
                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<style>
    .right {
        float: right;
    }
</style>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_SAN_PHAM_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_SAN_PHAM_AJAX_VALID); ?>";
    urlHot = "<?php echo base_url(QUAN_TRI_SAN_PHAM_AJAX_HOT); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_SAN_PHAM_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
        } else {
            window.location.href = urlForm;
        }
    }
</script>

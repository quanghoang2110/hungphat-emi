<div class="" role="document" xmlns="http://www.w3.org/1999/html">
    <div class="modal-content">
        <form id="form_update" method="post" action="<?php echo base_url(QUAN_TRI_SAN_PHAM_UPDATE); ?>">
            <div class="callout hidden" id="alert_message"></div>
            <div class="modal-body" style="min-height:400px;">
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="padding-0 col-sm-12">
                                <div class="form-group col-xs-8">
                                    <label>Tên sản phẩm <span class="require">(*)</span></label>
                                    <input class="form-control" name="name" id="name"
                                           value="<?php if (isset($list) && ($list->name != null)) echo $list->name; ?>">
                                    <span class='error' id='error_name'></span>
                                </div>
                                <div class="form-group col-xs-2">
                                    <label class="status">Trạng thái: </label>
                                    <select class="form-control" name="valid">
                                        <?php if (isset($list->valid) && $list->valid == 0) { ?>
                                            <option value="0" selected>Khóa</option>
                                            <option value="1">Mở</option>
                                        <?php } else { ?>
                                            <option value="1" selected>Mở</option>
                                            <option value="0">Khóa</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <label class="status">Nổi bật: </label>
                                    <select class="form-control" name="is_hot">
                                        <?php if (isset($list->valid) && $list->valid == 0) { ?>
                                            <option value="0" selected>Bình thường</option>
                                            <option value="1">Nổi bật</option>
                                        <?php } else { ?>
                                            <option value="1" selected>Nổi bật̉</option>
                                            <option value="0">Bình thường</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label>Nhà cung cấp<span class="require">(*)</span></label>
                                    <select class="form-control" name="publisher">
                                        <option value="-1">Chọn nhà cung cấp</option>
                                        <?php foreach ($publisher as $k1 => $v1): ?>
                                            <option
                                                value="<?php echo $v1->id; ?>" <?php if (isset($list) && ($list->product_publisher == $v1->id)) echo 'selected'; ?>><?php if (isset($v1->name)) echo $v1->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Nhóm sản phẩm<span class="require">(*)</span> </label>
                                    <select class="form-control" name="type">
                                        <option value="-1">Chọn nhóm</option>
                                        <?php foreach ($type as $k1 => $v1): ?>
                                            <option
                                                value="<?php echo $v1->id; ?>" <?php if (isset($list->product_type) && ($list->product_type == $v1->id)) echo 'selected'; ?>><?php if (isset($v1->name)) echo $v1->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 hidden">
                                    <label>Xuất sứ<span class="require">(*)</span></label>
                                    <input class="form-control"
                                           value="<?php if (isset($list->from)) echo $list->from; ?>"
                                           placeholder="xuất sứ" name="from">
                                </div>
                            </div>
                            <div class="form-group col-sm-12"></div>
                            <div class="form-group">
                                <div class="col-sm-2 hidden">
                                    <label>Giá niêm yết<span class="require">(*)</span></label>
                                    <input class="form-control"
                                           value="<?php if (isset($list->price_1)) echo $list->price_1; ?>"
                                           placeholder="Giá niêm yết" name="price_1">
                                </div>
                                <div class="col-sm-2 hidden">
                                    <label>Giákhuyến mại<span class="require">(*)</span></label>
                                    <input class="form-control"
                                           value="<?php if (isset($list->price_2)) echo $list->price_2; ?>"
                                           placeholder="Giá khuyến mại" name="price_2">
                                </div>
                                <div class="col-sm-2 hidden">
                                    <label>Đơn vị tính<span class="require">(*)</span></label>
                                    <input class="form-control"
                                           value="<?php if (isset($list->pice)) echo $list->pice; ?>"
                                           placeholder="Đơn vị tính" name="pice">
                                </div>
                                <div class="col-sm-2">
                                    <label>Đánh giá<span class="require">(*)</span></label>
                                    <select class="form-control" name="star">
                                        <option value="-1">Chọn đánh giá</option>
                                        <option
                                            value="1" <?php if (isset($list->star) && ($list->star == 1)) echo 'selected'; ?>>
                                            1
                                        </option>
                                        <option
                                            value="2" <?php if (isset($list->star) && ($list->star == 2)) echo 'selected'; ?>>
                                            2
                                        </option>
                                        <option
                                            value="3" <?php if (isset($list->star) && ($list->star == 3)) echo 'selected'; ?>>
                                            3
                                        </option>
                                        <option
                                            value="4" <?php if (isset($list->star) && ($list->star == 4)) echo 'selected'; ?>>
                                            4
                                        </option>
                                        <option
                                            value="5" <?php if (isset($list->star) && ($list->star == 5)) echo 'selected'; ?>>
                                            5
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label>Số lượt xem<span class="require">(*)</span></label>
                                    <input class="form-control"
                                           value="<?php if (isset($list->total_vote)) echo $list->total_vote; ?>"
                                           name="total_vote">
                                </div>
                            </div>
                            <div class="form-group col-sm-12"></div>
                            <div class="col-sm-12">
                                <label>Ảnh sản phẩm (800x720)</label>
                            </div>
                            <?php if (isset($list_image)): ?>
                                <?php $count_image = 0; ?>
                                <?php foreach ($list_image as $key => $val): ?>
                                    <div class="col-sm-2">
                                        <a class="file-manager">
                                            <img class="rfm-image table-image image_product"
                                                 src="<?php if (($val->img != null)) echo($val->img); else echo(DEFAULT_IMG); ?>">
                                            <input type="text" id="image_link" name="image[]" class="rfm-link hidden"
                                                   value="<?php if (isset($val) && ($val->img != null)) echo($val->img); ?>">
                                        </a>
                                    </div>
                                    <?php $count_image++; ?>
                                <?php endforeach; ?>
                                <?php if ($count_image < SUM_IMG_PRODUCT): ?>
                                    <?php for ($conthieu = 0; $conthieu < (SUM_IMG_PRODUCT - $count_image); $conthieu++): ?>
                                        <div class="col-sm-2" id="show_img">
                                            <a class="file-manager">
                                                <img class="rfm-image table-image image_product"
                                                     src="<?php echo(DEFAULT_IMG) ?>">
                                                <input type="text" id="image_link" name="image[]"
                                                       class="rfm-link hidden"
                                                       value="">
                                            </a>
                                        </div>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php for ($i = 0; $i < SUM_IMG_PRODUCT; $i++): ?>
                                    <div class="col-sm-3" id="show_img">
                                        <a class="file-manager">
                                            <img class="rfm-image table-image image_product"
                                                 src="<?php echo(DEFAULT_IMG) ?>">
                                            <input type="text" id="image_link" name="image[]" class="rfm-link hidden"
                                                   value="">
                                        </a>
                                    </div>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <div id="div_image_product" class="hidden"></div>
                        </div>
                        <div class="col-sm-12 form-group padding-0">
                            <label>SEO title</label>
                            <input class="form-control" value="<?php if (isset($list)) echo $list->head_title; ?>"
                                   name="head_title">
                        </div>
                        <div class="col-sm-12 form-group padding-0">
                            <label>SEO description</label>
                            <input class="form-control" value="<?php if (isset($list)) echo $list->head_description; ?>"
                                   name="head_description">
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-12">
                    <label for="link_download">Thông số kỹ thuật</label>
                    <textarea id="content" name="content" class="form-control"
                              placeholder="Thông số kỹ thuật"><?php if (isset($list)) echo $list->content; ?></textarea>
                </div>
                <div class="form-group col-xs-12">
                    <label for="link_download">Hướng dẫn sử dụng</label>
                    <textarea id="guide" name="guide" class="form-control"
                              placeholder="Nội dung hướng dẫn"><?php if (isset($list)) echo $list->guide; ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <br/>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px">
                    <a href='<?php echo base_url(QUAN_TRI_SAN_PHAM); ?>' type='button' class='btn btn-default'>
                        Hủy </a>
                    <button type="button" class="btn btn-primary" onclick="start_submit()">Lưu</button>
                    <button type="submit" class="hidden" id="ok_submit">OK</button>
                    <?php if (isset($list) && ($list->id != null)) { ?>
                        <input type="hidden" name="hdID" id="hdID" value="<?php echo $list->id ?>">
                    <?php } ?>
                </div>

            </div>
        </form>
    </div>
</div>
<style>
    .status {
        margin-right: 30px;
    }

    .padding-0 {
        padding: 0px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        show_textarea('guide', '500');
        show_textarea('content', '500');
    });
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    urlUpdate = "<?php echo base_url(QUAN_TRI_SAN_PHAM_UPDATE);?>";
    urlBack = "<?php echo base_url(QUAN_TRI_SAN_PHAM);?>?per_size="+per_size+"&per_page="+per_page;
    var frm_id = 'form_update';
    function start_submit() {
        var is_check = 1;
        var is_check_img = 0;
        $('#div_image_product').html('');
        $('.image_product').each(function () {
            var src_image = $(this).attr('src');
            $('#div_image_product').append("<input name='image_product[]' value='" + src_image + "'>");
            if (src_image != '<?php echo DEFAULT_IMG?>')
                is_check_img = 1;
        });

        var name = $('#name').val();
        var is_check_name = 1;
        if ((name == "") || (name = "")) {
            is_check = 0;
            is_check_name = 0;
        }
        if (is_check == 1) {
            $('#ok_submit').click();
        } else {
            var err = '';
            if (is_check_name == 0)
                err += '<p>Bạn chưa nhập tên sản phẩm</p>';
            if (is_check_img == 0)
                err += '<p>Bạn chưa nhập ảnh sản phẩm</p>';
            $('#alert_message').removeClass('hidden');
            $('#alert_message').addClass('callout-danger');
            $('#alert_message').html(err);
        }

    }
        SubmitForm(frm_id,'',urlBack);

</script>
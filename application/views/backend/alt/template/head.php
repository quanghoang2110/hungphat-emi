<link rel="stylesheet" href="<?php echo $theme_assets ?>bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo $theme_assets ?>dist/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo $theme_assets ?>dist/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo $theme_assets ?>dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo $theme_assets ?>dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="<?php echo $theme_assets ?>dist/js/html5shiv.min.js"></script>
<script src="<?php echo $theme_assets ?>dist/js/respond.min.js"></script>
<![endif]-->

<!-- daterange picker -->
<!--<link rel="stylesheet" href="--><?php //echo $theme_assets ?><!--plugins/daterangepicker/daterangepicker.css">-->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo $theme_assets ?>plugins/datepicker/datepicker3.css">

<link rel="stylesheet" href="<?php echo $theme_assets ?>custom.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<script type="text/javascript">

    var size = '<?php echo $page_size ?>';
    var base_url = '<?php echo base_url(); ?>';

    function submitSizeFilter() {
        $("#in_size").val(size);
        $("#form_filter").submit();
    }
    function submitSizeFilter(p_size) {
        size = p_size;
        submitSizeFilter();
    }

    $(document).ready(function () {
        $("body").addClass("hold-transition skin-black sidebar-mini");
        //Date picker
//        $('.datepicker').datepicker({
//            autoclose: true
//        });
    });
</script>
<style>
    body {
        font-family: "Segoe UI";
    }
</style>
<header class="main-header">
    <!-- Logo -->
    <a href="/quan-tri" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>HP</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>HP</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
            </ul>
        </div>
    </nav>
</header>

<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/images/udf.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('username') ?></p>
                <a href="#change-pass" onclick="changePass()"><i class="fa fa-circle text-success"></i> Đổi mật khẩu</a>
                <a href="/<?php echo ADMIN_DANG_XUAT ?>"><i class="fa fa-circle text-danger"></i> Thoát</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                        class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <!--            --><?php //print_r($_SERVER);?>

            <!--            --><?php //foreach ($menu as $item): ?>
            <!--                <li class="treeview -->
            <?php //echo base_url($current_path_info) == base_url($item->url) ? 'active' : '' ?><!--">-->
            <!--                    <a href="--><?php //echo base_url($item->url); ?><!--">-->
            <!--                        <i class="--><?php //echo $item->icon ?><!--"></i> <span>-->
            <?php //echo ucfirst($item->name) ?><!--</span>-->
            <!--                    </a>-->
            <!--                </li>-->
            <!--            --><?php //endforeach; ?><!-- -->

            <?php //print_r($_SERVER);?>
            <?php if (isset ($_SERVER['ORIG_PATH_INFO'])) $current_path_info = $_SERVER['ORIG_PATH_INFO']; else $current_path_info = $_SERVER['PATH_INFO']; ?>

            <li class="header">SẢN PHẨM</li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_NHA_CUNG_CAP) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP); ?>">
                    <i class="fa fa-creative-commons"></i> <span>Nhà cung cấp</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_LOAI_SAN_PHAM) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_LOAI_SAN_PHAM); ?>">
                    <i class="fa fa-gg"></i> <span>Nhóm sản phẩm</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_SAN_PHAM) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_SAN_PHAM); ?>">
                    <i class="fa fa-gg"></i> <span>Sản phẩm</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_DON_HANG) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_DON_HANG); ?>">
                    <i class="fa fa-truck"></i> <span>Đơn hàng</span>
                </a>
            </li>
            <!--            <li class="treeview -->
            <?php //echo base_url($current_path_info) == base_url(QUAN_TRI_NHOM_TIN_TUC) ? 'active' : '' ?><!--">-->
            <!--                <a href="--><?php //echo base_url(QUAN_TRI_NHOM_TIN_TUC); ?><!--">-->
            <!--                    <i class="fa fa-circle-thin"></i> <span>Nhóm tin tức</span>-->
            <!--                </a>-->
            <!--            </li>-->
            <li class="header">TIN TỨC</li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_TIN_TUC) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_TIN_TUC); ?>">
                    <i class="fa fa-newspaper-o"></i> <span>Tin tức</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_TRANG_TINH) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_TRANG_TINH); ?>">
                    <i class="fa fa-info-circle"></i> <span>Trang tĩnh</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_TRANG_GIOI_THIEU) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_TRANG_GIOI_THIEU); ?>">
                    <i class="fa fa-info"></i> <span>Trang giới thiệu</span>
                </a>
            </li>

            <li class="header">KHÁCH HÀNG</li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_LIEN_HE) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_LIEN_HE); ?>">
                    <i class="fa fa-map-o"></i> <span>Liên hệ</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG); ?>">
                    <i class="fa fa-commenting-o"></i> <span>Cảm nhận khách hàng</span>
                </a>
            </li>

            <li class="header">CẤU HÌNH</li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(ADMIN_ROUTE_SITE_SETTING) ? 'active' : '' ?>">
                <a href="<?php echo base_url(ADMIN_ROUTE_SITE_SETTING); ?>">
                    <i class="fa fa-gear"></i> <span>Site seting</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_SLIDER) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_SLIDER); ?>">
                    <i class="fa fa-picture-o"></i> <span>Slide</span>
                </a>
            </li>
            <li class="treeview <?php echo base_url($current_path_info) == base_url(QUAN_TRI_BANNER_QUANG_CAO) ? 'active' : '' ?>">
                <a href="<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO); ?>">
                    <i class="fa fa-picture-o"></i> <span>Banner quảng cáo</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
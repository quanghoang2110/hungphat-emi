<!-- responsive file manager -->
<div class="modal fade" id="modalRFM" style="z-index: 9999;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Quản lý file</h4>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="500" id="rfm-iframe"
                        src="<?php echo base_url(); ?>filemanager/dialog.php?type=1&field_id=image_link" frameborder="0"
                        style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.end responsive file manager -->


<div class="modal" id="modal_requesting">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body" id="content_modal_requesting">
                <p>Đang xử lý</p>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_product_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_product_type">Cập nhật</h4>
            </div>
            <div class="modal-body">
                <input id="product_type_id" value="" class="hidden">
                <label>Danh mục</label>

                <p class="error error_name hidden"> Chưa nhập tên danh mục</p>
                <input class="form-control" id="product_type_name" value="" placeholder="Tên danh mục">

                <div class="select_div_parent margin-top-15">
                    <label>Danh mục cha</label>

                    <p class="error error_parent hidden"> Chưa chọn danh mục cha</p>
                    <select id="product_type_parent_id" class="form-control">

                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-callback" data-dismiss="modal">Hủy</button>
                <button id="submit_product_type" onclick="submitUpdateProductType()" class="btn btn-primary"> Đồng ý
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    /**
     * base scripts
     */
    var urlDelete, deleteId;
    //    var urlValid, validId;
    var urlStatus, statusId;
    var urlUpdate, updateId;
    var urlForm,urlBack;
    var urlUpdatePass;
    var base_url = '<?php echo base_url();?>';

    function deleteAjax(id) {
        deleteId = id;
        $("#modal_delete").modal("show");
//        setTimeout(function(){
//            location.reload(true);
//        }, 2000);
    }
    function validAjax(id, cValid) {
        validId = id;
        $.post(urlValid, {id: validId, valid: cValid, type: 'type'})
            .done(function (data) {
                    var nValid = cValid == 1 ? 0 : 1;
                    $('#item-' + validId).find('button.item_valid').attr('onclick', 'validAjax(' + id + ',' + nValid + ')');
                    if (nValid == 1) {
                        $('#item-' + validId).find('button.item_valid i').removeClass().addClass("fa fa-check");
                        $('#item-' + validId).find('button.item_valid').attr("title", "Mở");
                        $('#item-' + validId).find('.text-show-valid').html('Mở &nbsp &nbsp');
                        $('#item-' + validId).find('.text-show-watched-comment').html('Đã xem &nbsp &nbsp');
                    } else {
                        $('#item-' + validId).find('button.item_valid i').removeClass().addClass("fa fa-ban");
                        $('#item-' + validId).find('button.item_valid').attr("title", "Khóa");
                        $('#item-' + validId).find('.text-show-valid').html('Khóa');
                        $('#item-' + validId).find('.text-show-watched-comment').html('Chưa xem');
                    }
                }
            )
        ;
    }

    function isHomeAjax(id, cValid) {
        validId = id;
        $.post(urlValid, {id: validId, valid: cValid, type: 'is-home'})
            .done(function (data) {
                    var nValid = cValid == 1 ? 0 : 1;
                    var obj = $('#item-' + validId).find('button.item_is_home');
                    console.log(obj);
                    obj.attr('onclick', 'isHomeAjax(' + id + ',' + nValid + ')');
                    if (nValid == 1) {
                        obj.addClass('btn-success').attr('title', 'Hiển thị trên trang chủ');
                    } else {
                        obj.removeClass('btn-success').attr('title', 'Không hiển thị ở trang chủ');
                    }
                }
            )
        ;
    }

    //    //tuyen
    //    function statusAjax(id, cStatus) {
    //        statusId = id;
    //        $.post(urlStatus, {id: statusId, status: cStatus})
    //            .done(function (data) {
    //                var nStatus = cStatus == 1 ? 0 : 1;
    //                $('#item-' + statusId).find('button.item_status').attr('onclick', 'statusAjax(' + id + ',' + nStatus + ')');
    //                if (nStatus == 1)
    //                    $('#item-' + statusId).find('button.item_status i').removeClass().addClass("fa fa-check");
    //                else
    //                    $('#item-' + statusId).find('button.item_status i').removeClass().addClass("fa fa-ban");
    //            }
    //        )
    //        ;
    //    }
    //tuyen
    function hotAjax(id, cHot) {
        hotId = id;
        $.post(urlHot, {id: hotId, hot: cHot})
            .done(function (data) {
                    var nHot = cHot == 1 ? 0 : 1;
                    $('#item-' + hotId).find('button.item_hot').attr('onclick', 'hotAjax(' + id + ',' + nHot + ')');
                    if (nHot == 1)
                        $('#item-' + hotId).find('button.item_hot i').removeClass().addClass("fa fa-check");
                    else
                        $('#item-' + hotId).find('button.item_hot i').removeClass().addClass("fa fa-ban");
                }
            )
        ;
    }
</script>

<div class="modal" id="modal_msg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Nội dung thông báo</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-callback" data-dismiss="modal">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="modal_delete_product_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Bạn có muốn xóa không</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-callback" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary btn-callback" data-dismiss="modal"
                        onclick="okDeleteProductType();">Đồng ý
                </button>
                <input class="hidden delete_product_type_id" value="">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="modal_contact">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Yêu cầu</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12" style="margin: auto">
                    <p>Họ tên : <span id="fullname-contact"></span></p>
                    <p>Ngày gửi : <span id="created-at-contact"></span></p>
                    <p>Email : <span id="email-contact"></span></p>
                    <p>Số điện thoại : <span id="phone-contact"></span></p>
                    <p>Địa chỉ : <span id="address-contact"></span></p>
                    <label>Yêu cầu</label>
                    <div class="col-xs-12" id="content-contact">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--xem bài thu âm-->
<div class="modal" id="modal_recording_audio">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Bài thu âm</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12" style="margin: auto">
                    <div id="audio" class="col-xs-12"></div>
                    <div id="result_recording_audio" class="col-xs-12"></div>
                    <div id="comment" class="col-xs-12"></div>
                </div>
            </div>
            <div class="modal-footer margin-top-15">
                <input type="hidden" id="recording_audio_id" value="">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button class="btn btn-primary" id="submit_comment_recording_audio">Lưu</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function showThongBao(text, callback) {
        $("#modal_msg .modal-body p").html(text);
        $("#modal_msg").modal("show");
        if (typeof (callback) !== 'undefined') {
            $("#modal_msg .btn-callback").click(function () {
                callback();
            });
        }
    }
    function showThongBaoReLoad(text, callback) {
        $("#modal_msg .modal-body p").html(text);
        $("#modal_msg").modal("show").on('hidden.bs.modal', function () {
            callback();
        });

        if (typeof (callback) !== 'undefined') {
            $("#modal_msg .btn-callback").click(function () {
                callback();
            });
        }
        setTimeout(function () {
            $("#modal_msg").modal("hide");
        }, 2500);
    }
</script>
<div class="modal" id="modal_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Xóa thông tin</h4>
            </div>
            <div class="modal-body">
                <p>Bạn có muốn xóa không?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="runAjaxDelete()">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function runAjaxDelete() {
        $.post(urlDelete, {id: deleteId})
            .done(function (data) {
                $('#modal_delete').modal("hide");
                showThongBao("Xóa thành công");
                $('#item-' + deleteId).remove();
                //location.reload(true);
                setTimeout(function () {
                    $('#modal_msg').modal("hide");
                    location.reload(true);
                }, 1500);

            });
    }

    //    function SubmitForm(frm_id='form_update', data_append) {
    //        var frm = $('#' + frm_id);
    //        frm.submit(function (ev) {
    //            $(this).find('.has-error').removeClass('has-error');
    //            var frm_data = new FormData(this);
    //            if (typeof (data_append) !== 'undefined') {
    //                $.each(data_append, function (key, value) {
    //                    frm_data.append(key, value);
    //                })
    //            }
    //            $('#modal_requesting').modal('show');
    //            $.ajax({
    //                type: 'POST',
    //                url: urlUpdate,
    //                data: frm_data,
    //                processData: false,
    //                contentType: false,
    //                success: function (data) {
    //                    setTimeout(function () {
    //                        $('#modal_requesting').modal('hide');
    //                        var result = JSON.parse(data);
    //                        if (result.b_Check != null) {
    //                            if (!result.b_Check || result.b_Check == false) {
    //                                if (result.msg_error != null) {
    //                                    var top = "";
    //                                    var index = 0;
    //                                    var msg = '';
    //                                    for (var i in result.msg_error) {
    //                                        msg = msg + '<p>' + result.msg_error[i] + '</p>';
    //                                        index++;
    //                                        if (index == 1) {
    //                                            top = i;
    //                                        }
    //                                        $('#' + i).closest('.form-group').addClass('has-error');
    //                                    }
    //                                    if (msg != '') {
    //                                        $("#" + top).focus();
    //                                        $(window).scrollTop(0);
    //                                        ShowAlertMessage('alert_message', 'callout-danger', msg);
    //                                        setTimeout(function () {
    //                                            $('#alert_message').css('display', 'none');
    //                                        }, 3000);
    //                                        return false;
    //                                    } else {
    //                                        $("#" + top).focus();
    //                                        $(window).scrollTop(0);
    //                                        var message = "Thêm thành công";
    //                                        if ($('#hdID').val() != '') {
    //                                            message = "Cập nhật thành công";
    //                                        }
    //                                        $('#modal_requesting').modal('show');
    //                                        $('#content_modal_requesting').html('<h4>' + message + '</h4>');
    //                                        setTimeout(function () {
    //                                            if (!result.urlreload || result.urlreload == '' || result.urlreload == null) {
    //                                                location.reload();
    //                                            } else {
    //                                                var urlreload = base_url + result.urlreload;
    //                                                urlreload = urlreload.replace("@", "/");
    //                                                window.location.href = urlreload;
    //                                            }
    //                                        }, 1500);
    //                                    }
    //                                }
    //                                else {
    //
    //                                }
    //                            }
    //                            else {
    //                                if (result.msg_error != '') {
    //                                    $("#" + top).focus();
    //                                    $(window).scrollTop(0);
    //                                    ShowAlertMessage('alert_message', 'callout-danger', result.msg);
    //                                    setTimeout(function () {
    //                                        $('#alert_message').css('display', 'none');
    //                                    }, 3000);
    //                                    return false;
    //                                } else {
    //                                }
    //                            }
    //                        }
    //                        else {
    //                            if (result.err == 0) {
    //                                $("#" + top).focus();
    //                                $(window).scrollTop(0);
    //                                var message = "Thêm thành công";
    //                                if ($('#hdID').val() != '') {
    //                                    message = "Cập nhật thành công";
    //                                }
    //                                $('#modal_requesting').modal('show');
    //                                $('#content_modal_requesting').html('<h4>' + message + '</h4>');
    //                                setTimeout(function () {
    //                                    if (!result.urlreload || result.urlreload == '' || result.urlreload == null) {
    //                                        location.reload();
    //                                    } else {
    //                                        var urlreload = base_url + result.urlreload;
    //                                        urlreload = urlreload.replace("@", "/");
    //                                        window.location.href = urlreload;
    //                                    }
    //                                }, 1500);
    //                            } else {
    //                                $("#" + top).focus();
    //                                $(window).scrollTop(0);
    //                                ShowAlertMessage('alert_message', 'callout-danger', result.msg);
    //                                setTimeout(function () {
    //                                    $('#alert_message').css('display', 'none');
    //                                }, 3000);
    //                            }
    //                        }
    //                    }, 500);
    //
    //                }
    //            });
    //            ev.preventDefault();
    //        });
    //    }

</script>
<div class="modal" id="modal_reset_pass">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Bạn có muốn reset mật khẩu cho tài khoản này?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnResetPass">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    function resetPass(id) {
        if (typeof (urlUpdatePass) !== 'undefined') {
            $.post(urlUpdatePass, {hdUpdatePassID: id})
                .done(function (data) {
                    $('#modal_reset_pass').modal("hide");
                    showThongBao("Reset mật khẩu thành công");
                });
        }
    }
</script>

<!-- change pass -->
<!-- BEGIN FORM MODAL MARKUP -->
<div class="modal fade" id="modal_change_pass" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Thay đổi mật khẩu</h4>
            </div>
            <form onsubmit="return submitchangePass()" class="form-horizontal form-validate floating-label" role="form"
                  novalidate="novalidate" method="post" id="form_change_pass">
                <div class="modal-body">
                    <div class="alert alert-danger" id="err_msg_" role="alert">
                        <!--                        <strong>Oh snap!</strong>-->
                        <div id="_msg"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="cur_pass" class="control-label">Mật khẩu hiện tại</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="cur_pass" id="cur_pass" class="form-control"
                                   placeholder="Mật khẩu hiện tại" required aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="new_pass" class="control-label">Mật khẩu mới</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="new_pass" id="new_pass" class="form-control"
                                   placeholder="Mật khẩu mới" required data-rule-minlength="6">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="re_new_pass" class="control-label">Mật khẩu mới</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="re_new_pass" id="re_new_pass" class="form-control"
                                   placeholder="Nhập lại mật khẩu mới" required data-rule-equalto="#new_pass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->
<script>
    function changePass() {
        document.getElementById("form_change_pass").reset();
        $('#form_change_pass #err_msg_').addClass('hide');
        $("#modal_change_pass").modal("show");
    }
    function submitchangePass() {
        $('#form_change_pass #err_msg_').addClass('hide');
        var status = 0;
        var msg = "";
        if ($('#cur_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu hiện tại!</p>";
        } else if ($('#new_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu mới.</p><p>Mật khẩu mới cần lớn hơn hoặc bằng 6 ký tự.</p>";
        } else if ($('#new_pass').val() != $('#re_new_pass').val()) {
            msg = "<p>Vui lòng nhập lại mật khẩu mới trùng với mật khẩu đã nhập.</p>";
        } else if ($('#new_pass').val().length < 6) {
            msg = "<p>Vui lòng nhập mật khẩu mới lớn hơn hoặc bằng 6 ký tự</p>";
        } else if ($('#cur_pass').val() == $('#new_pass').val()) {
            msg = "<p>Mật khẩu không thay đổi!</p>";
        } else {
            status = 1;
        }

        if (status == 0) {
            $('#form_change_pass #err_msg_').removeClass('hide');
            $("#_msg").html(msg);
            return false;
        }

        $('#form_change_pass #err_msg_').addClass('hide');
        $.post('/<?php echo ADMIN_DOI_MATKHAU ?>', {
            cur_pass: $('#cur_pass').val(),
            new_pass: $('#new_pass').val()
        }).done(function (data) {
            var obj = $.parseJSON(data);
            if (obj.status == 0) {
                $('#modal_change_pass #err_msg_').removeClass('hide');
                $('#modal_change_pass #err_msg_ #_msg').text(obj.msg);

            } else {
                $(".modal").modal("hide");
                if (obj.status == -1) {
                    showThongBao(obj.msg, 'reload');
                } else
                    showThongBao(obj.msg);
            }
        });
        return false;
    }
</script>
<style>
    .error {
        color: red;
    }

    .margin-top-15 {
        margin-top: 15px;
    }

    .div_parent .name {
        color: #367FA9;
        font-size: 16px;
        font-weight: 700;

    }

    .div_child {
        font-size: 14px;
        font-weight: 500;
    }

    .name {
        border-bottom: 1px solid #DCEBF9;
    }
</style>
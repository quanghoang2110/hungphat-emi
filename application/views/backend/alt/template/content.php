<!-- Site wrapper -->
<div class="wrapper">

    <?php include 'header.php' ?>
    <?php include 'script.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $module_title ?>
                <small><?php echo $module_desc ?></small>
            </h1>
        </section>

        <section class="content">
            <?php  if(isset($search_enable) && $search_enable){ ?>
                <form id="form_filter">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_length"><label> Hiển thị &nbsp;</label><label> <select name="per_size" aria-controls="example" class="form-control input-sm" onchange="$('#form_filter').submit()">
                                        <option value="10" <?php echo  $page_size == 10 ? 'selected="selected"' : '' ?>>10
                                        </option>
                                        <option value="30" <?php echo  $page_size == 30 ? 'selected="selected"' : '' ?>>30
                                        </option>
                                        <option value="50" <?php echo  $page_size == 50 ? 'selected="selected"' : '' ?>>50
                                        </option>
                                        <option value="100" <?php echo  $page_size == 100 ? 'selected="selected"' : '' ?>>100
                                        </option>
                                    </select></label><label> &nbsp; kết quả </label></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="example1_filter" class="dataTables_filter pull-right"><label><input type="search" name="search" class="form-control input-sm" placeholder="Tìm kiếm" value="<?php echo $keyword ?>" aria-controls="example1"></label>
                                <button style="margin-top: -1px" class="btn btn-sm" onclick="submitSizeFilter()"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } ?>
            <!-- Default box -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <?php $this->load->view($content) ?>
                        <?php if (isset($paging) && $paging) : ?>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="dataTables_info" role="status"
                                             aria-live="polite"><?php echo $paging_info ?></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                            <?php echo $paging ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!-- /.box -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.1.0
        </div>
        <strong>Copyright &copy; 2016 <?php if (date('Y') > 2016) echo ' - ' . date('Y'); ?> &nbsp;<a
                href="http://gosol.com.vn">Go Solutions JSC</a>.</strong> All rights
        reserved.
    </footer>
</div><!-- ./wrapper -->

<?php include_once ('tmp_modal.php');?>

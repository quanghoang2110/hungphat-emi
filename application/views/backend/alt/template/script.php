<script type="text/javascript">
    urlDelete = "<?php echo base_url($permission_url . '/' . ROUTE_DELETE); ?>";
    urlValid = "<?php echo base_url($permission_url . '/' . ROUTE_VALID); ?>";
    urlForm = "<?php echo base_url($permission_url . '/' . ROUTE_UPDATE) ?>";

    function updateAjax(id) {
        if (id) {
            window.location.href = urlForm + '?id=' + id + "&page=<?php echo $page?>";
        } else {
            window.location.href = urlForm + "?page=<?php echo $page ?>";
        }
    }

    function SubmitForm(frm_id, data_append,urlBack) {
        var frm = $('#' + frm_id);
        frm.submit(function (ev) {
            $(this).find('.has-error').removeClass('has-error');
            var frm_data = new FormData(this);
            if (typeof (data_append) !== 'undefined' || data_append) {
                $.each(data_append, function (key, value) {
                    frm_data.append(key, value);
                })
            }
            $.ajax({
                type: 'POST',
                url: urlUpdate,
                data: frm_data,
                processData: false,
                contentType: false,
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.b_Check != null) {
                        if (!result.b_Check) {
                            if (result.msg_error != null) {
                                var top = "";
                                var index = 0;
                                var msg = '';
                                for (var i in result.msg_error) {
                                    msg = msg + '<p>' + result.msg_error[i] + '</p>';
                                    index++;
                                    if (index == 1) {
                                        top = i;
                                    }
                                    $('#' + i).closest('.form-group').addClass('has-error');
                                }
                                ShowAlertMessage('alert_message', 'callout-danger', msg);
                                $("#" + top).focus();
                                return false;
                            }
                        }
                    } else {
                        if (result.err == 0) {
                            var message = "Thêm thành công";
                            if ($('#hdID').val() != '') {
                                message = "Cập nhật thành công";
                            }

                            showThongBaoReLoad(message);
                            if (typeof (urlBack) !== 'undefined'){
                                setTimeout(function(){
                                    window.location.href=urlBack;
                                },1000);
                            }else{
                                location.reload();
                            }


                        } else {
                            ShowAlertMessage('alert_message', 'callout-danger', 'Lỗi');
                        }
                    }
                }
            });
            ev.preventDefault();
        });
    }

    function backUrl() {
//        window.location.href = '<?php //echo base_url($permission_url . '?page=' . $this->input->get('page'))?>//';

    }

</script>
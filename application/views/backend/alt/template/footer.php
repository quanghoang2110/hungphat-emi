<!-- Bootstrap 3.3.5 -->
<script src="<?php echo $theme_assets ?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo $theme_assets ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $theme_assets ?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $theme_assets ?>dist/js/app.min.js"></script>

<!-- test online js:Tuyen Nguyen-->
<script src="<?php echo $theme_assets?>contentadmin.js"></script>
<!-- tiny MCE -->
<script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>

<!--<script src="--><?php //echo $theme_assets ?><!--plugins/daterangepicker/daterangepicker.js"></script>-->
<!-- bootstrap datepicker -->
<script src="<?php echo $theme_assets ?>plugins/datepicker/bootstrap-datepicker.js"></script>

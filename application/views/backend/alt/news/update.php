<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }

</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->

        <div class="row">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="form-group col-sm-3 col-xs-3">
                        <label for="group_name">Ảnh <span class="require">(*)</span></label>
                        <div id="show_img">
                            <a class="file-manager">
                                <!--                                --><?php //echo get_img_tag((isset($data->img) && $data->img != '') ? $data->img : '', '', 'rfm-image table-image', base_url('images/hungphat/default/image.png')); ?>
                                <img class="rfm-image table-image"
                                     src="<?php if (isset($data) && ($data->img != null)) echo($data->img); else echo(DEFAULT_IMG); ?>">
                                <input type="text" id="img" name="img" class="rfm-link hidden"
                                       value="<?php if (isset($data->img)) echo $data->img; ?>">
                            </a>
                            <p class="help-block text-center">640x320</p>
                        </div>
                        <span id="error_image" class="error"></span>
                    </div>
                    <div class="form-group col-xs-9">
                        <label for="title">Tên bài viết <span class="require">(*)</span></label></br>
                        <input id="title" class="form-control" type="text" name="name"
                               value="<?php if (isset($data)) echo $data->name; ?>"/>
                    </div>
                    <div class="col-sm-3">
                        <label>Thuộc nhóm tin tức</label>
                        <select class="form-control" id="news_type" name="news_type">
                            <option value="-1">Chọn nhóm tin tức</option>
                            <?php foreach ($news_type as $key => $val): ?>
                                <option
                                    <?php if (isset($data) && $val->id == $data->news_type) echo 'selected'; ?>
                                    value="<?php echo $val->id; ?>" ><?php echo $val->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="valid">Trang thái:</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                value="1">Mở
                            </option>
                            <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                value="0">Khóa
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="views">Lượt xem</label></br>
                        <input id="views" class="form-control" disabled type="text" name="views"
                               value="<?php if (isset($data)) echo $data->views; ?>"/>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 form-group padding-0">
                        <label>SEO title</label>
                        <input class="form-control" value="<?php if (isset($data)) echo $data->head_title; ?>"
                               name="head_title">
                        <label style="margin-top: 10px">SEO description</label>
                        <input class="form-control" value="<?php if (isset($data)) echo $data->head_description; ?>"
                               name="head_description">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Giới thiệu</label>
                <textarea id="desc" name="desc" class="form-control" maxlength="450"
                          placeholder="Mô tả nội dung...(max 450 ký tự)"><?php if (isset($data)) echo $data->desc; ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Nội dung tin tức <span class="require">(*)</span></label>
                <textarea id="content" name="content"
                          placeholder="Nội dung ..."><?php if (isset($data)) echo $data->content; ?></textarea>
            </div>
        </div>


        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
    <div class="box-footer">
        <!--        <button type="button" class="btn btn-default"-->
        <!--                onclick="backUrl()">-->
        <!--            Hủy-->
        <!--        </button>-->
        <a href='<?php echo base_url(QUAN_TRI_TIN_TUC); ?>' type='button' class='btn btn-default'>
            Hủy </a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<style>
    textarea {
        min-height: 80px;
    }
</style>
<?php $this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $(function () {
        show_textarea('content', '900');
    });
    urlUpdate = "<?php echo base_url(QUAN_TRI_TIN_TUC_UPDATE);?>";
    var frm_id = 'form_update';
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    urlBack = "<?php echo base_url(QUAN_TRI_TIN_TUC); ?>?per_size="+per_size+"&per_page="+per_page;
    SubmitForm(frm_id,'',urlBack);
</script>
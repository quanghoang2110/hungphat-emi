<style>
    .pl-0{
        padding-left: 0;
    }
    .pb-1{
        padding-bottom:1em;
    }
    .pr-0{padding-right: 0}
</style>
<div class="box-body">
    <div class="col-sm-3 pull-left pl-0 pb-1">
        <select class="form-control" id="news_type" name="news_type">
            <option value="">Chọn nhóm tin tức</option>
            <?php foreach ($news_type as $key => $val): ?>
                <option
                    value="<?php echo $val->id; ?>" <?php if (($this->input->get('news_type')) && ($val->id == $this->input->get('news_type'))) echo 'selected'; ?>><?php echo $val->name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
        <div class="col-sm-6 pull-right pr-0 pb-1 ">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_120">Hình ảnh</th>
            <th class="width_300">Tiêu đề</th>
            <th>Mô tả</th>
            <th class="width_80">Lượt xem</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($news_list) { ?>
            <?php foreach ($news_list as $index => $news) { ?>
                <tr id="item-<?php echo $news->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td align="center"
                        class="table-image">
                        <img
                            src="<?php if (isset($news) && ($news->img != null)) echo $news->img; else echo base_url(DEFAULT_IMG) ?>"
                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12"/>
                    </td>
                    <td><?php echo($news->name); ?></td>
                    <td class="text-muted"><?php echo word_limiter($news->desc, 70); ?></td>
                    <td><?php echo($news->views); ?></td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($news->valid == 1) echo 'Mở bài viết'; else echo 'Khóa bài viết'; ?>"
                                onclick="validAjax(<?php echo $news->id ?>, <?php echo $news->valid ?>)">
                            <i class="fa <?php if ($news->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            <span
                                class="text-show-valid"><?php if ($news->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>
                        <td align="center">
                            <button class="btn-repair-question btn btn-flat" title="Sửa"
                                    onclick="updateAjax(<?php echo $news->id ?>)">
                                <i class="fa fa-edit"></i>
                            </button>

                                <button class="btn btn-flat" title="Xóa" title="Xóa"
                                        onclick="deleteAjax(<?php echo $news->id ?>)">
                                    <i class="fa fa-close"></i>
                                </button>

                        </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_TIN_TUC_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_TIN_TUC_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_TIN_TUC_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
        } else {
            window.location.href = urlForm;
        }
    }
    $('#news_type').on('change',function(){
        $('#form_filter').append("<input type='hidden' value='"+$(this).val()+"' name='news_type'>");
        form_filter.submit();
    })
</script>


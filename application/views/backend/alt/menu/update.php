<style>
    .hint {
        width: 50%;
        border-bottom: 1px solid #eee;
        margin: 20px 0;
        padding-bottom: 5px;
        font-style: italic;
    }
</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->
        <div>
            <div class="">
                <div class="row">
                    <div class="form-group col-xs-4">
                        <label for="name">Tên menu</label>
                        <input type="text" class="form-control" id="name" name="name"
                               value="<?php if ($data) echo $data->name ?>"/>
                        <span id="error_name" class="error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-4">
                        <label for="name">Thuộc menu</label>
                        <select name="parent_menu" class="form-control">
                            <?php foreach ($parents as $p_item): ?>
                                <option
                                    value="<?php echo $p_item->id ?>" <?php echo $data && $p_item->id == $data->parent_id ? "selected='selected'" : "" ?>><?php echo $p_item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="valid">Khả dụng</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if ($data && $data->valid == 1) echo 'selected'; ?>
                                value="<?php echo 1; ?>">Hoạt động
                            </option>
                            <option <?php if ($data && $data->valid == 0) echo 'selected'; ?>
                                value="<?php echo 0; ?>">Không hoạt động
                            </option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-8">
                        <label for="name">Đường dẫn</label>
                        <input type="text" class="form-control" id="menu_url" name="menu_url" placeholder="http://..."
                               value="<?php if ($data) echo $data->url ?>"/>
                    </div>
                </div>
                <fieldset class="col-xs-8">
                    <legend>Chọn đường dẫn</legend>
                    <div class="row">
                        <div class="form-group col-xs-4">
                            <label for="valid">Danh mục</label>
                            <select class="form-control" id="chon-danh-muc"
                                    onchange="javascript:changeDanhMuc($(this))">
                                <option value="0">Chọn danh mục</option>
                                <option value="trang-tinh">Nội dung tĩnh</option>
<!--                                <option value="khoa-hoc">Khóa học</option>-->
                                <option value="thu-vien">Thư viện</option>
                                <option value="khac">Khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-hidden">
                        <div class="form-group col-xs-4">
                            <label for="valid">Link tới</label>
                            <div class="trangtinh div-hidden">
                                <select class="form-control" onchange="changeUrlKhac($(this))">
                                    <?php foreach ($trangtinh as $s_page): ?>
                                        <option
                                            value="<?php echo base_url(get_detail_url('', $s_page->title, $s_page->id)) ?>"><?php echo $s_page->title ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
<!--                            <div class="khoahoc div-hidden">-->
<!--                                <select class="form-control" onchange="changeUrlKhac($(this))">-->
<!--                                    --><?php //foreach ($khoahoc as $pack): ?>
<!--                                        <option-->
<!--                                            value="--><?php //echo base_url(get_detail_url('', $pack->pack_name, $pack->id)) ?><!--">--><?php //echo $pack->pack_name ?><!--</option>-->
<!--                                    --><?php //endforeach; ?>
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div class="khac div-hidden">-->
<!--                                <select class="form-control" onchange="changeUrlKhac($(this))">-->
<!--                                    <option value="--><?php //echo base_url(ROUTE_FRONTEND_LIEN_HE) ?><!--">Liên hệ</option>-->
<!--                                    </option>-->
<!--                                </select>-->
<!--                            </div>-->
                            <div class="thuvien div-hidden">
                                <select class="form-control" onchange="changeUrlKhac($(this))">
                                    <?php foreach ($thuvien as $tv): ?>
                                        <option value="<?php echo base_url(get_detail_url(ROUTE_GALLERY, $tv->name, $tv->id, "none")) ?>">
                                            <?php echo $tv->name ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row form-hidden">
                        <div class="form-group col-xs-4 text-right">
                            <button type="button" onclick="javascript:resetUrl()" class="btn btn-primary">Reset
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <input type="hidden" id="hdID" name="hdID" value="<?php if ($data) echo $data->id; ?>"/>
        </div>
    </div>
    <div class="box-footer">
        <a type="button" href="#cancel" onclick="backUrl()" class="btn btn-default">Quay lại</a>
        <button type="submit" class="btn btn-success">Lưu</button>
    </div>
</form>
<style>
    .form-hidden, .div-hidden, .button_reset {
        display: none;
    }

    fieldset {
        padding: 15px;
        border: 1px solid #ccc;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        background: #f5f6f7;
    }

    legend {
        margin: 0;
        width: auto;
        border: 0;
        font-size: inherit;
    }

    .input-focus {
        background-color: rgba(255, 177, 0, 0.4)
    }
</style>
<?php $this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">

    SubmitForm('form_update');

    function changeDanhMuc(menu) {
        var val = menu.val();
        console.log(val);
        $(".form-hidden").fadeIn();
        $(".form-hidden .div-hidden").hide();
        switch (val) {
            case 'trang-tinh':
                $(".trangtinh").fadeIn();
                $(".trangtinh select").change();
                break;
            case 'khoa-hoc':
                $(".khoahoc").fadeIn();
                $(".khoahoc select").change();
                break;
            case 'thu-vien':
                $(".thuvien").fadeIn();
                $(".thuvien select").change();
                break;
            case 'khac':
                $(".khac").fadeIn();
                $(".khac select").change();
                break;
            default:
                break;
        }
        $("#menu_url").addClass("input-focus");
    }

    function changeUrlKhac(urlKhac) {
        $("#menu_url").val(urlKhac.val());
    }

    var urlReset = '<?php echo $data ? $data->url : ''?>';
    function resetUrl() {
        $("#chon-danh-muc").val("0");
        $(".form-hidden").fadeOut();
        $("#menu_url").removeClass("input-focus").val(urlReset);
    }
</script>
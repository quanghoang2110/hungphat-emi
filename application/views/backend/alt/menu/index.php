<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50 w50" colspan="2">STT</th>
            <th>Tên</th>
            <th class="width_80">Icon</th>
            <th class="width_80">Khả dụng</th>
            <?php if ($permission_edit) { ?>
                <th class="width_100">Thao tác</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php if ($menu_list) {
            foreach ($menu_list as $index => $menu) { ?>
                <tr id="item-<?php echo $menu->id; ?>">
                    <td class="w50" colspan="2" valign="middle"
                        align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($menu->name); ?></td>
                    <td align="center">
                        <?php echo get_img_tag($menu->icon, '', 'img-thumbnail', base_url('images/default/image.png')); ?>
                    </td>
                    <td >
                        <button class="item_valid btn-valid btn btn-flat"
                                onclick="validAjax(<?php echo $menu->id ?>, <?php echo $menu->valid ?>, <?php echo $menu->is_home ?>)">
                            <i class="fa <?php if ($menu->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                        </button>
                    </td>
                    <td class="nodata">
                        <button
                            class="item_is_home btn-repair-question btn btn-flat <?php echo $menu->is_home ? 'btn-success' : '' ?>"
                            onclick="isHomeAjax(<?php echo $menu->id ?>, <?php echo $menu->is_home ?>)">
                            <i class="fa fa-home"></i>
                        </button>
                    </td>
                </tr>
                <?php foreach ($menu->sub as $sindex => $item): ?>
                    <tr id="item-<?php echo $item->id; ?>">
                        <?php if ($sindex == 0): ?>
                            <td class="w50 nodata" rowspan="<?php echo count($menu->sub) ?>"></td>
                        <?php endif; ?>
                        <td class="w50" align="center"><?php echo(($page - 1) * $page_size + $sindex + 1); ?></td>
                        <td><?php echo($item->name); ?></td>
                        <td></td>
                        <td align="center">
                            <button class="item_valid btn-valid btn btn-flat"
                                    onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->valid ?>)">
                                <i class="fa <?php if ($item->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            </button>
                        </td>
                        <?php if ($permission_edit) { ?>
                            <td align="center">
                                <button class="btn-repair-question btn btn-flat"
                                        onclick="updateAjax(<?php echo $item->id ?>)">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <?php if ($permission_delete) { ?>
                                    <button class="btn btn-flat" title="Xóa"
                                            onclick="deleteAjax(<?php echo $item->id ?>)">
                                        <i class="fa fa-close"></i>
                                    </button>
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<?php $this->load->view($theme_template . '/script'); ?>
<style>
    .w50 {
        width: 40px;
        max-width: 40px;
    }

    .nodata {
        background-color: #eee;
    }
    img.table-image{
        max-width: 50px;
        width: 50px;
    }
    .img-thumbnail{
        background: #c0c0c0;
    }
</style>

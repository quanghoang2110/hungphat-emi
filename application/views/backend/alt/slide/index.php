<div class="modal" id="modal_menu_update"></div>
<div class="box-body">
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th>Tên</th>
            <th class="width_120">Ảnh</th>
            <th class="width_250">Link</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_120">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($menu_list) {
            foreach ($menu_list as $index => $menu) { ?>
                <tr id="item-<?php echo $menu->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($menu->name); ?></td>
                    <td align="center" class="table-image">
                        <img src="<?php echo $menu->img ?>" height="100" class="table-image"/>
                    </td>
                    <td>
                        <a href="<?php echo $menu->link ?>" target="_blank"><?php echo $menu->link ?></a>
                    </td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($menu->valid == 1) echo 'Mở Slide'; else echo 'Khóa Slide'; ?>"
                                onclick="validAjax(<?php echo $menu->id ?>, <?php echo $menu->valid ?>)">
                            <i class="fa <?php if ($menu->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            <span class="text-show-valid"><?php if ($menu->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>


                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa" onclick="updateAjax(<?php echo $menu->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>

                            <button class="btn btn-flat" title="Xóa" title="Xóa"onclick="deleteAjax(<?php echo $menu->id ?>)">
                                <i class="fa fa-close"></i>
                            </button>

                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_SLIDER_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_SLIDER_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_SLIDER_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
        } else {
            window.location.href = urlForm;
        }
    }
</script>
<style>
    img {
        max-width: 70px;
    }

    td.table-image {
        width: 200px;
    }

    img.table-image {
        width: 100%;
        max-width: 100%;
        height: auto;
    }
</style>

<div class="" role="document">
    <div class="modal-content">
        <form id="form_update">
            <div class="callout" style="display: none;" id="alert_message"></div>
            <div class="box-body" style="min-height:400px;">
                <div class="form-group col-xs-6">
                    <label>Tên slide </label>
                    <input class="form-control" name="name" id="name"
                           value="<?php if (isset($data) && ($data->name != null)) echo $data->name; ?>">
                    <span class='error' id='error_name'></span>
                </div>
                <div class="form-group col-xs-3">
                    <label for="valid">Trạng thái</label>
                    <select class="form-control" id="valid" name="valid">
                        <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                            value="<?php echo 1; ?>">Mở
                        </option>
                        <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                            value="<?php echo 0; ?>">Khóa
                        </option>
                    </select>
                </div>
                <div class="form-group col-xs-3">
                    <label for="valid">Vị trí</label>
                    <input class="form-control" name="position"
                           value="<?php if (isset($data->position)) echo $data->position; else echo '99'; ?>">
                </div>
                <div class="form-group" style="width: 100%;">
                    <label>Ảnh slide (1920x700) <span class="require">(*)</span></label>
                    <div id="show_img" style="width: 549px;height: 200px;">
                        <a class="file-manager">
                            <img class="rfm-image"
                                 src="<?php echo isset($data) ? base_url($data->img) : base_url('images/hungphat/default/image.png') ?>">
                            <input type="text" id="image" name="image" class="rfm-link hidden"
                                   value="<?php echo isset($data) ? $data->img : '' ?>"/>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label>Link</label>
                    <input name="link" value="<?php echo isset($data) ? $data->link : '' ?>" class="form-control">
                </div>

            </div>
            <div class="box-footer">
                <br/>
                <a href='/<?php echo QUAN_TRI_SLIDER; ?>' type='button' class='btn btn-default'> Hủy </a>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                <?php if (isset($data) && ($data->id != null)) { ?>
                    <input type="hidden" name="hdID" id="hdID" value="<?php echo $data->id ?>">
                <?php } ?>
            </div>
        </form>
    </div>
</div>
<style>
    .status {
        margin-right: 30px;
    }

    .padding-0 {
        padding: 0px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    urlUpdate = "<?php echo base_url(QUAN_TRI_SLIDER_UPDATE);?>";
    urlBack = "<?php echo base_url(QUAN_TRI_SLIDER);?>?per_size="+per_size+"&per_page="+per_page;
    var frm_id = 'form_update';
    SubmitForm(frm_id,'',urlBack);
    $(document).ready(function () {
        show_textarea('content', '900');
    });
</script>
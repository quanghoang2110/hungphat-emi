<div class="box-body">
    <form id="form_update" method="post">
        <?php $count = 0; ?>
        <?php foreach ($introduce as $key => $list): ?>
            <?php $count = 1; ?>
            <?php $value = ($list->value) ? $list->value : ''; ?>
            <?php $mission = ($list->mission) ? $list->mission : ''; ?>
            <?php $prize = ($list->prize) ? $list->prize : ''; ?>
        <?php endforeach; ?>
        <div class="callout" style="display: none;" id="alert_message"></div>
        <div class="box-body">
            <div class="form-group">
                <label>Giới thiệu</label>
                <textarea type="text" name="value" id="value" class="form-control" maxlength="165"
                          placeholder=""><?php if (isset ($value)) echo $value; else echo ''; ?></textarea>
            </div>
<!--            <div class="form-group">-->
<!--                <label>Sứ mệnh</label>-->
<!--                <textarea type="text" name="mission" id="mission" class="form-control" maxlength="165"-->
<!--                          placeholder="">--><?php //if (isset ($mission)) echo $mission; else echo ''; ?><!--</textarea>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label>Giải thưởng</label>-->
<!--                <textarea type="text" name="prize" id="prize" class="form-control" maxlength="165"-->
<!--                          placeholder="">--><?php //if (isset ($prize)) echo $prize; else echo ''; ?><!--</textarea>-->
<!--            </div>-->
            <div class="box-footer">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary  pull-right">Lưu</button>
                    <a href='<?php echo base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG); ?>' type='button'
                       class='btn btn-default pull-right margin-right-10'> Hủy </a>
                    <?php if ($count == 1) { ?>
                        <input type="hidden" name="hdID" id="hdID" value="<?php if ($count == 1) echo $count; ?>">
                    <?php } ?>
                </div>
            </div>
        </div>

    </form>
</div>
<script type="text/javascript">
    var frm_id = 'form_update';
    urlUpdate = "<?php echo base_url(QUAN_TRI_TRANG_GIOI_THIEU_UPDATE); ?>";
    urlBack = "<?php echo base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG); ?>";
    SubmitForm(frm_id,'',urlBack);
    $(document).ready(function () {
        show_textarea('value', '600');
        show_textarea('mission', '600');
        show_textarea('prize', '600');
    });
</script>
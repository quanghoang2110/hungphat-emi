<div class="box-body">
    <form id="form_update" method="post">
        <?php foreach ($setting as $key => $list): ?>
        <div class="callout" style="display: none;" id="alert_message"></div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Tên website</label>
                    <input type="text" name="name" class="form-control" placeholder="Tên website"
                           value="<?php echo ($list) ? $list->name : ''; ?>">
                </div>
            </div>
            <!--            <div class="form-group">-->
            <!--                <label>Logo</label>-->
            <!--                <div id="show_img">-->
            <!--                    <a class="file-manager">-->
            <!--                        --><?php //echo get_img_tag((isset($list->logo) && $list->logo != '') ? $list->logo : '', '', 'rfm-image table-image', base_url('images/default/image.png')); ?>
            <!--                        <input type="text" id="image" name="logo" class="rfm-link hidden"-->
            <!--                               value="--><?php //echo ($list) ? $list->logo : ''; ?><!--">-->
            <!--                    </a>-->
            <!--                </div>-->
            <!--            </div>-->
            <div class="form-group">
                <label>SEO title (meta title)</label>
                <input type="text" name="meta_title" class="form-control" maxlength="65" placeholder=""
                       value="<?php echo $list ? $list->title : ''; ?>"/>
            </div>
            <div class="form-group">
                <label>SEO description (meta description)</label>
                <textarea type="text" name="description" class="form-control" maxlength="165"
                          placeholder=""><?php echo $list ? $list->description : ''; ?></textarea>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Link facebook fanpage</label>
                    <input type="text" name="fb_fanpage" class="form-control" placeholder="http://facebook.com/..."
                           value="<?php echo $list ? $list->fb_fanpage : ''; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label>Facebook APP</label>
                    <input type="text" name="fb_app_id" class="form-control" placeholder="id của facebook app"
                           value="<?php echo $list ? $list->fb_app_id : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Google plus</label>
                    <input type="text" name="gg_plus_id" class="form-control" placeholder="http://plus.google.com/..."
                           value="<?php echo $list ? $list->gg_plus_id : ''; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label>Google Analytics</label>
                    <input type="text" name="gg_analytics_id" class="form-control" placeholder="ID Google Analytics"
                           value="<?php echo $list ? $list->gg_analytics_id : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Kênh Youtube</label>
                    <input type="text" name="youtube" class="form-control" placeholder="link kênh youtube"
                           value="<?php echo $list ? $list->youtube : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Skype</label>
                    <input type="text" name="company_skype" class="form-control" placeholder="tài khoản skype"
                           value="<?php echo $list ? $list->company_skype : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Tên công ty</label>
                    <input type="text" name="company_name" class="form-control" placeholder="Tên website"
                           value="<?php echo $list ? $list->company_name : ''; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label>Email</label>
                    <input type="text" name="company_email" class="form-control" placeholder="email hỗ trợ"
                           value="<?php echo $list ? $list->company_email : ''; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label>Điện thoại</label>
                    <input type="text" name="company_phone" class="form-control" placeholder="điện thoại"
                           value="<?php echo $list ? $list->company_phone : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label>Thời gian làm việc</label>
                    <input type="text" name="company_time" class="form-control" placeholder="thời gian làm việc"
                           value="<?php echo $list ? $list->company_time : ''; ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Địa chỉ</label>
                    <input name="company_address" class="form-control" placeholder="địa chỉ"
                           value="<?php echo(($list) ? $list->company_address : ''); ?>">
                </div>
                <div class="col-sm-6">
                    <label>Văn phòng</label>
                    <input type="text" name="company_address2" class="form-control" placeholder="văn phòng "
                           value="<?php echo $list ? $list->company_address2 : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label>Thông tin khác</label>
                <textarea type="text" name="company_info" class="form-control" maxlength="165"
                          placeholder=""><?php echo $list ? $list->company_info : ''; ?></textarea>
            </div>
            <div class="col-sm-12 hidden">
                <label>link Google map</label>
                <input type="text" name="google_map" class="form-control" placeholder="google map "
                       value="<?php echo $list ? $list->google_map : ''; ?>">

            </div>
            <div class="box-footer">
                <div class="col-sm-12" style="height: 15px"></div>
                <div class="form-group col-sm-12">
                    <button type="submit" class="btn btn-primary  pull-right">Lưu</button>
                    <a href='<?php echo base_url(QUAN_TRI_CAM_NHAN_KHACH_HANG); ?>' type='button'
                       class='btn btn-default pull-right margin-right-10'> Hủy </a>
                    <?php if (isset($list) && ($list->id != null)) { ?>
                        <input type="hidden" name="hdID" id="hdID" value="<?php echo $list->id ?>">
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </form>
</div>
<script type="text/javascript">
    var frm_id = 'form_update';
    urlUpdate = "<?php echo base_url(ADMIN_ROUTE_SITE_SETTING_UPDATE); ?>";
    SubmitForm(frm_id);
    $(document).ready(function () {
        show_textarea('description', '400');
    });
</script>
<style>
    .margin-right-10 {
        margin-right: 10px
    }
</style>
<div class="modal" id="modal_group_update"></div>
<div class="box-body">
    <div class="col-sm-6 pull-right">
        <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_100">Ảnh đại diện</th>
            <th class="width_200">Nhà cung cấp</th>
            <th>Link xem Pdf</th>
            <th class="width_120">Trạng thái</th>
            <th class="width_160">Thao tác</th>
        </tr>

        </thead>
        <tbody>
        <?php if ($list) {
            foreach ($list as $index => $val) {
                ?>
                <tr id="item-<?php echo $val->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><img
                            src="<?php if ($val && $val->img != null) echo $val->img; else echo base_url(DEFAULT_IMG) ?>"
                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12"/></td>
                    <td><a title="<?php echo $val->site ?>" href="<?php echo $val->site ?>"
                           target="_blank"><?php if ($val && $val->name != null) echo $val->name ?></a></td>
                    <td>
                        <?php if ($val && $val->file_pdf != null) echo $val->file_pdf; ?>
                    </td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($val->valid == 1) echo 'Mở'; else echo 'Khóa'; ?>"
                                onclick="validAjax(<?php echo $val->id ?>, <?php echo $val->valid ?>)">
                            <i class="fa <?php if ($val->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i> <span
                                class="text-show-valid"><?php if ($val->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $val->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-flat" title="Xóa" title="Xóa"
                                onclick="deleteAjax(<?php echo $val->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>
                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<style>
    .right {
        float: right;
    }
</style>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
        } else {
            window.location.href = urlForm;
        }
    }
</script>

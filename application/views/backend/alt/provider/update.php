<form id="form_update">
        <div class="callout" style="display: none;" id="alert_message"></div>
    <div class="box-body" style="min-height:400px;">
            <div class="form-group col-sm-2 col-xs-3">
                <label>Ảnh đại diện <span class="require">(*)</span></label>
                <div id="show_img">
                    <a class="file-manager">
                        <img class="rfm-image table-image"
                             src="<?php if (isset($list) && ($list->img != null)) echo $list->img; else echo DEFAULT_IMG; ?>">
                        <input type="text" id="image_link" name="image" class="rfm-link hidden"
                               value="<?php if (isset($list) && ($list->img != null)) echo $list->img; else echo ''; ?>">
                    </a>
                </div>
                <p class="help-block text-center">200x200</p>
            </div>
        <div class="padding-0 col-sm-9 col-xs-9">
                <div class="form-group col-sm-5">
                    <label>Tên nhà cung cấp <span class="require">(*)</span></label>
                    <input class="form-control" name="name" id="name"
                           value="<?php if (isset($list) && isset($list->name) && ($list->name != null)) echo $list->name; ?>">
                </div>
                <div class="form-group col-sm-5">
                    <label>Website <span class="require">(*)</span></label>
                    <input class="form-control" name="site" id="site" placeholder="http://trang_chu_ncc.com/"
                           value="<?php if (isset($list) && isset($list->site) && ($list->site != null)) echo $list->site; else echo ''; ?>">
                </div>
                <div class="form-group col-sm-2">
                    <label class="status">Trạng thái: </label>
                    <select class="form-control" name="valid">
                        <?php if (isset($list->valid) && ($list->valid == 0)) { ?>
                            <option value="0" selected>Khóa</option>
                            <option value="1">Mở</option>
                        <?php } else { ?>
                            <option value="1" selected>Mở</option>
                            <option value="0">Khóa</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <label>Link Catalogue <span class="require">(*)</span></label>
                <input class="form-control"
                       value="<?php if (isset($list) && ($list->file_pdf != null)) echo $list->file_pdf; ?>"
                       placeholder="Nhập link pdf" name="file_pdf">
            </div>
    </div>
    <div class="box-footer">
        <br/>

        <div class="col-sm-12 col-xs-12" style="margin-top: 10px">
            <a href="<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP); ?>" type='button' class='btn btn-default'>Hủy </a>
            <button type="submit" class="btn btn-primary">Lưu</button>
            <?php if (isset($list) && ($list->id != null)) { ?>
                <input type="hidden" name="hdID" id="hdID" value="<?php echo $list->id ?>">
            <?php } ?>
            </div>

        </div>
</form>

    <style>
        .status {
            margin-right: 30px;
        }

        .padding-0 {
            padding: 0px;
        }
    </style>
    <script type="text/javascript">
        //        urlUpdate = "<?php //echo base_url(QUAN_TRI_NHA_CUNG_CAP_UPDATE);?>//";
        //        var frm_id = 'form_update';
        //        SubmitForm(frm_id);
        urlUpdate = "<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP_UPDATE);?>";
        var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
        var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
        urlBack = "<?php echo base_url(QUAN_TRI_NHA_CUNG_CAP);?>?per_size="+per_size+"&per_page="+per_page;
        var frm_id = 'form_update';
        SubmitForm(frm_id,'',urlBack);
    </script>
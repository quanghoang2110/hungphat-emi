<form id="form_update">
    <div class="modal-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->

        <div class="row">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="group_name">Ảnh <span class="require">(*)</span></label>
                        <div id="show_img">
                            <a class="file-manager">
                                <?php echo get_img_tag((isset($data->image) && $data->image != '') ? $data->image : '', '', 'rfm-image table-image', base_url('images/default/image.png')); ?>
                                <input type="text" id="image" name="image" class="rfm-link hidden"
                                       value="<?php if (isset($data->image)) echo $data->image; ?>">
                            </a>
                        </div>
                        <span id="error_image" class="error"></span>
                    </div>
                    <div class="form-group col-sm-9 col-xs-8">
                        <div class="col-md-6">
                            <label for="title">Tiêu đề 1 <span class="require">(*)</span></label></br>
                            <input id="title" class="form-control" type="text" name="title"
                                   value="<?php if (isset($data)) echo $data->title; ?>"/>
                        </div>
                        <div class="col-md-6">
                            <label for="title">Tiêu đề 2<span class="require">(*)</span></label></br>
                            <input class="form-control" type="text" name="title2"
                                   value="<?php if (isset($data)) echo $data->sub_title; ?>"/>
                        </div>
                    </div>

                    <div class="form-group col-sm-9 col-xs-8">
                        <div class="form-group col-xs-3 ">
                            <label for="menu">Danh mục menu</label></br>
                            <select class="form-control" id="menu_id" name="menu_id">
                                <option value="0">Không chọn</option>
                                <?php foreach ($list_menu as $index => $item) { ?>
                                    <option <?php if (isset($data) && $data->menu_id == $item->id) echo 'selected' ?>
                                        value="<?php echo $item->id; ?>">
                                        <?php echo $item->name ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <label for="valid">Trạng thái:</label>
                            <select class="form-control" id="valid" name="valid">
                                <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                    value="<?php echo 1; ?>">Mở
                                </option>
                                <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                    value="<?php echo 0; ?>">Khóa
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="description">Mô tả <span class="require">(*)</span></label>
                <textarea name="desc" class="form-control"
                          placeholder="Nội dung ..."><?php if (isset($data)) echo $data->description; ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12">
                <label for="link_download">Nội dung tin tức <span class="require">(*)</span></label>
                <textarea id="content" name="content"
                          placeholder="Nội dung ..."><?php if (isset($data)) echo $data->content; ?></textarea>
            </div>
        </div>


        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
    <div class="modal-footer" style="background: white">
        <a class="btn btn-default" href="#return" onclick="<?php echo base_url() ?>">Hủy</a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<?php $this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $(function () {
        show_textarea('content', '900');
        show_textarea('description', '200');
    });
    urlBack = "<?php echo base_url() ?>";
    SubmitForm('form_update','',urlBack);
</script>
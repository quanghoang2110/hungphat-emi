<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_110">Ảnh</th>
            <th class="width_300">Tiêu đề</th>
            <th class="width_300">Tiêu đề 2</th>

            <th>Mô tả</th>
            <th class="width_150">Danh mục menu</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_150">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($list) {
            foreach ($list as $index => $item) { ?>
                <tr id="item-<?php echo $item->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td class="table-image"><?php echo get_img_tag($item->image, $item->title, 'img-thumbnail',IMG_NEWS_DEFAULT)?></td>
                    <td><?php echo($item->title); ?></td>
                    <td><?php echo($item->sub_title); ?></td>
                    <td><?php echo word_limiter(strip_tags($item->description, '<p>'), 50); ?></td>
                    <td><?php echo $item->menu_name; ?></td>
                    <!--                    <td>--><?php //echo($item->static_name); ?><!--</td>-->
                    <!--                    <td>--><?php //echo($item->name); ?><!--</td>-->
                    <td align="center">
                        <button
                            class="item_valid btn-valid btn btn-flat"
                            title="<?php if ($item->valid == 1) echo 'Mở '; else echo 'Khóa'; ?>"
                            onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->valid ?>)">
                            <i class="fa <?php if ($item->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            <span
                                class="text-show-valid"><?php if ($item->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $item->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <?php if ($permission_delete) { ?>
                            <button class="btn btn-flat" title="Xóa" title="Xóa"
                                    onclick="deleteAjax(<?php echo $item->id ?>)">
                                <i class="fa fa-close"></i>
                            </button>
                        <?php } ?>
                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<?php $this->load->view($theme_template . '/script'); ?>

<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }
</style>
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->

        <div class="row">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="group_name">Ảnh <span class="require">(*)</span></label>

                        <div id="show_img">
                            <a class="file-manager">
                                <img
                                    src="<?php if (isset($data) && ($data->img != null)) echo $data->img; else echo base_url(DEFAULT_IMG) ?>"
                                    class="rfm-image table-image"/>
                                <input type="text" id="img" name="img" class="rfm-link hidden"
                                       value="<?php if (isset($data->img)) echo $data->img; ?>">
                            </a>

                            <p class="help-block text-center">*x*</p>
                        </div>
                        <span id="error_image" class="error"></span>
                    </div>
                    <div class="form-group col-xs-9">
                        <label for="title">Tên banner <span class="require">(*)</span></label></br>
                        <input id="title" class="form-control" type="text" name="name"
                               value="<?php if (isset($data)) echo $data->name; ?>"/>
                    </div>
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="valid">Trang thái:</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                value="1">Mở
                            </option>
                            <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                value="0">Khóa
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="views">Vị trí</label></br>
                        <input id="views" class="form-control" type="text" name="position"
                               value="<?php if (isset($data)) echo $data->position; ?>"/>
                    </div>
                    <div class="form-group col-sm-3 col-xs-4">
                        <label for="views">Loại banner</label></br>
                        <select class="form-control" name="type">
                            <option value="1" <?php echo isset($data) && $data->type == "1" ? 'selected' : '' ?>>Banner
                                tin tức (800x150)
                            </option>
                            <option value="2" <?php echo isset($data) && $data->type == "2" ? 'selected' : '' ?>>Banner
                                sản phẩm (264x285)
                            </option>
                            <option value="3" <?php echo isset($data) && $data->type == "3" ? 'selected' : '' ?>>Banner
                                đầu trang (1920x100)
                            </option>
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TOP_TRANG_CHU ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TOP_TRANG_CHU)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Banner top trang chủ-->
                            <!--                            </option>-->
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TRANG_CHU ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TRANG_CHU)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Trang chủ-->
                            <!--                            </option>-->
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TRANG_CHI_TIET_SAN_PHAM ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TRANG_CHI_TIET_SAN_PHAM)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Trang sản phẩm-->
                            <!--                            </option>-->
                            <!---->
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TRANG_TIN_TUC ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TRANG_TIN_TUC)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Trang tin tức-->
                            <!--                            </option>-->
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TRANG_TUYEN_DUNG ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TRANG_TUYEN_DUNG)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Trang tuyển dụng-->
                            <!--                            </option>-->
                            <!--                            <option-->
                            <!--                                value="-->
                            <?php //echo BANNER_QUANG_CAO_TRANG_KHUYEN_MAI ?><!--" -->
                            <?php //if (isset($data) && ($data->type == BANNER_QUANG_CAO_TRANG_KHUYEN_MAI)) echo 'selected'; ?><!-->
                            -->
                            <!--                                Trang khuyến mại-->
                            <!--                            </option>-->
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <label>Link</label>
                        <input value=" <?php if (isset($data) && ($data->type != null)) echo $data->type; ?>"
                               name="link" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
    </div>
    <div class="box-footer">
        <a href='<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO); ?>' type='button' class='btn btn-default'>
            Hủy </a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<style>
    textarea {
        min-height: 80px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    $(function () {
        show_textarea('content', '900');
    });
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    urlUpdate = "<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO_UPDATE);?>";
    urlBack = "<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO);?>?per_size="+per_size+"&per_page="+per_page;
    var frm_id = 'form_update';
    SubmitForm(frm_id,'',urlBack);
</script>
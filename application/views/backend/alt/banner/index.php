<div class="box-body">
    <div class="col-sm-6 pull-right">
        <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th class="width_300">Tên banner</th>
            <th>Ảnh</th>
            <th>Loại banner</th>
            <th class="width_80">Vị trí</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_160">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($news_list) { ?>
            <?php foreach ($news_list as $index => $news) { ?>
                <tr id="item-<?php echo $news->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($news->name); ?></td>
                    <td align="center"
                        class="table-image">
                        <img
                            src="<?php if (isset($news) && ($news->img != null)) echo $news->img; else echo base_url(DEFAULT_IMG) ?>"
                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12"/>
                    <td style="padding-left: 10px">
                        <?php if ($news->type == 1)
                            echo 'Banner tin tức';
                        elseif ($news->type == 2)
                            echo 'Banner sản phẩm';
                        elseif ($news->type == 3)
                            echo 'Banner đầu trang (top)';
                        ?>
                    </td>
                    <td><?php echo($news->position); ?></td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($news->valid == 1) echo 'Mở bài viết'; else echo 'Khóa bài viết'; ?>"
                                onclick="validAjax(<?php echo $news->id ?>, <?php echo $news->valid ?>)">
                            <i class="fa <?php if ($news->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            <span
                                class="text-show-valid"><?php if ($news->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>

                        <button class="btn btn-flat" title="Xóa" title="Xóa"
                                onclick="deleteAjax(<?php echo $news->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>

                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_BANNER_QUANG_CAO_FORM) ?>";
    var per_size = "<?php echo ($this->input->get('per_size')) ? $this->input->get('per_size') : 10; ?>";
    var per_page = "<?php echo ($this->input->get('per_page')) ? $this->input->get('per_page') : 1; ?>";
    function updateAjax(id) {
        if (typeof (id) !== 'underfined') {
            window.location.href = urlForm + '?id=' + id+'&per_size='+per_size+'&per_page='+per_page;
        } else {
            window.location.href = urlForm;
        }
    }
</script>
<style>
    .width_160 {
        width: 160px !important;
    }

    img {
        max-width: 70px;
    }

    td.table-image {
        width: 100px;
    }

    .table-image {
        width: 100%;
        max-width: inherit;
    }
</style>


<div class="modal" id="modal_update"></div>
<?php if ($permission_edit) { ?>
    <div class="modal" id="modal_update_pass">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="updatePassForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Thông báo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="callout" style="display: none;" id="alert_message_update_pass"></div>
                            <!-- general form elements -->
                            <div class="row">
                                <div class="form-group col-xs-4">
                                    <label for="password_old">Mật khẩu hiện tại</label>
                                    <input type="password" class="form-control" id="password_old" name="password_old"
                                           placeholder="Mật khẩu hiện tại" value=""/>
                                    <span id="error_password_old" class="error"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-4">
                                    <label for="password_new">Mật khẩu mới</label>
                                    <input type="password" class="form-control" id="password_new" name="password_new"
                                           placeholder="Mật khẩu mới" value=""/>
                                    <span id="error_password_new" class="error"></span>
                                </div>
                                <div class="form-group col-xs-4">
                                    <label for="repassword">Nhập lại mật khẩu</label>
                                    <input type="password" class="form-control" id="repassword" name="repassword"
                                           placeholder="Mật khẩu mới" value=""/>
                                    <span id="error_repassword" class="error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php } ?>
<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th style="width: 50px">STT</th>
            <th>Tài khoản</th>
            <th>Nhóm quản trị</th>
            <?php if ($permission_edit || $permission_delete) { ?>
                <th style="width: 80px">Trạng thái</th>
            <?php } ?>
            <?php if ($permission_edit || $permission_delete) { ?>
                <th style="width: 160px">Thao tác</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php if ($list) {
            foreach ($list as $index => $item) { ?>
                <tr id="item-<?php echo $item->id; ?>">
                    <td align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($item->username); ?></td>
                    <td><?php echo($item->group_name); ?></td>
                    <?php if ($permission_edit || $permission_delete) { ?>
                        <td align="center">
                            <button class="item_valid btn-valid btn btn-flat"
                                    title="<?php if ($item->valid == 1) echo 'Mở tài khoản'; else echo 'Khóa tài khoản'; ?>"
                                    <?php if ($permission_edit){ ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->valid ?>)" <?php } ?>>
                                <i class="fa <?php if ($item->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                                <span
                                    class="text-show-valid"><?php if ($item->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                            </button>
                        </td>
                        <td align="center">
                            <?php if ($permission_edit && $item->id != $user_id && $item->no_edit == 0) { ?>
                                <button class="btn-repair-question btn btn-flat"
                                        onclick="updateAjax(<?php echo $item->id ?>)">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-flat" title="Reset mật khẩu"
                                        onclick="updatePass(<?php echo $item->id ?>)">
                                    <i class="fa fa-refresh"></i>
                                </button>
                                <?php if ($permission_delete) { ?>
                                    <button class="btn btn-flat" title="Xóa"
                                            onclick="deleteAjax(<?php echo $item->id ?>)">
                                        <i class="fa fa-close"></i>
                                    </button>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<script type="text/javascript">
    urlDelete = "<?php echo base_url($permission_url . '/' . ROUTE_DELETE); ?>";
    urlValid = "<?php echo base_url($permission_url . '/' . ROUTE_VALID); ?>";
    urlUpdate = "<?php echo base_url($permission_url . '/' . ROUTE_UPDATE) ?>";
    urlUpdatePass = "<?php echo base_url(''); ?>";

    function updateAjax(id) {
        var data = {};
        if (typeof (id) !== 'undefined') {
            data = {id: id};
        }
        $.post(urlUpdate, data)
            .done(function (data) {
                    $('#modal_update').html(data).modal("show");
                    var frm = $('#form_update');
                    var url = "<?php echo base_url($permission_url)?>";
                    submitGroup(frm, url, id);
                }
            );
    }


    function submitGroup(frm, url, id) {
        frm.submit(function (ev) {
            $.ajax({
                type: 'POST',
                url: url,
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.b_Check != null) {
                        if (!result.b_Check) {
                            if (result.msg_error != null) {
                                var top = "";
                                var index = 0;
                                for (var i in result.msg_error) {
                                    index++;
                                    if (index == 1) {
                                        top = i;
                                    }
                                    $("#error_" + i).html(result.msg_error[i]);
                                    $("#error_" + i).show();
                                }
                                ShowAlertMessage('alert_message', 'callout-danger', 'Lỗi');
                                $("#" + top).focus();
                                return false;
                            }
                        }
                    } else {
                        if (result.err == 0) {
                            var message = "Thêm thành công";
                            if (typeof (id) !== 'undefined') {
                                message = "Cập nhật thành công";
                            }
                            $('.modal').modal('hide');
                            showThongBao(message, function () {
                                location.reload();
                            });
                            location.reload();
                        } else {
                            ShowAlertMessage('alert_message', 'callout-danger', result.msg);
                        }
                    }
                }
            });
            ev.preventDefault();
        });
    }

    function updatePass(id) {
//        if(typeof (id) !== 'undefined'){
//            if(id != <?php //echo ($this->session->userdata(ADMIN_SESSION_USERID)) ?>//){
//                $('#btnResetPass').attr('onclick', 'resetPass(' + id + ')');
//                $('#modal_reset_pass').modal('show');
//            }else {
//                $('#password_old').val('');
//                $('#password_new').val('');
//                $('#repassword').val('');
//                HideAlertMessage('alert_message_update_pass');
//                $('#modal_update_pass').modal('show');
//            }
//        }
    }

    var updatePassForm = $('#updatePassForm');
    updatePassForm.submit(function (ev) {
        $.ajax({
            type: 'POST',
            url: urlUpdatePass,
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.b_Check != null) {
                    if (!result.b_Check) {
                        if (result.msg_error != null) {
                            var top = "";
                            var index = 0;
                            for (var i in result.msg_error) {
                                index++;
                                if (index == 1) {
                                    top = i;
                                }
                                $("#error_" + i).html(result.msg_error[i]);
                                $("#error_" + i).show();
                            }
                            ShowAlertMessage('alert_message_update_pass', 'callout-danger', 'Lỗi');
                            $("#" + top).focus();
                            return false;
                        }
                    }
                } else {
                    if (result.err == 0) {
                        $('#modal_update_pass').modal('hide');
                        showThongBao("Đổi mật khẩu thành công");
                        setTimeout(function () {
                            $('#modal_msg').modal('hide');
                        }, 2000);
                    } else {
                        ShowAlertMessage('alert_message_update_pass', 'callout-danger', result.msg);
                    }
                }
            }
        });
        ev.preventDefault();
    });

</script>

<div class="modal-dialog">
    <div class="modal-content">
        <form id="form_update">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $title; ?></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="callout" style="display: none;" id="alert_message"></div>
                    <!-- general form elements -->
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="username">Tên tài khoản <span class="require">(*)</span></label>
                            <input type="text" class="form-control" id="username" name="username"
                                   placeholder="Tên tài khoản" required
                                   value="<?php echo $admin_data ? $admin_data->username : ''; ?>">
                            <span id="error_username" class="error"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="fullname">Họ tên <span class="require">(*)</span></label>
                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Họ tên" required
                                   value="<?php echo $admin_data ? $admin_data->fullname : ''; ?>"/>
                            <span id="error_fullname" class="error"></span>
                        </div>
                        <div class="form-group col-xs-4">
                            <label for="valid">Trạng thái</label>
                            <select class="form-control" id="valid" name="valid">
                                <option <?php echo $admin_data && $admin_data->valid ? 'selected' : ''; ?>
                                    value="<?php echo 1; ?>">Mở
                                </option>
                                <option <?php echo $admin_data && !$admin_data->valid ? 'selected' : ''; ?>
                                    value="<?php echo 0; ?>">Khóa
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if ($groups) { ?>
                            <div class="col-xs-12">
                                <label for="group_name">Danh sách nhóm quyền</label>
                            </div>
                            <?php foreach ($groups as $key => $value) { ?>
                                <div class="checkbox col-xs-6" style="margin-top: 10px;">
                                    <label>
                                        <input type="radio" name="group"
                                               value="<?php echo $value->id; ?>" <?php echo $admin_data && $value->id == $admin_data->group_id ? 'checked' : ''; ?>> <?php echo $value->name; ?>
                                    </label>
                                </div>
                            <?php }
                        } ?>
                    </div>
                    <input type="hidden" id="hdID" name="hdID"
                           value="<?php echo $admin_data ? $admin_data->id : ''; ?>"/>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <?php if ($permission_edit) { ?>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                <?php } ?>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
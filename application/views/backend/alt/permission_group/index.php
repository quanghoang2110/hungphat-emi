<div id="div_nhom" style="width: 60%">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Tên nhóm</th>
            <th>Khả dụng</th>
            <th>Cập nhật quyền</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $index => $item): ?>
            <tr>
                <td><?php echo $item->name ?></td>
                <td><?php echo $item->valid ?> </td>
                <td>
                    <button class="btn-valid btn btn-flat"
                            onclick="updatePermission(<?php echo $item->id ?>, '<?php echo $item->name ?>')"><i
                            class="fa fa-edit"></i></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="div_phanquyen" style="display: none"></div>

<script>
    function updatePermission(group_id, group_name) {
        $.post("<?php echo base_url(ROUTE_ADMIN_VIEW_PERMISSION)?>", {
            group_id: group_id,
            group_name: group_name
        }).done(function (data) {
            $('#div_phanquyen').html(data).fadeIn();
        });
    }
</script>
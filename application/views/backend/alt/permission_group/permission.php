<section>
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo $theme_assets ?>plugins/iCheck/all.css"/>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo $theme_assets ?>plugins/iCheck/icheck.min.js"></script>
    <div class="row">
        <div class="col-sm-6">
            <form action="<?php echo base_url(ROUTE_PERMISSION_UPDATE) ?>" id="form_permission"
                  method="post">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Phân quyền nhóm <strong><?php echo $group_name ?></strong></h3>
                        <input type="hidden" name="group_id" value="<?php echo $group_id ?>"/>
                    </div>
                    <div class="box-body">
                        <!-- Minimal style -->

                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Chức năng</th>
                                <th>Quyền truy cập</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($funcs as $func): ?>
                                <tr>
                                    <td><?php echo ucfirst($func->category_name); ?></td>
                                    <td>
                                        <?php $count = 0; ?>
                                        <?php foreach ($list as $index => $item): ?>
                                            <?php if ($item->category == $func->category): ?>
                                                <?php $count++ ?>
                                                <label>
                                                    <input type="checkbox" name="permission[]"
                                                           value="<?php echo $item->id ?>"
                                                           id="<?php echo $item->category . '-' . $count ?>"
                                                           class="flat-red" <?php echo $item->checked ?>
                                                           onclick="updatePermissionRole('<?php echo $item->category ?>', <?php echo $count ?>)">
                                                </label>
                                                <?php echo $item->name ?> &nbsp;&nbsp;
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-default" type="button" onclick="hideFormPermission()">
                            Hủy
                        </button>
                        <button class="btn btn-primary" type="button" onclick="submitPermissionForm()">Cập nhật</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="<?php echo $theme_assets ?>plugins/input-mask/jquery.inputmask.js"></script>
    <script>
        //iCheck for checkbox and radio inputs
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $(".iCheck-helper").click(function () {
            var obj = $(this).parent();
            obj.children()[0].click();
        });

        function updatePermissionRole(category, position) {
            console.log('here?');
            var inputFocus = $('#' + category + '-' + position);
            var i = 0, obj;
            var isChecked = inputFocus.prop("checked");
            if (isChecked) {
                inputFocus.prop("checked", false);
                if (position < 3) {
                    for (i = 3; i > position; i--) {
                        obj = $('#' + category + "-" + i);
                        obj.prop("checked", false);
                        obj.parent().removeClass("checked");
                    }
                }
            } else {
                inputFocus.prop("checked", true);
                if (position > 1) {
                    for (i = 1; i < position; i++) {
                        obj = $('#' + category + "-" + i);
                        obj.prop("checked", true);
                        obj.parent().addClass("checked");
                    }
                }
            }
            return isChecked;
        }

        function submitPermissionForm() {
            $.ajax({
                url: $('#form_permission').attr('action'),
                type: 'POST',
                data: $('#form_permission').serialize(),
                success: function (result) {
                    hideFormPermission();
                }
            });
        }

        function hideFormPermission() {
            $('#div_phanquyen').fadeOut('medium')
        }
    </script>
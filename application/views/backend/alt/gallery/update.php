<form id="form_update">
    <div class="modal-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->
        <div>
            <div class="row">
                <div class="form-group col-xs-4">
                    <label for="library_name">Tên danh mục <span class="require">(*)</span></label>
                    <input type="text" class="form-control" id="name" name="name"
                           value="<?php if (isset($data)) echo $data->name ?>"/>
                    <span id="error_name" class="error"></span>
                </div>
                <div class="row">
                    <div class="form-group col-xs-4">
                        <label for="valid">Trạng thái</label>
                        <select class="form-control" id="valid" name="valid">
                            <option <?php if (isset($data) && $data->valid == 1) echo 'selected'; ?>
                                value="<?php echo 1; ?>">Mở
                            </option>
                            <option <?php if (isset($data) && $data->valid == 0) echo 'selected'; ?>
                                value="<?php echo 0; ?>">Khóa
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hdID" name="hdID" value="<?php if (isset($data)) echo $data->id; ?>"/>
        </div>
    </div>
    <div class="modal-footer">
        <a class="btn btn-default" href="#cancel" onclick="backUrl()">Hủy</a>
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</form>
<?php $this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    SubmitForm('form_update');
</script>
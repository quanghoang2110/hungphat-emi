<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped table-responsive">
        <thead>
        <tr>
            <th class="width_50">STT</th>
            <th>Tên thư viện</th>
            <th class="width_80">Trạng thái</th>
            <th class="width_100">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($library_list) {
            foreach ($library_list as $index => $library) { ?>
                <tr id="item-<?php echo $library->id; ?>">
                    <td align="center"><?php echo($index + 1); ?></td>
                    <td><?php echo($library->name); ?></td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                title="<?php if ($library->valid == 1) echo 'Mở '; else echo 'Khóa'; ?>"
                                onclick="validAjax(<?php echo $library->id ?>, <?php echo $library->valid ?>)">
                            <i class="fa <?php if ($library->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            <span
                                class="text-show-valid"><?php if ($library->valid == 1) echo 'Mở &nbsp &nbsp'; else echo 'Khóa'; ?></span>
                        </button>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat" title="Sửa"
                                onclick="updateAjax(<?php echo $library->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <?php if ($permission_delete) { ?>
                            <button class="btn btn-flat" title="Xóa" onclick="deleteAjax(<?php echo $library->id ?>)">
                                <i class="fa fa-close"></i>
                            </button>
                        <?php } ?>
                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<?php $this->load->view($theme_template . '/script'); ?>

<style>
    .table-image {
        max-width: 120px;
        max-height: 86px;
    }

    #show_img {
        width: 120px;
        height: 86px;
    }
</style>

<div class="box-body">
    <div class="col-sm-12 pull-right padding-bottom-30">
        <button class="btn btn-primary" onclick="updateAjax(-1, -1,-1)">Thêm mới</button>
    </div>
    <div class="callout" style="display: none;" id="alert_message"></div>
    <!-- general form elements -->

    <div class="row">
        <?php if (count($list) > 0): ?>

            <?php foreach ($list as $key => $val): ?>
                <?php if ($val->parent_id == null): ?>
                    <!--  in thang cha-->
                    <div class="col-sm-12 parent div_parent">
                        <div class="col-sm-9 name">
                            - <?php echo $val->name; ?>
                        </div>
                        <div class="col-sm-3" id="#item-<?php echo $val->id; ?>">
                            <button class="item_valid btn-valid btn btn-flat"
                                    title="<?php if ($val->valid == 1) echo 'Mở'; else echo 'Khóa'; ?>"
                                    onclick="validAjax2(<?php echo $val->id; ?>, <?php echo $val->valid; ?>)">
                                <i class="fa <?php if ($val->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                                <span
                                    class="text-show-valid"><?php if ($val->valid == 1) echo 'Mở&nbsp &nbsp'; else echo 'Khóa'; ?></span>
                            </button>
                            <button class="btn-repair-question btn btn-flat" title="Sửa"
                                    onclick="updateAjax(<?php echo $val->id ?>, '<?php echo $val->name; ?>',-1)">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-flat" title="Xóa"
                                    onclick="deleteAjax2(<?php echo $val->id; ?>)">
                                <i class="fa fa-close"></i>
                            </button>
                        </div>
                    </div>
                    <?php $first = -1; ?>
                    <!-- In thang con 1-->
                    <?php foreach ($list as $k => $v): ?>
                        <?php if ($v->parent_id == $val->id): ?>
                            <div class="child col-sm-12 div_child">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-7 name"> + <?php echo $v->name; ?></div>
                                <div class="col-sm-3" id="#item-<?php echo $v->id; ?>">
                                    <button class="item_valid btn-valid btn btn-flat"
                                            title="<?php if ($v->valid == 1) echo 'Mở'; else echo 'Khóa'; ?>"
                                            onclick="validAjax2(<?php echo $v->id; ?>, <?php echo $v->valid; ?>)">
                                        <i class="fa <?php if ($v->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                                        <span
                                            class="text-show-valid"><?php if ($v->valid == 1) echo 'Mở&nbsp &nbsp'; else echo 'Khóa'; ?></span>
                                    </button>

                                    <button class="btn-repair-question btn btn-flat" title="Sửa"
                                            onclick="updateAjax(<?php echo $v->id; ?>, '<?php echo $v->name; ?>', <?php echo $v->parent_id; ?>)">
                                        <i class="fa fa-edit"></i>
                                    </button>

                                    <button class="btn btn-flat" title="Xóa"
                                            onclick="deleteAjax2(<?php echo $v->id ?>)">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </div>
                            </div>
                            <!--end in con 1-->
                            <!-- In thang con 2-->
                            <?php foreach ($list as $k2 => $v2): ?>
                                <?php if ($v2->parent_id == $v->id): ?>
                                    <div class="child col-sm-12 div_child">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-5 name"> * <?php echo $v2->name; ?></div>
                                        <div class="col-sm-3" id="#item-<?php echo $v2->id; ?>">
                                            <button class="item_valid btn-valid btn btn-flat"
                                                    title="<?php if ($v2->valid == 1) echo 'Mở'; else echo 'Khóa'; ?>"
                                                    onclick="validAjax2(<?php echo $v2->id; ?>, <?php echo $v2->valid; ?>)">
                                                <i class="fa <?php if ($v2->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                                        <span
                                            class="text-show-valid"><?php if ($v2->valid == 1) echo 'Mở&nbsp &nbsp'; else echo 'Khóa'; ?></span>
                                            </button>

                                            <button class="btn-repair-question btn btn-flat" title="Sửa"
                                                    onclick="updateAjax(<?php echo $v2->id; ?>, '<?php echo $v2->name; ?>', <?php echo $v2->parent_id; ?>)">
                                                <i class="fa fa-edit"></i>
                                            </button>

                                            <button class="btn btn-flat" title="Xóa"
                                                    onclick="deleteAjax2(<?php echo $v2->id ?>)">
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <!-- end in con 2 -->
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?><!-- Neu gap thang con thi thoat foreach-->
                    <?php break; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<!--<input type="hidden" id="hdCurrentPage" value="1"/>-->
<style>
    textarea {
        min-height: 80px;
    }
</style>
<?php //$this->load->view($theme_template . '/script'); ?>
<script type="text/javascript">
    urlDelete = "<?php echo base_url(QUAN_TRI_LOAI_SAN_PHAM_DELETE); ?>";
    urlValid = "<?php echo base_url(QUAN_TRI_LOAI_SAN_PHAM_AJAX_VALID); ?>";
    urlForm = "<?php echo base_url(QUAN_TRI_LOAI_SAN_PHAM_FORM) ?>";
    $(document).ready(function () {
        $('#product_type_parent_id').html("<option value='-1'> Danh mục này là danh mục cấp 1</option>");
        <?php foreach($list as $k1 => $v1): ?>
        <?php if($v1->parent_id == null):?>
        $('#product_type_parent_id').append(
            "<option value='<?php echo $v1->id ?>' id='option_parent_<?php echo $v1->id ?>'> <b>- <?php echo $v1->name ?></b></option>"
        );
        <?php foreach($list as $k2 => $v2): ?>
        <?php if($v2->parent_id == $v1->id):?>
        $('#product_type_parent_id').append(
            "<option value='<?php echo $v2->id ?>' id='option_parent_<?php echo $v2->id ?>'>  <?php echo ' &nbsp &nbsp &nbsp &nbsp'.$v2->name ?></option>"
        );
        <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
        <?php endforeach; ?>
    })

    function updateAjax(id, name, parent_id) {
        $('#modal_product_type').modal('show');
        if (id == -1) {
            $('.title_product_type').html('Thêm mới');
        } else {
            $('.title_product_type').html('Cập nhật');
        }
        $('#product_type_id').val(id);
        $('#product_type_parent_id option').removeClass('hidden');
        if (id != -1)
            $('#option_parent_' + id).addClass('hidden');

        if (name != -1) {
            $('#product_type_name').val(name);
        } else {
            $('#product_type_name').val('');
        }
        $('#product_type_parent_id').val(parent_id);
    }
    function validAjax2(id, cValid) {
        validId = id;
//        alert(validId);
//        alert(cValid);
        $.post(urlValid, {id: validId, valid: cValid})
            .done(function (data) {
//                var nValid = cValid == 1 ? 0 : 1;
//                $('#item-' + validId).find('button .item_valid').attr('onclick', 'validAjax2(' + id + ',' + nValid + ')');
//                if (nValid == 1) {
//                    alert('2222');
//                    $('#item-' + validId +' button .item_valid i').removeClass().addClass("fa fa-check");
//                    $('#item-' + validId +' button .item_valid').attr("title", "Mở");
//                    $('#item-' + validId + '.text-show-valid').html('Mở&nbsp &nbsp');
//                } else {
//                    alert('33333');
//                    $('#item-' + validId).find('button .item_valid i').removeClass().addClass("fa fa-ban");
//                    $('#item-' + validId).find('button .item_valid').attr("title", "Khóa");
//                    $('#item-' + validId).find('.text-show-valid').html('Khóa');
//                }
                location.reload(true);
            }
        )
        ;
    }
    function submitUpdateProductType() {
        var is_check = 1;
        var id_sb = $('#product_type_id').val();
        var name = $('#product_type_name').val();
        var parent = $('#product_type_parent_id').val();
        if (name == "" || name == null) {
            is_check = 0;
            $('.error_name').removeClass('hidden');
            setTimeout(function () {
                $('.error_name').addClass('hidden');
            }, 3000);
        }
        if (is_check == 1) {
            $.ajax({
                type: "POST",
                url: base_url + "backend/product_type/update",//trỏ tới file nào
                data: {
                    id: id_sb,
                    name: name,
                    parent: parent
                },
                success: function () {
                    showThongBao("Thao tác thành công");
                    location.reload(true);
                },
                error: function () {
                    showThongBao("Thất bại. xin vui lòng làm lại");
                }
            });
        }
    }

    function deleteAjax2(id) {
        $('#modal_delete_product_type').modal('show');
        $('.delete_product_type_id').val(id);
    }

    function okDeleteProductType() {
        $.ajax({
            type: "POST",
            url: base_url + "backend/product_type/delete",//trỏ tới file nào
            data: {
                id: $('.delete_product_type_id').val()
            },
            success: function () {
                showThongBao("Xóa thành công");
                location.reload(true);
            },
            error: function () {
                showThongBao("Thất bại. xin vui lòng làm lại");
            }
        });
    }
</script>
<style>
    .padding-bottom-30 {
        padding-bottom: 30px;
    }
</style>
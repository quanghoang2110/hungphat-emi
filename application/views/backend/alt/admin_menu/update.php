<style>
    .hint {
        width: 50%;
        border-bottom: 1px solid #eee;
        margin: 20px 0;
        padding-bottom: 5px;
        font-style: italic;
    }
</style>
<!--<link rel="stylesheet" href="/assets/backend/plugins/select2/select2.min.css">-->
<form id="form_update">
    <div class="box-body">
        <div class="callout" style="display: none;" id="alert_message"></div>
        <!-- general form elements -->
        <div>
            <div class="">
                <div class="row">
                    <div class="form-group col-xs-4">
                        <label for="name">Tên menu</label>
                        <input type="text" class="form-control" id="name" name="name"
                               value="<?php if ($data) echo $data->name ?>"/>
                        <span id="error_name" class="error"></span>
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="name">Category/Controller</label>
                        <select name="parent_menu" class="form-control">
                            <option value="0" selected>Không chọn</option>
                            <?php foreach ($parents as $p_item): ?>
                                <option
                                    value="<?php echo $p_item->id ?>" <?php echo $data && $p_item->category == $data->category ? "selected='selected'" : "" ?>><?php echo $p_item->category ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="name">Menu cha</label>
                        <select name="parent_menu" class="form-control">
                            <option value="0" selected>Không chọn</option>
                            <?php foreach ($parents as $p_item): ?>
                                <option
                                    value="<?php echo $p_item->id ?>" <?php echo $data && $p_item->id == $data->parent_id ? "selected='selected'" : "" ?>><?php echo ucfirst($p_item->name) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-8">
                        <label for="name">Đường dẫn</label>
                        <input type="text" class="form-control" id="menu_url" name="menu_url" placeholder="http://..."
                               value="<?php if ($data) echo $data->url ?>"/>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hdID" name="hdID" value="<?php if ($data) echo $data->id; ?>"/>
        </div>
    </div>
    <div class="box-footer">
        <a type="button" href="#cancel" onclick="backUrl()" class="btn btn-default">Quay lại</a>
        <button type="submit" class="btn btn-success">Lưu</button>
    </div>
</form>
<style>
    .form-hidden, .div-hidden, .button_reset {
        display: none;
    }

    fieldset {
        padding: 15px;
        border: 1px solid #ccc;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        background: #f5f6f7;
    }

    legend {
        margin: 0;
        width: auto;
        border: 0;
        font-size: inherit;
    }

    .input-focus {
        background-color: rgba(255, 177, 0, 0.4)
    }
</style>
<?php $this->load->view($theme_template . '/script'); ?>
<!--<script src="/assets/backend/plugins/select2/select2.full.min.js"></script>-->
<script type="text/javascript">

//    $(document).ready(function () {
//        $(".select2").select2();
//    });

    SubmitForm('form_update');
</script>
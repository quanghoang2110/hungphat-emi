<div class="box-body">
    <?php if ($permission_edit) { ?>
        <div class="col-sm-6 pull-right">
            <button class="btn btn-info pull-right" onclick="updateAjax()">Thêm mới</button>
        </div>
    <?php } ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="width_50" colspan="2">STT</th>
            <th>Tên</th>
            <th>Category</th>
            <th>URL</th>
            <th class="width_80">Khả dụng</th>
            <?php if ($permission_edit) { ?>
                <th class="width_150">Thao tác</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php if ($menu_list) {
            foreach ($menu_list as $index => $menu) { ?>
                <tr id="item-<?php echo $menu->id; ?>">
                    <td class="width_50" colspan="2" valign="middle"
                        align="center"><?php echo(($page - 1) * $page_size + $index + 1); ?></td>
                    <td><?php echo($menu->name); ?></td>
                    <td><?php echo($menu->category); ?></td>
                    <td><?php echo($menu->url); ?></td>
                    <td align="center">
                        <button class="item_valid btn-valid btn btn-flat"
                                onclick="validAjax(<?php echo $menu->id ?>, <?php echo $menu->valid ?>)">
                            <i class="fa <?php if ($menu->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                        </button>
                    </td>
                    <td align="center">
                        <button class="btn-repair-question btn btn-flat"
                                onclick="updateAjax(<?php echo $menu->id ?>)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <?php if ($permission_delete) { ?>
                            <button class="btn btn-flat" title="Xóa"
                                    onclick="deleteAjax(<?php echo $menu->id ?>)">
                                <i class="fa fa-close"></i>
                            </button>
                        <?php } ?>
                    </td>
                </tr>
                <?php foreach ($menu->sub as $sindex => $item): ?>
                    <tr id="item-<?php echo $item->id; ?>">
                        <?php if ($sindex == 0): ?>
                            <td class="w50 nodata" rowspan="<?php echo count($menu->sub) ?>"></td>
                        <?php endif; ?>
                        <td class="w50" align="center"><?php echo(($page - 1) * $page_size + $sindex + 1); ?></td>
                        <td><?php echo($item->name); ?></td>
                        <td align="center">
                            <button class="item_valid btn-valid btn btn-flat"
                                    onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->valid ?>)">
                                <i class="fa <?php if ($item->valid == 1) echo 'fa-check'; else echo 'fa-ban'; ?>"></i>
                            </button>
                        </td>
                        <?php if ($permission_edit) { ?>
                            <td align="center">
                                <button class="btn-repair-question btn btn-flat"
                                        onclick="updateAjax(<?php echo $item->id ?>)">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <?php if ($permission_delete) { ?>
                                    <button class="btn btn-flat" title="Xóa"
                                            onclick="deleteAjax(<?php echo $item->id ?>)">
                                        <i class="fa fa-close"></i>
                                    </button>
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
            <?php }
        } ?>
        </tbody>
    </table>
</div>
<input type="hidden" id="hdCurrentPage" value="1"/>
<?php $this->load->view($theme_template . '/script'); ?>
<style>
    .width_50 {
        width: 40px;
        max-width: 40px;
    }

    .nodata {
        background-color: #eee;
    }
</style>

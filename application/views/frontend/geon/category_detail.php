<!--Projects section v.3-->
<section class="section text-xs-center">

    <!--Section heading-->
    <h1 class="section-heading"><?php echo $cate_info->name ?></h1>
    <!--Section description-->
    <p class="section-description"><?php echo $cate_info->desc ?></p>

    <!--First row-->
    <div class="row">

        <?php foreach ($list as $item): ?>
            <div class="col-md-4 m-b-r relative">

                <!--Featured image-->
                <div class="img_hover img_content view overlay hm-white-slight z-depth-2">
                    <img src=""/>
                    <span class="img_hover_bg"
                          style="background-image: url(<?php echo get_img_url($item->img) ?>);"></span>
                    <a href="?theme=<?php echo $item->id ?>">
                        <div class="mask"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <div class="hidden card-block theme_name">
                    <a href="" class="red-text"><h5><i
                                class="fa <?php echo $cate_info->fa_class ?>"></i> <?php echo $item->name ?></h5></a>
                </div>

            </div>
        <?php endforeach; ?>

    </div>
    <!--/First row-->

</section>
<!--/Projects section v.3-->

<style>
    .img_hover {
        display: block;
        overflow: hidden;
        position: relative;
    }

    .img_hover img {
        /*border: 10px solid #eee;*/
        position: relative;
        width: 100%;
        height: 370px;
    }

    .img_hover:hover .img_hover_bg {
        background-position: center 100%;
    }

    .img_hover_bg {
        background-attachment: scroll;
        background-position: center 0;
        background-repeat: no-repeat;
        background-size: cover;
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        transition: background-position 4s ease 0s;
        width: 100%;
    }

    .theme_name {
        z-index: 999999;
        position: absolute;
        left: 15px;
        bottom: 0;
        background: rgba(27, 49, 68, 0.75);
        right: 15px;
    }

    .theme_name h5 {
        color: white;
    }
</style>
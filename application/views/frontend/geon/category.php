<div class="container text-xs-center">

    <!--Section: Templates-->
    <div>

        <!--Section heading-->
        <h1 class="section-heading">Các mẫu thiết kế</h1>
        <p class="control-sidebar-subheading">Bạn muốn phát triển website về..</p>

        <div class="row">

            <?php foreach ($categories as $category): ?>
                <div class="col-md-3 col-lg-4 m-b-r">
                    <!--Card-->
                    <div class="card card-cascade wider">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <?php echo get_img_tag($category->img, $category->name, 'img-fluid') ?>
                            <a href="<?php echo get_detail_url('', $category->name, $category->id) ?>">
                                <div class="mask"></div>
                            </a>
                        </div>
                        <!--/.Card image-->

                        <!--Card content-->
                        <div class="card-block text-xs-center">
                            <!--Category & Title-->
                            <h5><strong><a href="<?php echo get_detail_url('', $category->name, $category->id) ?>"><?php echo $category->name ?></a></strong></h5>

                        </div>
                        <!--/.Card content-->

                    </div>
                    <!--/.Card-->
                </div>
            <?php endforeach; ?>

        </div>

    </div>
    <!--/Section: Templates-->

</div>

<style>
    .card-block p {
        /*text-align: justify;*/
    }

    .card-block h5{
        height: 34px;
        overflow: hidden;
        -ms-text-overflow: ellipsis;
        text-overflow: ellipsis;;
    }

    .review-card .card-footer {
        background-color: #000;
        position: relative;
        width: 100%;
        color: #fff;
        height: 140px;
        padding: 14px;
    }

    div.view{
        max-height: 159px;
        ov
    }
</style>
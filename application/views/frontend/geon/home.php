<!--Section: Intro-->
<section>

    <!--MDB Intro-->
    <div class="row">
        <div class="col-md-12" id="ve_chung_toi">
            <!--Featured image-->
            <img src="/images/<?php echo $theme ?>/intro-1.jpg" alt="Material Design for Bootstrap"
                 class="img-fluid z-depth-1 wow fadeInUp">
            <!--Featured image-->

            <!--Main info-->
            <div class="jumbotron m-1 text-xs-center z-depth-1-half wow fadeInUp" data-wow-delay="0.3s">

                <h2 class="h2-responsive">GIỚI THIỆU VỀ GEON VIỆT</h2>
                <br>
                <p>
                    <strong>GEON Việt</strong> được thành lập bởi một nhóm những người yêu thích <strong>Thiết kế
                        website,
                        Ứng dụng di động</strong> và
                    <strong>SEO - Marketing Online</strong>.
                </p>
                <p>Chúng tôi thiết kế những website, ứng dụng độc đáo chất lượng cao theo chuẩn SEO dễ sử dụng, bảo mật
                    và thân thiện với các công cụ tìm kiếm theo <strong>yêu cầu riêng</strong> của từng khách hàng.
                </p>
                <p>Tất cả website đều được thiết kế <strong>responsive</strong> hiển thị trên các loại màn hình và tương
                    thích với các
                    trình duyệt khác nhau</p>
                <hr>
                <p><strong>Chúng tôi không cho thuê web giá rẻ mà tạo các website chất
                        lượng với giá cả
                        phù hợp nhất.</strong></p>

                <hr>
                <a id="APA-download" href="/<?php echo ROUTE_CAC_MAU_THIET_KE ?>" class="btn btn-primary btn-dtc">Các
                    mẫu
                    thiết kế <i class="fa fa-eye right"></i></a>
                <a id="APA-liveDemo" target="_blank"
                   href="#lien-he"
                   class="btn btn-primary">Liên hệ ngay <i class="fa fa-envelope-o"
                                                           aria-hidden="true"></i></a>
            </div>
            <!--/.Main info-->
        </div>
    </div>
    <!--/.MDB Intro-->

</section>
<!--/.Section: Intro-->

<!--Section: SAN PHAM VA DICH VU-->
<section id="sanphamvadichvu" class="section extra-margins text-xs-center">

    <!--Section heading-->
    <h1 class="section-heading wow fadeIn">Sản phẩm & Dịch vụ</h1>

    <!--First row-->
    <div class="row">

        <!--First column-->
        <div class="col-md-4 m-b-r wow fadeIn">
            <!--Featured image-->
            <div class="view overlay hm-white-slight m-b-2">
                <img src="/images/<?php echo $theme ?>/newsscreen-min.jpg">
                <a href="#web">
                    <div class="mask"></div>
                </a>
            </div>

            <!--Excerpt-->
            <h4 class="blue-text "><i class="fa fa-laptop" aria-hidden="true"></i> Phát triển Website</h4>

            <p>Tạo nên những website độc đáo về thiết kế, tối ưu về công nghệ, thân thiện với người dùng và hỗ trợ SEO
                chuẩn Google. Không ngừng tinh chỉnh, nâng cấp đảm bảo tính hiện đại cho website của bạn.</p>

            <a href="#xem" class="btn btn-primary btn-primary ">Tìm hiểu thêm</a>
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-4 m-b-r wow fadeIn" data-wow-delay="0.2s">
            <!--Featured image-->
            <div class="view overlay hm-white-slight m-b-2">
                <img src="/images/<?php echo $theme ?>/contactsectionmdb-min.jpg">
                <a href="#mobile">
                    <div class="mask"></div>
                </a>
            </div>

            <!--Excerpt-->
            <h4 class="red-text "><i class="fa fa-mobile" aria-hidden="true"></i> Ứng dụng di động</h4>

            <p>Thiết kế và lập trình ứng dụng di động phục vụ cho việc quảng bá hình ảnh, kinh doanh dịch vụ, đáp ứng
                tối đa nhu cầu khách hàng và phù hợp với tất các phiên bản đi động mới nhất.</p>

            <a href="#xem" class="btn btn-primary ">Tìm hiểu thêm</a>
        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-md-4 m-b-r wow fadeIn" data-wow-delay="0.4s">
            <!--Featured image-->
            <div class="view overlay hm-white-slight m-b-2">
                <img src="/images/<?php echo $theme ?>/navigationmdb-min.jpg">
                <a href="#seo">
                    <div class="mask"></div>
                </a>
            </div>

            <!--Excerpt-->
            <h4 class="teal-text "><i class="fa fa-globe" aria-hidden="true"></i> SEO - Marketing</h4>

            <p>Tối ưu hóa công cụ tìm kiếm với chi phí thấp và hiệu quả cũng như tư vấn triển khai các giải pháp
                Marketing Google, Facebook, GEON giúp bạn tiếp cận khác hàng mục tiêu một các thuận lợi, đạt hiệu quả
                cao nhất.</p>

            <a href="#xem" class="btn btn-primary ">Tìm hiểu thêm</a>
        </div>
        <!--/Third column-->

    </div>
    <!--/First row-->

</section>
<!--/Section: Blog v.2-->

<hr class="between-sections">

<!--Section: TAI SAO CHON CHUNG TOI-->
<section>

    <div class="row">
        <div class="col-md-12">
            <div class="text-xs-center wow fadeIn">

                <!--Before/after presentation-->
                <div class="before-after wow fadeIn" data-wow-delay="0.2s">
                    <h2 class="h2-responsive">GEON Việt – Tạo nên sự khác biệt</h2>
                    <p>Sự khác biệt tạo nên lợi thế cạnh tranh cho sản phẩm của bạn</p>

                    <div class="flex-center">

                        <!-- Before/After slider -->
                        <div id="before-after-section">
                            <ul id="controls">
                                <li class="to-start"><a class="btn btn-dtc">Website khác</a></li>
                                <li class="to-middle"><a class="btn btn-primary">So sánh</a></li>
                                <li class="to-end"><a class="btn btn-stc">GEON phát triển</a></li>
                            </ul>

                            <div id="before-after">
                                <div class="view view-after"><img src="/images/<?php echo $theme ?>/after.jpg"/>
                                </div>
                                <div class="view view-before"><img
                                        src="/images/<?php echo $theme ?>/before.jpg"/></div>
                                <div id="dragme"></div>
                            </div>
                        </div>
                        <!-- Before/After slider -->
                    </div>
                    <br>
                    <hr>
                    <br/>
                    <h3 class="h3-responsive  wow fadeIn">VÌ SAO NHIỀU NGƯỜI CHỌN CHÚNG TÔI?</h3>
                    <br>
                    <p class="wow fadeIn"><strong>“Chúng tôi tạo ra sự khác biệt cho website của bạn”</strong></p>
                    <p class="wow fadeIn">Ngay cả sau khi bàn giao, sự quan tâm của chúng tôi với Dự án không hề dừng
                        lại<br/> Trong quá trình website vận hành, phản ứng của người dùng luôn được chúng tôi theo dõi
                        & đo
                        lường nhằm đưa ra những cải tiến tiếp theo.</p>
                </div>
                <!--/.Before/after presentation-->
            </div>
        </div>
    </div>

</section id=>
<!--/.Section: Description & Before/after-->

<hr class="between-sections">

<!--Section: CONG NGHE VA NEN TANG-->
<section class="section feature-box" id="congnghevanentang">

    <!--Section heading-->
    <h1 class="section-heading wow fadeIn">GEON Việt sở hữu</h1>

    <!--First row-->
    <div class="row features-small">

        <!--First column-->
        <div class="col-md-5 m-b-r center-on-small-only">
            <img src="/images/<?php echo $theme ?>/img-2-min.jpg" alt="" class="z-depth-0 wow fadeIn">
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-7 center-on-small">
            <!--First row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2 col-sm-1">
                    <i class="material-icons indigo-text">touch_app</i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Nền tảng công nghệ ưu việt</h4>
                    <p class="grey-text">Tất cả các website được GEON được triển khai trên nền tảng công nghệ ưu việt
                        nhất, được tối ưu toàn bộ hệ thống, tương thích mọi thiết bị, đặc biệt kỹ thuật SEO tiên tiến
                        được áp dụng nhằm giúp thân thiện đối với các máy chủ tìm kiếm như Google, Bing, Yahoo.</p>
                </div>
            </div>
            <!--/First row-->

            <div class="gap"></div>

            <!--Second row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2 col-sm-1">
                    <i class="material-icons indigo-text">burst_mode</i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Giao diện thân thiện, hiện đại</h4>
                    <p class="grey-text">GEON luôn nắm vững các yếu tố về thị hiếu, trải nghiệm khách hàng nhằm tạo ra
                        sản phẩm phù hợp nhất, thiết kế hiện đại, đẹp mắt, thân thiện với người dùng, giúp website, ứng
                        dụng giúp bạn tiếp cận tối đa khách hàng.</p>
                </div>
            </div>
            <!--/Second row-->

            <div class="gap"></div>

            <!--Third row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2 col-sm-1">
                    <i class="material-icons indigo-text">devices_other</i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Hỗ trợ đa nền tảng</h4>
                    <p class="grey-text">Với tâm niệm tạo nên những website, ứng dụng, dịch vụ tốt nhất, GEON Việt sẵn
                        sàng tư vấn cho khách hàng từ giai đoạn lên ý tưởng cho tới thời điểm hoàn thành và không ngừng
                        hỗ trợ tối đa trong suốt quá trình vận hành của khách hàng.</p>
                </div>
            </div>
            <!--/Third row-->

            <!--            <div class="gap"></div>-->
            <!---->
            <!--            <div class="row wow fadeIn">-->
            <!--                <div class="col-xs-2 col-sm-1">-->
            <!--                    <i class="material-icons indigo-text">contacts</i>-->
            <!--                </div>-->
            <!--                <div class="col-xs-10">-->
            <!--                    <h4 class="feature-title">Hỗ trợ tối đa, bảo hành trọn đời</h4>-->
            <!--                    <p class="grey-text">Với tâm niệm tạo nên những website, ứng dụng, dịch vụ tốt nhất, GEON Việt sẵn-->
            <!--                        sàng tư vấn cho khách hàng từ giai đoạn lên ý tưởng cho tới thời điểm hoàn thành và không ngừng-->
            <!--                        hỗ trợ tối đa trong suốt quá trình vận hành của khách hàng.</p>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--/Third row-->
        </div>
        <!--/Second column-->
    </div>
    <!--/First row-->

</section>
<!--/Section: Features v.3-->

<hr class="between-sections">

<!--Section: BAN CHUA HAI LONG?-->
<section class="section feature-box">

    <!--Section heading-->
    <h1 class="section-heading">nổi trội với</h1>

    <!--First row-->
    <div class="row features-small">

        <!--First column-->
        <div class="col-md-4">
            <!--First row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-braille green-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Trên 5 năm kinh nghiệm</h4>
                    <p class="grey-text">Từ những người yêu thiết kế, thích lập trình chúng tôi không ngừng theo đuổi
                        đam mê tạo ra những sản phẩm tốt nhất, khác biệt nhất cho bạn.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/First row-->

            <!--Second row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-cubes teal-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Tối ưu hóa seo</h4>
                    <p class="grey-text">Các sản phẩm của Geon Việt đều được chú trọng tính toán để tối ưu hóa SEO, thân
                        thiện với công cụ tìm kiếm Google, Yahoo, Bing, Cốc Cốc…</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Second row-->

            <!--Third row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-fort-awesome cyan-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Công cụ quản lý chuyên biệt</h4>
                    <p class="grey-text">GEON Việt tạo ra công cụ quản lý chuyên biệt với từng sản phẩm, từng khách hàng
                        tương ứng với từng website, ứng dụng. Bổ sung công cụ quản lý trên mobile.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Third row-->

            <!--Fourth row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-mobile indigo-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Chi phí tối ưu</h4>
                    <p class="grey-text">Đảm bảo chi phí luôn rẻ hơn so với chất lượng sản phẩm tạo ra. Hỗ trợ tích hợp
                        miễn phí công cụ chát trực tuyến, đo đạc thông số website, tư vấn SEO, giải pháp marketing toàn
                        diện.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Fourth row-->
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-4 m-b-r flex-center">
            <img src="/images/geon/header-iphone.png" alt="" class="z-depth-0">
        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-md-4">
            <!--First row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-heart red-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Giao diện tối ưu đẹp mắt</h4>
                    <p class="grey-text">Được tạo ra từ những Designer lành nghề, từ các chuyên gia IT hàng đầu để mang
                        đến bạn và khách hàng của bạn giao diện tối ưu, đẹp mắt nhất.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/First row-->

            <!--Second row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-photo orange-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Hỗ trợ đa nền tảng</h4>
                    <p class="grey-text">Có đến hơn 70% người dùng truy cập website từ thiết bị di động, do đó, website
                        của GEON thực hiện luôn chạy được trên hầu hết các thiết bị: PC Smartphone, iPhone, iPad,
                        Tablet.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Second row-->

            <!--Third row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-magic deep-orange-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Bảo hành trọn đời</h4>
                    <p class="grey-text">
                        Cung cấp các dịch vụ đi kèm chuyên nghiệp, hỗ trợ bảo hành, tư vấn nhiệt tình cho khách hàng
                        trong suốt quá trình vận hành sản phẩm.</p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Third row-->

            <!--Fourth row-->
            <div class="row wow fadeIn">
                <div class="col-xs-2">
                    <i class="fa fa-gear lime-text"></i>
                </div>
                <div class="col-xs-10">
                    <h4 class="feature-title">Toàn quyền sở hữu website</h4>
                    <p class="grey-text">
                        Chúng tôi tạo ra website độc nhất cho bạn và chỉ duy nhất bạn sở hữu, chúng tôi bán chứ không
                        cho thuê. Sau khi hoàn thiện, chúng tôi sẽ bàn giao toàn bộ mã nguồn cho bạn quản lý.
                    </p>
                    <div class="gap-min"></div>
                </div>
            </div>
            <!--/Fourth row-->
        </div>
        <!--/Third column-->
    </div>
    <!--/First row-->

</section>
<!--/Section: Features v.4-->

<hr class="between-sections">

<!--Section: VA CON HON THE NUA-->
<section class="section feature-box">

    <!--Section heading-->
    <h1 class="section-heading wow fadeIn">...và không dừng lại ở đó</h1>

    <!--First row-->
    <div class="row features-big wow fadeIn">
        <!--First column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-firefox red-text"></i>
            <h4 class="feature-title">Theo dõi sản phẩm</h4>
            <p class="grey-text">
                Không dừng lại khi bàn giao snả phẩm. Trong quá trình website vận hành, phản ứng của người dùng luôn
                được chúng tôi theo dõi & đo lường và đưa ra những cải tiến tiếp theo.
            </p>
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-cubes green-text"></i>
            <h4 class="feature-title">Nâng cấp</h4>
            <p class="grey-text">
                Sẵn sàng nâng cấp website cũng như bổ sung nền tảng mobile, viết thêm ứng dụng trên di dộng theo yêu cầu
                của bạn với chi phí hợp lý nhất.
            </p>
        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-level-up blue-text"></i>
            <h4 class="feature-title">Hỗ trợ vận hành</h4>
            <p class="grey-text">
                Đừng ngại khi gặp sự cố hoặc có nhu cầu tư vấn vận hảnh website, chúng tôi luôn sẵn sàng để hỗ trợ bạn
                24/7.</p>
        </div>
        <!--/Third column-->
    </div>
    <!--/First row-->

    <!--Second row-->
    <div class="row features-big wow fadeIn">
        <!--First column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-question teal-text"></i>
            <h4 class="feature-title">Am hiểu</h4>
            <p class="grey-text">
                Am hiểu tạo nên sự thành công trong việc tiếp cận khách hàng và thỏa mãn họ ngay trong lần đầu tiên họ
                truy cập vào website của bạn.
            </p>
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-comments-o lime-text"></i>
            <h4 class="feature-title">Đồng hành</h4>
            <p class="grey-text">
                Không chỉ thiết kế ra một Website, chúng tôi thiết kế ra một Công cụ để giành lấy từng Khách hàng trong
                từng lượt truy cập về cho Doanh nghiệp.
            </p>
        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-th purple-text"></i>
            <h4 class="feature-title">Quan tâm</h4>
            <p class="grey-text">
                GEON Việt đặt vấn đề quan tâm đến nhu cầu của khách hàng lên hàng đầu và tư vấn chuyên sâu để thỏa mãn
                tốn đa các nhu cầu của khách hàng để tạo ra sản phẩm tốt nhất.</p>
        </div>
        <!--/Third column-->
    </div>
    <!--/Second row-->

    <!--Third row-->
    <div class="row features-big wow fadeIn">
        <!--First column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-code pink-text"></i>
            <h4 class="feature-title">kinh nghiệm</h4>
            <p class="grey-text">
                05 NĂM
            </p>
        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-file-code-o indigo-text"></i>
            <h4 class="feature-title">Khách hàng thân thiết</h4>
            <p class="grey-text">56</p>
        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-md-4 m-b-r">
            <i class="fa fa-wrench cyan-text"></i>
            <h4 class="feature-title">Tỷ lệ chuyển đổi</h4>
            <p class="grey-text">69%</p>
        </div>
        <!--/Third column-->
    </div>
    <!--/Third row-->

</section>
<!--/Section: Features v.1-->

<hr class="between-sections">

<!--Section: SideNav-->
<section class="section" id="duanganday">

    <!--Section heading-->
    <h1 class="section-heading wow fadeIn">Dự án do GEON phát triển 3 tháng gần đây</h1>

    <p class="flex-center wow fadeIn"><strong>"Chúng tôi không làm web giá rẻ, chúng tôi tạo ra web khác biệt với giá
            giá tốt nhất."</strong></p>

    <div class="row features-big wow fadeIn">
        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/blue.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-blue.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>

        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/dark.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-blue.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>

        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/graphite.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-green.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>

        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/green.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-green.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>

        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/orange.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-green.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>

        <div class="proj col-md-2 col-lg-4">
            <div class="view overlay hm-black-strong z-depth-2">
                <img src="http://mdbootstrap.com/live/_MDB/img/skins/grey.jpg" alt="" class="img-fluid">

                <div class="mask flex-center">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/skins/skin-green.html" target="_blank"
                       class="btn btn-danger btn-lg">Live preview</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section: SideNav-->

<hr class="between-sections">

<!--khach hang noi ve chung toi-->
<section class="section">

    <!--Section heading-->
    <h1 class="section-heading">Khách hàng nói về GOEN</h1>
    <!--Section sescription-->

    <!--Carousel Wrapper-->
    <div id="carousel-example-1" class="carousel testimonial-carousel slide carousel-fade" data-ride="carousel"
         data-interval="3000">

        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <!--First slide-->
            <div class="carousel-item active">

                <div class="testimonial">
                    <!--Avatar-->
                    <div class="avatar">
                        <img src="/images/<?php echo $theme ?>/mr.trung.ta.jpg" class="img-circle img-fluid">
                    </div>
                    <!--Content-->
                    <p><i class="fa fa-quote-left"></i> Sau nhiều lần sử dụng web theo mẫu nhưng không đạt hiệu quả kinh
                        doanh, tôi đã tìm hiểu và may mắn gặp GEON, với sự nhiệt thành, chuyên nghiệp GEON đã giúp tôi
                        tạo ra website cho công ty và site bán hàng thật sự khác biệt, chúng tôi đã tiếp cận khách hàng
                        tốt hơn, doanh số tăng sau khi đưa website vào vận hành. Chúc GEON không ngừng phát triển và tạo
                        ra nhiều sản phẩm độc đáo.</p>

                    <h4>Mr. Trung Tạ</h4>
                    <h5>CEO GoFood</h5>

                    <!--Review-->
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                </div>

            </div>
            <!--/First slide-->

            <!--Second slide-->
            <div class="carousel-item">

                <div class="testimonial">
                    <!--Avatar-->
                    <div class="avatar">
                        <img src="/images/geon/mr.mai.duc.thinh.jpg" class="img-circle img-fluid">
                    </div>
                    <!--Content-->
                    <p><i class="fa fa-quote-left"></i> Chúng tôi kinh doanh thiết bị máy móc nên khá mơ hồ về website,
                        quảng cáo trực tuyến nhưng sau khi hợp tác với GEO Việt, nhận được sự tư vấn phải nói là triệt
                        để mọi khía cạnh của các bạn chúng tôi đã có được website và ứng dụng về sản phẩm đặc thù này,
                        đồng thời lượng khách hàng online tiếp cận với chúng tôi đột biến so với các khách hàng trực
                        tiếp như trước đây, đúng là sức mạnh của marketing online thật không thể tin được, cảm ơn GEON.
                    </p>

                    <h4>Mr. Mai Đức Thịnh</h4>
                    <h5>CEO Hưng Phát</h5>

                    <!--Review-->
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                </div>

            </div>
            <!--/Second slide-->

            <!--Third slide-->
            <div class="carousel-item">

                <div class="testimonial">
                    <!--Avatar-->
                    <div class="avatar">
                        <img src="/images/geon/mrs.thu.huong.jpg" class="img-circle img-fluid">
                    </div>
                    <!--Content-->
                    <p><i class="fa fa-quote-left"></i> Thiết kế độc đáo, nổi trội, tôi đã đặt hàng với GEON không chỉ
                        website mà cả toàn bộ hình ảnh, catalog, banner, poster, brochure cho các sản phẩm thuộc hệ
                        thống Spa của chúng tôi. Giải pháp marekting các bạn tư vấn và triển khai đã giúp 1 spa mới mở
                        của chúng tôi có được hơn 100 khách hàng thân thiết chỉ sau 2 tháng mở cửa. Đánh giá sự tận tâm,
                        am hiểu và sáng tạo của các bạn. Và tất nhiên tôi sẽ còn tiếp tục hợp tác với các bạn lâu dài…
                    </p>

                    <h4>Mrs. Thu Hương</h4>
                    <h5>SEN SPA</h5>

                    <!--Review-->
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                    <i class="fa fa-star"> </i>
                </div>

            </div>
            <!--/Third slide-->
        </div>
        <!--/.Slides-->

        <!--Controls-->
        <a class="left carousel-control" href="#carousel-example-1" role="button" data-slide="prev">
            <span class="icon-prev" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-1" role="button" data-slide="next">
            <span class="icon-next" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

</section>

<hr class="between-sections">


<!--Section: Magazine v.2-->
<section class="section magazine-section">

    <!--Section heading-->
    <h1 class="section-heading">GEON Việt</h1>

    <!--First row-->
    <div class="row text-xs-left">

        <!--First column-->
        <div class="col-lg-6 col-md-12">

            <!--Featured news-->
            <div class="single-news">

                <!--Image-->
                <div class="view overlay hm-white-slight">
                    <img src="http://mdbootstrap.com/images/slides/slide%20(10).jpg">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <div class="news-data">
                    <a href="" class="blue-text"><h5><i class="fa fa-globe"></i> SEO</h5></a>
                    <p><strong><i class="fa fa-clock-o"></i> 27/02/2016</strong></p>
                </div>

                <h3><a>4 điều bạn cần biết khi kết hợp kiểu chữ</a></h3>

                <p>Việc kết hợp các mặt chữ đôi khi rất thú vị! Nhưng cũng có lúc bạn cảm thấy chán nản với kết quả của
                    mình. Một thiết kế infographic tốt không nên quá 2 mặt chữ (typefaces) hay..
                </p>

            </div>
            <!--/Featured news-->

        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-lg-6 col-md-12">

            <!--Small news-->
            <div class="single-news">

                <div class="row">
                    <div class="col-md-3">

                        <!--Image-->
                        <div class="view overlay hm-white-slight">
                            <img src="http://mdbootstrap.com/images/regular/nature/img%20(75).jpg">
                            <a>
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>

                    <!--Excerpt-->
                    <div class="col-md-9">
                        <p><strong>26/02/2016</strong></p>
                        <a>Cách sử dụng quảng cáo Facebook Leads hiệu quả
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
            <!--/Small news-->

            <!--Small news-->
            <div class="single-news">

                <div class="row">
                    <div class="col-md-3">

                        <!--Image-->
                        <div class="view overlay hm-white-slight">
                            <img src="http://mdbootstrap.com/images/regular/nature/img%20(74).jpg">
                            <a>
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>

                    <!--Excerpt-->
                    <div class="col-md-9">
                        <p><strong>25/02/2016</strong></p>
                        <a>Tích hợp facebook chat vào website.
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
            <!--/Small news-->

            <!--Small news-->
            <div class="single-news">

                <div class="row">
                    <div class="col-md-3">

                        <!--Image-->
                        <div class="view overlay hm-white-slight">
                            <img src="http://mdbootstrap.com/images/regular/food/img%20(16).jpg">
                            <a>
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>

                    <!--Excerpt-->
                    <div class="col-md-9">
                        <p><strong>24/02/2016</strong></p>
                        <a>Chrome sẽ cảnh báo nếu người dùng truy cập vào website không dùng HTTPS từ 2017.
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
            <!--/Small news-->

            <!--Small news-->
            <div class="single-news">

                <div class="row">
                    <div class="col-md-3">

                        <!--Image-->
                        <div class="view overlay hm-white-slight">
                            <img src="http://mdbootstrap.com/images/regular/nature/img%20(77).jpg">
                            <a>
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>

                    <!--Excerpt-->
                    <div class="col-md-9">
                        <p><strong>23/02/2016</strong></p>
                        <a>Những điều cần biết về Google RankBrain.
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
            <!--/Small news-->

        </div>
        <!--/Second column-->

    </div>
    <!--/First row-->

</section>
<!--/Section: Magazine v.2-->


<hr class="between-sections">

<!-- xem mau thiet ke -->
<section>
    <h3 class="h2-responsive text-xs-center wow fadeIn">Bạn muốn quảng bá thương hiệu và phát triển kinh doanh của mình
        ?</h3>
    <br>
    <div class="text-xs-center wow fadeIn">
        <a href="/<?php echo ROUTE_CAC_MAU_THIET_KE ?>" class="btn btn-danger btn-lg waves-effect waves-light">Xem mẫu
            thiết kế</a>
    </div>
    <br/>

</section>

<style>
    #congnghevanentang .material-icons {
        font-size: 40px;
    }

    .gap {
        height: 50px;
    }

    .gap-min {
        height: 15px;
    }
</style>
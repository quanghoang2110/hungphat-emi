<style>
    /*@media (max-width: 1440px) {*/
        /*.fixed-sn .double-nav, .fixed-sn footer, .fixed-sn main {*/
            /*padding-left: 15px;*/
        /*}*/
    /*}*/

    #before-after-section #before-after {
        position: relative;
        width: 845px;
        height: 610px;
        border: 2px solid grey;
        margin: 20px auto 0px;
    }

    #before-after-section .view {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    #before-after-section .view-before {
        z-index: 1;
    }

    #before-after-section .view-after {
        z-index: 2;
    }

    #before-after-section #dragme {
        position: absolute;
        width: 5px;
        height: 100%;
        top: 0px;
        left: 0px;
        background-color: black;
        cursor: pointer;
        z-index: 3;
    }

    /*Controls*/

    #before-after-section ul {
        text-align: center;
        margin-top: 30px;
    }

    #before-after-section ul li {
        list-style: none;
        display: inline-block;
        font-size: 17px;
        cursor: pointer;
        margin: 0px 20px 0px 20px
    }

    @media only screen and (min-width: 1201px) {
        #before-after-section #before-after {
            width: 750px;
        }
    }

    @media only screen and (max-width: 1200px) {
        #before-after-section #before-after {
            width: 730px;
            margin-left: -27px;
        }
    }

    @media only screen and (max-width: 992px) {
        #before-after-section #before-after {
            width: 700px;
            margin-left: 0;
        }
    }

    @media only screen and (max-width: 768px) {
        .before-after {
            display: none;
        }
    }

    .flex-center {
        height: auto;
    }

    .wow {
        visibility: hidden;
    }

    .proj {
        margin-top: 30px;
        height: 210px;
    }

    .proj img {
        width: 100%;
        max-height: 210px;
    }

    .mdb-skin .btn-stc {
        background: #1C2331;
    }

    .mdb-skin .btn-dtc, .mdb-skin .card .btn-action {
        background: #154771;
    }

    hr {
        margin-top: 1rem;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, .1);
    }

    .between-sections {
        margin-bottom: 4rem;
    }
</style>
<?php $this->load->view($theme_template . 'header'); ?>

<!--/Double Navigation-->

<!--Main layout-->
<main class="">
    <div class="container-fluid">
        <?php $this->load->view($content); ?>
        <?php $this->load->view($theme_template . 'contact'); ?>
    </div>

</main>
<!--/Main layout-->


<!--Footer-->
<footer class="page-footer center-on-small-only">

    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">

            <!--First column-->
            <div class="col-md-1 col-md-offset-1"></div>
            <div class="col-md-3 col-md-offset-1">
                <h5 class="title">GEON Việt</h5>
                <p>Chúng tôi thiết kế những website chất lượng cao theo chuẩn SEO dễ sử dụng, bảo mật và thân thiện với
                    công cụ tìm kiếm, thiết kế web theo yêu cầu riêng của từng khách hàng.</p>
            </div>
            <!--/.First column-->

            <hr class="hidden-md-up">

            <!--Second column-->
            <div class="col-md-2 col-md-offset-1">
            </div>
            <!--/.Second column-->

            <hr class="hidden-md-up">

            <!--Third column-->
            <div class="col-md-2">
                <h5 class="title">GEON Việt</h5>
                <ul>
                    <li><a href="/#ve-chung-toi" onclick="scrollToId('ve_chung_toi')">Về chúng tôi</a></li>
                    <li><a href="/#cong-nghe-va-nen-tang" onclick="scrollToId('congnghevanentang')">Công nghệ và nền tảng</a></li>
                    <li><a href="/#lien-he-ngay" onclick="scrollToId('lienhengay')">Liên hệ</a></li>
                </ul>
            </div>
            <!--/.Third column-->

            <hr class="hidden-md-up">

            <!--Fourth column-->
            <div class="col-md-2">
                <h5 class="title">Dịch vụ</h5>
                <ul>
                    <li><a href="/#san-pham-va-dich-vu" onclick="scrollToId('sanphamvadichvu')">Sản phẩm và dịch vụ</a></li>
                    <li><a href="/#du-an-gan-day" onclick="scrollToId('duanganday')">Dự án</a></li>
                    <li><a href="/<?php echo ROUTE_CAC_MAU_THIET_KE ?>">Các mẫu thiết kê</a></li>
                </ul>
            </div>
            <!--/.Fourth column-->

        </div>
    </div>
    <!--/.Footer Links-->

    <hr>

    <!--Call to action-->
    <div class="call-to-action">
        <ul>
            <li>
                <h5>Bạn muốn phát triển thương hiệu cá nhân?</h5>
            </li>
            <li><a href="#lien-he-ngay" onclick="scrollToId('lienhengay')" class="btn btn-danger">Liên hệ ngay</a></li>
        </ul>
    </div>
    <!--/.Call to action-->

    <hr>

    <!--Social buttons-->
    <div class="social-section">
        <ul>
            <li><a class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"> </i></a></li>
            <li><a class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"> </i></a></li>
            <li><a class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"> </i></a></li>
            <li><a class="btn-floating btn-small btn-li"><i class="fa fa-linkedin"> </i></a></li>
            <li><a class="btn-floating btn-small btn-git"><i class="fa fa-github"> </i></a></li>
            <li><a class="btn-floating btn-small btn-pin"><i class="fa fa-pinterest"> </i></a></li>
            <li><a class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"> </i></a></li>
        </ul>
    </div>
    <!--/.Social buttons-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © 2016 Copyright: <a href="/"> GEONViet.com </a>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->
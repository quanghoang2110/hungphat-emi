<!--<header>-->

<ul id="slide-out" class="side-nav fixed custom-scrollbar ps-container ps-theme-default"
    data-ps-id="2212cc7e-532e-7262-1c85-e5612c7a9f9a" style="transform: translateX(0%);">

    <!-- Logo -->
    <li>
        <div class="logo-wrapper waves-light sn-avatar-wrapper waves-effect waves-light">
            <a href="/" class="text-xs-center">
                <i id="icon" class="fa fa-connectdevelop"
                   style="text-shadow: rgb(20, 36, 50) 0px 0px 0px, rgb(21, 37, 50) 1px 1px 0px, rgb(21, 37, 51) 2px 2px 0px, rgb(21, 38, 52) 3px 3px 0px, rgb(21, 38, 52) 4px 4px 0px, rgb(22, 39, 53) 5px 5px 0px, rgb(22, 39, 53) 6px 6px 0px, rgb(22, 40, 54) 7px 7px 0px, rgb(22, 40, 55) 8px 8px 0px, rgb(23, 40, 55) 9px 9px 0px, rgb(23, 41, 56) 10px 10px 0px, rgb(23, 41, 56) 11px 11px 0px, rgb(23, 42, 57) 12px 12px 0px, rgb(24, 42, 58) 13px 13px 0px, rgb(24, 43, 58) 14px 14px 0px, rgb(24, 43, 59) 15px 15px 0px, rgb(24, 44, 59) 16px 16px 0px, rgb(25, 44, 60) 17px 17px 0px, rgb(25, 44, 61) 18px 18px 0px, rgb(25, 45, 61) 19px 19px 0px, rgb(25, 45, 62) 20px 20px 0px, rgb(26, 46, 62) 21px 21px 0px, rgb(26, 46, 63) 22px 22px 0px, rgb(26, 47, 64) 23px 23px 0px, rgb(26, 47, 64) 24px 24px 0px, rgb(27, 48, 65) 25px 25px 0px, rgb(27, 48, 66) 26px 26px 0px, rgb(27, 48, 66) 27px 27px 0px, rgb(27, 49, 67) 28px 28px 0px, rgb(28, 49, 67) 29px 29px 0px, rgb(28, 50, 68) 30px 30px 0px, rgb(28, 50, 69) 31px 31px 0px, rgb(28, 51, 69) 32px 32px 0px, rgb(29, 51, 70) 33px 33px 0px, rgb(29, 52, 70) 34px 34px 0px; font-size: 80px; color: rgb(255, 255, 255); height: 150px; width: 150px; line-height: 150px; border-radius: 0%; text-align: center; background-color: rgb(29, 52, 71);"></i>
            </a>
        </div>
    </li>
    <!--/. Logo -->

    <!--Social-->
    <li>
        <ul class="social">
            <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a></li>
            <li><a class="icons-sm git-ic"><i class="fa fa-github"> </i></a></li>
            <li><a class="icons-sm li-ic"><i class="fa fa-linkedin"> </i></a></li>
            <li><a class="icons-sm yt-ic"><i class="fa fa-youtube"> </i></a></li>
            <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a></li>
        </ul>
    </li>
    <!--/Social-->

    <!-- Side navigation links -->
    <li>
        <ul class="collapsible collapsible-accordion">
            <li>
                <a href="/" class="collapsible-header waves-effect arrow-r"><i class="fa fa-bank"></i> Trang chủ</a>
            </li>
            <li id="mn_mauthietke"
                class="<?php echo in_array($menu_current, array('cacmauthietke')) ? 'current' : '' ?>">
                <a href="#mau-thiet-ke" class="collapsible-header waves-effect arrow-r"><i
                        class="fa fa-newspaper-o"></i> Mẫu thiết
                    kế <i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <?php foreach ($menu as $mitem): ?>
                            <li class="<?php echo isset($cate_id) && $mitem->id == $cate_id ? 'current' : '' ?>">
                                <a href="<?php echo get_detail_url('', $mitem->name, $mitem->id) ?>"
                                   onclick="scrollToId('ve_chung_toi')"
                                   class="waves-effect"><?php echo $mitem->name ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </li>
            <li>
                <a href="/#du-an-gan-day" onclick="scrollToId('duanganday')"
                   class="collapsible-header waves-effect arrow-r"><i class="fa fa-newspaper-o"></i> Dự án</a>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-info"></i> Giới thiệu<i
                        class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body" style="">
                    <ul>
                        <li>
                            <a href="/#ve-chung-toi" onclick="scrollToId('ve_chung_toi')" class="waves-effect">Về chúng
                                tôi</a>
                        </li>
                        <li>
                            <a href="/#san-pham-va-dich-vu" onclick="scrollToId('sanphamvadichvu')"
                               class="waves-effect">Sản phẩm và dịch vụ</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-bar-chart"></i> SEO - Marketing<i
                        class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body" style="">
                    <ul>
                        <li><a href="#" class="waves-effect">How to start?</a>
                        </li>
                        <li><a href="#" class="waves-effect">Step by step</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-pencil"></i> Liên hệ<i
                        class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body" style="">
                    <ul>
                        <li><a href="#" class="waves-effect">Câu hỏi thường gặp</a>
                        </li>
                        <li data-toggle="modal" data-target="#modal-contact">
                            <a href="#" class="waves-effect">Liên hệ với chúng tôi</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <!--/. Side navigation links -->

    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
</ul>

<!--Navbar-->
<nav class="navbar navbar-fixed-top scrolling-navbar double-nav">
    <!-- SideNav slide-out button -->
    <div class="pull-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
    </div>
    <!-- Breadcrumb-->
    <div class="breadcrumb-dn">
        <p>GEON Việt</p>
    </div>


    <!--Navigation icons-->
    <ul class="nav navbar-nav pull-right">
        <li class="nav-item ">
            <a class="nav-link waves-effect waves-light" href="/<?php echo ROUTE_CAC_MAU_THIET_KE ?>"><i
                    class="fa fa-newspaper-o"></i> <span class="hidden-sm-down">Mẫu thiết kế</span></a>
        </li>
        <li class="nav-item " data-toggle="modal" data-target="#modal-contact">
            <a href="#lien-he-ngay" class="nav-link waves-effect waves-light"> <i
                    class="fa fa-envelope-o"></i> <span class="hidden-sm-down">Liên hệ</span></a></li>
        <li class="nav-item "><a href="#ho-tro"
                                 class="nav-link waves-effect waves-light"><i class="fa fa-comments-o"></i><span
                    class="hidden-sm-down">Hỗ trợ</span></a></li>
    </ul>
</nav>
<!--/.Navbar-->

<!--</header>-->

<style>
    li.current > a {
        background-color: #294a65;
        transition: all .3s linear;
    }

    .mdb-skin .side-nav .logo-wrapper,
    .graphite-skin .side-nav .logo-wrapper {
        /* background: url(http://mdbootstrap.com/images/regular/nature/img%20(67).jpg) center center no-repeat; */
        background-size: cover;
        background: #1d3447;
        height: 150px;
    }

    #icon {
        margin-top: -30px;
        margin-left: -190px;
    }
</style>

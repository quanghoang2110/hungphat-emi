<script>
    <?php if ($menu_current == "cacmauthietkedetail"): ?>
    $(document).ready(function () {
        $('#mn_mauthietke>a').click();
    });
    <?php endif; ?>


    function scrollToId(id) {
        if (typeof (id) == 'undefined') return true;
        $('html, body').animate({
            scrollTop: ($("#" + id).offset().top - 120)
        }, 500);
    }
    $("body").addClass("fixed-sn mdb-skin");
    // SideNav Options
    //    $(".button-collapse").sideNav();
</script>
<script>
    // Animations init
    new WOW().init();

    // Tooltip init

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });


    // MDB Lightbox Init
    $(function () {
//        $("#mdb-lightbox-ui").load("http://mdbootstrap.com/wp-content/themes/mdbootstrap4/mdb-addons/mdb-lightbox-ui.html");
    });

    $(document).ready(function () {

        var hash = location.hash;
        if (hash) {
            var idScroll = hash.replace(/-|#/g, '');
            scrollToId(idScroll.toString());
        }

        $("#license").addClass("mdb-select price-select");
        $("#billing_country").addClass("mdb-select");
        $('.mdb-select').material_select();
        $("#license").hide();
        $("#billing_country").hide();
    });

    // SideNav init
    $(".button-collapse").sideNav();
    var el = document.querySelector('.custom-scrollbar');
    Ps.initialize(el);

    $('body').scrollspy({
        target: '#scrollspy'
    });

    $(function () {
        $(".sticky").sticky({
            topSpacing: 90,
            zIndex: 2,
            stopper: "#footer"
        });
    });

    HighlightCurrentItemAndParent("active");
    window.onload = function () {
        // Initialize collapse button
        $('.collapsible').collapsible();
        HighlightCurrentItemAndParent("active");
        setTimeout(function () {
            HighlightCurrentItemAndParent("active");
        }, 100);
        setTimeout(function () {
            HighlightCurrentItemAndParent("active");
        }, 500);
        setTimeout(function () {
            HighlightCurrentItemAndParent("active");
        }, 1000);
    };

    function findUpTag(el, tag) {
        while (el.parentNode) {
            el = el.parentNode;
            if (el.tagName === tag)
                return el;
        }
        return null;
    }

    function HighlightCurrentItemAndParent(matchClass) {
        $('#side-menu').find('li').each(function () {
            if ((" " + this.className + " ").indexOf(" " + matchClass + " ") > -1) {
                var a = findUpTag(this, "DIV"); // search <a ...>
                if (a) {
                    a.style.display = 'block';
                    b = a.parentNode;
                    c = b.getElementsByTagName("A")[0];
                    c.className = c.className + " active";
                    d = c.parentNode;
                    d.className = d.className + " active";
                    a.style.display = 'block';
                }
            }
        });
    }
</script>

<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/utils/Draggable.min.js"></script>

<script>
    $(function () {

        wow = new WOW({
            boxClass: 'wow', // default
            animateClass: 'animated', // default
            offset: 0, // default
            mobile: true, // default
            live: true // default
        });
        wow.init();

        /*
         Dependencies : TweenMax and Draggable
         Images by Ivaylo Yovchev ( http://www.ivayloyovchev.com/ )

         Test on touch device @ http://cloud.bassta.bg/before-after.html
         */


        var $dragMe = $("#dragme");
        var $beforeAfter = $("#before-after");
        var $viewAfter = $(".view-after");

        Draggable.create($dragMe, {
            type: "left",
            bounds: $beforeAfter,
            onDrag: updateImages
        });

        function updateImages() {
            TweenLite.set($viewAfter, {width: $dragMe.css("left")});		//or this.x if only dragging
        }

        //Intro Animation
        animateTo(350);

        //Externall nav
        $(".to-start").on("click", function () {
            animateTo(0);
        });

        $(".to-middle").on("click", function () {
            animateTo(298);
        });

        $(".to-end").on("click", function () {
            animateTo(598);
        });

        function animateTo(_left) {
            TweenLite.to($dragMe, 1, {left: _left, onUpdate: updateImages});
        }

        //V2 Click added
        $beforeAfter.on("click", function (event) {
            var eventLeft = event.clientX - $beforeAfter.offset().left;
            animateTo(eventLeft);
        });


    });//end jQuery wrapper

</script>

<script src="/assets/base/lib/jquery/jquery.form.min.js"></script>

<script>
    var cResponse = false;
    var onloadCallback = function () {
        grecaptcha.render('html_element', {
            'sitekey': '6LeGVQkUAAAAAF2Xe-im34youj38RHcJSEVdJOiw',
            'callback': verifyCallback
        });
    };
    var verifyCallback = function (response) {
        cResponse = response;
        if (response) {
            $('#btnSubmitNow').removeClass('disabled');
            $('#submitErr').text('');
            $('#html_element>div').removeClass('error');
        }
    };

    function checkSubmit() {
        if (cResponse)
            return true;
        else {
            errorCaptcha();
            return false;
        }
    }
    function errorCaptcha() {
        $('#html_element>div').addClass('error');
        $('#submitErr').text("Vui lòng xác thực");
    }
    function errorSubmit(msg) {
        $('#modal-thongbao').modal('hide');
        $('#modal-contact').modal('show');
        $('#txt_err').text(msg);
    }

    $(document).ready(function () {

        var options = {
            beforeSubmit: showRequest,  // pre-submit callback
            success: showResponse  // post-submit callback
        };

        $('form').ajaxForm(options);
    });

    function showRequest() {
        $('#txt_err').text('');
        $("#modal-contact").modal("hide");
        $("#modal-thongbao").modal("show");
    }

    function showResponse(response) {
        $('#progress').hide();
        var obj = $.parseJSON(response);
        if (obj.status == 0) {
            errorCaptcha();
        } else if (obj.status == -1) {
            errorSubmit(obj.msg);
        } else {
            $("#modal-contact").modal("hide");
            $("#modal-thongbao").modal("show");
            $('#success').removeClass();
            $('#modal-thongbao').on('hidden.bs.modal', function (e) {
                location.href = "/";
            })
        }
    }
</script>
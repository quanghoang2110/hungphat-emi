<!DOCTYPE html>
<html>
<head>
    <title>Demo Preview</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content=""/>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/assets/base/lib/mdb-pro/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/base/lib/mdb-pro/css/mdb.min.css">

    <link rel="stylesheet" href="/assets/base/lib/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/frontend/geon/css/theme.css">

    <link rel="stylesheet" href="/assets/base/lib/mdl/material.font.css">

    <script>
        var cResponse = false;
    </script>
    <style>
        html, body {
            overflow: hidden !important;
        }

        main {
            height: 100%;
        }

        #btn-up {
            position: absolute;
            top: 0;
            right: 0;
            height: 50px;
            padding: 0 10px;
            z-index: 1000;
            color: red !important;
            font-size: 45px;
            /* border-left: 1px solid #ccc; */
            background-color: #eceff1;
            box-shadow: 0 0 1px #666;
            border-radius: 0;
        }

        button.back i {
            font-size: 25px;
        }

        button.back {
            margin: 0;
        }

        div.error {
            margin-bottom: 5px;
        }

        div.error iframe {
            border: 1px solid red;
        }

        p.error {
            color: red;
        }

        .card {
            border: 0;
            box-shadow: none;
        }

        .card .card-circle {
            border: 2px solid #e0e0e0;
            height: 90px !important;
            width: 90px !important;
            margin-bottom: 2rem;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
        }

        .card .card-circle, .card-overlay, .flex-center, .section.team-section .avatar, .testimonial-carousel .testimonial .avatar {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
    </style>
    <script>
        var onloadCallback = function () {
            grecaptcha.render('html_element', {
                'sitekey': '6LeGVQkUAAAAAF2Xe-im34youj38RHcJSEVdJOiw',
                'callback': verifyCallback
            });
        };
        var verifyCallback = function (response) {
            cResponse = response;
            if (response) {
                $('#btnSubmitNow').removeClass('disabled');
                $('#submitErr').text('');
                $('#html_element>div').removeClass('error');
            }
        };

        function checkSubmit() {
            if (cResponse)
                return true;
            else {
                errorCaptcha();
                return false;
            }
        }
        function errorCaptcha() {
            $('#html_element>div').addClass('error');
            $('#submitErr').text("Vui lòng xác thực");
        }
        function errorSubmit(msg) {
            $('#modal-thongbao').modal('hide');
            $('#modal-contact').modal('show');
            $('#txt_err').text(msg);
        }
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</head>
<body itemscope id="demo-page">
<main class="">
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MS2BNB" height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <div class="relative js-demo-upper-menu js-template-id " id="headerlivedemo">
        <div id="advanced">
            <div class="bg">
                <div class="top_container live-container">
                    <button class="btn-flat waves-effect back" onclick="location.href='/<?php echo $this->uri->segment(1); ?>';"><i
                                class="fa fa-angle-left"></i></button>
<!--                    <a class="brand brand_livedemo" href="/--><?php //echo $this->uri->segment(1); ?><!--">-->
<!--                        Xem các mẫu khác-->
<!--                    </a>-->
                    <div class="name_template">
                        <h1 title="<?php echo $info->name ?>"><?php echo $info->name ?><?php echo '#' . $info->id ?></h1>
                    </div>
                    <div class="responsive-block">
                        <ul id="responsivator">
                            <li title="Xem trên máy tính" class="response active" id="desktop"></li>
                            <li title="Xem trên máy tính bảng" class="response" id="tablet-portrait"></li>
                            <li title="Xem trên máy tính bảng" class="response" id="tablet-landscape"></li>
                            <li title="Xem trên điện thoại" class="response" id="iphone-portrait"></li>
                            <li title="Xem trên điện thoại" class="response" id="iphone-landscape"></li>
                        </ul>
                    </div>
                    <div class="topbar_info">
                        <!--                        <button class="btn-flat "-->
                        <div class="btn-group buy_now waves-effect" data-toggle="modal" data-target="#modal-contact">
                            <span class="button btn-important tm-icon js-demo-buy-button js-btn"><i
                                    class="fa fa-shopping-cart"></i> &nbsp;Chọn mẫu website
                            này</span>
                        </div>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>

        <span id="btn-up" class="waves-effect"><i class="fa fa-angle-up"></i> </span>

    </div>
    <div id="iframelive" class="">
        <div id="frameWrapper">
            <iframe id="frame" src="<?php echo $info->link ?>">
                [Your user agent does not support frames or is currently configured not to display frames. However,
                you
                may
                visit the related document.]
            </iframe>
        </div>
    </div>

    <div class="modal fade modal-ext" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <form action="/<?php echo ROUTE_CONTACT_NOW ?>" method="post" onsubmit="return checkSubmit(); ">
                    <!--Header-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3><i class="fa fa-check-square-o"></i> Chọn mẫu website #<?php echo $info->id ?></h3>
                    </div>
                    <!--Body-->
                    <div class="modal-body">
                        <p>Vui lòng để lại thông tin cá nhân, chúng tôi sẽ gọi lại và tư vấn giúp bạn.</p>
                        <br>
                        <p class="error" id="txt_err"></p>
                        <div class="md-form">
                            <i class="fa fa-user prefix"></i>
                            <input name="fullname" type="text" id="form22" class="form-control"
                                   data-msg-required="Vui lòng cho biết tên của bạn" required/>
                            <label for="form42">Tên của bạn</label>
                        </div>

                        <div class="md-form">
                            <i class="fa fa-phone prefix"></i>
                            <input name="phone" type="text" id="form32" class="form-control"
                                   data-msg-required="Chúng tôi sẽ liên hệ với qua sđt này" required/>
                            <label for="form34">Số điện thoại</label>
                        </div>

                        <div class="md-form">
                            <i class="fa fa-pencil prefix"></i>
                            <textarea name="note" type="text" id="form8" class="md-textarea"></textarea>
                            <label for="form8">Nội dung</label>
                        </div>

                        <div class="md-form">
                            <div id="html_element"></div>
                            <div id="submitErr" class="text-danger"></div>
                        </div>

                        <div class="text-xs-center">
                            <button id="btnSubmitNow" type="submit" class="btn btn-primary disabled">Đồng ý</button>
                            <div class="call">
                                <p>Hoặc gọi trực tiếp tới? <span class="cf-phone"><i
                                            class="fa fa-phone"> +<?php echo PHONE_CONTACT ?></i></span></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--/.Content-->
        </div>
    </div>

    <div class="modal fade modal-ext" id="modal-thongbao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3><i class="fa fa-check-square-o"></i> Chọn mẫu website #<?php echo $info->id ?></h3>
                </div>
                <!--Body-->
                <div class="modal-body">

                    <div id="progress" class="flex-center">

                        <div class="preloader-wrapper big active">
                            <div class="spinner-layer spinner-blue-only">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div><div class="gap-patch">
                                    <div class="circle"></div>
                                </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="success" class="hiddendiv">
                        <div class="card flex-center">
                            <div class="card-block">
                                <div class="card-circle">
                                    <i class="fa fa-check green-text"></i>
                                </div>
                            </div>
                        </div>

                        <h4>Chúng tôi sẽ liên hệ với bạn.</h4>
                        <h4>Xin cảm ơn.</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Đóng</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
</main>
<script src="/assets/base/lib/mdb-pro/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script>
    $("#responsivator li").click(function () {
        $("#responsivator li").removeClass('active');
        $(this).addClass('active');
        $("#iframelive").removeClass().addClass($(this).attr('id'));
    });

    $("#btn-up").click(function () {
        $("#advanced").slideToggle();
        $(this).find("i").toggleClass("fa-angle-down", 1000);
    });

    // wait for the DOM to be loaded
    $(document).ready(function () {

        var options = {
            beforeSubmit: showRequest,  // pre-submit callback
            success: showResponse  // post-submit callback
        };

        $('form').ajaxForm(options);
    });

    function showRequest() {
        $('#txt_err').text('');
        $("#modal-contact").modal("hide");
        $("#modal-thongbao").modal("show");
    }

    function showResponse(response) {
        $('#progress').hide();
        var obj = $.parseJSON(response);
        if (obj.status == 0) {
            errorCaptcha();
        } else if (obj.status == -1) {
            errorSubmit(obj.msg);
        } else {
            $("#modal-contact").modal("hide");
            $("#modal-thongbao").modal("show");
            $('#success').removeClass();
            $('#modal-thongbao').on('hidden.bs.modal', function (e) {
                location.href = "/";
            })
        }
    }
</script>

<script src="/assets/base/lib/jquery/jquery.form.min.js"></script>
<script src="/assets/base/lib/mdb-pro/js/tether.min.js"></script>
<script src="/assets/base/lib/mdb-pro/js/bootstrap.min.js"></script>
<script src="/assets/base/lib/mdb-pro/js/mdb.min.js"></script>

</body>
</html>

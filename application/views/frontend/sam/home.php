<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<div>
<!--    <div class="container-title text-center">-->
<!--        <p>HỆ THỐNG EFFORTLESS ENGLISH ĐỘC QUYỀN TẠI VIỆT NAM</p>-->
<!--    </div>-->
    <div class="content-second">
        <div class="container">
<!--            <div class="col-md-6 col-lg-7" align="center">-->
<!--                <iframe width="100%" height="400" src="https://www.youtube.com/embed/Qf9z6Nc8Sjw"-->
<!--                        class="content-second-video" frameborder="0" allowfullscreen></iframe>-->
<!--            </div> -->
            <div class="col-md-6 col-lg-7" align="center">
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/O1qoMmMAAXU"
                        class="content-second-video" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6 col-lg-5 content-second-list">
                <p><span class="glyphicon glyphicon-ok"></span><strong>Nói tiếng anh tự động như trẻ em</strong></p>
                <p><span class="glyphicon glyphicon-ok"></span><strong>Tăng cường sự tự tin và thoải mái khi giao
                        tiếp</strong></p>
                <p><span class="glyphicon glyphicon-ok"></span><strong>Phát âm tiếng anh chuẩn như người bản
                        ngữ</strong></p>
                <p><span class="glyphicon glyphicon-ok"></span><strong>Thổi bùng niềm đam mê yêu thích tiếng
                        anh</strong></p>
                <div class="container-fluid bg-2 text-center">
                    <?php if ($this->session->userdata(FRONTEND_SESSION_USERNAME)): ?>
                        <a href="/khoa-hoc" class="btn btn-default btn-lg">
                            Đăng ký ngay
                        </a>
                    <?php else : ?>
                        <a href="#" class="btn btn-default btn-lg" data-toggle="modal" data-target="#modalDangKy">
                            Đăng ký ngay
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-third">
        <div class="container">
            <div class="container-fluid bg-3 text-center">
                <h3><strong>TẠI SAO NÊN HỌC WOW ENGLISH?</strong></h3>
                <div class="content-third-fsize">

                    <div class="col-sm-4">
                        <img src="/assets/frontend/wow_english/img/1-13.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Phương pháp đã giúp hơn 10 triệu người trên thế giới</strong></h4>
                    </div>
                    <div class="col-sm-4">
                        <img src="/assets/frontend/wow_english/img/1-14.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Bộ bài học Effotless English mới độc quyền tại Việt Nam</strong></h4>
                    </div>
                    <div class="col-sm-4">
                        <img src="/assets/frontend/wow_english/img/1-15.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Hoc trực tiếp cùng tác giả A.J Hoge</strong></h4>
                    </div>
                    <div class="col-sm-4 content-third-row" style="clear: both;">
                        <img src="/assets/frontend/wow_english/img/1-16.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Bài học, câu lạc bộ và cơ hội gặp A.J Hoge trong cùng 1 tài khoản</strong></h4>
                    </div>
                    <div class="col-sm-4 content-third-row">
                        <img src="/assets/frontend/wow_english/img/1-17.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Hệ thống học Effortless English Online lần đầu tiên tại Việt Nam</strong></h4>
                    </div>
                    <div class="col-sm-4 content-third-row">
                        <img src="/assets/frontend/wow_english/img/1-18.png" class="content-third-image"
                             style="max-width: 50% !important;" alt="Image">
                        <h4><strong>Giáo viên và trợ giảng hỗ trợ thực hành</strong></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="work pro1">
        <div class="container">
            <h3 class="text-center camnhanhocvien"><strong>Tin tức nổi bật</strong></h3>
            <div class="row">
                <?php foreach ($news as $index => $item): ?>
                    <?php if ($index == 0) { ?>
                        <div class="col-sm-6 col-xs-12 first-block item">
                            <div class="img-overlay">
                                <div class="home-news-item1">
                                    <?php echo get_img_tag($item->image, $item->title, 'img-responsive', IMG_NEWS_DEFAULT) ?>
                                </div>
                                <div class="txt-title">
                                    <?php echo $item->title ?>
                                </div>
                                <a href="<?php echo get_news_url($item->title, $item->id); ?>">
                                    <div class="overlay">

                                        <div class="txt-desc">
                                            <div class="title">
                                                <?php echo $item->title ?>
                                            </div>
                                            <?php echo $item->desc; ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-3 col-xs-6 item">
                            <div class="img-overlay">
                                <div class="home-news-item">
                                    <?php echo get_img_tag($item->image, $item->title, 'img-responsive', IMG_NEWS_DEFAULT) ?>
                                </div>
                                <div class="txt-desc txt-title">
                                    <?php echo $item->title ?>
                                </div>
                                <a href="<?php echo get_news_url($item->title, $item->id); ?>">
                                    <div class="overlay">

                                        <div class="txt-desc">
                                            <div class="title">
                                                <?php echo $item->title ?>
                                            </div>
                                            <?php echo $item->desc; ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="row comments">
        <h3 class="camnhanhocvien text-center"><strong>Cảm nhận của học viên</strong></h3>
        <div class="comment-x">
            <div class="container">
                <?php foreach ($comments as $index => $student) { ?>
                    <div class="col-md-6 student">
                        <div class="avatar">
                            <img src="<?php echo $student->image; ?>"
                                 class="img-responsive img-circle student-image" alt="avatar">
                        </div>
                        <div class="txt-content text-center">
                            <div class="student-description"><?php echo $student->content; ?></div>
                            <img src="/assets/frontend/wow_english/img/thanhngang.png"
                                 class="img-responsive img-circle gachduoi" alt="">
                            <p class="student-name"><strong><?php echo $student->name; ?></strong></p>
                            <p class="student-major"><?php echo $student->regency ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

</div>
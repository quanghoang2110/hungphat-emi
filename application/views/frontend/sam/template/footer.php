<!-- footer -->
<div id="footer">
    <?php if (!isset($lession)) { ?>
        <div id="feature1">
            <img src="/images/mycom.png" alt="" class="center-blog text-center"
                 id="footer1">
            <div class="container relative">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 col-xs-12 text-center">
                    <h3>Đừng bỏ lỡ cơ hội học tiếng anh của bạn</h3>
                    <div class="form-group col-xs-12 col-sm-5 col-sm-offset-3" id="dangki">
                        <button class="btn btn-lg btn-block text-center font-weight-700" data-toggle="modal"
                                data-target="#modalDangKy">Tham gia ngay
                        </button>
                    </div>

                </div>

                <div class="col-sm-3 hidden-xs"></div>
            </div>
            <img src="/images/bang.png" alt="" class="center-blog text-center"
                 width="100%" id="dongho">
        </div>
    <?php } ?>
    <div id="feature3">
        <div class="container">
            <div class="col-sm-4">
                <h3 class="f-title"><?php echo $site_name ?></h3>
                <ul class="nav-link">
                    <li><a href="/gioi-thieu">Giới thiệu</a></li>
                    <li><a href="/tin-tuc">Tin tức</a></li>
                    <li><a href="/lien-he">Liên hệ</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
                <h3 class="f-title"><?php echo $company_name ?></h3>
                <ul class="nav-link">
                    <li><i class="fa fa-map-marker"></i> <?php echo $company_address ?></li>
                    <li><i class="fa fa-envelope"></i> <?php echo $company_email ?></li>
                    <li><i class="fa fa-phone-square"></i> <?php echo $company_phone ?></li>
                    <li><i class="fa fa-clock-o"></i> <?php echo $company_time ?></li>
                </ul>
            </div>
            <div class="col-sm-4">
                <h3 class="f-title">Theo dõi chúng tôi</h3>
                <ul class="nav-link-social">
                    <li><a href="<?php echo $fb_fanpage ?>" target="_blank"><img src="/images/1-29.png" height="32"
                                                                                 width="auto"/> </a></li>
                    <li><a href="<?php echo $google_plus ?>" target="_blank"><img src="/images/1-32.png" height="30"
                                                                                  width="auto"/> </a></li>
                </ul>
                <div class="clearfix"></div>
                <img
                    src="http://hoceffortlessenglish.com/public/templates/public/default/images/bocongthuong_md.png"
                    alt="" width="auto" height="60px">
            </div>

        </div>
        <div class="nav navbar-fixed-bottom" id="btn-top">
            <a href="" onclick="return false;"><span class="glyphicon glyphicon-chevron-up"
                                                     id="icon-back-top"></span></a>
        </div>
        <div class="container">
            <div id="congty">
                <span> 	&copy;</span><i>2015 Wow English-Học tiếng anh giao tiếp</i>
            </div>
        </div>
    </div>
</div>
<!-- end footer -->
<script src="<?php echo base_url('assets/backend/plugins/select2/select2.full.min.js'); ?>"></script>
<script type="text/javascript" src="/assets/frontend/sam/js/slick/slick.js"></script>
<script src="<?php echo $theme_assets ?>js/login.js"></script>
<script>
    $('.single-item').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 1000,
        speed: 1000

    });
</script>
<div id="fb-root"></div>
<script>
    //preload after 5s
    setTimeout(function () {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=493774460723389";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }, 5000);
</script>
<script src="<?php echo $theme_assets ?>js/ductm.js"></script>
<script src="<?php echo $theme_assets ?>js/tuyen.js"></script>
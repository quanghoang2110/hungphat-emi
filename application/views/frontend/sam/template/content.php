<?php $this->load->view($theme_template . 'modal'); ?>
<div id="page">
    <!--start header-->
    <div id="wrapper-header">
        <?php $this->load->view($theme_template . 'header'); ?>
        <a id="back-top" style="display: block;"></a>
    </div>
        <?php $this->load->view($content); ?>
    <?php
    if (isset($paging)) : ?>
        <br/>
        <div class="main-width">
            <div class="col-sm-12 no-padding" style="margin-top:10px;">
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <?php echo $paging ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

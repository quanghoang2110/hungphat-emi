<?php if (!isset($lession)) { ?>
    <div id="header" class="box-header">
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-9 text-left hidden-xs">
                        <div class="welcome_text">
                            <span><a href="tel:<?php echo $company_phone?>" class="white"><i class="fa fa-phone"
                                     aria-hidden="true"></i>&nbsp;  Gọi cho chúng tôi <b><?php echo $company_phone?></b></a></span>
                        </div>
                        <ul class="social">
                            <li><a href="mailto:<?php echo $company_email ?>"><img src="/images/mail.png"></a></li>
                            <li><a href="<?php echo $youtube ? $youtube : '#youtube';?>" target="_blank"><img
                                        src="/images/youtube.png"></a></li>
                            <li><a href="<?php echo $fb_fanpage ? $fb_fanpage : '#facebook';?>" target="_blank""><img
                                        src="/images/facebook.png"></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 text-right hidden-xs row header_user">
                        <ul class="taikhoan">
                            <li class="noidungtk">
                                <div class="user_avartar">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                        <span class="glyphicon glyphicon-edit icon-color-color-white"></span>
                                        <a href="#" data-toggle="modal" data-target="#modalDangKy"
                                           class="font-size-14 icon-color-color-white"> Đăng ký</a>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                        <span class="glyphicon glyphicon-log-in icon-color-white"></span><a
                                            href="#" data-toggle="modal" data-target="#modalDangNhap"
                                            class="font-size-14 icon-color-color-white"> Đăng nhập</a>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>


                    <!--mobile-->
                    <div class="col-xs-6 text-left mobile_topbar left hidden-md hidden-sm hidden-lg">
                        <div class="hidden-lg hidden-md">
                            <div class="mb-menu-title">
                                <a href="javascript:void(0);" class="bars-navigation"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right mobile_topbar right hidden-md hidden-sm hidden-lg">
                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#modalDangKy">Đăng ký</a></li>

                            <li><a href="#" data-toggle="modal" data-target="#modalDangNhap">Đăng nhập</a></li>

                        </ul>
                    </div>


                </div>
            </div>
        </div>

        <!-- menubar -->

        <?php $c_menu = isset($current_menu) ? $current_menu : 'nox'; ?>
        <div class="desktop_menu">
            <div class="container">
                <div class="row  max_height_menu">
                    <div class="left col-md-5 col-sm-5 hidden-xs ">
                        <ul class="top_menu top_menu-x left pull-right link_text_menu">
                            <?php foreach ($menu as $index => $value): ?>
                                <?php if ($index < 3): ?>
                                    <li class="has_sub <?php echo $c_menu == $value->slug ? 'active' : '' ?>">
                                        <a href="<?php echo base_url($value->slug) ?>"><?php echo $value->name ?></a>
                                        <?php if ($value->sub): ?>
                                            <ul class="dropdown_menu">
                                                <?php foreach ($value->sub as $item) { ?>
                                                    <li><a href="<?php echo $item->url ?>"><?php echo $item->name ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="logogiua col-md-2 col-sm-2 hidden-xs text-center">
                        <a href="/" class="logo" id="site-logo"><img src="<?php echo $logo ?>"></a>
                        <!--                        <div class="txt-logo">WOW English</div>-->
                    </div>
                    <div class="right col-md-5 col-sm-5 hidden-xs ">
                        <ul class="top_menu top_menu-x right pull-left link_text_menu">
                            <?php foreach ($menu as $index => $value): ?>
                                <?php if ($index >= 3): ?>
                                    <li class="has_sub <?php echo $c_menu == $value->slug ? 'active' : '' ?>">
                                        <a href="<?php echo base_url($value->slug) ?>"><?php echo $value->name ?></a>
                                        <?php if ($value->sub): ?>
                                            <ul class="dropdown_menu <?php if ($index == count($menu) - 1) echo "last"; ?>">
                                                <?php foreach ($value->sub as $item) { ?>
                                                    <li><a href="<?php echo $item->url ?>"><?php echo $item->name ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile_menu background-white">
            <div class="container">
                <div class="row">
                    <div class="mobile-menu col-xs-12 col-sm-12 hidden-md hidden-lg">
                        <div class="mobimenu" style="display: none">
                            <ul class="list-unstyled main_menu">
                                <?php foreach ($menu as $index => $value): ?>
                                    <li class="<?php echo $c_menu == $value->slug ? 'active' : '' ?>">
                                        <a href="<?php echo base_url($value->slug) ?>"><?php echo $value->name ?></a>
                                        <?php if ($value->sub): ?>
                                            <ul class="dropdown_menu">
                                                <?php foreach ($value->sub as $item) { ?>
                                                    <li><a href="<?php echo $item->url ?>"><?php echo $item->name ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end menubar -->


    </div>
    <!-- end header -->
<?php } ?>
<script>

    jQuery(function ($) {
        "use strict";
        //Mobile menu
        $('.bars-navigation').click(function () {
            if ($('.mobimenu').is(":hidden")) {
                $('.mobimenu').slideDown("fast");
            } else {
                $('.mobimenu').slideUp("fast");
            }
            return false;
        });

        $(".category").accordion({
            accordion: false,
            speed: 300,
            closedSign: '+',
            openedSign: '-'
        });
        $(".mobile-menu").accordion({
            accordion: false,
            speed: 300,
            closedSign: '+',
            openedSign: '-'
        });
    });
    (function ($) {
        $.fn.extend({
            accordion: function (options) {
                var defaults = {
                    accordion: 'true',
                    speed: 300,
                    closedSign: '[-]',
                    openedSign: '[+]'
                };
                var opts = $.extend(defaults, options);
                var $this = $(this);
                $this.find("li").each(function () {
                    if ($(this).find("ul").size() != 0) {
                        $(this).find("a:first").after("<em>" + opts.closedSign + "</em>");
                        if ($(this).find("a:first").attr('href') == "#") {
                            $(this).find("a:first").click(function () {
                                return false;
                            });
                        }
                    }
                });
                $this.find("li em").click(function () {
                    if ($(this).parent().find("ul").size() != 0) {
                        if (opts.accordion) {
                            //Do nothing when the list is open
                            if (!$(this).parent().find("ul").is(':visible')) {
                                parents = $(this).parent().parents("ul");
                                visible = $this.find("ul:visible");
                                visible.each(function (visibleIndex) {
                                    var close = true;
                                    parents.each(function (parentIndex) {
                                        if (parents[parentIndex] == visible[visibleIndex]) {
                                            close = false;
                                            return false;
                                        }
                                    });
                                    if (close) {
                                        if ($(this).parent().find("ul") != visible[visibleIndex]) {
                                            $(visible[visibleIndex]).slideUp(opts.speed, function () {
                                                $(this).parent("li").find("em:first").html(opts.closedSign);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        if ($(this).parent().find("ul:first").is(":visible")) {
                            $(this).parent().find("ul:first").slideUp(opts.speed, function () {
                                $(this).parent("li").find("em:first").delay(opts.speed).html(opts.closedSign);
                            });
                        } else {
                            $(this).parent().find("ul:first").slideDown(opts.speed, function () {
                                $(this).parent("li").find("em:first").delay(opts.speed).html(opts.openedSign);
                            });
                        }
                    }
                });
            }
        });
    })(jQuery);

    jQuery(document).ready(function ($) {
        if ($("#btn-top").length > 0) {
            $(window).scroll(function () {
                var e = $(window).scrollTop();
                if (e > 300) {
                    $("#btn-top").fadeIn('slow');
                } else {
                    $("#btn-top").fadeOut('slow');
                }
            });
            $("#btn-top").click(function () {
                $('body,html').animate({
                    scrollTop: 0
                })
            })
        }
    });

    window.onscroll = function () {
        siteScroll()
    };

    function siteScroll() {
//        console.log(document.body.scrollTop);
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            $('.txt-logo').fadeOut();
            $('#header').addClass('fixed-header');
            $('#site-logo').addClass('fixed-img');
            $('.top_menu-x').addClass('fixed-menu');
        } else {
            $('.txt-logo').fadeIn();
            $('#header').removeClass('fixed-header');
            $('.top_menu-x').removeClass('fixed-menu');
            $('#site-logo').removeClass('fixed-img');
        }
    }

    function menu_mobile() {
        $('.dropdown-menu').slideToggle();
    }

</script>
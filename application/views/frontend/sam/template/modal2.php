<style>
    #frm_login .form-group {
        overflow: hidden;
    }

    .btn_dang_nhap {
        text-align: center;
    }

    .modal-dialog-dangnhap {
        width: 500px;
        margin-left: auto;
        margin-right: auto;
    }

    .btn_dang_nhap button {
        width: 100%;
        background: #0099ff !important;
        text-transform: uppercase;
        font-weight: bold;
        border: solid 1px #0099ff !important;
    }

    .nhomk {
        text-align: left;
    }

    .quenmk {
        text-align: right;
    }
</style>
<!-- dang nhap -->
<div class="modal fade" id="modalDangNhap" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Đăng nhập vào hệ
                    thống</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-login"></div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Tài khoản</label>
                        <input type="text" class="form-control" placeholder="Tài khoản" name="username"
                               id="username">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Mật khẩu</label>
                        <input type="password" class="form-control" placeholder="Mật khẩu" name="password"
                               id="password">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 nhomk">
                            <span class="nhopwe"><input type="checkbox" value="">Ghi nhớ mật khẩu. </span>
                        </div>
                        <div class="col-md-6 quenmk">
                            <a href="#" onclick="showQuenMK()" class="color-primary">Bạn quên mật khẩu?</a>
                        </div>
                    </div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger">Đăng nhập vào hệ thống</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>Bạn chưa có tài khoản? <a href="#" onclick="showDangKy();" class="color-primary">Đăng ký ngay</a></p>
            </div>
        </div>
    </div>
</div>
<!-- dang ki -->
<div class="modal fade" id="modalDangKy" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Đăng ký tài khoản</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-login"></div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Họ" name="firstname" id="firstname">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Tên" name="lastname"  id="last">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email" name="email" id="email">
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="password" class="form-control" placeholder="Mật khẩu" name="password" id="password">
                        </div>
                        <div class="col-md-6">
                            <input type="password" class="form-control" placeholder="Nhập lại mật khẩu"
                                   name="repassword">
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group">
                        <span class="nhopwe"><input type="checkbox" value="">Tôi đồng ý với các điều khoản của WOW English. </span>
                    </div>
                    <div class="form-group btn_dang_nhap">
                        <button type="submit" class="btn btn-danger" ik="submit_register">Đăng ký tài khoản</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>Bạn đã có tài khoản? <a href="#" onclick="showDangNhap();" class="color-primary">Đăng nhập ngay</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--lay lai mat khau-->
<div class="modal fade" id="modalQuenMK" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Yêu cầu lấy lại mật khẩu</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-login"></div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email đăng nhập" name="email">
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group">
                        <span class="text-muted"><i>* Một mã kích hoạt sẽ được gửi vào email này.</i></span>
                    </div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger">gửi yêu cầu</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>
                    <a href="#" onclick="showDangNhap();" class="color-primary">Đăng nhập</a> | <a href="#" onclick="showDangKy();" class="color-primary">Đăng ký</a>
                </p>
            </div>
        </div>
    </div>
</div>

<script>
    function showDangNhap() {
        $(".modal").modal("hide");
        $("#modalDangNhap").modal("show");
    }
    function showDangKy() {
        $(".modal").modal("hide");
        $("#modalDangKy").modal("show");
    }

    function showQuenMK(){
        $(".modal").modal("hide");
        $("#modalQuenMK").modal("show");
    }
</script>
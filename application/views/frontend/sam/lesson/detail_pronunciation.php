<div class="container content">
    <section>
        <?php if($list){?>
            <div class="title_detail">
                <h3><?php echo $list->name; ?></h3>
                <a href="
                    <?php echo base_url().ROUTE_FRONTEND_PRONUNCIATION_LESSON; ?>
                ">Quay trở lại bài học</a>
            </div>
            <div class="row margin_top_30">
                <div class="col-sm-8 col-md-9">
                    <h4>Giới thiệu bài học</h4>
                    <div>
                        <?php echo $list->description; ?>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3 module_image">
                    <img class="baseImg thumbnail" src="<?php echo base_url().$list->image; ?>"/>
                </div>
            </div>
        <?php } ?>
    </section>
    <div class="news-other-title">
        <h2>Các bài học phát âm</h2>
    </div>
    <div class="row">
        <?php foreach ($suggest_pronunciation_detail as $index => $item): ?>
            <div class="col-xs-6 col-md-4 down-content margin_top_30">
                <div class="tintuc-item">
                    <div class="img">
                        <?php echo get_img_tag($item->image, $item->name, '', IMG_NEWS_DEFAULT); ?>
                    </div>
                    <div class="text-over">
                        <h1 class="title">
                            <a href="<?php echo base_url().'phat-am/'.$item->name.'-'.$item->id.'.html' ?>"
                                             class="customLink"><?php echo $item->name;?></a></h1>
                    </div>
                    <div class="text-over desc">
<!--                        --><?php //echo $item->description ?>
                        <?php echo word_limiter(strip_tags($item->description, '<p>'),30); ?>
                    </div>
                    <div class="views pull-right">
                        <!--                    <span class="glyphicon glyphicon-eye-open"></span><span-->
                        <!--                        class="soluong">&nbsp;--><?php //echo get_number_format($item->total_view) ?><!--</span>-->
                        <a href="<?php echo base_url().'phat-am/'.$item->name.'-'.$item->id.'.html' ?>" class="customLink">Xem
                            thêm...</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

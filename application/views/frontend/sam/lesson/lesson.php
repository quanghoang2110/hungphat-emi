<?php if(isset($list_pack) && count($list_pack) > 0):?>
<div class="container content">
    <section>
        <?php if(count($list_pack)>0){ ?>
            <div class="padd-15 sl_pack">
                <form class="form-inline">
                <label>Chọn khóa học : &nbsp;</label>
                    <select id="sl_pack" class="form-inline form-control input-sm">
                    <?php foreach($list_pack as $key=>$val){ ?>
                    <option <?php if($id_pack == $val->id) echo "selected"; ?> value="<?php echo $val->id; ?>"> <span><?php echo $val->pack_name; ?></span></option>
                    <?php } ?>
                    </select>
                </form>
            </div>
        <?php } ?>
        <div class="padd-15">
            <h3>Quản lý bài học 
                <?php if(isset($list) && count($list)>0 ){ $today = new DateTime('now');
                                    $date = new DateTime($list[0]->ended_at);
                    $diff = date_diff($date,$today);?>
                    <span class="nDay <?php if($diff->days < 10) echo 'error'; else echo 'success'; ?>">Số ngày học còn lại : <?php echo $diff->days + 1; ?> ngày</span>
                <?php } ?>
            </h3>
            <ul class="nav nav-tabs">
                <li role="presentation" <?php if($active == 'main') echo 'class="active"'; ?>><a href="<?php echo base_url(ROUTE_FRONTEND_LESSON); ?>/bai-hoc-chinh-<?php echo $id_pack; ?>">Bài học chính</a></li>
                <li role="presentation" <?php if($active == 'help') echo 'class="active"'; ?>><a href="<?php echo base_url(ROUTE_FRONTEND_LESSON); ?>/bai-hoc-bo-tro-<?php echo $id_pack; ?>">Bài học bổ trợ</a></li>
            </ul>
        </div>
        <ul class="itemsLess">
            <?php if(isset($id) && $id== PACK_BASIC && isset($type) && $type==BAI_HOC_CHINH){?>
            <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center">
                        <span >
                            <div class="image thumbnail">
                                <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                                <a href="<?php echo base_url().ROUTE_FRONTEND_PRONUNCIATION_LESSON; ?>" title="Bài học phát âm">
                                    <img class="baseImg" src="<?php echo base_url('images/default/phat-am-wow-english.jpg'); ?>" alt="Bài học phát ">
                                </a>
                            </div>
                            <div class="title">Bài học: <a href="" title="Bài học phát âm">"Bài học phát âm"</a></div>
                            <?php }?>
            <?php if(isset($list) && count($list)>0 ){?>
                <?php foreach($list as $key=>$val){
            ?>
            <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center">
                <span >
                    <div class="image thumbnail">
                        <?php if($key == 0){ ?>
                            <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                            <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php }elseif($val->unlocked==1 || $val->lock_type == 1 || ($val->finish < 99 && $val->finish > -1)){ ?>
                            <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                            <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php }else{ ?>
                            <i class="fa lock fa-lock" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php } ?>
                    </div>
                    <?php if($val->finish==100){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đã hoàn thành" class="btnA btnFinish">Đã hoàn thành</a>
                    <?php }else if(($val->finish < 99 && $val->finish > -1)){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đang học" class="btnA btnFinish">Đang học</a>
                    <?php }elseif(($val->finish == -1 && $val->lock_type == 1) || $key == 0){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đang học" class="btnA btnStart">Bắt đầu</a>
                    <?php }else{ ?>
                        <div class="title">Bài học: <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu"><?php echo $val->name; ?></a></div>
                        <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu" class="btnA btnStart">Bắt đầu</a>
                    <?php } ?>
                </span>
            </li>
            <?php } } else{ ?>
                 <div class="padd-15 sl_pack">
                    <h4>Khóa học này chưa có bài học nào</h4>
                 </div>
            <?php } ?>
        </ul>
    </section>
</div>
<?php else:?>
    <div class="container content">
        <div class="padd-15 sl_pack">
            <h4>Bạn chưa mua khóa học nào</h4>
        </div>
    </div>
<?php endif;?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog msg-popup modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="title">Bài học này chưa được mở khoá. Vui lòng hoàn thành bài học trước đó để mở khoá module này</p>
            </div>
        </div>
    </div>
</div>
<script>
    var urlLesson = "<?php echo base_url(ROUTE_FRONTEND_LESSON); ?>";
    $(document).ready(function(){
        $('#sl_pack').change(function(){
            window.location = urlLesson+'/bai-hoc-chinh-'+$(this).val();
        })
    })
</script>
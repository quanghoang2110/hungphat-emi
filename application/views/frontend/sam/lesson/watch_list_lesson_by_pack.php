<!-- bai hoc -->
<div class="modal fade" id="baihoc" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <div class="tieude-left col-sm-6 col-md-6">
                    <p>Quản lí tài khoản</p>
                </div>
                <div class="tieude-right col-sm-6">
                    <img src="img/daidien1.jpg">
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#baihoc1">Bài học chính</a></li>
                            <li><a data-toggle="tab" href="#baihoc2">Bài học bổ trợ</a></li>

                        </ul>

                        <div class="tab-content">
                            <div id="baihoc1" class="tab-pane fade in active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2 col-lg-3 col-xs-12 text-center">
                                            <img src="img/baihoc.png" alt="">
                                            <p class="text-center">Bài học <span style="color:#1a75bc">"Goal"</span></p>
                                            <button class="btn btn-danger">Đã hoàn thành</button>
                                        </div>
                                        <div class="col-md-2 col-md-pull-1 col-lg-5 col-xs-12 text-center">
                                            <img src="img/baihoc.png" alt="">
                                            <p class="text-center">Bài học <span style="color:#1a75bc">"Goal"</span></p>
                                            <button class="btn btn-danger">Đã hoàn thành</button>
                                        </div>


                                    </div>
                                </div> <!-- dong the container -->
                                <div class="tab-content">
                                    <div id="baihoc1" class="tab-pane fade in active">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-2 col-lg-3 col-xs-12 text-center">
                                                    <img src="img/baihoc.png" alt="">
                                                    <p class="text-center">Bài học <span style="color:#1a75bc">"Goal"</span></p>
                                                    <button class="btn btn-danger">Đã hoàn thành</button>
                                                </div>
                                                <div class="col-md-2 col-md-pull-1 col-lg-5 col-xs-12 text-center">
                                                    <img src="img/baihoc.png" alt="">
                                                    <p class="text-center">Bài học <span style="color:#1a75bc">"Goal"</span></p>
                                                    <button class="btn btn-danger">Đã hoàn thành</button>
                                                </div>


                                            </div>
                                        </div> <!-- dong the container -->







                                    </div>
                                </div> <!-- dong the tab-conent -->
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>

            </div>
        </div>

    </div>

    <!-- ket thuc bai hoc -->


    <!-- kích hoạt tài khoản -->


    <!-- kết thúc kích hoạt tài khoản -->
    <script>

        jQuery(function($) {
            "use strict";
            //Mobile menu
            $('.bars-navigation').click(function() {
                if ($('.mobimenu').is(":hidden"))
                {
                    $('.mobimenu').slideDown("fast");
                } else {
                    $('.mobimenu').slideUp("fast");
                }
                return false;
            });

            $(".category").accordion({
                accordion:false,
                speed: 300,
                closedSign: '+',
                openedSign: '-'
            });
            $(".mobile-menu").accordion({
                accordion:false,
                speed: 300,
                closedSign: '+',
                openedSign: '-'
            });
        });
        (function($){
            $.fn.extend({
                accordion: function(options) {
                    var defaults = {
                        accordion: 'true',
                        speed: 300,
                        closedSign: '[-]',
                        openedSign: '[+]'
                    };
                    var opts = $.extend(defaults, options);
                    var $this = $(this);
                    $this.find("li").each(function() {
                        if($(this).find("ul").size() != 0){
                            $(this).find("a:first").after("<em>"+ opts.closedSign +"</em>");
                            if($(this).find("a:first").attr('href') == "#"){
                                $(this).find("a:first").click(function(){return false;});
                            }
                        }
                    });
                    $this.find("li em").click(function() {
                        if($(this).parent().find("ul").size() != 0){
                            if(opts.accordion){
                                //Do nothing when the list is open
                                if(!$(this).parent().find("ul").is(':visible')){
                                    parents = $(this).parent().parents("ul");
                                    visible = $this.find("ul:visible");
                                    visible.each(function(visibleIndex){
                                        var close = true;
                                        parents.each(function(parentIndex){
                                            if(parents[parentIndex] == visible[visibleIndex]){
                                                close = false;
                                                return false;
                                            }
                                        });
                                        if(close){
                                            if($(this).parent().find("ul") != visible[visibleIndex]){
                                                $(visible[visibleIndex]).slideUp(opts.speed, function(){
                                                    $(this).parent("li").find("em:first").html(opts.closedSign);
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                            if($(this).parent().find("ul:first").is(":visible")){
                                $(this).parent().find("ul:first").slideUp(opts.speed, function(){
                                    $(this).parent("li").find("em:first").delay(opts.speed).html(opts.closedSign);
                                });
                            }else{
                                $(this).parent().find("ul:first").slideDown(opts.speed, function(){
                                    $(this).parent("li").find("em:first").delay(opts.speed).html(opts.openedSign);
                                });
                            }
                        }
                    });
                }
            });
        })(jQuery);

        jQuery(document).ready(function($){
            if($("#btn-top").length > 0){
                $(window).scroll(function () {
                    var e = $(window).scrollTop();
                    if (e > 300) {
                        $("#btn-top").show()
                    } else {
                        $("#btn-top").hide()
                    }
                });
                $("#btn-top").click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    })
                })
            }
        });
        function menu_mobile(){
            $('.dropdown-menu').slideToggle();
        }


    </script>
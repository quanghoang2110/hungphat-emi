<div class="container content">
    <section>
        <?php if(isset($list) && count($list) >0 ){ ?>
            <div class="title_detail">
                <h3><?php echo $list->name; ?></h3>
                <a href="<?php if(isset($slug)) echo $slug; ?>">Quay trở lại module</a>
            </div>
            <div>
                <?php echo $list->description; ?>
            </div>
            <?php if($part){ ?>
                <div class="main-part">
                    <?php foreach ($part as $key=>$val){?>
                        <span class="part">
                            <div class="image">
                                <?php if($val->unlocked==1 || $key == 0){ ?>
                                    <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                                    <a href="<?php echo generate_slug_add(base_url(ROUTE_FRONTEND_PART_DETAIL)."/",$val->name,$val->id).'?lesson_id='.$lesson_id.'&pack_id='.$pack_id   .'&module_id='.$module_id; ?>">
                                        <img class="baseImg thumbnail" alt="<?php echo $val->name; ?>" src="<?php if($val->image != '') echo base_url().$val->image; else base_url('images/default/image.png'); ?>"/>
                                    </a>
                                <?php }else{ ?>
                                    <i class="fa lock fa-lock" aria-hidden="true"></i>
                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                        <img class="baseImg thumbnail" alt="<?php echo $val->name; ?>" src="<?php if($val->image != '') echo base_url().$val->image; else base_url('images/default/image.png'); ?>"/>
                                    </a>
                                <?php } ?>
                            </div>
                            <h3><?php echo $val->name; ?></h3>
                            <div>
                                <?php echo $val->description; ?>
                            </div>
                        </span>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </section>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog msg-popup modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="title">Bạn cần hoàn thành part1 để có thể tiếp tục học part 2</p>
            </div>
        </div>
    </div>
</div>

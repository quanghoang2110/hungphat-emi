<div class="container content">
    <section>
        <?php if($part){?>
            <div class="title_detail">
                <h3><?php echo $part->name; ?></h3>
                <a href="<?php if(isset($slug_)) echo $slug_; ?>">Quay trở lại bài học</a>
            </div>
            <input type="hidden" id="part_type" value="<?php echo $part->type; ?>" />
            <input type="hidden" id="part_id" value="<?php echo $part->id; ?>" />
        <?php } ?>
            <div class="m-content">
                <?php if(!isset($html)){ ?>
                    <?php if(isset($total)){ ?>
                    <div class="fr quiz">
                        <div>
                            <div class="fr-head">
                                <span class="txt_percent">Tỉ lệ hoàn thành(%)</span>
                                <p class="ques"><?php if(isset($voca)) echo "Words "; else echo  "Question ";?>: <span><span class="pos">1</span>/<span class="total"><?php  echo $total; ?></span></span></p>
                                <div class="progress-out">
                                    <div class="progress-in">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                                 aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                        <span class="percent">0%</span>
                                    </div>
                                </div>
                                <span id="coutDown" class="sec"><b><?php if(isset($question)) echo $question->time; ?></b>SEC</span>
                            </div>
                        </div>
                        <div id="content_question">

                        </div>
                    </div>
                    <?php }else{ ?>
                        <div class="fr quiz">
                            <p>Chưa có bài học nào</p>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    <?php echo $html; ?>
                    <span id="coutDown" class="sec hidden"><b>0</b>SEC</span>
                <?php } ?>
            </div>
    </section>
    <input type="hidden" id="lesson_id" value="<?php if(isset($lesson_id)) echo $lesson_id; ?>"/>
    <input type="hidden" id="module_id" value="<?php if(isset($module_id)) echo $module_id; ?>"/>
    <input type="hidden" id="pack_id" value="<?php if(isset($pack_id)) echo $pack_id; ?>"/>
    <input type="hidden" value="<?php if(isset($slug)) echo $slug; ?>" id="slug"/>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID_first"/>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog msg-popup modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="title" id="title-msg">Bạn cần hoàn thành part1 để có thể tiếp tục học part 2</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var arrResult = {};var timeinterval;
    $(document).ready(function(){
        load_question();
    });

    function load_question(){
        clearIntervalOne();
        var url = "<?php echo base_url(ROUTE_FRONTEND_QUESTION); ?>";
        var $id = $("#hID_first").val();
        var lesson_id = $("#lesson_id").val();
        var part_type = $('#part_type').val();
        var part_id = $('#part_id').val();
        var data = {id:$id,lesson_id:lesson_id,part_type:part_type,part_id:part_id};
        $.post(url,data)
            .done(function(data){
                var result = JSON.parse(data);
                $('#content_question').html(result.html);
                if(result.sec >0){
                    $('.sec b').html(result.sec);
                }else{
                    $('.sec').hide();
                }
                var hType = $('#hType').val();
                if(hType == 'QUIZ' || hType == 'TEXT') setTime('.sec');
                if(part_type == 'VOCA' && $id == $("#unbreak").data( "next" )){
                    $('#unbreak').css("display","none");
                }
                hideErr();
            });
    }

    function unBreak(){
        clearIntervalOne();
        $('.sec').show();
        var url = "<?php echo base_url(ROUTE_FRONTEND_QUESTION); ?>";
        var $id = $("#unbreak").data( "next" );
        var lesson_id = $("#lesson_id").val();
        var module_id = $("#module_id").val();
        var part_type = $('#part_type').val();
        var data = {id:$id,lesson_id:lesson_id,part_type:part_type};
        var pos = $('.pos').html();
        var total = $('.total').html();
        var slug = $('#slug').val();
        var $cID = $('#hID').val();
        var hType = $('#hType').val();
        arrResult[$cID] = 0;
        if(pos == total){
            var lesson_id = $("#lesson_id").val();
            $.post("<?php echo base_url(ROUTE_FRONTEND_PART_RESULT); ?>",{"data":arrResult,"slug":slug,lesson_id:lesson_id,module_id:module_id})
                .done(function(data){
                    var result = JSON.parse(data);
                    $('.m-content').html(result.html);
                });
            return false;
        }
        $('.pos').html(parseInt(pos)+1);
        $.post(url,data)
            .done(function(data){
                var result = JSON.parse(data);
                $('#content_question').html(result.html);
                if(result.sec >0){
                    $('.sec b').html(result.sec);
                }else{
                    $('.sec').hide();
                }
                var hType = $('#hType').val();
                if(hType == 'QUIZ' || hType == 'TEXT') setTime('.sec');
            })
    }

    function Continue(){
        clearIntervalOne();
        $('.sec').show();
        var url = "<?php echo base_url(ROUTE_FRONTEND_QUESTION); ?>";
        var $id = $("#Continue").data( "next" );
        var lesson_id = $("#lesson_id").val();
        var module_id = $("#module_id").val();
        var part_type = $('#part_type').val();
        var data = {id:$id,lesson_id:lesson_id,part_type:part_type};
        var pos = $('.pos').html();
        var total = $('.total').html();
        var hType = $('#hType').val();
        var check = 0;
        var slug = $('#slug').val();
        var $cID = $('#hID').val();
        if(hType == 'QUIZ'){
            check = $('input:radio[name=rdo]:checked').val();
            if(check == undefined) check = 0;
        }else if(hType == 'TEXT'){
            var txt_answer = $('#txt_answer').val();
            var txt_answer_check  = $("#txt_answer").data( "text" );
            var arr_text = txt_answer_check.split("/");
            var a = arr_text.indexOf(txt_answer);
            if(a>-1){
                check = 1;
            }
        }else if(hType == 'LISTEN'){
            check = 1;
        }
        var flag = 0;
        for (var key in arrResult) {
            if(key == $cID) flag =1;
        }

        if(flag != 1){
            arrResult[$cID] = check;
            if(hType != 'LISTEN') {
                if (check != 1) {
                    showErr();
                    return false;
                } else {
                    showSuccess();
                    return false;
                }
            }

        }
        if(pos == total){
            $.post("<?php echo base_url(ROUTE_FRONTEND_PART_RESULT); ?>",{"data":arrResult,"slug":slug,lesson_id:lesson_id,module_id:module_id})
                .done(function(data){
                    var result = JSON.parse(data);
                    $('.m-content').html(result.html);
                });
            return false;
        }
        $('.pos').html(parseInt(pos)+1);
        $.post(url,data)
        .done(function(data){
            var result = JSON.parse(data);
            $('#content_question').html(result.html);
            if(result.sec >0){
                $('.sec b').html(result.sec);
            }else{
                $('.sec').hide();
            }
            var hType = $('#hType').val();
            if(hType == 'QUIZ' || hType == 'TEXT') setTime('.sec');
            var total_an = 0;
            for (var key in arrResult) {
                if(arrResult[key] == 1) total_an= total_an + 1;
            }
            $('.progress-bar').css("width",Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
            $('.percent').html(Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
            hideErr();
        })
    }

    function setTime(clas){
        var sec = $(clas+' b').html();
        function updateClock(){
            sec = parseInt(sec) - 1;
            $('.sec b').html(sec);
            if (sec <= 0) {
                Continue();
            }
        }
        updateClock();
        timeinterval = setInterval(updateClock,2000);
    }

    function clearIntervalOne(){
        clearInterval(timeinterval);
    }

    function showErr(){
        $('.btn-question').css('background','#f2dedf');
        $('.main-err').css('display','block');
        $('.btnStart').css('display','none');
    }

    function showSuccess(){
        $('.btn-question').css('background','#e7f4db');
        $('.main-success').css('display','block');
        $('.btnStart').css('display','none');
    }

    function hideErr(){
        $('.btn-question').css('background','#eaeaea');
        $('.main-msg').css('display','none');
        $('.btnStart').css('display','block');
    }
    
    function unBreakVoca(){
        $('#unbreak').show();
        var $cID = $('#hID').val();
        var url = "<?php echo base_url(ROUTE_FRONTEND_QUESTION); ?>";
        var $id = $("#unbreak").data( "next" );
        var lesson_id = $("#lesson_id").val();
        var moduel_id = $("#moduel_id").val();
        var part_type = $('#part_type').val();
        var part_id = $('#part_id').val();
        var pos = $('.pos').html();
        var total = $('.total').html();
        var data = {id:$id,lesson_id:lesson_id,part_type:part_type,part_id:part_id};
        delete arrResult[$id];
        $('.pos').html(parseInt(pos)-1);
        $.post(url,data)
            .done(function(data){
                var result = JSON.parse(data);
                $('#content_question').html(result.html);
                var hType = $('#hType').val();
                var total_an = 0;
                for (var key in arrResult) {
                    if(arrResult[key] == 1) total_an= total_an + 1;
                }
                $('.progress-bar').css("width",Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
                $('.percent').html(Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
                hideErr();
            })
    }

    function ContinueVoca(){
        $('#unbreak').show();
        var url = "<?php echo base_url(ROUTE_FRONTEND_QUESTION); ?>";
        var $id = $("#Continue").data( "next" );
        var lesson_id = $("#lesson_id").val();
        var module_id = $("#module_id").val();
        var part_type = $('#part_type').val();
        var slug = $('#slug').val();
        var part_id = $('#part_id').val();
        var $cID = $('#hID').val();
        var pos = $('.pos').html();
        var total = $('.total').html();
        var data = {id:$id,lesson_id:lesson_id,part_type:part_type,part_id:part_id};
        arrResult[$cID] = 1;
        if(pos == total){
            $.post("<?php echo base_url(ROUTE_FRONTEND_PART_RESULT); ?>",{"data":arrResult,"slug":slug,lesson_id:lesson_id,module_id:module_id})
                .done(function(data){
                    var result = JSON.parse(data);
                    $('.m-content').html(result.html);
                });
            return false;
        }   
        $('.pos').html(parseInt(pos)+1);
        $.post(url,data)
            .done(function(data){
                var result = JSON.parse(data);
                $('#content_question').html(result.html);
                var hType = $('#hType').val();
                var total_an = 0;
                for (var key in arrResult) {
                    if(arrResult[key] == 1) total_an= total_an + 1;
                }
                $('.progress-bar').css("width",Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
                $('.percent').html(Math.floor(parseInt(total_an)/parseInt(total)*100)+'%');
                hideErr();
            })
    }
    
    function move_part(){
        var lesson_id = $("#lesson_id").val();
        var module_id = $("#module_id").val();
        var part_id = $('#part_id').val();
        var pack_id = $('#pack_id').val();
        $.post("<?php echo base_url(ROUTE_FRONTEND_MOVE_PART); ?>",{lesson_id:lesson_id,module_id:module_id,part_id:part_id,pack_id:pack_id})
            .done(function(data){
                var result = JSON.parse(data);
                if(result.url != ''){
                    window.location = result.url;
                }else{
                    $('.m-content').html(result.html);
                }
            });
    }

</script>

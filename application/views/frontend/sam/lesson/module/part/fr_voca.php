<?php if($voca){ ?>
    <div class="voca">
        <span class="col-sm-offset-1 col-sm-2 col-md-offset-1 col-md-3">
            <img class="baseImg" alt="<?php echo $voca->define; ?>" src="<?php if($voca->img != '') echo base_url().$voca->img; else base_url('images/default/image.png'); ?>" /></span>
        <span class="col-sm-8 col-md-6">
            <h3 ><?php echo $voca->answer; ?> <span>(<?php echo $voca->type; ?>)</span></h3>
            <div class="main bor-das padBot">
                <i><?php echo $voca->speak; ?></i>
                <div class="audio">
                    <audio  controls autoplay>
                        <source src="<?php echo base_url().$voca->audio; ?>" type="audio/mpeg">
                    </audio>
                    
                </div>
            </div>
            <div class="main bor-das padBot">
                <span>Định nghĩa : </span>
                <span>
                    <?php echo $voca->define; ?>
                </span>
            </div>
            <div class="main padBot">
                <span>Example : </span>
                <span>
                    <?php echo $voca->example; ?>
                </span>
            </div>
        </span>
        <span class="col-sm-offset-1 col-md-2"></span>
    </div>
    <div class="btn-question">
        <?php if(isset($previous_id) && $previous_id){ ?>
        <input type="button" value="Làm lại" id="unbreak" data-next="<?php  echo $previous_id; ?>" onclick="unBreakVoca();" class="btn btnStart"/>
        <?php } ?>
        <input type="button" value="Tiếp tục" id="Continue" data-next="<?php if(isset($next)) echo $next; ?>"  onclick="ContinueVoca();" class="btn btnFinish"/>
    </div>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID"/>
    <input type="hidden" id="hType" value="<?php if(isset($question)) echo $question->type; if(isset($voca)) echo 'VOCA'; ?>"/>
<?php } ?>
<div class="page-result page-unsuccess">
    <img class="baseImg" alt="unsuccess" src="<?php echo base_url('assets/frontend/wow_english/images/banner/success.jpg'); ?>"/>
    <div class="fr-head">
        <span class="txt_percent">Tỉ lệ hoàn thành(%)</span>
        <div class="progress-out">
            <div class="progress-in">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?php if(isset($percent)) echo $percent; ?>%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>
                <span class="percent"><?php if(isset($percent)) echo $percent; ?>%</span>
            </div>
        </div>
    </div>
    <h5>Bạn đã hoàn thành part này</h5>
    <p>Hãy nhấn tiếp tục để hoàn thành part tiếp theo hoặc nhấn nút làm lại để cải thiện điểm của bạn.</p>
    <div class="btn-question-result">
        <a href="<?php if(isset($slug)) echo $slug; ?>" class="btn btnStart">Làm lại</a>
        <input type="button" value="Tiếp tục" id="Continue" data-next=""  onclick="move_part();" class="btn btnFinish"/>
    </div>
</div>
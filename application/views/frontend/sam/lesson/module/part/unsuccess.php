<div class="page-result page-success">
    <img  class="baseImg" alt="unsuccess" src="<?php echo base_url('assets/frontend/wow_english/images/banner/unsuccess.jpg'); ?>"/>
    <div class="fr-head">
        <span class="txt_percent">Tỉ lệ hoàn thành(%)</span>
        <div class="progress-out">
            <div class="progress-in">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?php if(isset($percent)) echo $percent; ?>%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>
                <span class="percent"><?php if(isset($percent)) echo $percent; ?>%</span>
            </div>
        </div>
    </div>
    <h5>Rất tiếc, bạn chưa hoàn thành phần này</h5>
    <p>Bạn không cần lo lắng. Thành công chỉ đến sau những lần tập luyện không ngừng. Hãy thử lại một lần nữa.</p>
    <div class="btn-question-result">
        <a href="<?php if(isset($slug)) echo $slug; ?>" class="btn btnStart">Làm lại</a>
    </div>
</div>
<?php if(isset($list) && count($list) > 0){ ?>
    <article>
        
        <?php if($list->image != ''){ ?>
            <div class="image">
                <img class="thumbnail baseImg" alt="<?php echo $list->content; ?>" src="<?php echo base_url().$list->image;?>" />
            </div>
        <?php } ?>
        <?php if($list->audio != ''){ ?>
            <audio  controls autoplay>
                <source src="<?php echo base_url().$list->audio; ?>" type="audio/mpeg">
            </audio>
        <?php } ?>
        <p class="content"><?php echo $list->content; ?></p>
    </article>


    <article>
        <ul>
            <?php foreach($answer as $k=>$val){ ?>
                <li class="col-sm-6">
                    <input type="radio" name="rdo" id="rdo<?php echo $k; ?>" value="<?php echo $val->is_correct; ?>"/>
                    <label for="rdo<?php echo $k; ?>"><?php echo $val->answer_content; ?></label>
                </li>
            <?php } ?>
        </ul>
    </article>
    <input type="hidden" id="hType" value="<?php echo $list->type; ?>"/>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID"/>
    <div class="btn-question">
        <input type="button" value="Bỏ qua" id="unbreak" data-next="<?php if(isset($next)) echo $next; ?>" onclick="unBreak();" class="btn btnStart"/>
        <input type="button" value="Tiếp tục" id="Continue" data-next="<?php if(isset($next)) echo $next; ?>"  onclick="Continue();" class="btn btnFinish"/>
        <div class="main-msg main-err"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án sai</span></div>
        <div class="main-msg main-success"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án đúng</span></div>
    </div>
<?php } ?>
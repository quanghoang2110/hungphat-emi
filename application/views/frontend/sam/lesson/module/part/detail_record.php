<div class="container content">
    <section>
        <?php if($part){?>
            <div class="title_detail">
                <h3><?php echo $part->name; ?></h3>
                <a href="<?php if(isset($slug_)) echo $slug_; ?>">Quay trở lại bài học</a>
            </div>
            <input type="hidden" id="part_type" value="<?php echo $part->type; ?>" />
            <input type="hidden" id="part_id" value="<?php echo $part->id; ?>" />
        <?php } ?>
        <div class="m-content">
            <div class="fr main-record">
                <div id="content_question">
                    <?php if($question){ ?>
                        <?php if(isset($recording) && $recording){ ?>
                            <?php if($recording->audio != ''){ ?>
                            <article class="content_script">
                                <h3>Bạn đã nộp bài thành công.Hãy chờ nhận xét của giảng viên.</h3>
                                <audio class="audio" controls autoplay>
                                    <source src="<?php echo base_url().$recording->audio; ?>" type="audio/mpeg">
                                </audio>
                                <?php if($recording->comment != ''){ ?>
                                    <div>
                                        <p>Nhận xét giảng viên</p>
                                        <?php echo $recording->comment; ?>
                                    </div>
                                <?php } ?>
                                <div>
                                    <a href="<?php if(isset($slug)) echo $slug; ?>" class="btn btnRadius btnStart">Ghi âm lại</a>
                                    <a class="btn btnFinish btnRadius btnFinishNew">Hoàn thành</a>
                                </div>
                            </article>
                            <?php } ?>
                        <?php }else{ ?>
                            <article class="content_script">
                                <h3>Hãy nhấn vào ghi âm và thu âm bài nói của bạn</h3>
<!--                                <audio controls autoplay src="" id="audio"></audio>-->
                                <div class="micro"><i class="fa fa-microphone record nonRecord" aria-hidden="true"></i></div>
                                <button class="btn btn-default btnScript" data-toggle="collapse" data-target="#script">Xem script</button>
                                <div id="script" class="collapse">
                                    <?php echo $question->suggest; ?>
                                </div>
                                <div>
                                    <a class="btn  btnRadius btnStart" id="record">Ghi âm</a>
                                    <a class="btn disabled one btnRadius btnStart" id="stop">Ghi âm lại</a>
                                    <a class="btn disabled one btnRadius btnFinishNew" id="save">Hoàn thành</a>
                                </div>
                            </article>
                        <?php } ?>
                        <input type="hidden" id="question_id" value="<?php echo $question->id; ?>" />
                    <?php }else{ ?>
                        Chưa có nội dung nào
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/frontend/wow_english/js/audio'); ?>/src/recorder.js"></script>
<script src="<?php echo base_url('assets/frontend/wow_english/js/audio'); ?>/src/Fr.voice.js"></script>
<script src="<?php echo base_url('assets/frontend/wow_english/js/audio'); ?>/js/app.js"></script>
<script type="text/javascript">
    var urlSave = "<?php echo base_url(ROUTE_FRONTEND_SAVE_AUDIO); ?>"
$(document).ready(function(){
    $(document).on("click", "#save:not(.disabled)", function(){
        Fr.voice.export(function(mp3){
            var question_id = $('#question_id').val();
            var formData = new FormData();
            formData.append('file', mp3);
            formData.append('question_id',question_id)
            $.ajax({
                url: urlSave,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data) {
//                    $("#audio").attr("src", data);
//                    $("#audio")[0].play();
//                    alert("Saved In Server. See audio element's src for URL");
                }
            });
        }, "mp3");
        restore();
    });
});
</script>

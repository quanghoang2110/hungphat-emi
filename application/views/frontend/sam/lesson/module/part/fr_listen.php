<?php if($list){ ?>
    <p>Hãy nghe hết audio/video bài học dưới đây</p>
    <p>Lưu ý :
    <ul>
        <li>Nghe hoặc xem audio/video tối thiểu 2 lần</li>
        <li>Bảo đảm hiểu nội dung của bài học</li>
    </ul>

    </p>
    <span>
        <?php if($list->audio != ''){ ?>
            <audio  controls autoplay>
                <source src="<?php echo base_url().$list->audio; ?>" type="audio/mpeg">
            </audio>
        <?php } ?>
    </span>
    <article class="content_script">
        <button class="btn btn-default" data-toggle="collapse" data-target="#script">Xem script</button>

        <div id="script" class="collapse">
            <?php echo $list->suggest; ?>
        </div>
    </article>
    <input type="hidden" id="hType" value="<?php echo $list->type; ?>"/>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID"/>
    <div class="btn-question">
        <input type="button" value="Tiếp tục" id="Continue" data-next="<?php if(isset($next)) echo $next; ?>"  onclick="Continue();" class="btn btnFinish"/>
        <div class="main-msg main-err"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án sai</span></div>
        <div class="main-msg main-success"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án đúng</span></div>
    </div>
<?php }else{ ?>
    Nội dung này chưa có

<?php } ?>
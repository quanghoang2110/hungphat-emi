<?php if($list){ ?>
    <p>Hãy ấn vào biểu tượng micro và thu âm bài nói của bạn</p>
    <span>
        <?php if($list->audio != ''){ ?>
            <audio  controls autoplay>
                <source src="<?php echo base_url().$list->audio; ?>" type="audio/mpeg">
            </audio>
        <?php } ?>
    </span>
    <article class="content_script">
        <button class="btn btn-default" data-toggle="collapse" data-target="#script">Xem script</button>
        <div id="script" class="collapse">
            <?php echo $list->suggest; ?>
        </div>
    </article>
    <input type="hidden" id="hType" value="<?php echo $list->type; ?>"/>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID"/>
<?php }else{ ?>
    Nội dung này chưa có
<?php } ?>
<?php if($list){ ?>
<article class="text">
    <?php if($list->image != ''){ ?>
        <div class="img-part">
            <img alt="<?php echo $list->content; ?>" src="<?php echo base_url().$list->image;?>" />
        </div>
    <?php } ?>
    <?php if($list->audio != ''){ ?>
        <audio  controls autoplay>
            <source src="<?php echo base_url().$list->audio; ?>" type="audio/mpeg">
        </audio>
    <?php } ?>
    <?php if($list->translate != ''){ ?>
            <div class="text-center text-pron"><?php echo $list->translate; ?></div>
    <?php } ?>
    <p class="content"><?php echo $list->content; ?></p>
    <?php foreach($answer as $k=>$val){ ?>
        <ul>
            <li class="col-sm-6 col-md-4 col-lg-4 "><input class="form-control"  id="txt_answer" type="text" data-text="<?php echo $val->answer_content; ?>" placeholder="Đáp án" value=""/></li>
        </ul>
    <?php }?>
</article>
    <input type="hidden" id="hType" value="<?php echo $list->type; ?>"/>
    <input type="hidden" value="<?php if(isset($id)) echo $id;?>" id="hID"/>
<div class="btn-question">
    <input type="button" value="Bỏ qua" id="unbreak" data-next="<?php if(isset($next)) echo $next; ?>" onclick="unBreak();" class="btn btnStart"/>
    <input type="button" value="Tiếp tục" id="Continue" data-next="<?php if(isset($next)) echo $next; ?>"  onclick="Continue();" class="btn btnFinish"/>
    <div class="main-msg main-err"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án sai</span></div>
    <div class="main-msg main-success"><i class="fa fa-times" aria-hidden="true"></i><span class="msg">Đáp án đúng</span></div>
</div>
<?php } ?>
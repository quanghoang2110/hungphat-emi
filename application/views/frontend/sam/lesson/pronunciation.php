<div class="container content">
    <section>
        <div class="title_detail">
            <h3>Bài học phát âm</h3>
            <a href="<?php echo base_url().ROUTE_FRONTEND_LESSON.'/'.LESSON_MAIN.'-'.PACK_BASIC; ?>">Quay trở lại bài học</a>
        </div>
        <?php if(count($list)>0){?>
            <ul class="itemsLess">
                <?php foreach($list as $key=>$val){?>
                <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center">
                <span >
                    <div class="image thumbnail">
                        <?php if($key == 0){ ?>
                            <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                            <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php }elseif($val->unlocked==1 || $val->lock_type == 1 || ($val->finish < 99 && $val->finish > -1)){ ?>
                            <i class="fa lock fa-unlock-alt" aria-hidden="true"></i>
                            <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php }else{ ?>
                            <i class="fa lock fa-lock" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu">
                                <img class="baseImg" src="<?php if($val->image != '') echo base_url().$val->image; else echo base_url('images/default/image.png'); ?>" alt="<?php echo $val->name; ?>">
                            </a>
                        <?php } ?>
                    </div>
                    <?php if($val->finish==100){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đã hoàn thành" class="btnA btnFinish">Đã hoàn thành</a>
                    <?php }else if(($val->finish < 99 && $val->finish > -1)){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đang học" class="btnA btnFinish">Đang học</a>
                    <?php }elseif((($val->finish == -1 && $val->lock_type == 1) || $key == 0)){ ?>
                        <div class="title">Bài học: <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id; ?>" title="<?php echo $val->name; ?>">"<?php echo $val->name; ?>"</a></div>
                        <a href="<?php echo generate_slug(base_url(ROUTE_FRONTEND_LESSON_DETAIL),$val->name,$val->id).'?pack_id='.$id ?>" title="Đang học" class="btnA btnStart">Bắt đầu</a>
                    <?php }else{ ?>
                        <div class="title">Bài học: <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu"><?php echo $val->name; ?></a></div>
                        <a href="#" data-toggle="modal" data-target="#myModal" title="Bắt đầu" class="btnA btnStart">Bắt đầu</a>
                    <?php } ?>
                </span>
                </li>
            <?php }?>
            </ul>
        <?php }else{ ?>
            <div class="fr quiz">
                <p>Chưa có nội dung nào</p>
            </div>
        <?php } ?>
    </section>
</div>
<style>
    .width_auto{
        width: auto!important;
    }
</style>
<script>
    $(document).ready(function(){
        $("video").addClass('width_auto');
    })

</script>
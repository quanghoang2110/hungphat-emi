<div class="container content">
    <section>
        <?php if($list){?>
            <div class="title_detail">
                <h3><?php echo $list->name; ?></h3>
                <a href="
                <?php if($module){ ?>
                    <?php if(isset($slug)) echo $slug; ?>
                <?php }else{ ?>
                    <?php echo base_url().ROUTE_FRONTEND_PRONUNCIATION_LESSON; ?>
                <?php } ?>
                ">Quay trở lại bài học</a>
            </div>
            <div class="row margin_top_30">
            <span class="col-sm-8 col-md-9">
                <h4>Giới thiệu bài học</h4>
                <div>
                    <?php echo $list->description; ?>
                </div>
            </span>
            <span class="col-sm-4 col-md-3 module_image">
                <img class="baseImg thumbnail" src="<?php echo base_url().$list->image; ?>"/>
            </span>
            </div>
            <?php if(isset($module) && count($module)>0){ ?>
            <div class="module">
                <div class="title-module">
                    <div class="title">Chọn module học</div>
                    <div class="fr-head">
                        <span class="txt_percent">Tỉ lệ hoàn thành(%)</span>
                        <div class="progress-out">
                            <div class="progress-in">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $list->finish; ?>%">
                                        <span class="sr-only">70% Complete</span>
                                    </div>
                                </div>
                                <span class="percent"><?php echo $list->finish; ?>%</span>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="text-center">
                        <?php foreach($module as $key=>$val){ ?>
                            <span class="col-sm-3">
                                <div>
                                    <a href="<?php echo generate_slug_add(base_url(ROUTE_FRONTEND_MODULE_DETAIL)."/",$val->name,$val->id).'?lesson_id='.$lesson_id.'&pack_id='.$pack_id; ?>">
                                        <img class="baseImg" alt="<?php echo $val->name;?> " src="<?php echo base_url().$val->image; ?>"/>
                                    </a>
                                </div>

                                 <?php if($val->lock_module==1 || ($val->lock_module==0 && $val->unlocked == 1)){ ?>
                                        <a href="<?php echo generate_slug_add(base_url(ROUTE_FRONTEND_MODULE_DETAIL)."/",$val->name,$val->id).'?lesson_id='.$lesson_id.'&pack_id='.$pack_id; ?>" title="Đang học" class="btnA btnFinish">
                                            <?php echo $val->name; ?>
                                        </a>
                                 <?php }else{ ?>
                                     <a href="#" data-toggle="modal" data-target="#myModal" title="Đang học" class="btnA btnStart">
                                         <?php echo $val->name; ?>
                                     </a>
                                 <?php } ?>
                            </span>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </section>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog msg-popup modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p class="title">Module này chưa được mở khoá. Vui lòng hoàn thành module trước đó để mở khoá module này</p>
            </div>
        </div>
    </div>
</div>
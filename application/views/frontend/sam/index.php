<html>
<head>
    <title><?php echo $title ?></title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="description" content="<?php echo $desc ?>">
    <meta name="author" content="">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- bootstrap magic -->
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">


    <!-- theme custom -->
    <link rel="stylesheet" href="/assets/frontend/sam/css/home_style.css">

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" type="text/css">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="/assets/frontend/sam/css/font-awesome.css">
    <!--    <link rel="stylesheet" type="text/css" href="/assets/lib/owl-carousel/owl.carousel.css">-->

    <!-- Owl Carousel Assets -->
    <link href="/assets/lib/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="/assets/lib/owl-carousel/owl.theme.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <style>

        #owl-demo .owl-item img {
            display: block;
            width: 100%;
            height: auto;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .services-2 img {
            width: 150px;
            height: auto;
        }

        .owl-controls {
            margin-top: -35px !important;
        }

        .owl-controls span {
            position: relative;
            background: transparent !important;
            /*border: 1px solid #000;*/
            box-shadow: 0 0 2px #000;
            width: 15px !important;
            height: 15px !important;
        }

        #owl-demo .item {
            display: block;
            padding: 30px 0px;
            margin: 5px;
            color: #FFF;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            text-align: center;
        }

        .owl-controls div.active span {
            background: white !important;;
        }

        .owl-theme .owl-controls .owl-buttons div {
            padding: 5px 9px;
        }

        .owl-theme .owl-buttons i {
            margin-top: 2px;
        }

        .owl-theme .owl-controls .owl-buttons div {
            position: absolute;
        }

        .owl-theme .owl-controls .owl-buttons .owl-prev {
            left: 45px;
            top: 55px;
        }

        .owl-theme .owl-controls .owl-buttons .owl-next {
            right: 45px;
            top: 55px;
        }

        @media (max-width: 420px) {
            .col-xs-6 {
                width: 100%;
            }
        }

        body, .services-2 {
            background: #ba0804;
        }

        .services-2 .service-box h3 {
            color: white !important;
            font-weight: 300 !important;
            font-family: Segoe UI;
        }

        .clr-1{
            text-transform: uppercase;
        }
    </style>

</head>

<body>

<div id="page" class="page">

    <div class="slider">
        <div id="owl-demo" class="owl-carousel">
            <?php foreach ($slider as $item) : ?>
                <div><img class="lazyOwl" data-src="<?php echo get_img_url($item->img) ?>"/></div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="services-2">
        <div class="container">
            <?php foreach ($home_menu as $mn): ?>
                <div class="col-xs-6 col-md-3">
                    <a href="/<?php echo $mn->slug ?>">
                        <div class="service-box">
                            <?php echo get_img_tag($mn->icon, $mn->name); ?>
                            <h3 class="clr-1"><?php echo $mn->name ?></h3>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>


<!-- Jquery Libs -->
<!-- Latest Version Of Jquery -->
<script type="text/javascript" src="/assets/frontend/sam/js/jquery-1.11.1.min.js"></script>
<!-- Bootstrap Jquery -->
<script type="text/javascript" src="/assets/base/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/lib/owl-carousel/owl.carousel.min.js"></script>

<script>

    $(document).ready(function () {

        $("#owl-demo").owlCarousel({
            autoPlay: 3000,
            navigation: false, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoHeight: true,
            lazyLoad: true
        });

    });
</script>
</body>
</html>
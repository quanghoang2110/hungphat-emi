<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<div class="container" style="margin-bottom: 50px; margin-top: 50px">
    <?php foreach ($list as $index => $item): ?>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6"
             style="border: 1px solid #ccc; margin-left: 20px; height: 450px">
            <img class="thumbnail margin-top-20 col-lg-12 col-md-12  col-sm-12 col-xs-12"
                 src="<?php echo $item->image ?>" style="height: 150px">
            <h3 class="text-over col-lg-12 col-md-12  col-sm-12 col-xs-12"
                style="height: 50px"><?php echo $item->title ?></h3>
            <div class=" text-over col-lg-12 col-md-12  col-sm-12 col-xs-12"
                 style="height: 150px"><?php echo $item->content ?></div>
            <div style="text-align: center" class=" col-lg-6 col-md-6col-sm-6 col-xs-6">
                <i class="glyphicon glyphicon-eye-open" style="margin-left: 5px"></i> <?php echo $item->total_view ?>
            </div>
            <div style="text-align: center" class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <a href="<?php echo base_url(ROUTE_FRONTEND_NEWS_DETAIL) . '?news=' . $item->id ?>">Xem thêm ...</a>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<style>
    .text-over {
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .margin-top-20 {
        margin-top: 20px;
    }

    .padding-left-20 {
        padding-left: 20px;
    }
</style>
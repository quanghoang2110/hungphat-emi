<div id="header-slide-home">
    <?php $this->load->view('frontend/sam/home_slide'); ?>
</div>
<div class="content">
    <div class="container tintuc">
        <?php foreach ($list as $index => $item): ?>
            <div class="col-xs-6 col-md-4 down-content">
                <div class="tintuc-item">
                    <div class="img">
                        <?php echo get_img_tag($item->image, $item->title, '', IMG_NEWS_DEFAULT); ?>
                    </div>
                    <div class="text-over">
                        <!--                        <h1 class="title">-->
                        <?php //echo get_news_tag($item->title, $item->id, "") ?><!--</h1>-->
                        <h1 class="title"><?php echo get_detail_tag(ROUTE_TINTUC, $item->title, $item->id, "") ?></h1>
                    </div>
                    <div class="text-over desc">
                        <?php echo $item->desc ?>
                    </div>
                    <div class="views pull-right">
                        <span class="glyphicon glyphicon-eye-open"></span><span
                            class="soluong">&nbsp;<?php echo number_format($item->total_view, 0, '.', ',') ?></span>
                        <a href="<?php echo get_detail_url(ROUTE_TINTUC, $item->title, $item->id) ?>"
                           class="customLink">Xem
                            thêm...</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<style>
    .tintuc {
        margin: 30px auto 60px;
    }
</style>
<!-- <div id="header-slide-home">
<!--    --><?php //$this->load->view('frontend/wow_english/home_slide'); ?>
<!--</div> -->
<div class="container content">
    <ul class="nav nav-tabs">
        <li><a class="title-panel-no-active padding-0" href="<?php echo base_url(ROUTE_FRONTEND_THONG_TIN_CA_NHAN)?>">  <i class="fa fa-info-circle" aria-hidden="true"></i> <span class="font_size_18">Thông tin cá nhân</span></a></li>
        <li class="active"><a data-toggle="tab" class="title-panel" href="#account"> <i class="fa fa-user" aria-hidden="true"></i> <span class="font_size_18">Tài khoản</span></a></li>
    </ul>
    <div class="tab-content">
        <!--        thong tin ca nhan-->
        <div id="account" class="tab-pane fade in active">
            <form id="frm-info" class="form-horizontal">
                <div class="col-xs-2"></div>
                <div class="margin_auto col-xs-8 box-body margin_bottom_50 margin_top_50">
                    <?php $now = new DateTime(Date('d-m-Y')); ?>
                    <?php if($count >0):?>
<!--                        --><?php //$count = 0; ?>
                        <?php foreach ($list_pack as $key => $val): ?>
                            <?php $ended_date = new DateTime(date("d-m-Y", strtotime($val->ended_at))); ?>
                            <?php $ended_date = $now->diff($ended_date); ?>
                            <?php $ended_date = $ended_date->format('%R%a days'); ?>
                            <?php $created_date = new DateTime(date("d-m-Y", strtotime($val->created_at))); ?>
                            <?php $created_date = $created_date->diff($now); ?>
                            <?php $created_date = $created_date->format('%R%a days') ?>
                            <div class="width_col_12 border-all padding-70  margin_bottom_50 margin_top_50">
                                <div class="row margin_bottom_20">
                                    <span class="col-xs-5">Khóa học: </span>
                                    <span class="col-xs-7 font-weight-700 color-green-tuyen"><?php echo $val->pack_name ?></span>
                                </div>
                                <div class="row margin_bottom_20">
                                    <span class="col-xs-5">Ngày kích hoạt: </span>
                                    <span
                                        class="col-xs-7 font-weight-700 color-red-tuyen"><?php echo date("d-m-Y H:i:s", strtotime($val->created_at)) ?></span>
                                </div>
                                <div class="row margin_bottom_20">
                                    <span class="col-xs-5">Thời gian còn lại: </span>
                                    <span class="col-xs-7 font-weight-700 .color-green-tuyen"><?php echo $ended_date ?></span>
                                </div>
                                <div class="row margin_bottom_20">
                                    <span class="col-xs-5">Thời gian đã học: </span>
                                    <span class="col-xs-7 font-weight-700 color-green-tuyen"><?php echo $created_date ?></span>
                                </div>
                                <div class="row ">
                                    <span class="col-xs-5">Số bài đã hoàn thành </span>
                                    <span class="col-xs-7 font-weight-700 color-green-tuyen"><?php if(isset($list_lesson[$count]))echo count($list_lesson[$count]); else echo '0'; ?></span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else:?>
                            <div class="width_col_12 border-all padding-70  margin_bottom_50 margin_top_50">
                                <span> Bạn chưa tham gia khóa học nào.</span>
                            </div>
                    <?php endif;?>
                </div>
            </form>
        </div>
        <!--        <div id="menu1" class="tab-pane fade">-->
        <!--            <h3>Menu 1</h3>-->
        <!--            <p>Some content in menu 1.</p>-->
        <!--        </div>-->
    </div>
</div>

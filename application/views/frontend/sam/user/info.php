<!-- <div id="header-slide-home">
<!--    --><?php //$this->load->view('frontend/wow_english/home_slide'); ?>
<!--</div> -->
<div class="container content">
    <ul class="nav nav-tabs">
        <li class="active"><a class="title-panel " data-toggle="tab" href="#info"> <i class="fa fa-info-circle" aria-hidden="true"></i> <span class="font_size_18">Thông tin cá nhân</span></a></li>
        <li><a class="title-panel-no-active" href="<?php echo base_url(ROUTE_FRONTEND_QUAN_LY_TAI_KHOAN)?>"> <i class="fa fa-user" aria-hidden="true"></i> <span class="font_size_18">Tài khoản</span></a></li>
    </ul>
    <div class="tab-content">
<!--        thong tin ca nhan-->
        <div id="info" class="tab-pane fade in active">
            <form  id="frm-info"class="form-horizontal">
                <div class="col-xs-2"></div>
               <div class="margin_auto col-xs-8 box-body margin_bottom_50 margin_top_50">

                       <?php foreach ($info as $key=>$val):?>
                           <div class="form-group ">
                               <label class="col-sm-2 control-label" for="inputEmail3">Họ</label>
                               <div class="col-sm-4">
                                   <input  class="form-control" type="text" name="first_name" id="first_name"placeholder="Họ" value="<?php echo $val->first_name?>">
                               </div>
                               <label class="col-sm-2 control-label" for="inputEmail3">Tên</label>
                               <div class="col-sm-4">
                                   <input  class="form-control" type="text" placeholder="Tên" name="last_name" id="last_name"value="<?php echo $val->last_name?>">
                               </div>
                           </div>  <div class="form-group ">
                                <label class="col-sm-2 control-label" for="inputEmail3">Ngày sinh</label>
                                <div class="col-sm-10">
                                    <input  class="form-control datepicker" name="birthday" id="birthday" type="text" placeholder="Ngày sinh dd/mm/yyyy" value="<?php echo date("d-m-Y", strtotime($val->birthday));?>">
                                </div>
                            </div>
                           <div class="form-group ">
                               <label class="col-sm-2 control-label" for="inputEmail3">Giới tính</label>
                               <div class="col-sm-10">
                                   <select class="form-control" name="gender" id="gender">
                                        <option value="nam" <?php if($val->gender == 1) echo 'selected'?>> Nam </option>
                                       <option value="nữ" <?php if($val->gender == 0) echo 'selected'?>> Nữ </option>
                                   </select>
<!--                                   <input class="form-control" type="text" placeholder="Giới tính" value="--><?php //echo $val->gender?><!--">-->
                               </div>
                           </div>
                           <div class="form-group ">
                               <label class="col-sm-2 control-label" for="inputEmail3">Số CMND</label>
                               <div class="col-sm-10">
                                   <input  class="form-control" name="identify_code"  id="identify_code"type="text" placeholder="Số CMND"  value="<?php echo $val->identify_code?>">
                               </div>
                           </div>
                           <div class="form-group ">
                               <label class="col-sm-2 control-label" for="inputEmail3">Địa chỉ</label>
                               <div class="col-sm-10">
                                   <input  class="form-control" type="text" name="address" id="address"placeholder="Địa chỉ"  value="<?php echo $val->address?>">
                               </div>
                           </div>
                           <div class="form-group ">
                               <label class="col-sm-2 control-label" for="inputEmail3">Nghề nghiệp</label>
                               <div class="col-sm-10">
                                   <input  class="form-control" name="job" id="job" type="text" placeholder="Nghề nghiệp"  value="<?php echo $val->job?>">
                               </div>
                           </div>

                       <?php endforeach;?>
                    <div class="col-xs-2"></div>
                    <div class=" box-footer margin_bottom_30">
                        <button class="btn btn-danger margin_auto" type="button" onclick="updateInfo()"> Cập nhật thông tin</button>
                        <button class="btn btn-primary margin_auto" type="button"  onclick="change_pass();"> Thay đổi mật khẩu</button>
                    </div>
                    <div class=" color_red border_all_red margin_bottom_30">
<!--                        Lưu ý: Việc cập nhật email của bạn giúp chúng tôi bảo vệ tài khoản của bạn tốt hơn và bảo vệ lợi ích của bạn khi có tranh chấp-->
                        Lưu ý: Việc cập nhật email giúp chúng tôi bảo vệ tài khoản và  lợi ích của bạn khi có tranh chấp
                    </div>
               </div>
            </form>
        </div>
<!--        <div id="menu1" class="tab-pane fade">-->
<!--            <h3>Menu 1</h3>-->
<!--            <p>Some content in menu 1.</p>-->
<!--        </div>-->
    </div>
</div>

<!-- dang ki -->
<div class="modal fade" id="dangkihockhoahoc" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <h4>Đăng kí mua khóa học thành công</h4>
            </div>
        </div>

    </div>
</div>
<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<!-- content -->
<div class="content">
    <div class="container">
        <div class="content1">
            <!-- <div class="container"> -->
            <div class="container-fluid bg-2 text-center">
                <p class="text-content1">FULL SERVICE DIGITAL MARKETING AGENCY</p>
                <p class="text-content2">Search Engine & Social Media Optimization Experts</p>
            </div>
            <!-- </div> -->
        </div>
    </div>
    <div class="content2">
        <div class="container">
            <p class="text-content3 margin">Hệ thống khóa học</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<!--                he thong cac khoa hoc-->
                <?php foreach ($list as $index => $item):?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="container-fluid bg-1 text-center">
                            <img src="<?php echo base_url($item->image)?>" class="img-responsive img-circle margin display-inline img-logo-pack"  alt="<?php echo $item->pack_name?>" width="350" height="350">
                            <h3><?php echo $item->pack_name?></h3>
                            <p class="description-pack"><?php echo $item->description?></p>
                            <?php if($item->id == $watch_pack_id){?>
                                <a href="<?php echo base_url(ROUTE_FRONTEND_PACK_WATCH).'?pack='.$item->id;?>" class="no-margin btn btn-default btn-lg click-watch-pack ">
                                    <span class="color-word-default font-weight-700"> Xem thêm </span>
                                </a>
                                <?php $pack_name = $item->pack_name;?>
                                <?php $description = $item->description;?>
                                <?php $time = $item->pack_time?>
                            <?php }else{?>
                                <a href="<?php echo base_url(ROUTE_FRONTEND_PACK_WATCH).'?pack='.$item->id;?>" class="no-margin btn btn-primary btn-lg click-watch-pack">
                                    <span class="color-white font-weight-700"> Xem thêm </span>
                                </a>
                            <?php }?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>

</div>
    <!-- the mua khoa hoc -->
    <?php if(isset($pack_name)):?>
        <div class="muakhoahoc">
        <div class="container">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <p class="tieude_1 col-xs-12 col-sm-8 col-md-8 col-lg-8"><?php echo $pack_name?></p>
                <button class="tieude_2 col-xs-12 col-sm-4 col-md-4 col-lg-4" id="buy_pack"
                        data-session="<?php if(!$this->session->userdata(FRONTEND_SESSION_USERID)) {echo 'none-session';}else {echo $this->session->userdata(FRONTEND_SESSION_USERID);}?>"
                        data-pack="<?php echo $watch_pack_id;?>"
                        data-time = "<?php echo $time?>"data-href="<?php echo base_url(ROUTE_FRONTEND_PACK_WATCH_LIST_LESSON).'?pack='.$watch_pack_id;?>">Mua Khóa Học Này</button>
                <div class="noidungmua col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php echo $description;?>
                </div>
            </div>
            <!-- .quyenloicuakh -->
            <div class="quyenloikh1 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="container-fluid bg-2 text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="chu">
                        <p class="margin text-ql text-center"><strong>Quyền lợi của tài khoản</strong></p>
                        <?php $count_permission = 0;?>
                        <?php foreach($permission as $key => $val):?>
                            <div class="chu1">
                                <?php $count_permission += 1;?>
                                <div class="chu1-left">
                                    <p class="margin"><?php echo $val->permission;?></p>
                                </div>
                                <div class="chu1-right1">
                                    <?php echo $count_permission;?>
                                </div>
                            </div>
                        <?php endforeach;?>
                        </div>
                    </div> <!-- dong the chu -->
                </div> <!-- dong the container-fluid bg-2 text-center -->
            </div><!-- ket thuc quyenn loi khach hang -->
        </div>
    <?php endif;?>
    </div> <!-- ket thuc muakhoahoc -->
    <!-- ket thuc noi dung mua -->
<!--    <div class="commentfacebook">-->
<!---->
<!---->
<!--    </div>-->
<div class="background-color-blue col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="endcontent  container-fluid bg-2 text-center padding-top-70">
        <p class="margin end-text">HƯỚNG DẪN HỌC WOW ENGLISH ONLINE</p>
        <p>Hệ thông Efforltess English Online gồm các bài học bản quyền của Ajhope,được thiết kế thành lộ trình học bài bản</p>
    </div>
    <div class="timhieungay container-fluid bg-2 text-center ">
            <a href="#"><button type="button" class="button-timhieungay  btn-warning">Tìm hiểu ngay</button></a>
    </div>

</div>

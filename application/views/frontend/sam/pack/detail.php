<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<div>
    <div class="container">
        <div class="news-title text-center">
            <h2>Hệ thống khóa học</h2></div>
        <div class="row pack_content">
            <?php foreach ($list as $key=>$item): ?>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <span class="khoahoc_row_3item khoahoc_row_3item_first">
                        <div class="image">
                            <a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>"><?php echo get_img_tag($item->image, $item->pack_name, 'img_pack', IMG_PACK_DEFAULT) ?></a>
                        </div>
                        <div class="title">
                            <a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>"
                               title="<?php echo $item->pack_name ?>"><h3><?php echo $item->pack_name ?></h3></a>
                        </div>
                        <div class="pack_desc text-over"><?php echo $item->description ?></div>
                        <?php if ($info && $info->id != $item->id){ ?>
                            <a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>" title="Xem thêm" class="view"><button
                                    class="btn btn-pack">Xem thêm</button></a>
                        <?php }else{?>
                            <?php $time = $item->pack_time;?>
                            <?php $image = $item->image?>
                            <?php $pack_name = $item->pack_name;?>
                            <?php $money = $item->money;?>
                            <?php $pack_id = $item->id?>
                        <?php } ?>
                    </span>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
<?php //if ($info && count($info) > 0): ?>
<?php if (isset($pack_id)): ?>
    <div class="khoahoc_category_more">
        <div class="container pack_content">
            <div class="col-md-8 overflow-hidden">
                <div class="news-title overflow-hidden">
                    <div class="col-sm-8"><h2><?php echo $info->pack_name ?></h2></div>
                    <div class="col-sm-4 text-right">
                        <?php if($count > 0):?>
                            <a class="btn btn-danger " href="<?php echo base_url().'pack/bai-hoc-chinh-'.$pack_id?>"> Vào học » </a>
                        <?php else:?>
<!--                            <button class="btn btn-danger btn-muakh" id="buy_pack"-->
<!--                                    data-session="--><?php //if(!$this->session->userdata(FRONTEND_SESSION_USERID)) {echo 'none-session';}else {echo $this->session->userdata(FRONTEND_SESSION_USERID);}?><!--"-->
<!--                                    data-pack-id="--><?php //echo $pack_id;?><!--"-->
<!--                                    data-pack-name="--><?php //echo $pack_name?><!--"-->
<!--                                    data-image="--><?php //echo $image?><!--"-->
<!--                                    data-money="--><?php //echo $money?><!--"-->
<!--                                    data-time = "--><?php //echo $time?><!--">Mua khóa học này »</button>-->
                            <button class="btn btn-danger btn-muakh" id="buy_pack"
                                    data-session="<?php if(!$this->session->userdata(FRONTEND_SESSION_USERID)) {echo 'none-session';}else {echo $this->session->userdata(FRONTEND_SESSION_USERID);}?>"
                                    data-pack-id="<?php echo $pack_id;?>"
                                    data-pack-name="<?php echo $pack_name?>"
                                    data-image="<?php echo $image?>"
                                    data-money="<?php echo $money?>"
                                    data-time = "<?php echo $time?>">Mua khóa học này »</button>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <?php echo $info->content; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="khoahoc_sidebar">
                    <img src="/assets/frontend/wow_english/img/khoahoc_sidebar_head.jpg"
                         class="khoahoc_sidebar_head_image">
                    <div class="khoahoc_sidebar_head">Quyền lợi của tài khoản</div>
                    <ul>
                        <?php foreach ($permission as $index => $pr): ?>
                            <li><span class="sidebar_item_content"><?php echo $pr->permission ?></span><span
                                    class="sidebar_item_number"><span><?php echo($index + 1) ?></span></span></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="pull-left">
                    <!-- Place this tag in your head or just before your close body tag. -->
                    <script src="https://apis.google.com/js/platform.js" async defer></script>

                    <!-- Place this tag where you want the +1 button to render. -->
                    <div class="g-plusone" data-annotation="inline" data-width="300"></div>
                </div>
                <div class="pull-left">
                    <div class="fb-like" data-href="<?php echo base_url($slug) ?>" data-layout="standard"
                         data-action="like"
                         data-size="small" data-show-faces="true" data-share="true"></div>
                </div>
            </div>
            <div class="news-comment">
                <div class="fb-comments" data-href="<?php echo base_url($slug) ?>" data-numposts="3"></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<!--huong dan-->
<div class="khoahoc_category_more">
    <div class="container pack_content">

        <div class="news-title text-center">
            <h2>Hướng dẫn học Online</h2></div>
        <div class="col-sm-3"></div>
        <div class="info col-sm-6 text-center">
            <div>
                <a href="/huong-dan-hoc" title="Hướng dẫn học Effortless English Online">
                    <img class="img_pack" src="<?php echo IMG_PACK_DEFAULT ?>"
                         alt="Hướng dẫn học Online"> </a>
            </div>
            <div class="desc padding-top-15 padding-bottom-15">Hệ thống học Efforltess English Online gồm các bài học bản quyền của A.J.Hoge,
                được thiết
                kế thành lộ trình học bài bản kết hợp các tính năng học thông minh
            </div>
            <a href="/huong-dan-hoc" title="Xem thêm" class="text-center">
                <button class="btn btn-primary">Tìm hiểu ngay</button>
            </a>
        </div>
    </div>
</div>

<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<div>
    <div class="container">
        <div class="news-title text-center">
            <h2>Hệ thống khóa học</h2></div>
        <div class="row pack_content">
            <?php foreach ($list as $item): ?>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
		<span class="khoahoc_row_3item khoahoc_row_3item_first">
			<div class="image">
                <a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>"><?php echo get_img_tag($item->image, $item->pack_name, 'img_pack', IMG_PACK_DEFAULT) ?></a>
            </div>
			<div class="title">
                <a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>"
                   title="<?php echo $item->pack_name ?>"><h3><?php echo $item->pack_name ?></h3></a>
            </div>
			<div class="pack_desc text-over"><?php echo $item->description ?></div>
			<a href="<?php echo get_pack_url($item->pack_name, $item->id) ?>" title="Xem thêm" class="view"><button
                    class="btn btn-pack">Xem thêm</button></a>
		</span>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
<div class="khoahoc_category_more">
    <div class="container pack_content">

        <div class="news-title text-center">
            <h2>Hướng dẫn học Online</h2></div>
        <div class="col-sm-3"></div>
        <div class="info col-sm-6 text-center">
            <div>
                <a href="/huong-dan-hoc" title="Hướng dẫn học Effortless English Online">
                    <img class="img_pack" src="<?php echo IMG_PACK_DEFAULT ?>"
                         alt="Hướng dẫn học Online"> </a>
            </div>
            <div class="desc">Hệ thống học Efforltess English Online gồm các bài học bản quyền của A.J.Hoge,
                được thiết
                kế thành lộ trình học bài bản kết hợp các tính năng học thông minh
            </div>
            <a href="/huong-dan-hoc" title="Xem thêm" class="text-center">
                <button class="btn btn-primary">Tìm hiểu ngay</button>
            </a>
        </div>
    </div>
</div>
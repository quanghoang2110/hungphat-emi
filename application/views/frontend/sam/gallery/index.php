<div id="header-slide-home">
    <?php $this->load->view($theme_view . '/home_slide'); ?>
</div>
<div class="content">
    <div class="container tintuc">
        <?php foreach ($list as $index => $item): ?>
            <div class="col-xs-6 col-md-4 down-content">
                <div class="tintuc-item">
                    <div class="img">
                        <?php echo get_img_link_tag(ROUTE_GALLERY, $item->title, $item->id, $item->image); ?>
                    </div>
                    <div class="text-over">
                        <h1 class="title"><a
                                href="<?php echo get_detail_url(ROUTE_GALLERY, $item->title, $item->id) ?>"><?php echo $item->title ?></a>
                        </h1>
                    </div>
                    <div class="text-over desc">
                        <?php echo $item->description ?>
                    </div>
                    <div class="views pull-right">
                        <span class="glyphicon glyphicon-eye-open"></span><span
                            class="soluong">&nbsp;<?php echo number_format($item->total_view, 1, '.', ',') ?></span>
                        <a href="<?php echo get_detail_url(ROUTE_GALLERY, $item->title, $item->id) ?>"
                           class="customLink">Xem
                            thêm...</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <!--    <div class="container-fluid bg-2 text-center">-->
    <!--        <a href="#" class="btn btn-default btn-lg">-->
    <!--            Xem thêm-->
    <!--        </a>-->
    <!--    </div>-->
</div>
<style>
    .tintuc {
        margin: 30px auto 60px;
    }
</style>
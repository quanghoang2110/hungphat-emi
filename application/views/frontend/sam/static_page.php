<div id="header-slide-home">
    <?php $this->load->view('frontend/sam/home_slide'); ?>
</div>

<?php foreach ($list as $index => $item) {
    if (!($index % 2) == 0) {
        ?>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <a class="inline-block"
                           href="<?php echo get_detail_url('', $item->title, $item->id) ?>"><?php echo get_img_tag($item->image, $item->title, 'img-circle', IMG_PACK_DEFAULT); ?></a>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-center">
                            <h3 class="title"><a
                                    href="<?php echo get_detail_url('', $item->title, $item->id) ?>"><?php echo $item->title ?></a>
                            </h3>
                            <h5 class="sub_title inline-block"><?php echo $item->sub_title ?></h5>
                        </div>
                        <p class="desc"><?php echo $item->description ?></p>
                        <a href="<?php echo get_detail_url('', $item->title, $item->id) ?>" class="xemthem">Xem thêm
                            >></a>
                    </div>
                </div>
            </div>

        </div>
    <?php } else { ?>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-6 text-center">
                        <a onclick="detail(<?php echo $item->id ?>)"
                           class="inline-block"><?php echo get_img_tag($item->image, $item->title, 'img-circle', IMG_PACK_DEFAULT); ?></a>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6">
                        <div class="text-center">
                            <h3 class="title"><a
                                    href="<?php echo get_detail_url('', $item->title, $item->id) ?>"><?php echo $item->title ?></a>
                            </h3>
                            <h5 class="sub_title inline-block"><?php echo $item->sub_title ?></h5>
                        </div>
                        <p class="desc"><?php echo $item->description ?></p>
                        <a href="<?php echo get_detail_url('', $item->title, $item->id) ?>" class="xemthem">Xem thêm
                            >></a>
                    </div>

                </div>
            </div>

        </div>

    <?php }
} ?>
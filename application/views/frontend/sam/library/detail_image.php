<!--chi tiết hình ảnh-->
<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<?php
//var_dump($image);
?>
<?php if ($image) { ?>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="news-title">
                    <h2><?php echo $image->title ?></h2>
                </div>

                <div class="news-content">
                    <?php echo $image->description ?>
                </div
            </div>
        </div>
    </div>
<?php } ?>

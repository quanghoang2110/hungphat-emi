<!--hình ảnh-->
<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>
<div class="content">
    <div class="col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3" style="text-align: center;">
        <h2 id="tieudend" style="color:#1a75bc; text-align: center;"><span>Full Service Digital marketing Agency</span></h2>
        <span id="tieudend">Search Engine & Social Media optimization Experts</span>
        <hr style="width: 60%; color: #ccc; margin-bottom: 2%;" />
    </div>
    <?php foreach ($video as $index => $item): ?>
    <div class="container">
        <div class="col-xs-12 col-md-4 down-content" style="width: 31%; padding: 2%; border: 1px solid #ccc; margin: 0 2% 2% 0;">
            <div class="video">
                <img style="width: 100%; height: 30%;" src="<?php echo get_image_url($item->image);?>" alt="">
            </div>
            <div class="phuongphap">
                <h1><span class="tieudepp"><?php echo $item->title;?></span></h1>

            </div>
            <div class="noidungphuongphap">
                <span class="ndphuongphap"><?php echo $item->description;?></span><br/>
                <div style="float: right;">
                    <span class="glyphicon glyphicon-eye-open">&nbsp;</span><span class="soluong">
                        <?php echo $item->total_view;?>&nbsp
                        <a href="<?php echo base_url() . $item->title . '/' . $item->id;?>">
                            <span class="chitietxemthem" style="color:#1a75bc">Xem thêm...</span>
                        </a>
                    </span>
                </div>

            </div>
        </div>
    </div>
    <?php endforeach; ?>

    <div class="container-fluid bg-2 text-center">
        <a href="#" class="btn btn-default btn-lg">
            Xem thêm
        </a>
    </div>
</div>

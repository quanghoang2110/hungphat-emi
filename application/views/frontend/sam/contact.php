<div id="header-slide-home">
    <?php $this->load->view('frontend/sam/home_slide'); ?>
</div>
<div class="container">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3 class="border_bottom_contact">Thông tin liên hệ</h3>
        <p>
            <?php echo $company_info ?>
        </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin_bottom_50">
        <h3 class="border_bottom_contact ">Gửi liên hệ</h3>
        <form method="post">
            <div class="form-group">
                <?php echo isset($err_msg) ? $err_msg : ''; ?>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control" placeholder="Họ và tên"
                       value="<?php echo $input_data ? $input_data['fullname'] : '' ?>" name="fullname" required>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control" placeholder="Số điện thoại" name="phone"
                       required value="<?php echo $input_data ? $input_data['phone'] : '' ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-bottom-15">
                <input type="email" class="form-control" placeholder="Email" name="email_contact"
                       required value="<?php echo $input_data ? $input_data['email_contact'] : '' ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-bottom-15">
                <input type="text" class="form-control" placeholder="Nơi ở hiện tại" name="address"
                       required value="<?php echo $input_data ? $input_data['address'] : '' ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-bottom-15">
                <input type="text" class="form-control" placeholder="Nhập mã bảo vệ" name="code"
                       required value="<?php echo $input_data ? $input_data['code'] : '' ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-bottom-15">
                <?php echo docreateCaptcha(); ?>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <textarea class="form-group form-control col-lg-12 col-md-12 col-sm-12 col-xs-12" rows="4"
                      placeholder="Nội dung bạn muốn chia sẻ với Wow" required name="content"
                      id="content_contact"><?php echo $input_data ? $input_data['content'] : '' ?></textarea>
                <button type="submit" class="btn btn-primary"> Gửi thông tin</button>
            </div>
        </form>
    </div>
</div>
<style>
    .form-group p{
        text-align: center;
    }
</style>

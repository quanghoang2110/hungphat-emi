<div id="header-slide-home">
    <?php $this->load->view('frontend/wow_english/home_slide'); ?>
</div>

<?php foreach ($list_introduce as $index => $item) {
	if(($item->id)%2 == 1){

?>
<div id="content">
 	<div class="container">
 		<div class="row">
 			<div class="col-sm-6">	
 				<a onclick= "detail(<?php echo $item->id ?>)"><?php echo get_img_tag($item->image,'', base_url('images/default/image.png')); ?></a>
 			</div>
			<div class="col-sm-6">
				<h3 onclick= "detail(<?php echo $item->id ?>)"><?php echo $item->title ?></h3>
				<p class="lead"><?php echo $item->description ?></p>
				<a onclick= "detail(<?php echo $item->id ?>)"><p class="xemthem">xem thêm>></p></a>
			</div>
		</div>
	</div>
</div>
<?php }else{ ?>
<div id="content">
 	<div class="container">
 		<div class="row">
 			<div class="col-sm-6 col-sm-push-6">	
 				<a onclick= "detail(<?php echo $item->id ?>)"><?php echo get_img_tag($item->image,'', base_url('images/default/image.png')); ?></a>
 			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<h3 onclick= "detail(<?php echo $item->id ?>)"><?php echo $item->title ?></h3>
				<p class="lead"><?php echo $item->description ?></p>
				<a onclick= "detail(<?php echo $item->id ?>)"><p class="xemthem">xem thêm>></p></a>
			</div>
			
		</div>
	</div>
	
</div>

<?php } } ?>
<input type="hidden" id="hdCurrentPage" value="1"/>

<script type="text/javascript">
	urlDetail = "<?php echo base_url(ROUTE_FRONTEND_INTRODUCE_DETAIL); ?>";

	function detail(id){
		window.location.href =  urlDetail + '?id=' + id;
	}
</script>
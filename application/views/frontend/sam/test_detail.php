<div id="back_top"></div>
<div id="header-2">
    <div class="container">
        <div class="row">
            <div class="kiemtratest">
                <div class="col-lg-4 col-sm-4 col-xs-4">
                    <p class="thoigian">
                        <span class="col-sm-4 col-xs-12">
                              <img src="/uploads/default/imgpsh_fullsize.png" class="clock margin_auto">
                        </span>
                        <span class="time_clock" id="m"></span>
                        <span class="time_clock"> :</span>
                        <span class="time_clock" id="s"></span>
                    </p>
                </div>
                <div class="col-lg-4 text-right2 text-center col-sm-4 col-xs-4">
                    <p class="thoigian title_test"><?php echo $name; ?></p>
                </div>
                <div class="col-lg-4 text-right1 col-sm-4 col-sx-4">
                    <p class="thoigian">Question <span id="stt_question">1-2</span>/<span
                            class="sum_exam_question"><?php echo $total; ?></span></p>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- <input type="button" value="Start" onclick="start()"/> -->
<div class="content2 margin_bottom_50">
    <div class="container col-xs-12">
        <div class="nhungcauhoi">
            <div class="row ">
                <div class="baitest col-xs-12" align="center" id="select_page">
                    <p>Bài Test <span class="color_link"><?php echo $name ?></span></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-hidden"></div>
                <div class="margin_bottom_50 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!--						danh sach cac topic trong bai test-->
                    <?php $sum = $sum_topic = $count_text = $count_quiz = $count_member = 0;//dem tong so cau hoi // dem so cau hoi TEXT ?>
                    <?php echo $topic_name = $question_img = $question_audio = $question_name = $question_id = ''; ?>
                    <?php for ($i = 0; $i < $count; $i++): ?>
                        <!--							foreach danh sach cac cau hoi trong phan thi do-->
                        <div class="row container_question ">
                            <?php foreach ($list_question_answer[$i] as $key => $val): ?>
                                <?php if ($topic_name == ''): ?>
                                    <?php $sum_topic = $sum_topic + 1 ?>
                                    <!--							--><?php //echo $sum_topic;?>
                                    <?php $topic_name = $val->t_name; ?>
                                    <!--										tieu de-->
                                    <div>
                                        <div
                                            class="text-center tuvungvanguphap padding-top-20 border_top width_col_12 topic_name_<?php echo $sum + 1; ?>"
                                            style=";">
                                            <strong>Part <?php echo($sum_topic); ?> <?php echo $topic_name; ?></strong>
                                            : <b><?php echo $val->t_description; ?></b></div>
                                    </div>
                                <?php elseif ($val->t_name != $topic_name): ?>
                                    <?php $sum_topic = $sum_topic + 1; ?>
                                    <!--							--><?php //echo $sum_topic;?>
                                    <?php $topic_name = $val->t_name; ?>
                                    <!--										tieu de-->
                                    <div
                                        class="text-center tuvungvanguphap border_top width_col_12 topic_name_<?php echo $sum + 1; ?>"
                                        style="color:#0485c7;">
                                        <strong>Part <?php echo($sum_topic); ?> <?php echo $topic_name; ?></strong> :
                                        <b><?php echo $val->t_description; ?></b></div>
                                <?php endif; ?>
                                <!--										caau hoi-->
                                <?php if ($val->eq_content != $question_name || $val->eq_img != $question_img || $val->eq_audio != $question_audio): ?>
                                    <?php $question_name = $val->eq_content; ?>
                                    <?php $question_img = $val->eq_img; ?>
                                    <?php $question_audio = $val->eq_audio; ?>
                                    <?php $question_id = $val->eq_id; ?>
                                    <?php $sum += 1; ?>
                                    <div class="col-xs-12 height-20 height-20-<?php echo $sum; ?>" ></div>
                                    <h4 class="cau1 font_weight_700 question_<?php echo $sum ?> color_link margin_top_30" >
                                        Câu <?php echo $sum; ?>:</h4>
                                    <div
                                        class="header_question_<?php echo $sum; ?> col-lg-12 col-md-12 col-xm-12 col-xs-12">
                                        <!--											neu co audio-->
                                        <?php if ($val->eq_audio != null && $val->eq_audio != '') { ?>
                                            <div class="width_col_12 margin_top_30" align="center">
                                                <audio controls>
                                                    <source src="<?php echo base_url($val->eq_audio) ?>"
                                                            type="audio/mpeg">
                                                    Your browser does not support the audio element.
                                                </audio>
                                            </div>
                                        <?php } ?>
                                        <?php if ($val->eq_img != null && $val->eq_img != '') { ?>
                                            <div class="width_col_12 margin_top_20" align="center">
                                                <img src="<?php echo base_url($val->eq_img) ?>" class="img_question_test "/>
                                            </div>
                                        <?php } ?>
                                        <p class="noidungcauhoi margin_top_15 margin_bottom_30 color_link"> <?php echo $val->eq_content; ?></p>
                                    </div>
                                    <!--							<h4 class="cau1 font_weight_700 question_--><?php //echo $sum?><!--" style="color:#66a027">Câu : --><?php //echo $sum;?><!--</h4>-->
                                    <!--										neu cau tra loi dang Text thi in inp input cho người dùng nhập-->
                                    <?php if ($val->eq_type == 'QUIZ'): ?>
                                        <div class="cautraloi col-sx-6 col-sm-6 col-md-6 answer_<?php echo $sum ?>">
                                            <?php $count_quiz += 1; ?>
                                            <p><input type="radio" data-checked='0' class="width_50"
                                                      name="fill_quiz_answer_<?php echo $count_quiz ?>"
                                                      data-correct="<?php echo $val->ea_is_correct ?>">
                                                 <?php echo $val->ea_content; ?></p>
                                            <?php $count_member = 1 ?>
                                        </div>
                                    <?php elseif ($val->eq_type == 'TEXT'): ?>
                                        <?php $count_text += 1; ?>
                                        <input id="fill_text_answer_<?php echo $sum ?>" class="form-control"
                                               name="fill_text_answer_<?php echo $sum ?>">
                                        <input type="hidden" id="answer_<?php echo $count_text ?>"
                                               value="<?php echo $val->ea_content ?>">
                                    <?php endif; ?>
                                <?php elseif ($val->eq_id != $question_id): ?>
                                    <?php $question_id = $val->eq_id; ?>
                                    <?php $sum += 1; ?>
                                    <div class="col-xs-12 height-20 height-20-<?php echo $sum ?>" ></div>
                                    <h4 class="cau1 font_weight_700 question_<?php echo $sum ?> color_link margin_top_20" >
                                        Câu <?php echo $sum; ?>: </h4>
                                    <?php if ($val->eq_type == 'QUIZ'): ?>
                                        <div class="cautraloi col-sx-6 col-sm-6 col-md-6 answer_<?php echo $sum ?>">
                                            <?php $count_quiz += 1; ?>
                                            <p><input type="radio" data-checked='0' class="width_50"
                                                      name="fill_quiz_answer_<?php echo $count_quiz ?>"
                                                      data-correct="<?php echo $val->ea_is_correct ?>">
                                                 <?php echo $val->ea_content; ?></p>
                                        </div>
                                    <?php elseif ($val->eq_type == 'TEXT'): ?>
                                        <?php $count_text += 1; ?>
                                        <input id="fill_text_answer_<?php echo $sum ?>" class="form-control"
                                               name="fill_text_answer_<?php echo $sum ?>">
                                        <input type="hidden" id="answer_<?php echo $sum ?>"
                                               value="<?php echo $val->ea_content ?>">
                                    <?php endif; ?>
                                <?php elseif ($val->eq_id == $question_id): ?>
                                    <?php if ($val->eq_type == 'QUIZ'): ?>
                                        <div class="cautraloi col-sx-6 col-sm-6 col-md-6 answer_<?php echo $sum ?>">
                                            <p><input type="radio" data-checked='0' class="width_50"
                                                      name="fill_quiz_answer_<?php echo $count_quiz ?>"
                                                      data-correct="<?php echo $val->ea_is_correct ?>">
                                                 <?php echo $val->ea_content; ?></p>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-hidden"></div>
                <!--				end slide-->
                <input id="input_sum_exam_question" value="<?php echo $sum ?>" type="hidden">
                <input id="input_sum_exam_question_type_text" value="<?php echo $count_text ?>" type="hidden">
                <input id="input_sum_exam_question_type_quiz" value="<?php echo $count_quiz ?>" type="hidden">
                <input id="input_sum_page_exam_question" value="<?php echo ceil($sum / 2) ?>" type="hidden">
                <input id="input_correct_answer_exam_question" value="0" type="hidden"/>
            </div> <!-- ket thu row -->
        </div><!--  ket thuc nhung phan cau hoi -->
    </div>
    <div class="phantrang text-content">
        <div class="container-fluid bg-2 text-center">
            <div class="width_col_12 text-content form-inline">
                <a href="#select_page">
                    <button class=" btn btn-default glyphicon glyphicon-chevron-left display_none page_exam_question"
                            id="prev_page" data-prev-page="0"></button>
                    <button class="btn btn-default ba-cham-truoc display_none page_exam_question"> ...</button>
                    <?php for ($i = 1; $i <= ceil($sum / 2); $i++): ?>
                        <button class="btn btn-default trang1 page_exam_question page_click" id="page_<?php echo $i ?>"
                                data-page="<?php echo $i ?>"><?php echo $i ?></button>
                    <?php endfor; ?>
                    <button class="btn btn-default ba-cham-sau page_exam_question"> ...</button>
                    <button class="btn btn-default display_none glyphicon glyphicon-chevron-right page_exam_question"
                            id="next_page" data-next-page="2"></button>
                </a>
                <select id="stt_page" class="form-control max_width_100">
                    <?php for ($i = 1; $i <= ceil($sum / 2); $i++): ?>
                        <option value="<?php echo $i?>"><a href="#back_top">Câu <?php echo ($i*2 - 1). '-'.($i*2)?></a></option>
                    <?php endfor;?>
                    <?php ?>
                </select>
            </div>
            <div class="before margin_top_30">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong>Sau khi hoàn thiện bài hãy ấn vào nút nộp
                        bài bên dưới</strong></div>
                <a onclick="exam_ajax()" class="btn btn-success margin_top_30" id="send_my_answer" data-show="1">Nộp
                    bài</a>
            </div>

            <div class="hidden after margin_top_20">
                <p><strong>Bạn còn <span id="M" style="color:red"></span> phút <span id="S" style="color:red"></span>
                        giây để làm bài!Bạn có chắc chắn muốn nộp bài</strong></p>
                <p class="nuttiep_huy"  class="margin-bottom-0 margin_top_30"><a onclick="savetest()" href="#back_top"
                                                                                class="btn btn-success" id="save_test">Có</a>
                    <a onclick="not_send()" class="btn btn-danger" id="not_save_test">Không</a></p>
            </div>
        </div>
    </div>

</div> <!-- ket thuc container -->
</div> <!-- ket thuc content2 -->

<div class="content3 margin_bottom_50 margin_top_50">
    <div class="container">
        <div class="nhungcauhoi">
            <div class="row">
<!--                <div class="baitest col-xs-12" align="center">-->
<!--                    <p >Bài Test <span class="color_link"> > --><?php //echo $name ?><!--</span></p>-->
<!--                </div>-->
                <div class="trinhdocuaban">
                    <div class="container-fluid bg-2 text-center">

                        <p><strong>Bạn đã làm đúng <span class="color_link result_exam_test">0</span>/<span
                                    class="sum_exam_question"><?php echo $total ?></span> câu</strong></p>
                        <button class="btn btn-success btn-lg" onclick="show_pack_suggest()">
                           Xem trình độ hiện tại của bạn
                        </button>
                    </div>

                </div>
            </div> <!-- ket thu row -->
        </div><!--  ket thuc nhung phan cau hoi -->

    </div> <!-- ket thuc container -->
</div> <!-- ket thuc content2 -->

<div class="content5 display_none margin_bottom_50">
    <div class="container">
        <div class="row">
<!--            <p class="baitest">Bài test > <span class="color_link">--><?php //echo $name ?><!--</span></p>-->
            <div class="container-fluid bg-2 text-center">
<!--                <p><span class="color_link">Bài kiểm tra trình độ</span><span>(--><?php //echo $name; ?><!--)</span></p>-->
                <br>
                <p><strong>Bạn đạt </strong><span class="color_link result_exam_test">0</span> / <span
                        class="sum_exam_question">80</span></p>
                <p><strong>Với kết quả này bạn có thể lựa chọn trình độ : </strong> <span style="color:#0088c7"
                                                                                          id="pack_suggest_name">Trính đọ sơ cấp cơ bản 1</span>
                </p>
                <p><span style="color:#727272">(Xem thêm thông tin chi tiết bên dưới)</span></p>
<!--                <div class="col-xs-12" id="pack_suggest_description">-->
<!---->
<!--                </div>-->
            </div>
            <div class="width_col_12" id="pack_suggest_description">

            </div>
            <div>
                <?php echo $test_info ? $test_info->content : '' ?>
            </div>
            <br><br>
            <div class="container-fluid bg-2 text-center">
                <a href="<?php echo base_url('bai-test/chi-tiet') ?>" class="btn btn-success btn-lg"
                   style="color:white">
                    Làm lại bài test
                </a>
            </div>

        </div>
    </div>
</div>
<input type="hidden" id="time_exam_question" value="<?php echo $time ?>">
<!--<script type="text/javascript">-->
<!---->
<!--</script>-->
<style>
    .fixed-header {
        position: inherit!important;
    }
</style>
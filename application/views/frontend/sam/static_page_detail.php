<div id="header-slide-home">
    <?php $this->load->view('frontend/sam/home_slide'); ?>
</div>

<?php if ($news) { ?>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="news-title">
                    <h2><?php echo $news->title ?></h2>
                </div>
                <div class="news-content">
                    <?php echo $news->content ?>
                </div>
                <div class="col-xs-12">
                    <div class="pull-left">
                        <!-- Place this tag in your head or just before your close body tag. -->
                        <script src="https://apis.google.com/js/platform.js" async defer></script>

                        <!-- Place this tag where you want the +1 button to render. -->
                        <div class="g-plusone" data-annotation="inline" data-width="300"></div>
                    </div>
                    <div class="pull-left">
                        <div class="fb-like" data-href="<?php echo base_url($slug) ?>" data-layout="standard"
                             data-action="like"
                             data-size="small" data-show-faces="true" data-share="true"></div>
                    </div>
                </div>
                <div class="news-comment">
                    <div class="fb-comments" data-href="<?php echo base_url($slug) ?>" data-numposts="3"></div>
                </div>
                <div class="news-other-title">
                    <h2>Bài viết cùng chuyên mục</h2>
                </div>
                <div class="row">
                    <?php foreach ($list as $index => $item): ?>
                        <div class="col-xs-6 col-md-4 down-content">
                            <div class="tintuc-item">
                                <div class="img">
                                    <?php echo get_img_tag($item->image, $item->title, '', IMG_NEWS_DEFAULT); ?>
                                </div>
                                <div class="text-over">
                                    <h1 class="title"><?php echo get_detail_tag('',$item->title, $item->id, "") ?></h1>
                                </div>
                                <div class="text-over desc">
                                    <?php echo word_limiter($item->description, 120) ?>
                                </div>
                                <div class="views pull-right">
                                    <a href="<?php echo get_detail_url('',$item->title, $item->id) ?>" class="customLink pull-right">Xem
                                        thêm...</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

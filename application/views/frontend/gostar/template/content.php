<!-- The drawer is always open in large screens. The header is always shown,
  even in small screens. -->
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer-->
<!--            mdl-layout--fixed-header">-->
<div class="demo-layout-waterfall mdl-layout mdl-js-layout">
    <?php $this->load->view($theme_template . 'header'); ?>
    <main class="mdl-layout__content">
        <div class="page-content">
            <?php $this->load->view($content); ?>

            <!--            footer-->
            <footer class="page-footer center-on-small-only ">

                <div class="container">
                    <!--Footer Links-->
                    <div class="container-fluid">
                        <div class="row">

                            <!--First column-->
                            <div class="col-md-3 offset-md-1">
                                <h5 class="title">GEON VIETNAM</h5>

                                <p>Chúng tôi thiết kế những website chất lượng cao theo chuẩn SEO dễ sử dụng, bảo mật và
                                    thân thiện với công cụ tìm kiếm, thiết kế web theo yêu cầu riêng của từng khách
                                    hàng.</p>
                            </div>
                            <!--/.First column-->

                            <!--Second column-->
                            <div class="col-md-2 offset-md-1">
                                <h5 class="title">First column</h5>
                                <ul>
                                    <li><a href="#!">Link 1</a></li>
                                    <li><a href="#!">Link 2</a></li>
                                    <li><a href="#!">Link 3</a></li>
                                    <li><a href="#!">Link 4</a></li>
                                </ul>
                            </div>
                            <!--/.Second column-->

                            <!--Third column-->
                            <div class="col-md-2">
                                <h5 class="title">Second column</h5>
                                <ul>
                                    <li><a href="#!">Link 1</a></li>
                                    <li><a href="#!">Link 2</a></li>
                                    <li><a href="#!">Link 3</a></li>
                                    <li><a href="#!">Link 4</a></li>
                                </ul>
                            </div>
                            <!--/.Third column-->

                            <!--Fourth column-->
                            <div class="col-md-2">
                                <h5 class="title">Third column</h5>
                                <ul>
                                    <li><a href="#!">Link 1</a></li>
                                    <li><a href="#!">Link 2</a></li>
                                    <li><a href="#!">Link 3</a></li>
                                    <li><a href="#!">Link 4</a></li>
                                </ul>
                            </div>
                            <!--/.Fourth column-->

                        </div>
                    </div>
                    <!--/.Footer Links-->

                    <hr>

                    <!--Call to action-->
                    <div class="call-to-action">
                        <h4>Bạn cần thêm thông tin?</h4>
                        <ul>
                            <li>
                                <h5>Liên hệ để được hỗ trợ</h5></li>
                            <li><a target="_blank" href="#lien-he"
                                   class="btn btn-danger waves-effect waves-light">LIÊN HỆ</a></li>
                            <li><a target="_blank" href="#tim-hieu"
                                   class="btn btn-default waves-effect waves-light">Tìm hiểu thêm</a></li>
                        </ul>
                    </div>
                    <!--/.Call to action-->
                </div>
                <!--Copyright-->
                <div class="footer-copyright">
                    <div class="container-fluid">
                        © 2016 Copyright: <a href="/"> GO2.VN </a>

                    </div>
                </div>
                <!--/.Copyright-->
            </footer>
            <!--            /footer-->
        </div>
    </main>
</div>
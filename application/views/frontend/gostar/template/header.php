<header class="mdl-layout__header mdl-layout__header--waterfall">
    <div class="mdl-layout__header-row">
        <div class="mdl-layout-spacer"></div>
        <div class="">
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link mdl-js-button mdl-js-ripple-effect" href="#"><i class="material-icons">devices</i> &nbsp; Mẫu thiết kế</a>
                <a class="mdl-navigation__link mdl-js-button mdl-js-ripple-effect" href="#"><i class="material-icons">mail_outline</i>  &nbsp;Liên hệ</a>
                <a class="mdl-navigation__link mdl-js-button mdl-js-ripple-effect" href="#"><i class="material-icons">comment</i>  &nbsp;Hỗ trợ</a>
            </nav>
        </div>
    </div>
    <!-- Bottom row, not visible on scroll -->
    <div class="row_extra">
        <div class="mdl-layout-spacer"></div>
    </div>
</header>

<div class="mdl-layout__drawer">
    <span class="mdl-layout-title">
        <img id="MDB-logo" src="http://mdbootstrap.com/wp-content/uploads/2015/09/MDB_transparent2-1.png" class="img-fluid flex-center">
    </span>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href=""><i class="material-icons">flight_takeoff</i> &nbsp;Về chúng tôi</a>
        <a class="mdl-navigation__link" href=""><i class="material-icons">extension</i> &nbsp;Dịch vụ</a>
        <a class="mdl-navigation__link" href=""><i class="material-icons">extension</i> &nbsp;Dịch vụ</a>
        <a class="mdl-navigation__link" href=""><i class="material-icons">extension</i> &nbsp;Dịch vụ</a>
    </nav>
</div>

<style>
    .mdl-layout__header {
        min-height: 54px;
    }

    .mdl-layout__header-row {
        height: 54px;
    }

    .mdl-layout__header.is-compact {
        max-height: 54px;
    }

    .row_extra {
        height: 10px;
        /*background-color: #3f729b;*/
    }

    header.mdl-layout__header{
        /*background: #3f729b;*/
    }

    header .mdl-button--icon {
        margin-top: 7px;
    }

    .mdl-layout__header.is-compact .mdl-button--icon {
        margin-top: 2px;
    }

    .mdl-layout__header .mdl-navigation {
        margin-top: 10px;
        transition: .3s;
    }

    .mdl-layout__header.is-compact .mdl-navigation {
        margin-top: 2px;
        transition: .3s;
    }

    .mdl-layout__drawer {
        color: white;
        background: #000;
        background: -moz-linear-gradient(-45deg,#000 0,#3f729b 100%);
        background: -webkit-linear-gradient(-45deg,#000 0,#3f729b 100%);
        background: linear-gradient(135deg,#000 0,#3f729b 100%);
    }
    .mdl-layout__drawer .mdl-navigation .mdl-navigation__link{
        color: white;
        font-weight: 300;
    }
    .mdl-layout__drawer .mdl-layout-title{
        background: url(http://mdbootstrap.com/images/sidenavs/mdb.jpg) center center no-repeat;
        background-size: cover;
        height: 80px;
    }
    .mdl-layout__drawer .mdl-layout-title img{
        max-width: 100%;
        padding: 7% 50px;
        padding-left: 0;
    }
    .mdl-navigation__link:hover{
        text-decoration: none;
    }
    .mdl-navigation .mdl-js-button{
        position: relative;
    }
</style>
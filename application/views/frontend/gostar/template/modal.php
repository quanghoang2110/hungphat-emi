<style>
    #frm_login .form-group {
        overflow: hidden;
    }

    .btn_dang_nhap {
        text-align: center;
    }

    .modal-dialog-dangnhap {
        width: 500px;
        margin-left: auto;
        margin-right: auto;
    }

    .btn_dang_nhap button {
        width: 100%;
        background: #0099ff !important;
        text-transform: uppercase;
        font-weight: bold;
        border: solid 1px #0099ff !important;
    }

    .nhomk {
        text-align: left;
    }

    .quenmk {
        text-align: right;
    }
</style>
<!-- dang nhap -->
<div class="modal fade" id="modalDangNhap" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Đăng nhập vào hệ
                    thống</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-login"></div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Tài khoản email</label>
                        <input type="text" class="form-control" placeholder="Tài khoản email" name="username"
                               id="username">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Mật khẩu</label>
                        <input type="password" class="form-control" placeholder="Mật khẩu" name="password"
                               id="password">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 nhomk">
                            <span class="nhopwe"><input type="checkbox" value="">Ghi nhớ mật khẩu. </span>
                        </div>
                        <div class="col-md-6 quenmk">
                            <a href="#" onclick="showQuenMK()" class="color-primary">Bạn quên mật khẩu?</a>
                        </div>
                    </div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger" onclick="return false;" id="submit_login">Đăng nhập vào hệ thống</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>Bạn chưa có tài khoản? <a href="#" onclick="showDangKy();" class="color-primary">Đăng ký ngay</a></p>
            </div>
        </div>
    </div>
</div>
<!-- dang ki -->
<div class="modal fade" id="modalDangKy" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Đăng ký tài khoản</h4>
            </div>

            <div class="modal-body">
                <form id="frm_regis" method="post">
                    <div id="frm_login">
                        <div id="alert_regis"></div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Họ" name="firstname">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Tên" name="lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <input type="password" class="form-control" placeholder="Mật khẩu" name="password">
                            </div>
                            <div class="col-md-6">
                                <input type="password" class="form-control" placeholder="Nhập lại mật khẩu"
                                       name="repassword">
                            </div>
                        </div>
                        <div class="form-group"></div>
                        <div class="form-group">
                            <span class="nhopwe"><input type="checkbox" id="chk_agree" value="">Tôi đồng ý với các điều khoản của WOW English. </span>
                        </div>
                        <div class="form-group btn_dang_nhap">
                            <button type="submit" class="btn btn-danger" onclick="return false;" id="register" disabled="disabled">Đăng ký tài khoản</button>
                        </div> <!-- dong the xac nhan -->
                    </div>
                </form>


            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>Bạn đã có tài khoản? <a href="#" onclick="showDangNhap();" class="color-primary">Đăng nhập ngay</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--lay lai mat khau-->
<div class="modal fade" id="modalQuenMK" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Lấy lại mật khẩu</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-check-mail" class="error_frontend margin_bottom_20 hidden"></div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email đăng nhập" name="email" id="fill-mail-forget-pass">
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger" onclick="forget_pass()">gửi yêu cầu</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>
                    <a href="#" onclick="showDangNhap();" class="color-primary">Đăng nhập</a> | <a href="#" onclick="showDangKy();" class="color-primary">Đăng ký</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--doi mat khau-->
<div class="modal fade" id="modalThayMK" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Yêu cầu đổi mật khẩu</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-check-mail" class="error_frontend margin_bottom_20 hidden"></div>
                    <div class="show-error-change-pass form-group hidden" id="check-fill">
                        <span class="error hidden col-xs-12" id="no-fill-old-pass"><i>Bạn chưa nhập mật khẩu</i></span>
                        <span class="error hidden col-xs-12" id="no-fill-new-pass"><i>Bạn chưa nhập mật khẩu mới</i></span>
                        <span class="error hidden col-xs-12" id="fill-false-pass"><i>Bạn nhập sai mật khẩu</i></span>
                        <span class="error hidden col-xs-12" id="new-pass-false"><i>Mật khẩu mới phải tối thiểu 6 kí tự trở lên</i></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mật khẩu cũ" name="old_pass" id="fill-old-pass">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mật khẩu mới" name="new_pass" id="fill-new-pass">
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger" onclick="send_change_pass()">gửi yêu cầu</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
        </div>
    </div>
</div>
<!--tuyen: div thong bao-->
<div class="modal" id="modal_msg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Nội dung thông báo</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-callback" data-dismiss="modal">Đóng</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--tuyen: modal mua khóa học-->
<div class="modal" id="modal_buy_pack">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Mua khóa học</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 margin_bottom_20" id="image_pack_buy" align="center">

                </div>
                <div class="col-xs-12  margin_bottom_20" >
                    <div class="col-xs-4"><span class="float_right">Khóa học</span></div>
                    <div class="col-xs-8 color-green-tuyen" id="name_pack_buy"></div>
                </div>
                <div class="col-xs-12  margin_bottom_20" >
                    <div class="col-xs-4"><span class="float_right">Thời gian</span></div>
                    <div class="col-xs-8 color-green-tuyen" id="time_pack_buy"></div>
                </div>
                <div class="col-xs-12 margin_bottom_20" >
                    <div class="col-xs-4"><span class="float_right">Giá tiền</span></div>
                    <div class="col-xs-8 color-green-tuyen" id="money_pack_buy"></div>
                    <input type="hidden" value="0" id="input_money_pack_buy"/>
                </div>
                <div class="form-group col-xs-12 margin_bottom_20" >
                    <div class="col-xs-4"><span class="float_right">Mã khuyến mại (nếu có)</span></div>
                    <div class="col-xs-4">
                        <input class="form-control" id="code_pack_buy" id="code_pack_buy" onchange="check_code_pack_buy();">
                        <i class="hidden error"  id="error_code"></i>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-primary"> <i class="fa fa-check" aria-hidden="true"></i>Kiểm tra</button>
                    </div>
                </div>
                <div class="hidden col-xs-12 margin_bottom_20" id="div_discount_pack_buy_have_code" >
                    <div class="col-xs-4"><span class="float_right">Khuyến mại </span></div>
                    <div class="col-xs-8 color-red-tuyen" id="discount_pack_buy"></div>
                </div>
                <div class="hidden col-xs-12 margin_bottom_20" id="div_money_pack_buy_have_code" >
                    <div class="col-xs-4"><span class="float_right">Giá tiền phải trả </span></div>
                    <div class="col-xs-8 color-red-tuyen" id="money_pack_buy_have_code"></div>
                </div>
                <div class=" form-group col-xs-12 .margin_bottom_20" align="center">
                    <div class="col-xs-4">
                        <input type="checkbox" class="input_checkbox_float_right">
                    </div>
                    <div class="col-xs-8">
                        <i class="float_left"> Tôi đồng ý mua</i>
                    </div>
                </div>

            </div>
<!--            <div class="modal-footer">-->
            <div align="center" class="margin_bottom_20">
                <input type="hidden" id="input_id_pack_buy" value=""/>
                <input type="hidden" id="input_time_pack_buy" value=""/>
                <input type="hidden" value="0" id="input_discount_pack_buy"/>
                <input type="hidden" value="0" id="input_money_pack_buy_have_code"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" onclick="modal_buy_pack();"> Đồng ý </button>
            </div>
<!--            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function showDangNhap() {
        $(".modal").modal("hide");
        $("#modalDangNhap").modal("show");
    }
    function showDangKy() {
        $(".modal").modal("hide");
        $("#modalDangKy").modal("show");
    }

    function showQuenMK(){
        $(".modal").modal("hide");
        $("#modalQuenMK").modal("show");
    }
    $(document).ready(function(){
        $('#chk_agree').change(function() {
            if ($(this).is(':checked')) {
                $('#register').prop('disabled',false);
            }else{
                $('#register').prop('disabled',true);
            }
        });
    })
</script>
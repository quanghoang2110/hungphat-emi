<div class="gap"></div>
<section>
    <div class="container feature">
        <!--Featured image-->
        <img src="http://mdbootstrap.com/images/about/intro-1.jpg" alt="Material Design for Bootstrap"
             class="img-fluid z-depth-2 wow fadeInUp"
             style="visibility: visible; animation-name: fadeInUp; max-width: 100%">
        <!--Featured image-->

        <!--Main info-->
        <div class="jumbotron text-center">

            <h2 class="h2-responsive">GIỚI THIỆU VỀ GEON VIỆT</h2>
            <br>
            <p>
                <strong>GEON Việt</strong> được thành lập bởi một nhóm những người yêu thích <strong>Thiết kế website,
                    Ứng dụng di động</strong> và
                <strong>SEO - Marketing Online</strong>.
            </p>
            <hr/>
            <br/>
            <p>Chúng tôi thiết kế những website chất lượng cao theo chuẩn
                SEO dễ sử dụng, bảo mật và thân thiện với công cụ tìm kiếm, thiết kế web theo yêu cầu riêng của từng
                khách hàng.
            </p>

            <hr>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
                <i class="material-icons">devices</i> &nbsp; Xem các mẫu thiết kế
            </button>
        </div>
        <!--/.Main info-->
    </div>
</section>
<hr class="container between-sections">
<div class="gap"></div>

<section class="section text-center">

    <div class="container feature">
        <!--Section heading-->
        <h1 class="section-heading">Sản phẩm & Dịch vụ</h1>
        <!--First row-->
        <div class="row">

            <!--First column-->
            <div class="col-md-4 m-b-r wow fadeIn">
                <!--Featured image-->
                <div class="view overlay hm-white-slight m-b-2">
                    <img src="http://mdbootstrap.com/images/about/newsscreen-min.jpg">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/presentation-free/lp-components.html">
                        <div class="mask waves-effect waves-light"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h4 class="blue-text ">Phát triển website</h4>

                <p>Không dừng lại ở việc hoàn thành, GOEN giúp bạn xử lý dữ liệu ban đầu, tinh chỉnh hình ảnh và hổ trợ
                    SEO theo chuẩn Google</p>
                <a href="#web"
                   class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"><i
                        class="material-icons">phonelink</i>
                    &nbsp; Xem thêm</a>
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-4 m-b-r wow fadeIn" data-wow-delay="0.2s"
                 style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
                <!--Featured image-->
                <div class="view overlay hm-white-slight m-b-2">
                    <img src="http://mdbootstrap.com/images/about/contactsectionmdb-min.jpg">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/presentation-free/lp-sections.html">
                        <div class="mask waves-effect waves-light"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h4 class="red-text ">Ứng dụng di động</h4>

                <p>Thiết kế và lập trình ứng dụng di động phục vụ cho kinh doanh sản xuất, dịch vụ trực tuyến đáp ứng
                    các phiên bản đi động mới nhất.</p>
                <a href="#web"
                   class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"><i
                        class="material-icons">smartphone</i>
                    &nbsp; Xem thêm</a>
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-4 m-b-r wow fadeIn" data-wow-delay="0.4s"
                 style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;">
                <!--Featured image-->
                <div class="view overlay hm-white-slight m-b-2">
                    <img src="http://mdbootstrap.com/images/about/navigationmdb-min.jpg">
                    <a href="http://mdbootstrap.com/live/_MDB/index/docs/presentation-free/lp-templates.html">
                        <div class="mask waves-effect waves-light"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h4 class="teal-text"><span>SEO và Marketing</span></h4>

                <p>Phong cách thiết kế web đa dạng theo các xu hướng mới nhất của thế giới, phù hợp với các mã nguồn phổ
                    biến hiện nay.</p>
                <a href="#web"
                   class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored"><i
                        class="material-icons">language</i>
                    &nbsp; Xem thêm</a>
            </div>
            <!--/Third column-->

        </div>
        <!--/First row-->

    </div>
</section>
<hr class="container between-sections">
<div class="gap">&nbsp;</div>

<section class="section feature-box">

    <div class="container">
        <!--Section heading-->
        <h1 class="section-heading text-center">Công nghệ & nền tảng</h1>
        <p class="section-description text-center">
            GOEN luôn cập nhật và sử dụng các công nghệ nền tảng mới nhất<br/> phục vụ việc thiết kế và lập trình.
        </p>
        <div class="gap"></div>
        <!--First row-->
        <div class="row features-small">

            <!--First column-->
            <div class="col-md-5 m-b-r center-on-small-only">
                <img src="http://mdbootstrap.com/images/about/img-2-min.jpg" alt="" class="z-depth-0 wow fadeIn"
                >
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-7 center-on-small">
                <!--First row-->
                <div class="row wow fadeIn">
                    <div class="col-xs-1">
                        <i class="material-icons indigo-text">developer_mode</i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Tối ưu mã nguồn</h4>
                        <p class="grey-text">Tất cả các website được Esoluz thực hiện luôn được tối ưu, đặc biệt kỹ
                            thuật SEO luôn luôn được áp dụng nhằm giúp thân thiện đối với các máy chủ tìm kiếm như
                            Google, Bing.</p>
                    </div>
                </div>
                <!--/First row-->

                <div style="height:50px"></div>

                <!--Second row-->
                <div class="row wow fadeIn">
                    <div class="col-xs-1">
                        <i class="material-icons indigo-text">wallpaper</i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Thiết kế giao diện</h4>
                        <p class="grey-text">ENGO luôn nắm vững các yếu tố nhằm xây dựng thương hiệu, yếu tố nhận biết
                            khách hàng và trải nghiệm người dùng để đưa vào các thiết kế.</p>
                    </div>
                </div>
                <!--/Second row-->

                <div style="height:50px"></div>

                <!--Third row-->
                <div class="row wow fadeIn">
                    <div class="col-xs-1">
                        <i class="material-icons indigo-text">devices</i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Hỗ trợ đa nền tảng</h4>
                        <p class="grey-text">Có đến hơn 50% người dùng truy cập website từ thiết bị di động, do đó,
                            website của Esoluz thực hiện luôn chạy được trên hầu hết các thiết bị: Smartphone, iPhone,
                            iPad, Tablet.</p>
                    </div>
                </div>
                <!--/Third row-->
            </div>
            <!--/Second column-->
        </div>
        <!--/First row-->
    </div>

</section>

<hr class="hidden container between-sections">
<div class="hidden gap"></div>
<section class="hidden section feature-box">
    <div class="container">
        <!--Section heading-->
        <h1 class="section-heading text-center">Tại sao nên chọn chúng tôi?</h1>

        <!--First row-->
        <div class="row features-small">

            <!--First column-->
            <div class="col-md-4">
                <!--First row-->
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-money green-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Free for personal and commercial use</h4>
                        <p class="grey-text">Our license is user friendly. Feel free to use MDB for both private as well
                            as
                            commercial projects.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/First row-->

                <!--Second row-->
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-cubes teal-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">400+ UI elements</h4>
                        <p class="grey-text">Impressive collection of flexible components allows you to develop any
                            project.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Second row-->

                <!--Third row-->
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-fort-awesome cyan-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">600+ icons</h4>
                        <p class="grey-text">Hundreds of useful, scalable, vector icons at your disposal.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Third row-->

                <!--Fourth row-->
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-mobile indigo-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Fully responsive</h4>
                        <p class="grey-text">It doesn't matter whether your project will be displayed on desktop,
                            laptop,
                            tablet or mobile phone. MDB looks great on each screen.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Fourth row-->
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-4 m-b-r flex-center">
                <img src="http://mdbootstrap.com/images/mockups/header-iphone.png" alt="" class="z-depth-0">
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-4">
                <!--First row-->
                <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="col-xs-2">
                        <i class="fa fa-heart red-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">70+ CSS animations</h4>
                        <p class="grey-text">Neat and easy to use animations, which will increase interactivity of your
                            project and delight your visitors.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/First row-->

                <!--Second row-->
                <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="col-xs-2">
                        <i class="fa fa-photo orange-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Plenty of useful templates</h4>
                        <p class="grey-text">Need inspiration? Use one of our predefined templates for free.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Second row-->

                <!--Third row-->
                <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="col-xs-2">
                        <i class="fa fa-magic deep-orange-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Easy installation</h4>
                        <p class="grey-text">5 minutes, a few clicks and... done. You will be surprised at how easy it
                            is.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Third row-->

                <!--Fourth row-->
                <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="col-xs-2">
                        <i class="fa fa-gear lime-text"></i>
                    </div>
                    <div class="col-xs-10">
                        <h4 class="feature-title">Easy to use and customize</h4>
                        <p class="grey-text">Using MDB is simple and pleasant. Components flexibility allows you deep
                            customization. You will easily adjust each component to suit your needs.</p>
                        <div style="height:15px"></div>
                    </div>
                </div>
                <!--/Fourth row-->
            </div>
            <!--/Third column-->
        </div>
        <!--/First row-->
    </div>
</section>

<!--tai sao chon chung toi-->
<section class="section hidden-xs">
    <div class="container feature-compare">
        <!--Section heading-->
        <h1 class="section-heading text-center">Tại sao chọn chúng tôi?</h1>
        <p class="section-description text-center">GOEN luôn tạo lên sự khác biệt.<br/><strong>Hãy thử so sánh</strong>
        </p>
        <div class="gap"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center wow fadeIn">

                    <!--Before/after presentation-->
                    <div class="before-after wow fadeIn">
                        <div class="flex-center">

                            <!-- Before/After slider -->
                            <div id="before-after-section">
                                <ul id="controls">
                                    <li class="to-start"><a class="btn btn-primary btn-dtc waves-effect waves-light">Website
                                            khác</a>
                                    </li>
                                    <li class="to-middle"><a class="btn btn-primary btn-ptc waves-effect waves-light">So
                                            sánh</a>
                                    </li>
                                    <li class="to-end"><a
                                            class="btn btn-primary btn-stc waves-effect waves-light">GOEN phát triển</a>
                                    </li>
                                </ul>

                                <div id="before-after">
                                    <div class="view view-after" style="width: 598px;"><img
                                            src="http://mdbootstrap.com/images/about/after.jpg"></div>
                                    <div class="view view-before"><img
                                            src="http://mdbootstrap.com/images/about/before.jpg"></div>
                                    <div id="dragme"
                                         style="transform: translate3d(0px, 0px, 0px); cursor: move; touch-action: pan-y; -webkit-user-select: none; left: 598px;"></div>
                                </div>
                            </div>
                            <!-- Before/After slider -->
                        </div>
                    </div>
                    <!--/.Before/after presentation-->
                </div>
            </div>
        </div>
    </div>

</section>

<hr class="container container between-sections">
<div class="gap"></div>

<!--gioi thieu cac du an-->
<section class="section">

    <div class="container">
        <!--Section heading-->
        <h1 class="section-heading text-center">Giới thiệu dự án</h1>

        <p class="lead text-center">Các dự án được hoàn thành gần đây</p>

        <div class="row">
            <?php $i = 0; ?>
            <?php while ($i < 6): ?>
                <div class="proj col-md-2 col-lg-4">
                    <div class="view overlay hm-black-strong z-depth-2">
                        <img src="http://mdbootstrap.com/live/_MDB/img/intros/register-social-form.png" alt="" class="img-fluid">

                        <div class="mask flex-center waves-effect waves-light">
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary"><i class="material-icons">remove_red_eye</i> Xem chi tiết</button>
                        </div>
                    </div>

                </div>
                <?php $i++; endwhile; ?>
        </div>
    </div>

</section>

<hr class="hidden container between-sections">
<div class="hidden gap"></div>

<!--hidden gioi thieu du an-->
<section class="hidden section section_proj">
    <div class="container">
        <h1 class="section-heading text-center">Giới thiệu dự án</h1>
        <p class="section-description text-center">Các dự án được hoàn thành trong 3 tháng gần đây.</p>
        <div class="row text-xs-center">
            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/people/img%20(86).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="purple-text"><h5><i
                                class="material-icons">add_shopping_cart</i> Website thương mại điện tử</h5>
                    </a></div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/city/img%20(29).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="orange-text"><h5><i class="fa fa-money"></i> Business</h5></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/city/img%20(32).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="indigo-text"><h5><i class="fa fa-plane"></i> Travels</h5></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/people/img%20(86).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="purple-text"><h5><i
                                class="material-icons">add_shopping_cart</i> Website thương mại điện tử</h5>
                    </a></div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/city/img%20(29).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="orange-text"><h5><i class="fa fa-money"></i> Business</h5></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-6 m-b-r">
                <div class="view overlay hm-white-slight z-depth-2"><img
                        src="http://mdbootstrap.com/images/regular/city/img%20(32).jpg"> <a>
                        <div class="mask waves-effect waves-light"></div>
                    </a></div>
                <div class="card-block"><a href="" class="indigo-text"><h5><i class="fa fa-plane"></i> Travels</h5></a>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="container between-sections">

<!--khach hang noi ve chung toi-->
<section class="section feature-2">
    <div class="container">
        <!--Section heading-->
        <h1 class="section-heading text-center">Khách hàng nói về GOEN</h1>
        <!--Section sescription-->

        <!--First row-->
        <div class="row text-xs-center">

            <!--Second column-->
            <div class="col-md-6 m-b-r">
                <!--Card-->
                <div class="card card-cascade wider wow fadeIn">

                    <!--Card image-->
                    <div class="view overlay hm-white-slight">
                        <img src="http://mdbootstrap.com/images/about/bootstrap-tutorial-1.jpg" class="img-fluid"
                             alt="">
                        <a href="http://mdbootstrap.com/bootstrap-tutorial/">
                            <div class="mask waves-effect waves-light"></div>
                        </a>
                    </div>
                    <!--/.Card image-->

                    <!--Card content-->
                    <div class="card-block">
                        <!--Text-->
                        <p class="card-text">"Sau 2 lần thất bại với các công ty phát triển web khác, chúng tôi cảm thấy
                            rất hài lòng khi quyết định hợp tác với lamwebviet.com, website được tối ưu theo chuẩn seo
                            và rất dễ quản trị..."
                        </p>
                        <br/>
                        <span class="mdl-chip mdl-chip--contact pull-right">
                            <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white"><i
                                    class="material-icons">person</i> </span>
                            <span class="mdl-chip__text"><strong>Mr.Sinh</strong>, chuyên viên SEO</span>
                        </span>
                        <br/>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->

            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-6 m-b-r">

                <!--Card-->
                <div class="card card-cascade wider wow fadeIn">

                    <!--Card image-->
                    <div class="view overlay hm-white-slight">
                        <img src="http://mdbootstrap.com/images/about/wordpress-min-1.jpg" class="img-fluid" alt="">
                        <a href="http://mdbootstrap.com/wordpress-tutorial/">
                            <div class="mask waves-effect waves-light"></div>
                        </a>
                    </div>
                    <!--/.Card image-->

                    <!--Card content-->
                    <div class="card-block">
                        <!--Text-->
                        <p class="card-text">"Họ đã làm tốt hơn hẳn hai công ty trước đó mà tôi đã hợp tác cả về thời
                            gian lẫn chi phí. Cách họ khắc phục sự cố và tìm ra giải pháp thật sự là rất tuyệt
                            vời..."</p>
                        <br/>
                        <span class="mdl-chip mdl-chip--contact pull-right">
                            <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white"><i
                                    class="material-icons">person</i> </span>
                            <span class="mdl-chip__text"><strong>Mrs.Thuy</strong>, Diamon Shop</span>
                        </span>
                        <br/>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->

            </div>
            <!--/Third column-->
        </div>
        <!--/First row-->
    </div>
</section>

<hr class="container container between-sections">
<div class="gap"></div>

<!-- xem mau thiet ke -->
<section class="section text-center">
    <div class="container">
        <h3 class="text-center">Bạn muốn quảng bá thương hiệu và phát triển kinh doanh của mình ?</h3>
        <br>
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
            <i class="material-icons">devices</i> &nbsp; Xem các mẫu thiết kế
        </button>
    </div>
</section>

<hr class="container container between-sections">
<div class="gap"></div>

<style>
    .author-box, .jumbotron {
        margin-top: -20px;
        background-color: #fff;
        padding: 2rem;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);
        z-index: 50;
        position: relative;
        margin-left: 3%;
        margin-right: 3%;
    }

    .jumbotron p {
        font-weight: 300;
        font-size: 1rem;
    }

    #before-after-section ul li {
        list-style: none;
        display: inline-block;
        font-size: 17px;
        cursor: pointer;
        margin: 0px 20px 0px 20px;
    }

    .mdb-skin-custom .btn-dtc, .mdb-skin-custom .card .btn-action {
        background: #154771;
    }

    #before-after-section #before-after {
        position: relative;
        height: 610px;
        border: 2px solid grey;
        margin: 20px auto 0px;
    }

    #before-after-section .view {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    #before-after-section .view-before {
        z-index: 1;
    }

    #before-after-section .view {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    #before-after-section #dragme {
        position: absolute;
        width: 5px;
        height: 100%;
        top: 0px;
        left: 0px;
        background-color: black;
        cursor: pointer;
        z-index: 3;
    }

    #before-after-section .view-after {
        z-index: 2;
    }

    @media only screen and (min-width: 1201px) {
        #before-after-section #before-after {
            width: 750px;
        }
    }

    .view {
        overflow: hidden;
        position: relative;
        cursor: default;
    }

    .view img, .view video {
        display: block;
        position: relative;
    }

    .container.feature-compare img {
        max-width: none;
    }

    .hm-white-slight .full-bg-img, .hm-white-slight .mask {
        background-color: rgba(255, 255, 255, .1);
    }

    .overlay .mask {
        opacity: 0;
        transition: all .4s ease-in-out;
    }

    .view .content, .view .mask {
        position: absolute;
    }

    .full-bg-img, .view .content, .view .mask {
        overflow: hidden;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }

    .waves-effect {
        position: relative;
        cursor: pointer;
        overflow: hidden;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        z-index: 1;
    }

    .pswp__container, .pswp__img, .waves-effect, a, html {
        -webkit-tap-highlight-color: transparent;
    }

    .feature-box .material-icons {
        margin-top: 10px;
        font-size: 2rem;
    }

    .container between-sections {
        margin-top: 4rem;
    }

    .section_proj .card-block {
        position: absolute;
        margin-top: -70px;
    }

    @media (min-width: 992px) {
        .section_proj .m-b-r {
            margin-bottom: 30px;
        }
    }

    @media (max-width: 992px) {
        .m-b-r {
            margin-bottom: 2rem !important;
        }
    }

    .card img, .comments-list img, .img-fluid, .reply-form img, .section .view, .section img {
        display: block;
        max-width: 100%;
        height: auto;
    }

    .btn-danger:focus, .btn-danger:hover {
        background-color: #db0000 !important;
    }

    .overlay:hover .mask {
        opacity: 1;
    }

    .view.overlay.hm-black-strong.z-depth-2 {
        text-align: center;
    }

    .proj button {
        margin: auto;
        top: 45%;
    }

    .hm-black-strong .full-bg-img, .hm-black-strong .mask {
        background-color: rgba(0, 0, 0, .7);
    }

    .overlay .mask {
        opacity: 0;
        transition: all .4s ease-in-out;
    }

    .view .content, .view .mask {
        position: absolute;
    }

    div.proj {
        margin-top: 20px;
        /*max-height: 170px;*/
        /*overflow: hidden;*/
    }

</style>
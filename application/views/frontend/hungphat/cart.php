<?php if($list): ?>
<div class="row hidden-540 giohang">
    <div>
        <form id="form_cart" action="/xac-nhan-don-hang.html" method="post">
            <table class="table table table-shopping-cart">
                <tbody><tr>
                    <th>Sản phẩm</th>
                    <th>Tên</th>
                    <th>Số lượng</th>
                    <th>Remove</th>
                </tr>
                </tbody><tbody>
                <?php $total = 0; foreach($list as $key=>$item): ?>
                <?php $total =  ($item->price_2) ? $total + $sl[$key]*$item->price_2 : $total + $sl[$key]*$item->price_1 ?>
                <tr class="p_item_<?php echo $item->id ?>">
                    <td class="table-shopping-cart-img">
                        <a href="/<?php echo get_detail_url(ROUTE_PRODUCT, $item->name, $item->id) ?>" target="">
                            <?php echo get_img_tag($item->avatar, $item->name) ?>
                        </a>
                    </td>
                    <td class="table-shopping-cart-title">
                        <a href="/<?php echo get_detail_url(ROUTE_PRODUCT, $item->name, $item->id) ?>" title="<?php echo $item->name ?>" class=""><?php echo $item->name ?></a>
                    </td>
                    <td>
                        <input name="product[]" value="81" type="hidden">
                        <select name="soluong[]" class="form-control soluong" onchange="thayDoiSoLuong(<?php echo $item->id ?>, $(this).val())">
                            <?php for($i=1;$i<=20;$i++): ?>
                                <option value="<?php echo $i ?>" <?php if($i == $sl[$key]) echo "selected" ?>><?php echo $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>
                    <td>
                        <a class="fa fa-close table-shopping-remove" href="#remove-sanpham" onclick="removeItemInCart(<?php echo $item->id ?>)"></a>
                    </td>
                </tr>
                <?php endforeach;?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
            <div class="gap gap-small"></div>
        </form>
    </div>
</div>
<div class="vissible-540">
    <ul class="media-list product-order-list">
        <?php $total = 0; foreach($list as $key=>$item): ?>
            <?php $total =  ($item->price_2) ? $total + $sl[$key]*$item->price_2 : $total + $sl[$key]*$item->price_1 ?>
            <li class="relative media p_item_<?php echo $item->id ?> chungchung">
                <div class="media-left product-thumbnail">
                    <a href="/<?php echo get_detail_url(ROUTE_PRODUCT, $item->name, $item->id) ?>" target="">
                        <?php echo get_img_tag($item->avatar, $item->name) ?>
                    </a>
<!--                    <span class="lu-text text-price">--><?php //echo ($item->price_2) ? number_format($sl[$key]*$item->price_2,0,'.','.') : number_format($sl[$key]*$item->price_1,0,'.','.') ?><!--<span>đ</span></span>-->
                </div>
                <div class="xxyzx media-body product-detail">
                    <p class="product-description"><?php echo $item->name ?></p>
                    <p>
                        <select name="soluong" class="form-control soluong" onchange="thayDoiSoLuong(<?php echo $item->id ?>, $(this).val())">
                            <?php for($i=1;$i<=20;$i++): ?>
                                <option value="<?php echo $i ?>" <?php if($i == $sl[$key]) echo "selected" ?>><?php echo $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </p>
<!--                    <p class="product-price">-->
<!--                        Giá:-->
<!--                        <span class="text-price total_price_81">--><?php //echo ($sl[$key]*$item->price_2) ? number_format($sl[$key]*$item->price_2,0,'.','.') : number_format($sl[$key]*$item->price_1,0,'.','.') ?><!--<span>đ</span></span>-->
<!--                    </p>-->
                    <p class="absolute">
                        <a class="#remove-sanpham" onclick="removeItemInCart(<?php echo $item->id ?>)"><i class="fa fa-close table-shopping-remove"></i> </a>
                    </p>
                </div>
                <div class="notify-cart product-tag warning hidden"></div>
            </li>
        <?php endforeach;?>
<!--        <li class="tongdongia">-->
<!--            <p>Tổng đơn hàng: <span class="text-price total_price">--><?php //echo number_format($total,0,'.','.') ?><!--<span>đ</span></span></p>-->
<!--        </li>-->
    </ul>

</div>
<?php else:?>
Chưa có sản phẩm nào.
<?php endif; ?>

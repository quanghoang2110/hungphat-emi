<div class="container new detail-new">
        <ol class="breadcrumb no-padding">
            <li><a href="/">Trang chủ</a></li>
            <li class="active">Download</li>
        </ol>
</div>
<div class="product_publisher">
    <div class="container">
        <h3 align="center">Download Catalog từ Nhà cung cấp</h3>
        <br/>
        <div class="row">
            <?php foreach ($list as $key => $val): ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 padding_bottom_15">
                    <div class="catalog">
                        <a title="Tải xuống catalogue của nhà cung cấp này"
                           href="<?php if (isset($val->file_pdf)) echo $val->file_pdf; ?>" target="_blank"><img
                                class="cus_img img_provider" src="<?php if (isset($val->img)) echo $val->img; ?>"><br/>
                            Tải xuống</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="col-xs-12 no-padding">
    <div class="container">
        <div class="act">
            <!--email-->
            <div class="send-mail-home">
                <div class="bd">
                    <div class="note">
                        <span>Đăng ký <br>nhận bản tin</span>
                        <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
                    </div>
                    <div class="note_sm">
                        Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá
                        riêng biệt
                    </div>
                    <div class="email">
                        <form id="frm_send_email">
                            <input type="email" id="email_get_info" name="email" spellcheck="false"
                                   autocomplete="off" value="Email"
                                   class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                                   onfocus="javascript:if(this.value=='Email'){this.value=''}">
                            <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                                Gửi
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            <!--end email-->
        </div>
    </div>
</div>
<!--end van chuyen-->

<style>
    .catalog {
        background: white;
        text-align: center;
        padding: 5px 10px 15px;
    }
</style>
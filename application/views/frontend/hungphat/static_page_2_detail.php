<div class="container new detail-new">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="/<?php echo ROUTE_NEWS . '.html' ?>">Tin tức</a></li>
        <li class="active"><?php echo $info->name ?></li>
    </ol>
    <div class="content">
        <!--new left-->
        <h3><?php echo $info->name ?></h3>
        <!--        <div>-->
        <!--            <span class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i><i>&nbsp;&nbsp;Sep 13,16</i></span>-->
        <!--            &nbsp;&nbsp;&nbsp;-->
        <!--            <span class="author"><i class="fa fa-user" aria-hidden="true"></i><i>&nbsp;&nbsp;ductran</i></span>-->
        <!--        </div>-->
        <div class="left col-sm-8 no-padding">
            <div class="main-detail">
                <div class="item">
                    <div class="cnt text-justify">
                        <?php echo $info->content ?>
                    </div>
                </div>
            </div>
        </div>
        <!--new left-->
        <!-- sidebar -->
        <div class="right col-sm-4 no-padding">
            <div class="product-hot-home maintain_new">
                <h3><a>Bài viết mới</a></h3>
            </div>
            <div class="news">
                <?php if ($list): ?>
                <?php foreach ($list as $key => $item): ?>
                <div class="item">
                    <a href="<?php echo get_detail_url(ROUTE_NEWS, $item->name, $item->id) ?>">
                        <div class="col-sm-5 no-padding"
                        ">
                        <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                </div>
                <div class="col-sm-7 no-padding"
                ">
                <h3><?php echo $item->name ?></h3>

                <p class="hidden-md hidden-sm hidden-lg"><?php echo $item->desc ?></p>
            </div>
            </a>
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
        <div class="pull-right read-continue"><a
                href="<?php echo get_detail_url(ROUTE_NEWS, $item->name, $item->id) ?>">Xem thêm>></a></div>
    </div>
</div>
<!-- sidebar -->
</div>

<!--email-->
<div class="send-mail-home">
    <div class="bd">
        <div class="note">
            <span>Đăng ký <br>nhận bản tin</span>
            <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
        </div>
        <div class="note_sm">
            Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
        </div>
        <div class="email">
            <form id="frm_send_email">
                <input type="text" name="email" spellcheck="false" autocomplete="off" value="Email"
                       class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                       onfocus="javascript:if(this.value=='Email'){this.value=''}">
                <a title="Email" href="javascript:void(0)" onclick="document.frm_send_email.submit()">
                    Gửi
                </a>
            </form>
        </div>
    </div>
</div>
<!--end email-->
</div>
<div class="slider-home-banner">
    <?php if (isset($list_slider)): ?>
        <?php foreach ($list_slider as $key => $val): ?>
            <div><a href="<?php echo $val->link; ?>" target="_blank"><img class="cus_img" src="<?php echo $val->img; ?>"
                                                                          alt="<?php echo $val->name; ?>"></a></div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<!--end header-->
<!--content-->
<div class="main-content">
    <!--van chuyen-->
    <div class="hidden-xs cnt-top-home">
        <div class="container">
            <div class="act">
                <div class="transfer col-sm-4">
                    <h3>Vận chuyển</h3>
                    <p>Miễn phí vận chuyển trong nước</p>
                </div>
                <div class="payment col-sm-4">
                    <h3>Thanh toán trực tiếp</h3>
                    <p>Thanh toán trực tiếp khi nhận hàng hoặc chuyển khoản</p>
                </div>
                <div class="extra col-sm-4">
                    <h3>Đổi trả hàng</h3>
                    <p>Thời gian đổi trả hàng lên đến 30 ngày</p>
                </div>
            </div>
        </div>
    </div>
    <!--end van chuyen-->
    <!--slider-->
    <div class="product-hot-home">
        <div class="container">
            <h3><a href="<?php echo base_url(ROUTE_PRODUCT_HOT).'moi-nhat.html' ?>">SẢN PHẨM MỚI NHẤT</a></h3>
            <div class="w_24 no-padding ads hidden-xs">
                <?php foreach ($banner as $index => $item): ?>
                    <?php if ($index < 2): ?>
                        <div>
                            <a href="<?php echo $item->link ?>"><?php echo get_img_tag($item->img, $item->name, 'cus_img') ?></a>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="w_76 no-padding product">
                <div class="slider-product-home slider-product-new-home">
                    <!--8 sp -->
                    <?php foreach ($product_new as $index => $item): ?>
                        <?php if ($index == 0 || $index == 8): ?>
                            <div>
                        <?php endif; ?>

                        <?php if (in_array($index, array(0, 4, 8, 12))): ?>
                        <div class="items">
                    <?php endif; ?>
                        <div class="item">
                            <?php $this->load->view($theme_template . '/product_item', array('product' => $item)); ?>
                        </div>
                        <?php if (in_array($index, array(3, 7, 11, 15)) || $index == count($product_new) - 1): ?>
                        </div>
                    <?php endif; ?>

                        <?php if ($index == 7 || $index == count($product_new) - 1): ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <!--end slider-->

    <div class="product-hot-home">
        <div class="container">
            <h3><a  href="<?php echo base_url(ROUTE_PRODUCT_HOT).'noi-bat.html' ?>">SẢN PHẨM NỔI BẬT</a></h3>
            <div class="w_24 no-padding ads hidden-xs">
                <?php foreach ($banner as $index => $item): ?>
                    <?php if ($index >= 2): ?>
                        <div>
                            <a href="<?php echo $item->link ?>"><?php echo get_img_tag($item->img, $item->name, 'cus_img') ?></a>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="w_76 no-padding product">
                <div class="slider-product-home slider-product-hot-home">
                    <?php foreach ($product_hot as $index => $item): ?>
                        <?php if ($index == 0 || $index == 8): ?>
                            <div>
                        <?php endif; ?>

                        <?php if (in_array($index, array(0, 4, 8, 12))): ?>
                        <div class="items">
                    <?php endif; ?>
                        <div class="item">
                            <?php $this->load->view($theme_template . '/product_item', array('product' => $item)); ?>
                        </div>
                        <?php if (in_array($index, array(3, 7, 11, 15)) || $index == count($product_hot) - 1): ?>
                        </div>
                    <?php endif; ?>

                        <?php if ($index == 7 || $index == count($product_hot) - 1): ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!--end slider-->

    <!--new-->
    <div class="product-hot-home">
        <div class="container">
            <h3><a href="<?php echo base_url(ROUTE_NEWS); ?>" target="_blank">Tin tức</a></h3>
            <div class="new-home">
                <div class="bd">
                    <?php foreach ($news as $index => $item): ?>
                        <?php if ($index == 0): ?>
                            <div class="item">
                                <a href="<?php echo get_detail_url(ROUTE_NEWS, $item->name, $item->id) ?>">
                                    <div
                                        class="news-img"><?php echo get_img_tag($item->img, $item->name, 'cus_img') ?></div>
                                    <h3 class="title"><?php echo $item->name ?></h3><br/>
                                    <!--                                    <p>-->
                                    <?php //echo word_limiter($item->desc, 30) ?><!--</p>-->
                                </a>
                            </div>
                        <?php else: ?>
                            <?php if ($index == 1): ?>
                                <div class="item hidden-xs">
                            <?php endif; ?>
                            <div class="sub-new">
                                <a href="<?php echo get_detail_url(ROUTE_NEWS, $item->name, $item->id) ?>">
                                    <div
                                        class="news-img-smallest"><?php echo get_img_tag($item->img, $item->name, 'cus_img') ?></div>
                                    <h3 class="title"><?php echo $item->name ?></h3>
                                    <!--                                    <p>--><?php //echo $item->desc ?><!--</p>-->
                                </a>
                            </div>
                            <?php if ($index > 0 && $index == count($news) - 1): ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="cmt-home slider-cmt-home">
                <?php if (isset($list_comment)): ?>
                    <?php foreach ($list_comment as $key => $val): ?>
                        <div class="bd">

                            <div><?php echo get_img_tag($val->avartar, $val->title, 'cus_img') ?></div>
                            <div class="cmt-cnt">
                                <p>"<?php echo word_limiter($val->content, 70) ?>"</p>
                                <span><i><?php echo $val->from; ?></i></span>
                            </div>

                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!--end new-->

    <!--van chuyen mobile-->
    <div class="hidden-sm hidden-md hidden-lg cnt-top-home-mb">
        <div class="container">
            <div class="act">
                <div class="transfer">
                    <h3>Vận chuyển</h3>
                    <p>Miễn phí bán kính 200KM</p>
                </div>
                <div class="payment">
                    <h3>Thanh toán Cod</h3>
                    <p>Thanh toán trực tiếp khi khách hàng nhận</p>
                </div>
                <div class="extra">
                    <h3>Đổi trả hàng</h3>
                    <p>Thời gian đổi trả hàng lên đến 30 ngày</p>
                </div>
            </div>
        </div>
    </div>
    <!--end van chuyen-->

    <!--provider-->
    <div class="product-hot-home">
        <div class="container">
            <h3><a href="<?php echo base_url(ROUTE_PRODUCT_PUBLISHER); ?>" target="_blank">Nhà cung cấp</a></h3>
            <div class="slider-provider-home">
                <?php foreach ($publisher as $item): ?>
                    <div class="item"><a href="<?php echo $item->site; ?>"
                                         target="_blank"><?php echo get_img_tag($item->img, $item->name, 'cus_img') ?></a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--end provider-->
    <!--email-->
    <div class="send-mail-home">
        <div class="container">
            <div class="bd">
                <div class="note">
                    <span>Đăng ký <br>nhận bản tin</span>
                    <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
                </div>
                <div class="note_sm">
                    Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
                </div>
                <div class="email">
                    <!--                    <form id="frm_send_email">-->
                    <!--                        <input type="text" name="email" spellcheck="false" autocomplete="off" value="Email"-->
                    <!--                               class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"-->
                    <!--                               onfocus="javascript:if(this.value=='Email'){this.value=''}">-->
                    <!--                        <a title="Email" href="javascript:void(0)" onclick="document.frm_send_email.submit()">-->
                    <!--                            Gửi-->
                    <!--                        </a>-->
                    <!--                    </form>-->
                    <form id="frm_send_email">
                        <input type="email" id="email_get_info" name="email" spellcheck="false" autocomplete="off"
                               value="Email"
                               class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                               onfocus="javascript:if(this.value=='Email'){this.value=''}">
                        <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                            Gửi
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end email-->
</div>
<!--end content-->

<!--footer-->
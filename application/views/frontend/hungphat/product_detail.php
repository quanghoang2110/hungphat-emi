<div id="fb-root"></div>
<!--<script>(function (d, s, id) {-->
<!--        var js, fjs = d.getElementsByTagName(s)[0];-->
<!--        if (d.getElementById(id)) return;-->
<!--        js = d.createElement(s);-->
<!--        js.id = id;-->
<!--        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1676668252647445";-->
<!--        fjs.parentNode.insertBefore(js, fjs);-->
<!--    }(document, 'script', 'facebook-jssdk'));</script>-->
<div class="container detail-product">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="#">Sản phẩm</a></li>
        <li class="active"><?php echo $info->name ?></li>
    </ol>
    <div class="content">
        <!--new left-->
        <div class="col-sm-9">
            <div class="main-detail">
                <div class="item">
                    <!-- product detail -->
                    <div class=" col-sm-6 no-padding">
                        <div class="zoom-wrapper">
                            <div class="zoom-left">

                                <?php if ($imgs): ?>
                                    <?php $img_link = get_img_url($imgs[0]->img); ?>
                                    <img id="zoom_03"
                                         src="<?php echo $img_link ?>"
                                         data-zoom-image="<?php echo $img_link ?>" width="500">
                                    <div id="gal1">
                                        <?php foreach ($imgs as $index => $p_img): ?>
                                            <?php $img_link = get_img_url($p_img->img); ?>
                                            <a href="#"
                                               class="elevatezoom-gallery <?php echo $index == 0 ? 'active' : ''; ?>"
                                               data-update=""
                                               data-image="<?php echo $img_link ?>"
                                               data-zoom-image="<?php echo $img_link ?>">
                                                <img id="zoom_03"
                                                     src="<?php echo $img_link ?>">
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="right col-sm-6 no-padding">
                        <h3><?php echo $info->name ?></h3>
                        <span>
                        <?php $star = (isset($info->star)) ? $info->star : 0; ?>
                            <?php for ($i = 0; $i < $star; $i++): ?>
                                <span class="glyphicon glyphicon-star" style="color: red"></span>
                            <?php endfor; ?>
                            <?php for ($i = 0; $i < (5 - $star); $i++): ?>
                                <span class="glyphicon glyphicon-star"></span>
                            <?php endfor; ?>
                             </span>
                        <?php if (isset($info->total_vote)): ?>
                            <span> ( <span class="fb-comments-count"
                                           data-href="<?php echo base_url($_SERVER['REQUEST_URI']) ?>"></span> đánh giá )</span>
                        <?php endif; ?>
                        <span class="pull-right"><i class="fa fa-eye"></i> <?php echo $info->viewed ?> lượt xem&nbsp;&nbsp;</span>
                        <!--                        <p><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;7000</p>-->
                        <div class="number">
                            <span>Số lượng</span>&nbsp;&nbsp;&nbsp;
                            <select name="soluong" class="form-control soluong" id="soluong_detail">
                                <?php for ($i = 1; $i <= 20; $i++): ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php endfor; ?>
                            </select>
                            <!--                            <span class="pull-right"><i class="fa fa-eye"></i> -->
                            <?php //echo $info->viewed ?><!-- lượt xem&nbsp;&nbsp;</span>-->
                        </div>
                        <div>
                            <a href="#datmua" onclick="addProductToCart(<?php echo $info->id ?>,1)" class="btn-cart"><i
                                    class="fa fa-shopping-cart" aria-hidden="true"></i>Thêm vào
                                giỏ hàng</a>
                            <a href="#lienhe" data-toggle="modal" data-target="#contact" class="btn-contact">Liên hệ đặt
                                hàng</a>
                        </div>
                        <div class="col-xs-12 like_product">
                            <div class="fb-like" data-href="<?php echo base_url($_SERVER['REQUEST_URI']); ?>"
                                 data-layout="button_count" data-action="like" data-size="small" data-show-faces="false"
                                 data-share="true"></div>
                            <g:plusone></g:plusone>
                            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                        </div>
                    </div>
                </div>
                <!-- product -->
            </div>
            <div class="item">
                <div class="cnt">
                    <div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Thông số kỹ thuật</a></li>
                            <li><a data-toggle="tab" href="#menu1">Hướng dẫn sử dụng</a></li>
                            <li><a data-toggle="tab" href="#menu2">Đánh giá và bình luận</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <br/>
                                <?php echo $info->content ?>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <br/>
                                <?php echo $info->guide ?>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="fb-comments" style="width:100%"
                                     data-href="<?php echo base_url(get_detail_url('', $info->name, $info->id)) ?>"
                                     data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--new left-->
        <!-- sidebar -->
        <div class="col-sm-3">
            <?php if ($ads): ?>
                <?php foreach ($ads as $key => $item): ?>
                    <div class="ads">
                        <div>
                            <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <!-- sidebar -->
    </div>
    <div class="product-hot-home product-other">
        <?php if ($info_hot): ?>
            <h3><a href="#">SẢN PHẨM NỔI BẬT</a></h3>
            <div class="slider-product-home slider-product-detail">
                <?php foreach ($info_hot as $key => $item): ?>
                    <div class="item">
                        <div class="bd">
                            <a href="<?php echo get_detail_url('', $item->name, $item->id) ?>">
                                <?php echo get_img_tag($item->avatar, $item->name, 'cus_img') ?>
                                <p title="<?php echo $item->name ?>"><?php echo $item->name ?></p>
                            </a>
                            <a href="#" data-toggle="modal" data-target="#contact" class="contact">Vui lòng liên
                                hệ</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <!--email-->
        <div class="send-mail-home">
            <div class="bd">
                <div class="note">
                    <span>Đăng ký <br>nhận bản tin</span>
                    <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
                </div>
                <div class="note_sm">
                    Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng
                    biệt
                </div>
                <div class="email">
                    <form id="frm_send_email">
                        <input type="text" name="email" spellcheck="false" autocomplete="off" value="Email"
                               class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                               onfocus="javascript:if(this.value=='Email'){this.value=''}">
                        <a title="Email" href="javascript:void(0)" onclick="document.frm_send_email.submit()">
                            Gửi
                        </a>
                    </form>
                </div>
            </div>
        </div>
        <!--end email-->
    </div>
</div>

<!-- product -->
<script src="/assets/frontend/hungphat/plugin/zoom/jquery.elevatezoom.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.slider-product-detail').slick({
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    });

    $("#zoom_03").elevateZoom({
        gallery: 'gal1',
        cursor: 'pointer',
        galleryActiveClass: 'active',
        imageCrossfade: true,
        loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
    });
    //
    //pass the images to Fancybox
    $("#zoom_03").bind("click", function (e) {
        var ez = $('#img_01').data('elevateZoom');
        $.fancybox(ez.getGalleryList());
        return false;
    });

    function addProductToCart($id, $true) {
        var url = "/<?php echo ROUTE_BUY_CART; ?>";
        var data = {id: $id, soluong: $('#soluong_detail').val()};
        $.post(url, data)
            .done(function (res) {
                if (res) {
                    $('.service .icon-msg').html(res);
                    showCart();
                }
            });
    }

    function increaseViewed() {
        $.post('/<?php echo ROUTE_AJAX_INCREASE_VIEWED ?>', {id: '<?php echo $info->id ?>'});
    }

    setTimeout(increaseViewed, 5000);
</script>
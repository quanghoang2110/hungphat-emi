<hr class="between-sections">

<!--Section: Contact v.1-->
<section class="section m-b-4" id="lienhengay">

    <!--Section heading-->
    <h1 class="section-heading">Liên hệ</h1>
    <!--Section sescription-->
    <div class="row">

        <!--First column-->

            <!--Google map-->
            <div id="map-container" class="z-depth-1-half map-container" style="height: 400px"></div>

            <br>
            <!--Buttons-->
            <div class="row text-xs-center">
                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-map-marker"></i></a>
                    <p>Tòa nhà Packexim, Ngõ 49</p>
                    <p>An Dương Vương, Tây Hồ, Hà Nội</p>
                </div>

                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-phone"></i></a>
                    <p><a href="tel:049981666">04.9981.666</a></p>
                    <p>T2 - CN, 8:00-22:00</p>
                </div>

                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-envelope"></i></a>
                    <p><a href="mailto:info@geonviet.com">info@geonviet.com</a></p>
                    <p><a href="mailto:hotro@geonviet.com">hotro@geonviet.com</a></p>
                </div>
            </div>

        </div>
        <!--/Second column-->

    </div>

</section>

<div>
    <!-- Modal Contact -->
    <div class="modal fade modal-ext" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->

            <div class="modal-content">
                <form action="/<?php echo ROUTE_CONTACT_NOW ?>" method="post" onsubmit="return checkSubmit(); ">
                    <!--Header-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3><i class="fa fa-phone"></i> Yêu cầu gọi lại</h3>
                    </div>
                    <!--Body-->
                    <div class="modal-body">
                        <p>Vui lòng để lại thông tin cá nhân, chúng tôi sẽ gọi lại và tư vấn giúp bạn.</p>
                        <br>
                        <p class="error" id="txt_err"></p>
                        <div class="md-form">
                            <i class="fa fa-user prefix"></i>
                            <input name="fullname" type="text" id="form22" class="form-control"
                                   data-msg-required="Vui lòng cho biết tên của bạn" required/>
                            <label for="form42">Tên của bạn</label>
                        </div>

                        <div class="md-form">
                            <i class="fa fa-phone prefix"></i>
                            <input name="phone" type="text" id="form32" class="form-control"
                                   data-msg-required="Chúng tôi sẽ liên hệ với qua sđt này" required/>
                            <label for="form34">Số điện thoại</label>
                        </div>

                        <div class="md-form">
                            <i class="fa fa-pencil prefix"></i>
                            <textarea name="note" type="text" id="form8" class="md-textarea"></textarea>
                            <label for="form8">Nội dung</label>
                        </div>

                        <div class="md-form">
                            <div id="html_element"></div>
                            <div id="submitErr" class="text-danger"></div>
                        </div>

                        <div class="text-xs-center">
                            <button id="btnSubmitNow" type="submit" class="btn btn-primary disabled">Đồng ý</button>
                            <div class="call">
                                <p>Hoặc gọi trực tiếp tới? <span class="cf-phone"><i
                                            class="fa fa-phone"> +<?php echo PHONE_CONTACT ?></i></span></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--/.Content-->
        </div>
    </div>

    <div data-toggle="modal" data-target="#modal-contact" class="hidden-xs-down yeucaugoilai ">
        <div class="description" data-toggle="tooltip" data-placement="top" title="Yêu cầu gọi lại">
            <div class="phone_animation">
                <div class="phone_animation_circle"></div>
                <div class="phone_animation_circle_fill"></div>
                <div class="phone_animation_circle_fill_img"></div>
            </div>
        </div>
    </div>

    <div class="hidden-sm-up yeucaugoilai" data-toggle="modal" data-target="#modal-contact">
        <div class="description" data-toggle="tooltip" data-placement="top" title="Yêu cầu gọi lại"><a
                href="tel:049981666">
                <div class="phone_animation">
                    <div class="phone_animation_circle"></div>
                    <div class="phone_animation_circle_fill"></div>
                    <div class="phone_animation_circle_fill_img"></div>
                </div>
            </a></div>
    </div>

    <div class="modal fade modal-ext" id="modal-thongbao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3><i class="fa fa-phone"></i> Liên hệ </h3>
                </div>
                <!--Body-->
                <div class="modal-body">

                    <div id="progress" class="flex-center">

                        <div class="preloader-wrapper big active">
                            <div class="spinner-layer spinner-blue-only">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div><div class="gap-patch">
                                    <div class="circle"></div>
                                </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="success" class="hiddendiv">
                        <div class="card flex-center">
                            <div class="card-block">
                                <div class="card-circle">
                                    <i class="fa fa-check green-text"></i>
                                </div>
                            </div>
                        </div>

                        <h4>Chúng tôi sẽ liên hệ với bạn.</h4>
                        <h4>Xin cảm ơn.</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Đóng</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>

</div>

<style>

    #success.card {
        border: 0;
        box-shadow: none;
    }

    .hidden-sm-up .phone_animation {
        width: 100px;
        height: 100px;
    }

    .hidden-sm-up .phone_animation_circle {
        width: 80px;
        height: 80px;
        top: 10px;
        left: 10px;
    }

    .hidden-sm-up .phone_animation_circle_fill {
        width: 50px;
        height: 50px;
        top: 25px;
        left: 25px;
    }

    .hidden-sm-up .phone_animation_circle_fill_img {
        width: 30px;
        height: 30px;
        top: 35px;
        left: 35px;
    }

    .yeucaugoilai .description {
        position: fixed;
        right: 0;
        bottom: 0;
        z-index: 100;
    }

    .phone_animation {
        background-color: transparent;
        width: 200px;
        height: 200px;
        cursor: pointer;
        z-index: 200000 !important;
        -webkit-backface-visibility: hidden;
        -webkit-transform: translateZ(0);
        -webkit-transition: visibility .5s;
        -moz-transition: visibility .5s;
        -o-transition: visibility .5s;
        transition: visibility .5s;
        visibility: visible;
        margin: 0 auto;
    }

    .phone_animation_circle {
        width: 160px;
        height: 160px;
        top: 20px;
        left: 20px;
        position: absolute;
        background-color: transparent;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        border: 2px solid rgba(30, 30, 30, 0.4);
        opacity: 0.5;
        -webkit-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
        -moz-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
        -ms-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
        -o-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
        animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
        -webkit-transition: all .5s;
        -moz-transition: all .5s;
        -o-transition: all .5s;
        transition: all .5s;
        -webkit-transform-origin: 50% 50%;
        -moz-transform-origin: 50% 50%;
        -ms-transform-origin: 50% 50%;
        -o-transform-origin: 50% 50%;
        transform-origin: 50% 50%;
        border-color: #00aff2;
    }

    .phone_animation_circle_fill {
        width: 100px;
        height: 100px;
        top: 50px;
        left: 50px;
        position: absolute;
        background-color: #00aff2;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        border: 2px solid transparent;
        opacity: .2;
        -webkit-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
        -moz-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
        -ms-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
        -o-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
        animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
        -webkit-transition: all .5s;
        -moz-transition: all .5s;
        -o-transition: all .5s;
        transition: all .5s;
        -webkit-transform-origin: 50% 50%;
        -moz-transform-origin: 50% 50%;
        -ms-transform-origin: 50% 50%;
        -o-transform-origin: 50% 50%;
        transform-origin: 50% 50%;
    }

    .phone_animation_circle_fill_img {
        width: 60px;
        height: 60px;
        top: 70px;
        left: 70px;
        position: absolute;
        background: #F44336 url(/images/geon/iconphone.png) no-repeat center center;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        border: 2px solid transparent;
        -webkit-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
        -moz-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
        -ms-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
        -o-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
        animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
        -webkit-transform-origin: 50% 50%;
        -moz-transform-origin: 50% 50%;
        -ms-transform-origin: 50% 50%;
        -o-transform-origin: 50% 50%;
        transform-origin: 50% 50%;

    }

    @-moz-keyframes coccoc-alo-circle-anim {
        0% {
            -moz-transform: rotate(0) scale(.5) skew(1deg);
            opacity: .1;
            -moz-opacity: .1;
            -webkit-opacity: .1;
            -o-opacity: .1
        }
        30% {
            -moz-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .5;
            -moz-opacity: .5;
            -webkit-opacity: .5;
            -o-opacity: .5
        }
        100% {
            -moz-transform: rotate(0) scale(1) skew(1deg);
            opacity: .6;
            -moz-opacity: .6;
            -webkit-opacity: .6;
            -o-opacity: .1
        }
    }

    @-webkit-keyframes coccoc-alo-circle-anim {
        0% {
            -webkit-transform: rotate(0) scale(.5) skew(1deg);
            -webkit-opacity: .1
        }
        30% {
            -webkit-transform: rotate(0) scale(.7) skew(1deg);
            -webkit-opacity: .5
        }
        100% {
            -webkit-transform: rotate(0) scale(1) skew(1deg);
            -webkit-opacity: .1
        }
    }

    @-o-keyframes coccoc-alo-circle-anim {
        0% {
            -o-transform: rotate(0) kscale(.5) skew(1deg);
            -o-opacity: .1
        }
        30% {
            -o-transform: rotate(0) scale(.7) skew(1deg);
            -o-opacity: .5
        }
        100% {
            -o-transform: rotate(0) scale(1) skew(1deg);
            -o-opacity: .1
        }
    }

    @-moz-keyframes coccoc-alo-circle-fill-anim {
        0% {
            -moz-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
        50% {
            -moz-transform: rotate(0) -moz-scale(1) skew(1deg);
            opacity: .2
        }
        100% {
            -moz-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
    }

    @-webkit-keyframes coccoc-alo-circle-fill-anim {
        0% {
            -webkit-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
        50% {
            -webkit-transform: rotate(0) scale(1) skew(1deg);
            opacity: .2
        }
        100% {
            -webkit-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
    }

    @-o-keyframes coccoc-alo-circle-fill-anim {
        0% {
            -o-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
        50% {
            -o-transform: rotate(0) scale(1) skew(1deg);
            opacity: .2
        }
        100% {
            -o-transform: rotate(0) scale(.7) skew(1deg);
            opacity: .2
        }
    }

    @-moz-keyframes coccoc-alo-circle-img-anim {
        0% {
            transform: rotate(0) scale(1) skew(1deg)
        }
        10% {
            -moz-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        20% {
            -moz-transform: rotate(25deg) scale(1) skew(1deg)
        }
        30% {
            -moz-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        40% {
            -moz-transform: rotate(25deg) scale(1) skew(1deg)
        }
        50% {
            -moz-transform: rotate(0) scale(1) skew(1deg)
        }
        100% {
            -moz-transform: rotate(0) scale(1) skew(1deg)
        }
    }

    @-webkit-keyframes coccoc-alo-circle-img-anim {
        0% {
            -webkit-transform: rotate(0) scale(1) skew(1deg)
        }
        10% {
            -webkit-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        20% {
            -webkit-transform: rotate(25deg) scale(1) skew(1deg)
        }
        30% {
            -webkit-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        40% {
            -webkit-transform: rotate(25deg) scale(1) skew(1deg)
        }
        50% {
            -webkit-transform: rotate(0) scale(1) skew(1deg)
        }
        100% {
            -webkit-transform: rotate(0) scale(1) skew(1deg)
        }
    }

    @-o-keyframes coccoc-alo-circle-img-anim {
        0% {
            -o-transform: rotate(0) scale(1) skew(1deg)
        }
        10% {
            -o-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        20% {
            -o-transform: rotate(25deg) scale(1) skew(1deg)
        }
        30% {
            -o-transform: rotate(-25deg) scale(1) skew(1deg)
        }
        40% {
            -o-transform: rotate(25deg) scale(1) skew(1deg)
        }
        50% {
            -o-transform: rotate(0) scale(1) skew(1deg)
        }
        100% {
            -o-transform: rotate(0) scale(1) skew(1deg)
        }
    }

    .yeucaugoilai .description .phone_animation {

    }

    .yeucaugoilai .phone_animation_circle {

    }

    .yeucaugoilai .phone_animation_circle_fill_img {
        background-size: 70%;
    }

    .yeucaugoilai .phone_animation_circle_fill {

    }
</style>

<!--/Section: Contact v.1-->
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB_LVf5PQmiBXjwcRbpFWJEWHCjzr7EZoY"></script>
<script>
    function init_map() {

        var var_location = new google.maps.LatLng(21.0871737, 105.8119157, 17.75);

        var var_mapoptions = {
            center: var_location,

            zoom: 14
        };

        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "New York"
        });

        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);

        var_marker.setMap(var_map);

    }

    google.maps.event.addDomListener(window, 'load', init_map);
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
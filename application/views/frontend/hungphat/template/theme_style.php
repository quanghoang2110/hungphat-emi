<style>
    .gap {
        display: inline-block;
        margin-top: 15px;
        margin-bottom: 15px;
        clear: both;
    }
    .gap-small {
        display: inline-block;
        margin-top: 15px;
        clear: both;
    }
    .gap-mega {
        display: inline-block;
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .product_item .carousel-item div[class*='col'] {
        padding-left: 5px;
        padding-right: 5px;
    }

    .product_item .carousel-item div[class*='col'] .card{
        margin-bottom: 10px;
    }
    .product_item .card-block{
        padding-top: 10px;
        padding-bottom: 0;
    }
    .text-red{
        color: red;
    }

    .btn-red {
        background: #ed3125;
        color: white;
    }

    .back-to-top {
        cursor: pointer;
        position: fixed;
        bottom: 50px;
        right: 10px;
        display: none;
    }

    .content img{
        max-width: 99%;
        height: auto;
        margin: 15px auto;
    }

    .content.product_content img{
        width: 100%;
        max-width: 100%;
        border: 1px solid #ccc;
        border-right: 0;
        border-left: 0;
        margin: 0;
    }
    .content.product_content div.item{
        margin-left: 2px;
    }

</style>
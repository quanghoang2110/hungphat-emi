<div id="wrapper">
    <?php $this->load->view($theme_template . 'header'); ?>
    <?php $this->load->view($theme_template . 'modal'); ?>
    <?php $this->load->view($content); ?>
</div>
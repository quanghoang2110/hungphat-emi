<!-- Bootstrap 3.3.6 -->
<!--<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">-->
<!-- Font Awesome -->
<!--<link rel="stylesheet" href="assets/fontawesome/css/font-awesome.min.css">-->
<!-- Plugin slick slider-->
<link rel="stylesheet" href="/assets/frontend/hungphat/plugin/slick/slick.css">
<link rel="stylesheet" href="/assets/frontend/hungphat/plugin/slick/slick-theme.css">
<!-- normalize Css-->
<link rel="stylesheet" href="/assets/frontend/hungphat/css/normalize.css">
<!-- frontend Css mobile-->
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/menu/css/default.css"/>
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/menu/css/component.css"/>

<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/mobile.css"/>
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/mobile1.css" media="screen and (min-width:401px)" />
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/mobile2.css" media="screen and (min-width:551px)" />
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/tablet.css" media="screen and (min-width:768px)" />
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/style.css" media="screen and (min-width:992px)" />
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/styles-lg.css" media="screen and (min-width:1200px)" />
<link rel="stylesheet" type="text/css" href="/assets/frontend/hungphat/css/tuyen.css"/>


<script>
    var base_url = '<?php echo base_url(); ?>';
</script>
<script src="/assets/frontend/hungphat/menu/js/modernizr.custom.js""></script>

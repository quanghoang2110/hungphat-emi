<div id="sp-run" class="op">
    <div>
        <p>Support</p>
        <p class="h"><a href="tel:<?php echo $company_phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></p>
        <p class="h"><a href="skype:<?php echo $company_skype; ?>"><i class="fa fa-skype" aria-hidden="true"></i></a>
        </p>
    </div>
    <div class="hot-home">
        <p>Hotline</p>
        <p><?php echo $company_phone; ?></p>
    </div>

</div>
<?php if ($static_page): ?>
    <?php $name_static_page = $id_static_page = array(); ?>
    <?php $item_static_page = 1; ?>
    <?php foreach ($static_page as $key => $val): ?>
        <?php $name_static_page[$item_static_page] = $val->name; ?>
        <?php $id_static_page[$item_static_page] = $val->id; ?>
        <?php $item_static_page++; ?>
    <?php endforeach; ?>
<?php endif; ?>
<header class="main-header">
    <?php //if (!($item_static_page)): ?>
    <div class="banner">
        <?php if ($img_banner_top_home): ?>
            <a href="<?php echo $link_banner_top_home; ?>" target="_blank"><img class="cus_img"
                                                                                src="<?php echo $img_banner_top_home; ?>"
                                                                                alt="<?php echo $name_banner_top_home; ?>"/></a>
        <?php endif; ?>
    </div>
    <?php //endif; ?>
    <!--header top -->
    <div class="head-top hidden-xs">
        <div class="container">
            <div class="pull-left cont-left">
                <a href="tel:<?php echo $company_phone; ?>"><span class="bg-phone"></span>Tổng đài bàn
                hàng: <?php echo $company_phone; ?></a>.Từ <?php echo $company_time; ?>
            </div>
            <div class="pull-right">
                <?php if ($this->session->userdata(ID_FRONTEND_SESSION)): ?>
                    <div class="dropdown pull-right act-user-1">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                            <?php echo $this->session->userdata(USERNAME_FRONTEND_SESSION) ?>
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="/dang-xuat.html">Thoát</a></li>
                        </ul>
                    </div>
                <?php else: ?>
                    <div class="pull-right act-user">
                        <span class="mrk"></span>
                        <a class="active" href="#dangky" onclick="showTab('register')"
                           data-toggle="modal" data-target="#modalLogin">Đăng ký</a>
                        &nbsp;<a href="#dangnhap" data-toggle="modal" onclick="showTab('login')" data-target="#modalLogin">Đăng
                            nhập</a>&nbsp;&nbsp;

                    </div>
                <?php endif; ?>
                <div class="language pull-right">
                    <!--                    <span class="pull-left">Ngôn ngữ</span>-->
                    <!--                    <div class="dropdown-language pull-left">-->
                    <!--                        <div><a href="#"><img src="/images/hungphat/icon/1-24.png"/> </a></div>-->
                    <!--                        <div><a href="#"><img src="/images/hungphat/icon/1-24.png"/> </a></div>-->
                    <!--                        <div><a href="#"><img src="/images/hungphat/icon/1-24.png"/> </a></div>-->
                    <!--                    </div>-->
                    <!--                    <span class="caret"></span>-->
                    <div id="google_translate_element" class="pull-left"></div>
                    <img class="pull-left" src="/images/hungphat/icon/1-24.png"/>
                </div>
            </div>
        </div>
    </div>
    <!--end header top-->
    <!--header between-->
    <div class="head-bet">
        <div class="container">
            <div class="logo">
                <h1>
                    <a href="/"
                       title="Tin tuc hung phat">
                        <img alt="Tin tuc hung phat" class="cus_img"
                             src="<?php if (isset($logo)) echo $logo; else echo '/images/hungphat/logo/1-26.png'; ?>">
                    </a>
                </h1>
            </div>
            <div class="search">
                <form name="search_box" id="search_box" action="/<?php echo ROUTE_SEARCH; ?>">
                    <input type="text" name="q" spellcheck="false" autocomplete="off"
                           value="<?php echo ($this->input->get('q')) ? $this->input->get('q') : '' ?>"
                           placeholder="Nhập nội dung tìm kiếm"
                           class="searchTxt">
                    <a title="Nhập nội dung tìm kiếm" class="searchIcon" href="javascript:void(0)"
                       onclick="document.search_box.submit()">
                        <img width="30px" height="30px" alt="search" class="cus_center"
                             src="/images/hungphat/icon/1-35.png">
                    </a>
                </form>
            </div>
            <div class="service pull-right">
                <a href="#giohang" onclick="showCart()" class="relative">
                    <span class="absolute cus_center icon-cart cus_icon active">
                        <?php
                        $ids = get_cookie('cart_product');
                        $id_arr = array();
                        $pos = strpos($ids, ',');
                        if ($pos !== false)
                            $id_arr = explode(',', $ids);
                        if (count($id_arr) > 0):
                            ?>
                            <span class="icon-msg">

                            <?php echo count($id_arr); ?>
                        </span>
                        <?php endif; ?>
                    </span> Giỏ hàng</a>
                <a href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_HUONG_DAN], $id_static_page[TRANG_HUONG_DAN]) ?>"
                   class="relative"><span class="absolute cus_center icon-guide cus_icon"></span>Hướng dẫn</a>
                <a href="<?php echo base_url(ROUTE_CONTACT_PAGE); ?>" class="relative"><span
                        class="absolute cus_center icon-helper cus_icon"></span>Hỗ trợ</a>
            </div>
        </div>
    </div>
    <!--end header between-->
    <!--header bottom-->
    <div class="head-bot">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header hidden-xs">
                        <a href="/">
                        <span class="hidden-xs"><i class="fa fa-home" aria-hidden="true"></i><span
                                class="bd-right"></span></span></a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown dropdown_product">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">Danh sách sản phẩm<span
                                        class="caret"></span></a>
                                <div class="dropdown_content">
                                    <ul class="dropdown-menu">
                                        <?php foreach ($product_type as $parent): ?>
                                            <li class="dropdown-submenu">
                                                <a tabindex="-1"
                                                   href="<?php echo get_detail_url(ROUTE_PRODUCT, $parent->name, $parent->id) ?>"><?php echo $parent->name ?></a>
                                                <?php if (key_exists('sub', $parent) && $parent->sub): ?>
                                                    <ul class="dropdown-menu">
                                                        <?php foreach ($parent->sub as $sub_1): ?>
                                                            <li class="dropdown-submenu">
                                                                <a href="<?php echo get_detail_url(ROUTE_PRODUCT, $sub_1->name, $sub_1->id) ?>"><?php echo $sub_1->name ?></a>
                                                                <?php if (key_exists('sub', $sub_1) && $sub_1->sub): ?>
                                                                    <ul class="dropdown-menu">
                                                                        <?php foreach ($sub_1->sub as $sub_2): ?>
                                                                            <li>
                                                                                <a href="<?php echo get_detail_url(ROUTE_PRODUCT, $sub_2->name, $sub_2->id) ?>"><?php echo $sub_2->name ?></a>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                            <li class="<?php if (isset($current_menu) && $current_menu == 'introduce') echo 'active' ?>">
                                <a href="<?php echo base_url(ROUTE_INTRODUCE); ?>">Giới thiệu</a></li>
                            <li class="<?php if (isset($current_menu) && $current_menu == 'news') echo 'active' ?>"><a
                                    href="/<?php echo ROUTE_NEWS ?>.html">Tin tức</a></li>
                            <li class="<?php if (isset($current_menu) && $current_menu == 'news_recruitment') echo 'active' ?>">
                                <a href=" <?php echo base_url(ROUTE_NEWS_RECRUITMENT); ?>.html">Tuyển dụng</a></li>
                            <li class="<?php if (isset($current_menu) && $current_menu == 'news_sale') echo 'active' ?>">
                                <a href=" <?php echo base_url(ROUTE_NEWS_SALE); ?>.html">Khuyến mại</a></li>
                            <li class="<?php if (isset($current_menu) && $current_menu == 'product_publisher') echo 'active' ?>">
                                <a href="<?php echo base_url(ROUTE_PRODUCT_PUBLISHER); ?>">Download</a></li>
                            <!--                            <li class="-->
                            <?php //if(isset($current_menu) && $current_menu=='download') echo 'active' ?><!--"><a href="#">Download</a></li>-->
                        </ul>
                    </div><!-- /.navbar-collapse -->


<!--                    menu mobile-->
                    <div class="column">
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                <?php foreach ($product_type as $parent): ?>
                                    <li>
                                        <a tabindex="-1"
                                           href="<?php echo get_detail_url(ROUTE_PRODUCT, $parent->name, $parent->id) ?>"><?php echo $parent->name ?></a>
                                        <?php if (key_exists('sub', $parent) && $parent->sub): ?>
                                            <ul class="dl-submenu">
                                                <?php foreach ($parent->sub as $sub_1): ?>
                                                    <li>
                                                        <a href="<?php echo get_detail_url(ROUTE_PRODUCT, $sub_1->name, $sub_1->id) ?>"><?php echo $sub_1->name ?></a>
                                                        <?php if (key_exists('sub', $sub_1) && $sub_1->sub): ?>
                                                            <ul class="dl-submenu">
                                                                <?php foreach ($sub_1->sub as $sub_2): ?>
                                                                    <li>
                                                                        <a href="<?php echo get_detail_url(ROUTE_PRODUCT, $sub_2->name, $sub_2->id) ?>"><?php echo $sub_2->name ?></a>
                                                                    </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                                <li class="<?php if (isset($current_menu) && $current_menu == 'introduce') echo 'active' ?>">
                                    <a href="<?php echo base_url(ROUTE_INTRODUCE); ?>">Giới thiệu</a></li>
                                <li class="<?php if (isset($current_menu) && $current_menu == 'news') echo 'active' ?>"><a
                                        href="/<?php echo ROUTE_NEWS ?>.html">Tin tức</a></li>
                                <li class="<?php if (isset($current_menu) && $current_menu == 'news_recruitment') echo 'active' ?>">
                                    <a href=" <?php echo base_url(ROUTE_NEWS_RECRUITMENT); ?>.html">Tuyển dụng</a></li>
                                <li class="<?php if (isset($current_menu) && $current_menu == 'news_sale') echo 'active' ?>">
                                    <a href=" <?php echo base_url(ROUTE_NEWS_SALE); ?>.html">Khuyến mại</a></li>
                                <li class="<?php if (isset($current_menu) && $current_menu == 'product_publisher') echo 'active' ?>">
                                    <a href="<?php echo base_url(ROUTE_PRODUCT_PUBLISHER); ?>">Download</a></li>
                            </ul>
                        </div><!-- /dl-menuwrapper -->
                    </div>
<!--                    end menu mobile-->


                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <!--end header bottom-->
</header>

<style>
    /* Show the dropdown menu on hover */
    #bs-example-navbar-collapse-1 .dropdown_product:hover div.dropdown_content > ul:first-child {
        display: block;
    }

    .dropdown_content {
        margin-top: -15px !important;
        padding-top: 13px;
    }

    ul.dropdown-menu {
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2) !important;
    }
</style>
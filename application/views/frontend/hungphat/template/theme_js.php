<script src="/assets/frontend/hungphat/plugin/slick/slick.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.slider-home-banner').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 2000
        });
        $('.slider-product-hot-home').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true
        });
        $('.slider-product-new-home').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $('.slider-cmt-home').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $('.slider-provider-home').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '60px',
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '100px',
                        slidesToShow: 1
                    }
                }
            ]
        });

    })
</script>
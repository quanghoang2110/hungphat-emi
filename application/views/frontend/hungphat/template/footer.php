<?php if ($static_page): ?>
    <?php $name_static_page = $id_static_page = array(); ?>
    <?php $item_static_page = 1; ?>
    <?php foreach ($static_page as $key => $val): ?>
        <?php $name_static_page[$item_static_page] = $val->name; ?>
        <?php $id_static_page[$item_static_page] = $val->id; ?>
        <?php $item_static_page++; ?>
    <?php endforeach; ?>
<?php endif; ?>
<footer>

    <div class="fot-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4><?php echo $company_name ?></h4>
                    <span class="underline"></span>
                    <ul>
                        <li><i class="fa fa-home"></i> <?php echo $company_address; ?></li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a
                                href="mailto:<?php echo $company_email ?>"><?php echo $company_email; ?></a></li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:0912176564">0912.176.564</a>
                        </li>
                    </ul>
                </div>


                <div class="col-md-4">
                    <h4>Dành cho khách hàng</h4>
                    <span class="underline"></span>
                    <ul>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a
                                href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_PHUONG_THUC_VAN_CHUYEN], $id_static_page[TRANG_PHUONG_THUC_VAN_CHUYEN]) ?>">Phương
                                thức vận chuyển</a></li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a
                                href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_DIEU_KIEN_BAO_HANH], $id_static_page[TRANG_DIEU_KIEN_BAO_HANH]) ?>">Điều
                                kiện bảo hành</a></li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a
                                href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_CHINH_SACH_VA_BAO_MAT], $id_static_page[TRANG_CHINH_SACH_VA_BAO_MAT]) ?>">Chính
                                sách và bảo mật</a></li>
                        <!--                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a-->
                        <!--                            href="/-->
                        <?php //echo ROUTE_NEWS . '.html' ?><!--">Tin tức sự kiện</a></li>-->
                    </ul>
                </div>

                <div class="col-md-4">
                    <h4>Hướng dẫn</h4>
                    <span class="underline"></span>
                    <ul>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a
                                href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_HUONG_DAN], $id_static_page[TRANG_HUONG_DAN]) ?>">Đặt
                                hàng</a></li>
                        <li><i class="fa fa-caret-right" aria-hidden="true"></i><a
                                href="<?php echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_THANH_TOAN_VA_MUA_HANG], $id_static_page[TRANG_THANH_TOAN_VA_MUA_HANG]) ?>">Thanh
                                toán và mua hàng</a></li>
                        <!--                    <li><i class="fa fa-caret-right" aria-hidden="true"></i><a-->
                        <!--                            href="-->
                        <?php //echo get_detail_url(ROUTE_STATIC_PAGE, $name_static_page[TRANG_USER_LINK], $id_static_page[TRANG_USER_LINK]) ?><!--">Userlink</a>-->
                        <!--                    </li>-->
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="fot-end">
        <div class="container">
            <div class="main">
                <div class="txtFo">&copy; Công ty Cổ phần thiết bị vật tư công nghiệp Hưng Phát</div>
                <div class="icon">
                    <a href="<?php echo $fb_fanpage; ?>" target="_blank" class="icon-fb"></a>
                    <a href="<?php echo $youtube; ?>" target="_blank" class="icon-youtube"></a>
                    <a href="<?php echo $google_plus; ?>" target="_blank" class="icon-gmail"></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<style>
    .fot-top {
        padding-top: 15px;
    }

    footer ul {
        list-style: none;
        padding: 0;
    }

    footer ul li {
        margin-top: 15px;
        padding-left: 5px;
    }

    footer ul li i {
        margin-right: 5px;
    }

    footer ul li a {
        color: white;
    }
</style>

<a id="back-to-top" href="#" class="btn btn-danger btn-red btn-lg back-to-top" role="button"
   title="Lên đầu trang" data-toggle="tooltip" data-placement="left"><span
        class="glyphicon glyphicon-chevron-up"></span></a>

<script>
    $(document).ready(function () {
        $( '#dl-menu' ).dlmenu();
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
<script src="<?php echo '/assets/frontend/hungphat/js/tuyen.js' ?>"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5811d0bb1e35c727dc149812/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<script type="text/javascript" src="/assets/frontend/hungphat/menu/js/jquery.dlmenu.js"></script>
<!--End of Tawk.to Script-->

<!-- jQuery 1.12.4 -->
<!--<script src="assets/jquery/jquery.min.js"></script>-->
<!-- Bootstrap 3.3.7 -->
<!--<script src="assets/bootstrap/js/bootstrap.min.js"></script>-->
<!-- Slick slider -->
<!--end footer-->
<style>
    #frm_login .form-group {
        overflow: hidden;
    }

    .btn_dang_nhap {
        text-align: center;
    }

    .modal-dialog-dangnhap {
        width: 500px;
        margin-left: auto;
        margin-right: auto;
    }

    .btn_dang_nhap button {
        width: 100%;
        background: #0099ff !important;
        text-transform: uppercase;
        font-weight: bold;
        border: solid 1px #0099ff !important;
    }

    .nhomk {
        text-align: left;
    }

    .quenmk {
        text-align: right;
    }
</style>
<!-- dang nhap -->
<div id="modalLogin" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="bg">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#login">Đăng nhập</a></li>
                        <li><a data-toggle="tab" href="#register">Đăng ký</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="login" class="tab-pane fade in active">
                            <form action="" id="frmLogin" onsubmit="return login()" class="form-horizonta">
                                <div class="callout" id="alert_message_frmLogin"></div>
                                <div class="left col-sm-12">
                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Địa chỉ email*</span>
                                        <div class="col-sm-8">
                                            <input type="text" name="email" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Mật khẩu*</span>
                                        <div class="col-sm-8">
                                            <input type="password" name="password" class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <button class="btn btn-default" type="submit">Đăng nhập</button>
                                            <a href="#quanmatkhau" onclick="hideAllModal()" data-toggle="modal" data-target="#modalQuenMK">Quên mật khẩu?</a>
                                        </div>
                                    </div>
                                    <div  class="form-group">
                                        <span class="control-label col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <div class="fb-login-button btn-fb" onlogin="checkLoginState();" data-max-rowsdiv class="fb-l"large" data-show-faces="false" data-auto-logout-link="false">Đăng nhập bằng facebook</div>
<!--                                            <div onlogin="checkLoginState();" class="btn-fb"><i class="fa fa-facebook-official" aria-hidden="true"></i>Đăng nhập bằng facebook</div>-->
                                        </div>
                                    </div>
<!--                                    <div  class="form-group">-->
<!--                                        <div class="g-signin2" data-onsuccess="onSignIn"></div>-->
<!--                                    </div>-->
                                </div>
                            </form>
                        </div>
                        <div id="register" class="tab-pane fade">
                            <form action="" id="frmRegister" onsubmit="return register()" class="form-horizonta">
                                <div class="callout" id="alert_message_frmRegister"></div>
                                <div class="left col-sm-12">
                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Tên*</span>
                                        <div class="col-sm-8">
                                            <input type="text" name="fullname" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Địa chỉ email*</span>
                                        <div class="col-sm-8">
                                            <input type="text" name="email" class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Mật khẩu*</span>
                                        <div class="col-sm-8">
                                            <input type="password" name="password" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Nhập lại mật khẩu*</span>
                                        <div class="col-sm-8">
                                            <input type="password" name="repassword" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Số điện thoại</span>
                                        <div class="col-sm-8">
                                            <input type="text" name="phone" class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4">Địa chỉ</span>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" type="text" name="address" id="address" rows="4"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <span class="control-label col-sm-4"></span>
                                        <div class="col-sm-8">
                                            <button class="btn btn-default" type="submit">Gửi</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--lay lai mat khau-->
<div class="modal fade" id="modalQuenMK" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Lấy lại mật khẩu</h4>
            </div>

            <div class="modal-body">
                <form id="frmForgetPass" onsubmit="return forget_pass()"">
                    <div class="callout" id="alert_message_frmForgetPass"></div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email đăng nhập" name="email" id="fill-mail-forget-pass">
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group btn_dang_nhap">
                        <button type="submit" class="btn btn-danger">gửi yêu cầu</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
            <div class="modal-footer">
                <p>
                    <a href="#dangnhap" data-toggle="modal" onclick="showTab('login')" data-target="#modalLogin">Đăng nhập</a>&nbsp;&nbsp;&nbsp;<a href="#dangky" onclick="showTab('register')" data-toggle="modal" data-target="#modalLogin">Đăng ký</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--doi mat khau-->
<div class="modal fade" id="modalThayMK" role="dialog">
    <div class="modal-dialog modal-dialog-dangnhap">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title form-modal-title col-lg-12 col-md-12 col-sm-12 col-xs-12">Yêu cầu đổi mật khẩu</h4>
            </div>

            <div class="modal-body">
                <form id="frm_login">
                    <div id="alert-check-mail" class="error_frontend margin_bottom_20 hidden"></div>
                    <div class="show-error-change-pass form-group hidden" id="check-fill">
                        <span class="error hidden col-xs-12" id="no-fill-old-pass"><i>Bạn chưa nhập mật khẩu</i></span>
                        <span class="error hidden col-xs-12" id="no-fill-new-pass"><i>Bạn chưa nhập mật khẩu mới</i></span>
                        <span class="error hidden col-xs-12" id="fill-false-pass"><i>Bạn nhập sai mật khẩu</i></span>
                        <span class="error hidden col-xs-12" id="new-pass-false"><i>Mật khẩu mới phải tối thiểu 6 kí tự trở lên</i></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mật khẩu cũ" name="old_pass" id="fill-old-pass">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mật khẩu mới" name="new_pass" id="fill-new-pass">
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group btn_dang_nhap">
                        <button type="button" class="btn btn-danger" onclick="send_change_pass()">gửi yêu cầu</button>
                    </div> <!-- dong the xac nhan -->
                </form>

            </div><!--  dong the dang nhap -->
        </div>
    </div>
</div>
<!--tuyen: div thong bao-->
<div class="modal" id="modal_msg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Nội dung thông báo</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-callback" data-dismiss="modal">Đóng</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--modal contact-->
<div id="contact" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" id="frm_contact">
                <div class="modal-header">
                    <h4 class="modal-title">Vui lòng liên hệ</h4>
                </div>

                <div class="modal-body">
                    <p>Vui lòng để lại thông tin, chúng tôi sẽ liên lạc ngay với quý khách hàng.</p>

                    <p class="errorhoten hidden error" align="left"></p>

                    <p><input type="text" class="form-control" name="honten" id="hotenContact" placeholder="Tên"/></p>

                    <p class="errorsodienthoai hidden error" align="left"></p>

                    <p><input type="text" class="form-control" name="sodienthoai" id="sodienthoaiContact"
                              placeholder="Số điện thoại"/></p>

                    <p class="erroremail hidden error" align="left"></p>

                    <p><input type="text" class="form-control" name="email" id="emailContact" placeholder="Email"/></p>

                    <p><textarea class="form-control" placeholder="Ghi chú" name="note" id="noteContact"
                                 rows="4"></textarea></p>
                </div>

                <div class="modal-footer">
                    <!--                    <button type="submit" class="btn btn-default">Gửi</button>-->
                    <button type="button" class="btn btn-default" onclick="submitContact();">Gửi</button>

                </div>
            </form>
        </div>
    </div>
</div>
<!--end modal contact-->

<!--modal cart-->
<div id="modalCart" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Giỏ hàng</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="row no-margin">
                    <div class="col-xs-5 no-padding"><a class="btn btn-default pull-left" href="/"> Tiếp tục mua</a>
                    </div>
                    <div class="col-xs-7 no-padding"><a class="btn btn-primary" href="/<?php echo ROUTE_CONTACT_PAGE ?>" > Xác nhận đơn hàng</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end model cart-->

<div class="modal" id="modal_msg_submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Bạn chắc chắn muốn thanh toán đơn hàng này không?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-danger" id="msgOK">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="modal_msg_cart">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Bạn chắc chắn muốn thanh toán đơn hàng này không?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-danger" id="msgOK" onclick="submitThanhToan()">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
    $(document).ready(function(){
        $('#sp-run').on('click',function(){
            if($(this).hasClass('op')) {
                $('#sp-run').addClass('cl').removeClass('op');
                $('.hot-home').css('display','block');
            }else{
                $('#sp-run').addClass('op').removeClass('cl');
                $('.hot-home').css('display','none');
            }
        })
    })
    var afterThongBaoModal = "";
    function showThongBaoOther(type=0,text, action='', onactionModal='') {
        if(type != 0) {
            $("#modal_msg_submit .modal-body p").html(text);
            $("#modal_msg_submit").modal("show");
            if (onactionModal) {
                afterThongBaoModal = onactionModal;
                $(onactionModal).modal('hide');
                afterThongBao();
            }
        }else{
            $("#modal_msg .modal-body p").html(text);
            $("#modal_msg").modal("show");
        }
    }

    function hideAllModal(){
        $('.modal').modal('hide');
    }


    function showDangNhap() {
        $(".modal").modal("hide");
        $("#modalDangNhap").modal("show");
    }
    function showDangKy() {
        $(".modal").modal("hide");
        $("#modalDangKy").modal("show");
    }

    function showQuenMK(){
        $(".modal").modal("hide");
        $("#modalQuenMK").modal("show");
    }
    $(document).ready(function(){
        $('#chk_agree').change(function() {
            if ($(this).is(':checked')) {
                $('#register').prop('disabled',false);
            }else{
                $('#register').prop('disabled',true);
            }
        });
    })

    function showCart(){
        var url = "/<?php echo ROUTE_SHOW_CART; ?>";
        $.post(url)
            .done(function(res){
                var result = JSON.parse(res);
                $('#modalCart .modal-body').html(result.html);
                $('#modalCart').modal('show');
            });
    }

    function removeItemInCart($id) {
        $('.p_item_'+$id).remove();
        var url = "/<?php echo ROUTE_DELTE_ITEM_CART; ?>";
        var data = {id:$id};
        $.post(url,data)
            .done(function(res){
                if(res) {
                    $('.service .icon-msg').html(res);
                    showCart();
                }
            });
    }

    function thayDoiSoLuong($id,$soluong){
        $('.p_item_'+$id).remove();
        var url = "/<?php echo ROUTE_ADD_NUMBER_CART; ?>";
        var data = {id:$id,soluong:$soluong};
        $.post(url,data)
            .done(function(res){
                showCart();
            });
    }

    function showTab(page) {
        hideAllModal();
        $('#modalLogin a[href="#'+page+'"]').tab('show');
    }

    function register(){
        var data = $( "#frmRegister" ).serialize();
        var url = "/kiem-tra-dang-ky.html";
        $.post(url,data)
            .done(function(res){
                var result = JSON.parse(res);
                if(result.error == 1){
                    var index = 0;
                    var top = 0;
                    var p = '';
                    for (var i in result.msg_error) {
                        index++;
                        if (index == 1) {
                            top = i
                        }
                        p = p + result.msg_error[i] + "<br >";
                    }
                    $("#" + top).focus();
                    ShowAlertMessage('alert_message_frmRegister', 'callout-danger', p);
                }else{
                    hideAllModal();
                    showThongBaoOther(0,result.msg);
                    setTimeout(function(){
                        window.location.href=window.location.href;
                    },500);
                }
            });
        return false;
    }


    function login(){
        var data = $( "#frmLogin" ).serialize();
        var url = "/kiem-tra-dang-nhap.html";
        $.post(url,data)
            .done(function(res){
                var result = JSON.parse(res);
                if(result.err == 1){
                    ShowAlertMessage('alert_message_frmLogin', 'callout-danger', result.msg);
                }else{
                    hideAllModal();
                    showThongBaoOther(0,result.msg);
                    setTimeout(function(){
                        window.location.href=window.location.href;
                    },500);
                }
            });
        return false;
    }

    function ShowAlertMessage(id,class_name, message){
        $('#' + id).removeClass();
        $('#' + id).addClass('callout');
        $('#' + id).addClass(class_name);
        $('#' + id).html(message);
        $('#' + id).show();
    }

/*facebook*/
    function statusChangeCallback(response) {
//        console.log('statusChangeCallback');
//        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            // appId      : '322954791389705',// 4.ducthien.vn
            appId      : '1676668252647445',// ducthien.vn
            // appId      : '1791069974471842',
            //cookie     : true,  // enable cookies to allow the server to access
            // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.7' // use graph api version 2.5
        });

    };
    var url_facebook = '<?php echo base_url(FRONTEND_LOGIN_FACEBOOK) ?>';
    function login_facebook(name,id,email){
        $.post(url_facebook,{name:name,id:id,email:email}, function(data){
            var result = JSON.parse(data);
            if (result.err == 0) {
                var message = "";
                if ($('#hdID').val() != '') {
                    message = result.msg;
                }
                $('.modal').modal('hide');
                location.reload();
                window.location.href=window.location.href;
            } else {
                ShowAlertMessage('alert_message_' + frm_id, 'callout-danger', result.msg);
            }
        });
    }


    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
//        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me',{fields:'name,email'},function(response) {
            login_facebook(response.name,response.id,response.email);
        });
    }
</script>
<!--<meta name="google-signin-client_id" content="AIzaSyDZO_uz1-2_T4QHmlQ5JxtWp-QxcTtE7Sk.apps.googleusercontent.com">-->
<!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
<!--<script type="text/javascript">-->
<!--    function onSignIn(googleUser) {-->
<!--        var profile = googleUser.getBasicProfile();-->
<!--        console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.-->
<!--        console.log('Name: ' + profile.getName());-->
<!--        console.log('Image URL: ' + profile.getImageUrl());-->
<!--        console.log('Email: ' + profile.getEmail());-->
<!--    }-->
<!--</script>-->
<!---->
<!--<script type="text/javascript">-->
<!--    function googleTranslateElementInit() {-->
<!--        new google.translate.TranslateElement({pageLanguage: 'vi', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');-->
<!--    }-->
<!--</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->
<script type="text/javascript">
    function forget_pass() {
        var data = $( "#frmForgetPass" ).serialize();
        var url = "/lay-lai-mat-khau.html";
        $.post(url,data)
            .done(function(res){
                var result = JSON.parse(res);
                if(result.err == 1){
                    ShowAlertMessage('alert_message_frmForgetPass', 'callout-danger', result.msg);
                }else{
                    hideAllModal();
                    showThongBaoOther(0,result.msg);
                    setTimeout(function(){
                        window.location.href=window.location.href;
                    },500);
                }
            });
        return false;
    }
</script>
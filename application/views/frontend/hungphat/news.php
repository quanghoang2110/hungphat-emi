<div class="container new">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li class="active"><?php echo $breadcum ?></li>
    </ol>
    <div class="content">
        <!--new left-->
        <div class="left col-sm-8 no-padding">
            <?php if ($banner): ?>
                <div class="banner">
                    <div>
                        <?php echo get_img_tag($banner->img, $banner->name, 'cus_img') ?>
                    </div>
                </div>
            <?php endif; ?>


<!--            <div class="product-hot-home maintain_new">-->
<!--                <h3><a>Cam kết chất lượng bảo hành 4 năm</a></h3>-->
<!--                <p>Hưng Phát tự hào là công ty sản xuất hàng tại Việt Nam. Chúng tôi tự tin về chất lượng đưa ra thị-->
<!--                    trường.</p>-->
<!--            </div>-->
            <div class="news">
                <?php if ($list):?>
                    <?php foreach ($list as $key => $item): ?>
                        <div class="item">
                            <a href="<?php echo get_detail_url($route, $item->name, $item->id) ?>">
                                <div class="col-sm-5">
                                    <div class="news-img">
                                        <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <h3><?php echo $item->name ?></h3>
                                    <p><?php echo $item->desc ?></p>
                                    <div align="right">Xem thêm</div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div>
                <?php echo $paging ?>
            </div>
        </div>
        <!--new left-->
        <!-- sidebar -->
        <div class="right col-sm-4 no-padding">
            <div class="product-hot-home maintain_new">
                <h3><a>Bài viết mới</a></h3>
            </div>
            <div class="news">
                <?php if ($list_new): ?>
                    <?php foreach ($list_new as $key => $item): ?>
                        <div class="item">
                            <a href="<?php echo get_detail_url(str_replace('.html', '', $route), $item->name, $item->id) ?>">
                                <div class="col-sm-5">
                                    <div class="news-img-small">
                                        <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <h3><?php echo $item->name ?></h3>
                                    <!--                                    <p>--><?php //echo $item->desc ?><!--</p>-->
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- sidebar -->
    </div>

    <!--email-->
    <div class="send-mail-home">
        <div class="bd">
            <div class="note">
                <span>Đăng ký <br>nhận bản tin</span>
                <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
            </div>
            <div class="note_sm">
                Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
            </div>
            <div class="email">
                <!--                <form id="frm_send_email">-->
                <!--                    <input type="text" name="email" spellcheck="false" autocomplete="off" value="Email"-->
                <!--                           class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"-->
                <!--                           onfocus="javascript:if(this.value=='Email'){this.value=''}">-->
                <!--                    <a title="Email" href="javascript:void(0)" onclick="document.frm_send_email.submit()">-->
                <!--                        Gửi-->
                <!--                    </a>-->
                <!--                </form>-->
                <form id="frm_send_email">
                    <input type="email" id="email_get_info" name="email" spellcheck="false" autocomplete="off"
                           value="Email"
                           class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                           onfocus="javascript:if(this.value=='Email'){this.value=''}">
                    <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                        Gửi
                    </a>
                </form>
            </div>
        </div>
    </div>
    <!--end email-->
</div>
<div class="container">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li><a href="/<?php echo ROUTE_CONTACT_PAGE; ?>">Liên hệ </a></li>
    </ol>
    <div class="col-sm-6 col-xs-12">
        <div class="col-xs-12 mg-top-3">
            <?php if (isset($company_address2)): ?>
                <h4><img src="/images/hungphat/default/home.png" class="margin_bottom_5"> <span
                        class="">Văn phòng giao dịch</span>
                </h4>
                <p class="margin_bottom_50 margin_left_30"><?php echo $company_address2; ?></p>
            <?php endif; ?>
        </div>
        <div class="col-xs-12">
            <?php if (isset($company_email)): ?>
                <h4><img src="/images/hungphat/default/email.png" class="margin_bottom_5"> <span
                        class="">Email</span></h4>
                <p class="margin_bottom_50 margin_left_30"><?php echo $company_email; ?></p>
            <?php endif; ?>
        </div>
        <div class="col-xs-12">
            <?php if (isset($company_phone)): ?>
                <h4><img src="/images/hungphat/default/phone.png" class="margin_bottom_5"> <span>Số điện thoại</span>
                </h4>
                <p class="margin_bottom_50 margin_left_30"><?php echo $company_phone; ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <h4>GỬI LIÊN HỆ CHO CHÚNG TÔI</h4>

        <p class="error hidden" id="error_name_user"></p>
        <input class="form-control margin_bottom_15" placeholder="Nhập họ tên" value="" type="text" id="name_user">

        <p class="error hidden" id="error_phone_user"></p>
        <input class="form-control margin_bottom_15" placeholder="Nhập số điện thoại" value="" type="text"
               id="phone_user">

        <p class="error hidden" id="error_email_user"></p>
        <input class="form-control margin_bottom_15" placeholder="Nhập email" value="" type="email" id="email_user">

        <p class="error hidden" id="error_content_user"></p>
        <textarea class="form-control margin_bottom_15" placeholder="Nội dung liên hệ" rows="5"
                  id="content_user"></textarea>
        <button class="btn btn-danger" id="submit_contact_page" onclick="submitContactPage();"> GỬI LIÊN HỆ̣</button>
    </div>
    <div class="col-xs-12 mg-top-30">
        <!--        --><?php //if (isset($google_map)): ?>
        <!--            <p>Địa chỉ đy đến công ty . Rất hân hạnh phục vụ quý khách</p>-->
        <!--            --><?php //echo $google_map; ?>
        <!--        --><?php //endif; ?>
        <iframe src="https://www.google.com/maps/d/embed?mid=1RqHasuOuHW4s4naRg0ZNaVokBRw" width="100%"
                height="400px"></iframe>
    </div>
    <!--email-->
    <div class="send-mail-home">
        <div class="container">
            <div class="bd">
                <div class="note">
                    <span>Đăng ký <br>nhận bản tin</span>
                    <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
                </div>
                <div class="note_sm">
                    Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
                </div>
                <div class="email">
                    <form id="frm_send_email">
                        <input type="email" id="email_get_info" name="email" spellcheck="false" autocomplete="off"
                               value="Email"
                               class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                               onfocus="javascript:if(this.value=='Email'){this.value=''}">
                        <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                            Gửi
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end email-->
</div>
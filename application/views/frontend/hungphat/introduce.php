<div class="container new detail-new">

    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li class="active">Giới thiệu</li>
    </ol>

    <div class="content">
        <!--new left-->
        <?php if (isset($info)): ?>
            <?php foreach ($info as $key => $val): ?>
                <div class="left col-sm-8 col-xs-12 no-padding">
                    <?php if($val->value): ?>
                    <div class="product-hot-home maintain_new">
                        <h3><a>GIỚI THIỆU</a></h3>
                    </div>

                    <div class="item">
                        <div class="cnt text-justify">
                            <?php echo $val->value; ?>
                        </div>
                    </div>
                    <?php endif; ?>
<!--                    --><?php //if($val->mission): ?>
<!--                    <div class="product-hot-home maintain_new">-->
<!--                        <h3><a>SỨ MỆNH</a></h3>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="cnt text-justify">-->
<!--                            --><?php //echo $val->mission; ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                    --><?php //endif; ?>
<!--                    --><?php //if($val->prize): ?>
<!--                    <div class="product-hot-home maintain_new">-->
<!--                        <h3><a>GIẢI THƯỞNG</a></h3>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="cnt text-justify">-->
<!--                            --><?php //echo $val->prize; ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                    --><?php //endif; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--new left-->
        <!-- sidebar -->
        <div class="right col-sm-4 col-xs-12 no-padding">
            <div class="product-hot-home maintain_new">
                <h3><a>Bài viết mới</a></h3>
            </div>
            <div class="news">
                <?php if ($list): ?>
                <?php foreach ($list as $key => $item): ?>
                <div class="item">
                    <a href="<?php echo get_detail_url(ROUTE_NEWS, $item->name, $item->id) ?>">
                        <div class="col-sm-5 no-padding"
                        ">
                        <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                    </a>
                </div>
                <div class="col-sm-7 no-padding"
                ">
                <h3><?php echo $item->name ?></h3>

                <p class="hidden-md hidden-sm hidden-lg"><?php echo $item->desc ?></p>
            </div>
        </div>
    <?php endforeach; ?>
    <?php endif; ?>
        <div class="pull-right read-continue"><a href="#">Xem thêm>></a></div>
    </div>
</div>
<!-- sidebar -->
<!--</div>-->

<!--email-->
<div class="send-mail-home">
    <div class="bd">
        <div class="note">
            <span>Đăng ký <br>nhận bản tin</span>
            <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
        </div>
        <div class="note_sm">
            Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
        </div>
        <div class="email">
            <form id="frm_send_email">
                <input type="email" id="email_get_info" name="email" spellcheck="false" autocomplete="off" value="Email"
                       class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                       onfocus="javascript:if(this.value=='Email'){this.value=''}">
                <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                    Gửi
                </a>
            </form>
        </div>
    </div>
</div>
<!--end email-->
<!--</div>-->
<div class="container">
    <!--xac nhan thanh toan-->
    <form action="/dat-hang-thanh-cong.html" method="post" onsubmit="return submitThanhToan()" id="checkCart">
        <div class="row row-col-gap box box-x no-margin" daa-gutter="60">
            <div class="col-md-8 border-right">
                <div class="tt_info">
                    <div>
                        <h3 class="widget-title">Thông tin người nhận</h3>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Họ và tên</label>
                                <input class="form-control" type="text" name="fullname" value="" placeholder="Họ và tên người nhận" id="fullname">
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Số điện thoại</label>
                                <input class="form-control" type="tel" name="phone" value="" id="phone" placeholder="Số điện thoại người nhận">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ giao/nhận hàng</label>
                            <input class="form-control" type="text" name="address" id="address" value="" placeholder="Số nhà, tên đường">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Ghi chú</label>
                        <textarea name="note" class="form-control" rows="4"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3 class="widget-title">Đơn hàng</h3>
                <div class="box">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Sản phẩm <span class="sp_x">1</span></th>
                            <th>SL</th>
                            <th>Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total = 0; foreach($list as $key=>$item): ?>
                        <?php $total =  ($item->price_2) ? $total + $sl[$key]*$item->price_2 : $total + $sl[$key]*$item->price_1 ?>
                        <tr>
                            <td><?php echo $item->name ?></td>
                            <td><?php echo $sl[$key] ?></td>
                            <td>
                                <p class="product-price text-price"><?php echo ($sl[$key]*$item->price_2) ? number_format($sl[$key]*$item->price_2,0,'.','.') : number_format($sl[$key]*$item->price_1,0,'.','.') ?><span>đ</span></p>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="gap-small"></div>
                <div class="box">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Thanh toán</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th colspan="2" class="product-price"><p>Tổng tiền cần thanh toán</p></th>
                            <th>
                                <p class="product-price text-price" id="total_all_price_text"><?php echo number_format($total,0,'.','.') ?><span>đ</span></p>
                            </th>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <div class="col-xs-6">
                                    <a href="#gio-hang" onclick="showCart()" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Giỏ hàng&nbsp;</a>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" id="btnThanhToan" class="btn btn-danger" data-toggle="modal" data-target="#modal_msg_cart"><i class="fa fa-credit-card"></i>Thanh
                                        toán
                                    </button>
                                </div>

                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    function submitThanhToan(){
        var data = $( "#checkCart" ).serialize();
        var url = "/dat-hang-thanh-cong.html";
        $.post(url,data)
            .done(function(res){
                var result = JSON.parse(res);
                hideAllModal();
                if(result.error == 1){
                    showThongBaoOther(0,result.msg);
                }else{
                    showThongBaoOther(0,result.msg);
                    setTimeout(function(){
                        window.location.href='/';
                    },500);
                }
            });
        return false;
    }
</script>
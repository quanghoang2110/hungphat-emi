<div class="container new">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li class="active"><?php echo $info->name ?></li>
    </ol>
    <div class="content product_content">
        <!--new left-->
        <div class="left col-sm-12 no-padding">
            <div class="product-hot-home maintain_new">
                <h3><a><?php echo $info->name ?></a></h3>
            </div>
            <div class="row">
                <?php if ($list): ?>
                    <?php foreach ($list as $key => $item): ?>
                        <div class="col-xs-2 col-sm-2 col-md-3">
                            <div class="item">
                                <?php $this->load->view($theme_template . '/product_item', array('product' => $item)); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="phantrang">
                <?php echo $paging ?>
            </div>
        </div>
        <!--new left-->
        <!-- sidebar -->
<!--        <div class="right col-sm-4 no-padding">-->
<!--            <div class="product-hot-home maintain_new">-->
<!--                <h3><a>Danh mục</a></h3>-->
<!--            </div>-->
<!--            <div class="tree well">-->
<!--                <ul>-->
<!--                    --><?php //foreach ($product_type as $index => $item): ?>
<!--                        <li>-->
<!--                            <span class="--><?php //echo $item->id == $info->id ? 'active' : '' ?><!--"><i-->
<!--                                    class="fa fa-minus"></i> --><?php //echo $item->name ?><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a-->
<!--                                    href="--><?php //echo get_detail_url(ROUTE_PRODUCT, $item->name, $item->id) ?><!--"><i-->
<!--                                        class="fa fa-eye"></i> </a></span>-->
<!--                            --><?php //if (key_exists('sub', $item)): ?>
<!--                                <ul>-->
<!--                                    --><?php //foreach ($item->sub as $item1): ?>
<!--                                        <li>-->
<!--                                            <span class="--><?php //echo $item1->id == $info->id ? 'active' : '' ?><!--"><i-->
<!--                                                    class="fa fa-minus"></i> --><?php //echo $item1->name ?><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a-->
<!--                                                    href="--><?php //echo get_detail_url(ROUTE_PRODUCT, $item1->name, $item1->id) ?><!--"><i-->
<!--                                                        class="fa fa-eye"></i></a></span>-->
<!--                                            --><?php //if (key_exists('sub', $item1)): ?>
<!--                                                <ul>-->
<!--                                                    --><?php //foreach ($item1->sub as $item2): ?>
<!--                                                        <li class="--><?php //echo $item2->id == $info->id ? 'active' : '' ?><!--">-->
<!--                                                            <a href="--><?php //echo get_detail_url(ROUTE_PRODUCT, $item2->name, $item2->id) ?><!--">--><?php //echo $item2->name ?><!--</a>-->
<!--                                                        </li>-->
<!--                                                    --><?php //endforeach; ?>
<!--                                                </ul>-->
<!--                                            --><?php //endif; ?>
<!--                                        </li>-->
<!--                                    --><?php //endforeach; ?>
<!--                                </ul>-->
<!--                            --><?php //endif; ?>
<!--                        </li>-->
<!--                    --><?php //endforeach; ?>
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
        <!-- sidebar -->
    </div>

    <!--email-->
    <div class="send-mail-home">
        <div class="bd">
            <div class="note">
                <span>Đăng ký <br>nhận bản tin</span>
                <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
            </div>
            <div class="note_sm">
                Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
            </div>
            <div class="email">
                <form id="frm_send_email">
                    <input type="text" name="email" spellcheck="false" autocomplete="off" value="Email"
                           class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                           onfocus="javascript:if(this.value=='Email'){this.value=''}">
                    <a title="Email" href="javascript:void(0)" onclick="document.frm_send_email.submit()">
                        Gửi
                    </a>
                </form>
            </div>
        </div>
    </div>
    <!--end email-->
</div>
<script>
    $(function () {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-plus').removeClass('fa-minus');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa fa-minus').removeClass('fa-plus');
            }
            e.stopPropagation();
        });
    });
</script>
<style>

    .product_itemx p {
        padding: 10px 10px;
        overflow: hidden;
        text-overflow: ellipsis;
        height: 55px;
    }

    .product_itemx a:last-child {
        color: #02ade3;
        padding: 5px 10px 15px;
        text-align: center;
        width: 100%;
        display: inline-block;
    }

    .item {
        box-shadow: 0 5px 10px 0 rgba(4, 4, 4, .2), 0 1px 5px 0 rgba(9, 9, 9, .19);
        margin-bottom: 15px;
    }

    .tree {
        min-height: 20px;
        padding: 19px;
        margin-bottom: 20px;
        background-color: #fbfbfb;
        border: 1px solid #999;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05)
    }

    .tree li {
        list-style-type: none;
        margin: 0;
        padding: 10px 5px 0 5px;
        position: relative
    }

    .tree li::before, .tree li::after {
        content: '';
        left: -20px;
        position: absolute;
        right: auto
    }

    .tree li::before {
        border-left: 1px solid #999;
        bottom: 50px;
        height: 100%;
        top: 0;
        width: 1px
    }

    .tree li::after {
        border-top: 1px solid #999;
        height: 20px;
        top: 25px;
        width: 25px
    }

    .tree li span {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 5px 8px;
        text-decoration: none
    }

    .tree li.parent_li > span {
        cursor: pointer
    }

    .tree > ul > li::before, .tree > ul > li::after {
        border: 0
    }

    .tree li:last-child::before {
        height: 30px
    }

    .tree li.parent_li > span:hover, .tree li.parent_li > span:hover + ul li span {
        background: #eee;
        border: 1px solid #94a0b4;
        color: #000
    }

    .tree.well {
        padding: 0;
        margin: 0;
        border: 0;
        box-shadow: none;
        margin-top: -10px;
    }

    .well > ul {
        padding: 0;
        margin: 0;
    }

    .tree li span.active {
        color: red;
        border-color: red;
    }

    .tree li.parent_li ul li.active a {
        color: red;
    }
</style>
<div class="container newcruitment">
    <ol class="breadcrumb no-padding">
        <li><a href="/">Trang chủ</a></li>
        <li class="active"><?php echo $breadcum ?></li>
    </ol>
    <div class="content">
        <?php if ($list): ?>
        <!--new left-->
        <div class="col-offset-sm-2 left col-sm-10 no-padding">
            <div class="news">
                <?php foreach ($list as $key => $item): ?>
                    <div class="item">
                        <a href="<?php echo get_detail_url($route, $item->name, $item->id) ?>">
                            <div class="col-sm-7 no-padding">
                                <h3><?php echo $item->name ?></h3>
                                <p><?php echo $item->desc ?></p>
                            </div>
                            <div class="col-sm-5">
                                <div class="news-img">
                                    <?php echo get_img_tag($item->img, $item->name, 'cus_img') ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div>
                <?php echo $paging ?>
            </div>

            <div>
                <div class="fb-comments" style="width:100%"
                     data-href="<?php echo base_url(get_detail_url($route, $item->name, $item->id)) ?>"
                     data-numposts="5"></div>
            </div>
        </div>
        <?php endif; ?>
        <!--new left-->
    </div>

    <!--email-->
    <div class="send-mail-home">
        <div class="bd">
            <div class="note">
                <span>Đăng ký <br>nhận bản tin</span>
                <span>Cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt</span>
            </div>
            <div class="note_sm">
                Đăng ký nhận bản tin để cập nhật thông tin khuyến mại nhanh nhất hưởng quyền lợi giảm giá riêng biệt
            </div>
            <div class="email">
                <form id="frm_send_email">
                    <input type="email" id="email_get_info" name="email" spellcheck="false" autocomplete="off"
                           value="Email"
                           class="inputText" onblur="javascript:if(this.value==''){this.value='Email'}"
                           onfocus="javascript:if(this.value=='Email'){this.value=''}">
                    <a title="Email" href="javascript:void(0)" onclick="frm_send_email();">
                        Gửi
                    </a>
                </form>
            </div>
        </div>
    </div>
    <!--end email-->
</div>
<script>
    /**
     * Created by Max Mun on 1/8/2016.
     */

    var onCheckoutStep1 = false;

    function ajaxLoading() {
        $('#ajaxLoading').show();
    }
    function disableLoading() {
        $("#ajaxLoading").fadeOut("slow");
    }

    function changeNumberItem(new_num, price, id) {
        var old_price = $('#old_num_item_' + id).val() * price;
        var new_price = new_num * price;
        $('#price_hidden_' + id).val(new_price);
        $('#new_price_' + id).html(new_price.formatMoney(0, ',', '.') + " VNĐ");

        var new_total = parseInt($('#total_price_hidden').val()) + new_price - old_price;
        console.log(old_price);
        $('#total_price_hidden').val(new_total);
        $('#total_price_show').html(new_total.formatMoney(0, ',', '.') + ' VNĐ');

        $('#old_num_item_' + id).val(new_num);
    }

//    function themVaoGioHang(id, soluong) {
//        if (id && soluong > 0) {
//            ajaxLoading();
//            $.post("/<?php //echo ROUTE_PUBLIC_CART_ADDMORE ?>//", {product: id, number: soluong}).done(function (data) {
//                refreshCart();
//            });
//        }
//    }

    function showRemoveItemInCartOnStep1(id) {
        onCheckoutStep1 = true;
        showRemoveItemInCart(id);
    }

    function showRemoveItemInCart(id) {
        productId = id;
        $('#modal_confirm_msg .modal-body').html('<p>Bạn muốn xóa sản phẩm này khỏi giỏ hàng?</p>');
        $('#modal_confirm_msg .btn-primary').attr('onclick', 'removeItemInCart()');
        $('#modal_confirm_msg').modal('show');
//        $("#modalProduct").modal("show");
    }

    function removeQuickCart() {
        console.log(productId);
        $('#li-p-id-' + productId).remove();
    }

//    function removeCheckoutStep1() {
//        console.log(onCheckoutStep1);
//        if (onCheckoutStep1) {
//            var total_price = parseInt($('#total_price_hidden').val()) - parseInt($('#price_hidden_' + productId).val());
//            $('#p-id-' + productId).remove();
//            $('#total_price_hidden').val(total_price);
//            $('#total_price_show').html(total_price.formatMoney(0, ',', '.') + " VNĐ");
//            if (total_price <= 0) {
//                $('.for-thanhtoan .btn-success').remove();
//                var qc_action = '<a class="btn-primary btn_cart">Giỏ hàng</a>' +
//                    '<a class="btn-check-out text-muted">Thanh toán</a>';
//            } else {
//                var qc_action = '<a href="/<?php //echo ROUTE_PUBLIC_CHECKOUT_1 ?>//" class="btn-primary btn_cart">Giỏ hàng</a>' +
//                    '<a href="/<?php //echo ROUTE_PUBLIC_CHECKOUT_2 ?>// class="btn-check-out">Thanh toán</a>';
//            }
//            $('#quick-cart .cart-buttons').html(qc_action);
//            console.log(productId);
//        }
//    }

    function removeItemInCart() {
        removeQuickCart();
        removeCheckoutStep1();
        ajaxLoading();
        $.post('/xoa-san-pham-khoi-gio-hang', {product: productId}).done(function (data) {
            refreshCart();
        });
    }

    function likeProduct(id) {

    }

    function thanhtoan() {
//        ajaxLoading();
        $.post("/check-login").done(function (data) {
            if (data == 1) {
                $('#checkout-step-1').submit();
            } else {
                $('#xx-modal').html(data);
                $('#myModal').modal('show');
            }
        }).done(function () {
            disableLoading();
        });
    }

    function refreshCart() {
        ajaxLoading();
        $.post('/<?php echo PUBLIC_ROUTE_AJAX_REFRESH_CART ?>').done(function (data) {
            $('#quick-cart').html(data);
//            $('#quick-cart-ontop').html(data);
            disableLoading();
            initMagnificPopup();
        });
    }

    $(document).ready(function(){
        $(".cart-block").animate({scrollTop: 10000});
    });

//    function nhanThongBao(val) {
//        if (val) {
//            ajaxLoading();
//            $.post('/<?php //echo ROUTE_PUBLIC_NHANTHONGBAO ?>//', {email: val}).done(function (data) {
//                disableLoading();
//                var obj = $.parseJSON(data);
//                showThongBao(obj.msg);
//                if (obj.status == 1) {
//                    $('#in_email_thongbao').val('');
//                } else
//                    $('#in_email_thongbao').focus();
//            });
//        } else {
//            showThongBao('Vui lòng nhập Email đăng ký!');
//            $('#in_email_thongbao').focus();
//        }
//    }

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
</script>

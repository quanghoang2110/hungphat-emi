<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title><?php echo ($_seo_title_detail) ? $_seo_title_detail : $seo_title ?></title>
    <meta name="description"
          content="<?php echo ($_seo_description_detail) ? $_seo_description_detail : $seo_description ?>"/>
    <meta name="keywords"
          content="<?php echo ($_seo_keyword_detail) ? $_seo_keyword_detail : $seo_keyword ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--    facebook tag-->
    <meta property="fb:app_id" content="<?php echo $fb_app_id ?>"/>
    <meta property="og:url"
          content="<?php echo ($_seo_url_detail) ? $_seo_url_detail : $seo_url ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo ($_seo_title_detail) ? $_seo_title_detail : $seo_title ?>"/>
    <meta property="og:description" content="<?php echo ($_seo_description_detail) ? $_seo_description_detail : $seo_description ?>"/>
    <meta property="og:image"
          content="<?php if($_seo_image_detail) echo $_seo_image_detail; elseif ($seo_image) echo $seo_image; else echo $logo; ?>"/>

    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?php echo ($_seo_title_detail) ? $_seo_title_detail : $seo_title ?>"/>
	
	<!-- google console -->
	<meta name="google-site-verification" content="jIDb5g56SrkOmFlRnla7b2Gg-5fFhHABxK2HyI5ih5I" />

    <link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon">

    <?php if ($this->config->item('material') == true): ?>
        <link rel="stylesheet" href="/assets/base/lib/mdl/material.min.css">
        <script src="/assets/base/lib/mdl/material.min.js"></script>
    <?php endif; ?>

    <?php if ($this->config->item('mdb_lib') == true): ?>

        <!-- Material Design Bootstrap -->
        <link href="/assets/base/lib/mdb-pro/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/base/lib/mdb-pro/css/mdb.min.css" rel="stylesheet">
    <?php else: ?>
        <link href="/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <?php endif; ?>

    <!--    load fontroboto-->
    <?php if ($this->config->item('font.roboto') == true): ?>
        <link rel="stylesheet" href="/assets/base/lib/roboto/font.roboto.css">
    <?php endif; ?>
    <!--    load fontawesome-->
    <?php if ($this->config->item('font.awesome')): ?>
        <link href="/assets/base/lib/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <?php endif ?>
    <?php if ($this->config->item('font.material') == true): ?>
        <link rel="stylesheet" href="/assets/base/lib/mdl/material.font.css"/>
    <?php endif; ?>

    <!-- load head tag -->
    <?php
    if (file_exists(APPPATH . "views/{$theme_template}/head.php"))
        $this->load->view("{$theme_template}/head");

    if (file_exists(APPPATH . "views/{$theme_template}/theme_style_min.php"))
        $this->load->view("{$theme_template}/theme_style_min");
    else if (file_exists(APPPATH . "views/{$theme_template}/theme_style.php"))
        $this->load->view("{$theme_template}/theme_style");
    ?>

    <?php
        if (file_exists(APPPATH . "views/frontend/base/google_analytics.php"))
            include_once ('google_analytics.php');
    ?>

    <!--load jquery-->
    <?php if ($this->config->item('jquery') == true): ?>
        <script src="/assets/base/lib/jquery/jquery-3.1.1.min.js"></script>
    <?php endif; ?>

</head>

<body>

<!--load content-->
<?php
//echo $theme_template;
$go_base_content = $theme_template . 'content';
if (file_exists(APPPATH . "views/{$go_base_content}.php"))
    $this->load->view($go_base_content) ?>

<!--load footer-->
<?php
$go_base_footer = $theme_template . 'footer';
if (file_exists(APPPATH . "views/{$go_base_footer}.php"))
    $this->load->view($go_base_footer);
?>

<!--load mdb js-->
<?php if ($this->config->item('mdb_lib') == true): ?>
    <script src="/assets/base/lib/mdb-pro/js/jquery-2.2.3.min.js"></script>
    <script src="/assets/base/lib/mdb-pro/js/tether.min.js" type="text/javascript"></script>
    <script src="/assets/base/lib/mdb-pro/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/base/lib/mdb-pro/js/mdb.min.js" type="text/javascript"></script>
    <!--    <script src="/assets/base/lib/mdb-pro/js/compiled.min.js?ver=3.1.0" type="text/javascript"></script>-->
<?php else: ?>
    <script src="/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<?php endif; ?>

<?php //$this->load->view('frontend/base/base_js'); ?>

<!--base tmp-->
<?php //$this->load->view('frontend/base/modal_tmp'); ?>

<?php
if ($this->config->item('site_type') == 'ecomerce') {
    $this->load->view('frontend/base/ecomerce');
}
?>

<!--script-->
<?php
if (file_exists(APPPATH . "views/{$theme_template}/theme_js_min.php"))
    $this->load->view("{$theme_template}/theme_js_min");
else
    if (file_exists(APPPATH . "views/{$theme_template}/theme_js.php"))
        $this->load->view("{$theme_template}/theme_js");
//if (file_exists(APPPATH . "views/{$page_asset}_js.php"))
//    $this->load->view($page_asset . '_js');

?>
<!--<div id="ajaxLoading">-->
<!--    --><?php //$this->load->view('frontend/base/loading');?>
<!--</div>-->

</body>

</html>
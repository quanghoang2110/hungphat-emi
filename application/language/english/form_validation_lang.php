<?php

$lang['required']			= "Vui lòng nhập vào %s.";
$lang['is_count']			= "%s phải là kiểu số.";
$lang['image']			    = "Vui lòng chọn ảnh.";
$lang['barcode']			= "Mã vạch đã có người sử dụng";
$lang['dropdownlist']		= "Vui lòng chọn %s.";
$lang['isset']				= "The %s field must have a value.";
$lang['form_validation_valid_email']		= "%s nhập vào không hợp lệ.";
$lang['form_validation_numeric']		= '%s phải là kiểu số.';
$lang['form_validation_max_length']		= '%s có tối đa %d ký tự.';
$lang['form_validation_min_length']		= '%s có tối thiểu %d ký tự.';
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['form_validation_is_natural']		= '{field} phải là kiểu số.';
$lang['form_validation_matches']		= '{field} không khớp với {param}.';
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "Vui lòng nhập %s có ít nhất %s ký tự.";
$lang['max_length']			= "Vui lòng nhập %s không lớn hơn %s ký tự.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "%s không thể chứa số hoặc ký tự đặc biệt.";
$lang['alpha_width_space']	= "%s không thể chứa số hoặc ký tự đặc biệt.";
$lang['alpha_numeric']		= "%s không thể chứa ký tự đặc biệt.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "%s đã tồn tại.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "%s không khớp với %s.";
$lang['form_validation_is_unique'] 	= "%s đã tổn tại";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "%s phải nhỏ hơn %s.";
$lang['greater_than']		= "%s phải lớn hơn %s.";

$lang['is_exists']              = "%s đã được tổn tại.";
$lang['is_exists_phone']		= "%s đã được sử dụng.";
$lang['is_exists_email']		= "%s đã được sử dụng.";
$lang['email_too_long']		    = "%s không hợp lệ.";
$lang['too_long']	            = "%s quá dài.";
$lang['too_short']              = "%s quá ngắn.";
$lang['check_fullname_number']  = "%s không được là số.";
$lang['check_characters_special']  = "%s không được chứa ký tự đặc biệt.";
$lang['is_exists_account']		= "%s đã được sử dụng.";
$lang['check_current_password']		= "% không hợp lệ.";
$lang['check_phone_number']		= "%s không hợp lệ.";
$lang['check_phone_number_f']		= "%s đã được sử dụng.";
$lang['check_forgot_password']		= "%s Không trùng khớp với mật mới.";
$lang['check_forgot_password_code']		= "Mã xác nhận không chính xác";
$lang['check_forgot_password_day']		= "Thời gian đổi mật khẩu đã hết. Vui lòng sử dụng lại tính năng quên mật khẩu.";
$lang['check_not_empty']		= "%s không được để trống.";
$lang['check_valid_token'] = 'Phiên làm việc đã hết hạn! Xin vui lòng đăng nhập lại...';
$lang['date_from_to'] = 'Ngày kết thúc phải lớn hơn ngày bắt đầu';
$lang['date_to_not_past'] = 'Ngày kết thúc không được ở quá khứ';
$lang['date_between'] = '%s phải trong khoảng ngày bắt đầu và ngày kết thúc';
$lang['price_promotion'] = 'Giá khuyến mại không được lớn hơn giá gốc';
$lang['is_exists_name']		= "%s đã được sử dụng.";
$lang['expried_days']		= "Vui lòng nhập vào Thời hạn.";
$lang['end_point']		= "Điểm cận trên phải lớn hơn hoặc bằng điểm cận dưới";
$lang['expired_date_from_to'] = 'Ngày hết hạn phải lớn hơn ngày bắt đầu';
$lang['expired_date_to_not_past'] = 'Ngày hết hạn thúc không được ở quá khứ';
$lang['is_exists_card_code'] = 'Mã thẻ đã tồn tại';

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
$lang['update_success'] = "Cập nhật thành công";
$lang['insert_success'] = "Thêm mới thành công";

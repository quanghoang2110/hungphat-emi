<?php
class Model_staticpage extends Go_Model{
    function __construct()
    {
        parent::__construct();
        $this->table_name = 'tbl_staticpage';
    }

    function get_search_limit($where_arr, $keyword, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*", $row_search="name")
    {
        $this->db->select('tbl_staticpage.*, tbl_menu.name as menu_name');
        $this->db->join('tbl_menu', 'tbl_staticpage.menu_id = tbl_menu.id','left');
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if($row_search) {
            if ($keyword) {
                $this->db->like($row_search, $keyword);
            }
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if($limit) {
            $this->db->limit($limit, $offset);
        }
        return $this->db->get($this->table_name)->result();
    }

    /**
     * get list static page by menu slug
     * @param $slug
     * @param $page
     * @param int $limit
     * @return mixed
     */
    function get_static_pages_by_menu($slug, $page, $limit=ADMIN_ROW_LIMIT){
        $offset = ($page - 1) * $limit;
        $this->db->select("a.*");
        $this->db->from("tbl_staticpage a");
        $this->db->join('tbl_menu b', 'a.menu_id=b.id', 'left');
        $this->db->where('b.slug', $slug);
        $this->db->order_by("position", "desc");
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    function get_top_one($where_arr, $order_arr = "", $select = "*")
    {
        $this->db->select($select);
        $this->db->join('tbl_menu', 'tbl_staticpage.menu_id = tbl_menu.id','left');
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit(1);
        return $this->db->get($this->table_name)->row();
    }

    //tuyen: lay noi dung static page
    function get_content_static_page($id){
        $this->db->select('*');
        $this->db->from('tbl_staticpage');
        $this->db->where('id',$id);
        return $this->db->get()->result();
    }

}
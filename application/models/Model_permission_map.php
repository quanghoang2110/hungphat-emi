<?php

/**
 * Class BaseModel
 */
class Model_permission_map extends Go_Model
{
    function get_permission($group_id)
    {
//        $this->db->select('a.*');
//        $this->db->from('permissions a');
//        $this->db->join('permission_map b', 'a.id=b.permission_id', 'left');
//        $this->db->where('b.group_id', $group_id);
//        $this->db->order_by('category', 'asc');
//        $this->db->order_by('id', 'asc');
//        return $this->db->get()->result();
    }

    function get_permission_read($group_id){
        $this->db->where('a.key', 'read');
        return $this->get_permission($group_id);
    }

    function get_group_permission($group_id)
    {
        $query = "(SELECT IF(COUNT(*)=1, 'checked', '') FROM tbl_permission_map b WHERE b.group_id='{$group_id}' AND b.permission_id=a.id) checked";
        $this->db->select("a.*");
        $this->db->select($query, false);
        $this->db->from('permissions a');
        $this->db->order_by('category', 'asc');
        $this->db->order_by('id', 'asc');
        return $this->db->get()->result();
    }
}
<?php

class Model_gallery_content extends Go_Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = 'tbl_gallery_content';
    }

    function get_list_content($keyword, $page = 1, $limit = 10)
    {
        $offset = ($page - 1) * $limit;
        $this->db->select('a.*, b.name as gallery_name');
        $this->db->from('tbl_gallery_content a');
        $this->db->join('tbl_gallery b', 'a.gallery_id=b.id', 'left');
        if ($keyword) {
            $this->db->like('a.title', $keyword);
        }
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

}
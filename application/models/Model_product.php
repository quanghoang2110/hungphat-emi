<?php

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 8/12/2016
 * Time: 10:09 AM
 */
class Model_product extends Go_Model
{
    function get_limit($where_arr, $order_arr = "", $page = 1, $limit = 10, $select_other = "", $select_product = "")
    {
        //page >= 1
        $page = $page > 0 ? $page : 1;

        $select_product = $select_product == "" ? "a.id, a.name, a.ship, a.star, a.is_new, a.is_discount, a.is_best_seller, a.is_popular, a.price_1, a.price_2, a.is_empty" : "*";
        $this->db->select($select_product);
        if ($select_other)
            $this->db->select($select_other);
        $this->db->select("b.slug as product_type_slug");
        $offset = ($page - 1) * $limit;
//        print_r($where_arr);
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $this->db->from("product as a");
        $this->db->join("product_type b", "a.product_type=b.id", "left");

        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    function get_in_parent($cat_id, $limit)
    {
        $this->db->select('a.*, b.slug as product_type_slug, (select img from product_img where product_id=a.id limit 1) img');
        $this->db->from('product a');
        $this->db->join("product_type b", "a.product_type=b.id", "left");
        $this->db->where('a.valid', 1);
        $this->db->where("b.parent_id", $cat_id);
        $this->db->order_by("position", "asc");
        $this->db->order_by("update_time", "desc");
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, 0);
        return $this->db->get()->result();
    }

    function _get_search_limit($where_arr, $keyword, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*", $row_search = "name")
    {
//        $this->db->select('product.*, product_img.img as img_avatar, product_publisher.name as name_publisher, product_type.name as name_type');
        $this->db->select('product.*, product_publisher.name as name_publisher, product_type.name as name_type');
        $this->db->from('product');
        $this->db->join('product_publisher', 'product_publisher.id = product.product_publisher', 'left');
        $this->db->join('product_type', 'product_type.id = product.product_type', 'left');
//        $this->db->join('product_img', 'product_img.product_id = product.id and product_img.order = 1', 'left');
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($row_search) {
            if ($keyword) {
                $this->db->like($row_search, $keyword);
            }
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        return $this->db->get()->result();
    }

    function _count_search($where_arr = "", $keyword = "", $row_search = "name")
    {
        if ($where_arr)
            $this->db->where($where_arr);
        if ($keyword)
            $this->db->like($row_search, $keyword);
        $this->db->from('product');
        return $this->db->count_all_results();
    }

    function get_in_category($cate_id, $page, $limit = 12)
    {
        //page >= 1
        $page = $page > 0 ? $page : 1;
        $offset = ($page - 1) * $limit;

        $this->db->select('a.*');
        $this->db->where("a.valid=1 and (a.product_type={$cate_id} or b.parent_id={$cate_id} or c.parent_id={$cate_id})");

        $this->db->from("product as a");
        $this->db->join("product_type b", "a.product_type=b.id", "left");
        $this->db->join("product_type c", "b.parent_id=c.id", "left");

        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    function count_in_category($cate_id)
    {
        $this->db->select('a.*');

        $this->db->where("a.valid=1 and (a.product_type={$cate_id} or b.parent_id={$cate_id} or c.parent_id={$cate_id})");

        $this->db->from("product as a");
        $this->db->join("product_type b", "a.product_type=b.id", "left");
        $this->db->join("product_type c", "b.parent_id=c.id", "left");

        return $this->db->get()->num_rows();
    }
}
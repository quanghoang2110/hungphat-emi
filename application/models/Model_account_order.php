<?php

/**
 * Created by PhpStorm.
 * User: tuyen
 * Date: 8/21/2016
 * Time: 19:30
 */
class Model_account_order extends Go_Model
{
    function get_limit($where_arr, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*")
    {
        $this->db->select('account_order.*, account.username');
        $this->db->from('account_order');
        $this->db->join('account', 'account.id = account_order.account_id', 'left');
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }

        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }

        if ($this->_search_keyword) {
            $this->db->like($this->_search_row, $this->_search_keyword);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    function get_order($id)
    {
        $this->db->select("product.id, product.name, product.pice, account_order_product.number, account_order_product.price_per_one");
        $this->db->from('account_order');
        $this->db->join('account_order_product', 'account_order.id = account_order_product.order_info_id', 'left');
        $this->db->join('product', 'account_order_product.product_id = product.id', 'left');
        if ($id != -1)
            $this->db->where('account_order_product.order_info_id', $id);
        return $this->db->get()->result();
    }

    function get_data_account($where_arr = "", $select = "*")
    {
        $this->db->select('account_order.*, account.username');
        if ($where_arr)
            $this->db->where($where_arr);
        $this->db->from('account_order');
        $this->db->join('account', 'account.id = account_order.account_id', 'left');
        return $this->db->get()->row();
    }
}
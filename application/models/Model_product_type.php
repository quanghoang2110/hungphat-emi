<?php

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 8/12/2016
 * Time: 10:09 AM
 */
class Model_product_type extends Go_Model
{
    function get_info($where_arr)
    {
        $this->db->select('a.*, b.name as parent, b.slug as parent_slug');
        $this->db->from('product_type a');
        $this->db->join('product_type b', 'a.parent_id=b.id', 'left');
        $this->db->where($where_arr);
        return $this->db->get()->first_row();
    }
}
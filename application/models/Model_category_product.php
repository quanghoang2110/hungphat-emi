<?php

class Model_category_product extends Go_Model
{
    function __construct()
    {
        parent::__construct();
//        $this->set_table_name('tbl_provider');
        $this->set_table_name('');
    }

    function _get_search_limit($where_arr, $keyword, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*", $row_search = "name")
    {
        $this->db->select($select);
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($row_search) {
            if ($keyword) {
                $this->db->like($row_search, $keyword);
            }
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        return $this->db->get('tbl_provider')->result();
    }

    function _count_search($where_arr = "", $keyword = "", $row_search = "name")
    {
        if ($where_arr)
            $this->db->where($where_arr);
        if ($keyword)
            $this->db->like($row_search, $keyword);
        $this->db->from('tbl_provider');
        return $this->db->count_all_results();
    }
}
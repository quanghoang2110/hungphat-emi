<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @copyright Go Solution JSC
 * Created by PhpStorm.
 * User: Hoang Quang
 * Date: 8/7/2015
 * Time: 10:35 AM
 */
class Go_Model extends CI_Model
{
    public $table_name;
    public $_query = "", $_search_keyword = "", $_search_row = "name";

    function reset_all()
    {
        $this->_query = "";
        $this->_search_keyword = "";
        $this->_search_row = "name";
    }

    function set_table_name($tbl)
    {
        $this->table_name = $tbl;
    }
    private function search()
    {
        if ($this->_search_keyword) {
            $arr = explode(" ", $this->_search_keyword);
            $keyword = "";
            foreach ($arr as $index => $k) {
                $keyword .= $index == 0 ? $k : " " . $k;
                $this->db->like($this->_search_row, $keyword);
            }
        }
    }

    /**
     * count all result from db where
     * @param $where_arr
     * @return mixed
     */
    function _count($where_arr = "")
    {
        if ($where_arr)
            $this->db->where($where_arr);

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }

        //search query
        $this->search();

        $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    /**
     * count all result from db where
     * @param $where_arr
     * @param $where_query
     * @return mixed
     */
    function _count_query($where_arr, $where_query)
    {
        $this->db->where($where_arr);
        $this->db->where($where_query, null, false);
        $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    /**
     * @param string $where_arr
     * @param string $keyword
     * @return mixed
     */
    function _count_search($where_arr = "", $keyword = "", $row_search = "name")
    {
        if ($where_arr)
            $this->db->where($where_arr);
        if ($keyword)
            $this->db->like($row_search, $keyword);
        $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    /**
     * get data in table from database
     * @param $where_arr
     * @param $order_arr
     * @return mixed
     */
    function _get($where_arr = "", $order_arr = "", $select = "*")
    {

        $this->db->select($select);

        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result();
    }

    function _get_array($where_arr = "", $order_arr = "", $select = "*")
    {

        $this->db->select($select);

        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result_array();
    }

    /**
     * get data in table from database (sql query)
     * @param $where_query
     * @param $order_arr
     * @param $select
     * @return mixed
     */
    function _get_query($where_query, $order_arr = "", $select = "*")
    {
        $this->db->select($select);
        $this->db->where($where_query, null, false);
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result();
    }

    /**
     * get all data from query select
     * @param string $select
     * @param string $where_query
     * @param string $order_arr
     * @return mixed
     */
    function _get_query_select($select = "*", $where_query = "", $order_arr = "")
    {
        $this->db->select($select);
        if ($where_query)
            $this->db->where($where_query, null, false);
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result();
    }

    /**
     * get limit result from query
     * @param $where_arr
     * @param string $order_arr
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    function _get_limit($where_arr, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*")
    {
        $this->db->select($select);
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }

        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }

        if ($this->_search_keyword) {
            $this->db->like($this->_search_row, $this->_search_keyword);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table_name)->result();
    }

    /**
     * get 1st row from query
     * @param $where_arr
     * @param string $order_arr
     * @param string $select
     * @return mixed
     */
    function _get_top_one($where_arr, $order_arr = "", $select = "*")
    {
        $this->db->select($select);
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit(1);
        return $this->db->get($this->table_name)->row();
    }


    /**
     * return row data
     * @param $where_arr
     * @param $select
     * @return mixed
     */
    function _get_row($where_arr = "", $select = "*")
    {
        $this->db->select($select);
        if ($where_arr)
            $this->db->where($where_arr);
        return $this->db->get($this->table_name)->row();
    }

    /**
     * delete row in database
     * @param $where_arr
     */
    function _delete($where_arr = "")
    {
        if ($where_arr)
            $this->db->where($where_arr);
        $this->db->delete($this->table_name);
    }

    /**
     * insert data to database
     * @param $data_arr
     * @return mixed
     */
    function _insert($data_arr)
    {
        $this->db->insert($this->table_name, $data_arr);
        $err = $this->db->error();
//        print_r($err);
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->insert_id();
    }

    function _insert_duplicate($data_arr)
    {
        $updatestr = array();
        $keystr = array();
        $valstr = array();

        foreach ($data_arr as $key => $val) {
            $updatestr[] = "`{$key}` = '" . $val . "'";
            $keystr[] = "`{$key}`";
            $valstr[] = "'{$val}'";
        }

        $sql = "INSERT INTO " . $this->config->item('dbprefix') . $this->table_name . " (" . implode(', ', $keystr) . ") ";
        $sql .= "VALUES (" . implode(', ', $valstr) . ") ";
        $sql .= "ON DUPLICATE KEY UPDATE " . implode(', ', $updatestr);
        return $this->db->query($sql);
    }

    /**
     * insert_batch multi data to db
     * @param $data_arr
     * @return mixed
     */
    function _insert_batch($data_arr)
    {
//        print_r($data_arr);
        $this->db->insert_batch($this->table_name, $data_arr);
        $err = $this->db->error();
//        print_r($err);
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->insert_id();
    }

    /**
     * update data to database
     * @param $data_arr
     * @param $where_arr
     */
    function _update($data_arr, $where_arr)
    {
        $this->db->update($this->table_name, $data_arr, $where_arr);
        $err = $this->db->error();
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->affected_rows();
    }

    /**
     * update data to database
     * @param $data_arr
     * @param $where_arr
     * @param $where_query
     */
    function _update_query($data_arr, $where_arr, $where_query)
    {
        $this->db->where($where_arr);
        $this->db->where($where_query, null, false);
        $this->db->update($this->table_name, $data_arr);
        $err = $this->db->error();
//        print_r($err);
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->affected_rows();
    }

    /**
     * increase field
     * @param $total
     * @param $total_plus
     * @param $where_arr
     */
    function _increase_field($total, $total_plus, $where_arr)
    {
        $this->db->where($where_arr);
        $this->db->set($total, $total_plus, FALSE);
        $this->db->update($this->table_name);
    }

    function is_exists_check($where_arr, $array_id = '')
    {
        if ($array_id != '') {
            $this->db->where($array_id);
        }
        $this->db->select("*")
            ->from($this->table_name)
            ->where($where_arr);
        return $this->db->count_all_results() > 0;
    }


//    /**
//     * only for permission =============================================================================================
//     */
    function get_user_permission($group_id)
    {
        $this->db->select("category, key");
        $this->db->from('permissions a');
        $this->db->join('permission_map b', 'a.id=b.permission_id', "left");
        $this->db->where('group_id', $group_id);
        return $this->db->get()->result();
    }

    function get_permission_func()
    {
        $this->db->select('category, category_name');
        $this->db->group_by('category');
        return $this->db->get('permissions')->result();
    }

    function get_permission_menu($group_id)
    {

    }

    function delete_invalid_admin_menu()
    {
        $query = "delete from tbl_admin_menu where category not in (select category from tbl_permissions group by category) and parent_id != null";
        $this->db->query($query);
    }

    /**
     * get limit result from query "where like"
     * @param $where_arr
     * @param string $order_arr
     * @param string $keyword
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    function _get_search_limit($where_arr, $keyword, $order_arr = "", $page = 1, $limit = ADMIN_ROW_LIMIT, $select = "*", $row_search = "name")
    {
        $this->db->select($select);
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }
        if ($row_search) {
            if ($keyword) {
                $this->db->like($row_search, $keyword);
            }
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        return $this->db->get($this->table_name)->result();
    }
}
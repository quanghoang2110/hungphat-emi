<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Frontend_Controller
 * base frontend controller
 * @author quanghv
 */
class GoFrontendController extends BaseController
{
    protected $_sort_by;

    protected $_logo, $_favicon, $_site_name, $_seo_title, $_seo_description, $_seo_keyword, $_seo_url, $_seo_image;
    protected $_fb_app_id, $_fb_fanpage, $_cache_on, $_gg_analytics_id, $_youtube, $_google_plus;
    protected $_com_name, $_com_email, $_com_phone, $_com_address, $_com_info, $_com_time,$_com_address2,$_com_google_map,$_com_skype;

    protected $_title, $_primary_name, $_default_name;

    protected $_top_slide_on_page = true;


    protected $_seo_title_detail,$_seo_description_detail,$_seo_keyword_detail,$_seo_image_detail,$_seo_url_detail;
    function __construct()
    {
        // Construct our parent class
        parent::__construct();

        //set option config
        $folder_arr = $this->config->item("folder");
        $this->_folder = $folder_arr['frontend'];

        $theme_arr = $this->config->item('theme');
        $this->_theme = $theme_arr['frontend'];

        if ($this->input->get('theme'))
            $this->_theme = $this->input->get('theme');

        $this->load->helper('go_url_helper');

        $this->_theme_tmp = $this->_folder . '/' . $this->_theme . '/template/';
        $this->_theme_assets = "/assets/{$this->_folder}/{$this->_theme}/";
        $this->_theme_view = $this->_folder . '/' . $this->_theme;

        $this->load->helper('cookie');
    }

    private function load_site_setting()
    {
        $tbl_site_setting = $this->config->item('setting');
//        if ($tbl_site_setting) {
        //load site info from data ------------------
//            $this->_load_data_from($tbl_site_setting);
        $this->_load_data_from('setting');
        $setting = $this->_model->_get_row();
        //meta tags
        $this->_site_name = $setting ? $setting->name : "";
        $this->_seo_title = $setting ? $setting->title : "";
        $this->_logo = $setting ? $setting->logo : "";
//            $this->_favicon = $setting ? $setting->favicon : "";
        $this->_seo_description = $setting ? $setting->description : "";
//            $this->_fb_app_id = $setting ? $setting->fb_app_id : "";
        $this->_fb_fanpage = $setting ? $setting->fb_fanpage : "";
        $this->_fb_app_id = $setting ? $setting->fb_app_id : "";
//            $this->_youtube = $setting ? $setting->youtube : "";
        $this->_google_plus = $setting ? $setting->gg_plus_id : "";
        $this->_cache_on = $setting ? $setting->cache_on : $this->config->item('cache_on');

        //compay site info
        $this->_com_address2 = $setting ? $setting->company_address2 : '';
        $this->_com_google_map = $setting ? $setting->google_map : '';
        //compay site info
        $this->_com_name = $setting ? $setting->company_name : '';
        $this->_com_email = $setting ? $setting->company_email : '';
        $this->_com_phone = $setting ? $setting->company_phone : '';
        $this->_com_time = $setting ? $setting->company_time : '';
        $this->_com_address = $setting ? $setting->company_address : '';
        $this->_com_info = $setting ? $setting->company_info : '';
        $this->_com_skype = $setting ? $setting->company_skype : '';

        $this->_seo_url = "";
//            $this->_seo_image = $setting ? $setting->df_img : $this->_logo;
//            $this->_gg_analytics_id = $setting ? $setting->gg_analytics_id : "";
//        }
    }

    private function load_static_page()
    {
//        if ($tbl_site_setting) {
        //load site info from data ------------------
//            $this->_load_data_from($tbl_site_setting);
        $this->_load_data_from('static_page');
        $this->_static_page = $static_page = $this->_model->_get();
    }

    private function load_menu()
    {
        //TODO load site menu here
        $this->_load_data_from('product_type');
        $menu = $this->_model->_get(array('valid' => 1), array('parent_id' => 'asc', 'position' => 'desc', 'id' => 'asc'), 'id,name,parent_id');
        $parent_arr = array();
        $sub_arr = array();

        //module 3 cap
        foreach ($menu as $item) {
            if (!$item->parent_id) {
                $parent_arr[] = $item;
            } else {

                foreach ($parent_arr as $index => $sub_1) {
                    if ($sub_1->id == $item->parent_id) {
                        $sub_arr[$index][] = $item;
                        $parent_arr[$index]->sub[] = $item;
                    }
                }

                foreach ($sub_arr as $index => $sub_1) {
                    foreach ($sub_1 as $index2 => $last) {
                        if ($item->parent_id == $last->id) {
                            $parent_arr[$index]->sub[$index2]->sub[] = $item;
                        }
                    }
                }
            }
        }

        $this->_module_vars['product_type'] = $parent_arr;
    }

    function _init_page()
    {
        $this->load->helper('go_paging');
        $this->_base_init_page();
        $this->_sort_by = $this->input->get('sort_by', true);
    }

    /**
     * @param $user_arr
     * user_id
     * user_fullname
     */
    function set_usersession($user_arr)
    {
        $user_id = $user_arr['user_id'];
        $user_fullname = $user_arr['user_fullname'];
        $user_email = $user_arr['user_email'];
        $session_arr = $this->config->item('session');
        $this->session->set_userdata(
            array(
                $session_arr['frontend_userid'] => $user_id,
                $session_arr['frontend_userfullname'] => $user_fullname,
                $session_arr['frontend_useremail'] => $user_email
            )
        );
    }

    /**
     * @return mixed
     */
    function get_usersession()
    {
        $session_arr = $this->config->item('session');
        $user['user_id'] = $this->session->userdata($session_arr['frontend_userid']);
        $user['user_email'] = $this->session->userdata($session_arr['frontend_useremail']);
        $user['user_fullname'] = $this->session->userdata($session_arr['frontend_userfullname']);
        return $user;
    }

    /**
     *
     */
    function desstroy_usersession()
    {
        $session_arr = $this->config->item('session');
        $this->session->unset_userdata($session_arr['frontend_userid']);
        $this->session->unset_userdata($session_arr['frontend_useremail']);
        $this->session->unset_userdata($session_arr['frontend_userfullname']);
    }

    /**
     * load template to view
     */
    //tuyen: load banner top trang chu
    private function load_banner_top_home()
    {
        $this->_load_data_from('banner');
        $baner_top = $this->_banner_top_home = $banner_top_home = $this->_model->_get_top_one(array('type' => BANNER_TOP, 'valid' => 1), array('id' => 'desc'));
        $this->_img_banner_top_home = $baner_top ? $baner_top->img : '';
        $this->_link_banner_top_home = $baner_top ? $baner_top->link : '';
        $this->_name_banner_top_home = $baner_top ? $baner_top->name : '';
    }
    function _load_tmp($view = '')
    {
        $this->load_banner_top_home();
        $this->load_site_setting();
        $this->load_static_page();
        $this->load_menu();

        //filter
        $this->_module_vars['page'] = $this->_page;
        $this->_module_vars['page_size'] = $this->_limit;
        $this->_module_vars['page_keyword'] = $this->_search;
        $this->_module_vars['page_sort_by'] = $this->_sort_by;

//        $vars['style'] = $this->_style;
        $this->_module_vars['site_name'] = $this->_site_name;
        $this->_module_vars['seo_title'] = ellipsize($this->_seo_title, 65);
        $this->_module_vars['_seo_title_detail'] = ellipsize($this->_seo_title_detail, 65);
        $this->_module_vars['logo'] = $this->_logo;
        $this->_module_vars['favicon'] = $this->_favicon;
        $this->_module_vars['seo_description'] = ellipsize($this->_seo_description, 165);
        $this->_module_vars['_seo_description_detail'] = ellipsize($this->_seo_description_detail, 165);
        $this->_module_vars['seo_keyword'] = $this->_seo_keyword ? $this->_seo_keyword : "";
        $this->_module_vars['_seo_keyword_detail'] = $this->_seo_keyword_detail ? $this->_seo_keyword_detail : "";
        $this->_module_vars['seo_url'] = $this->_seo_url;
        $this->_module_vars['_seo_url_detail'] = $this->_seo_url_detail;
        if (strpos($this->_seo_image, 'http') == false)
            $this->_seo_image = base_url($this->_seo_image);
        $this->_module_vars['seo_image'] = $this->_seo_image;
        $this->_module_vars['_seo_image_detail'] = $this->_seo_image_detail;
        $this->_module_vars['fb_fanpage'] = $this->_fb_fanpage;
        $this->_module_vars['youtube'] = $this->_youtube;
        $this->_module_vars['google_plus'] = $this->_google_plus;
        $this->_module_vars['fb_app_id'] = $this->_fb_app_id;
        $this->_module_vars['gg_analytics_id'] = $this->_gg_analytics_id;


        $user = $this->get_usersession();
        $this->_module_vars['user_id'] = $user['user_id'];
        $this->_module_vars['user_email'] = $user['user_email'];
        $this->_module_vars['user_fullname'] = $user['user_fullname'];

        $this->_module_vars['company_name'] = $this->_com_name;
        $this->_module_vars['company_phone'] = $this->_com_phone;
        $this->_module_vars['company_email'] = $this->_com_email;
        $this->_module_vars['company_address'] = $this->_com_address;
        $this->_module_vars['company_address2'] = $this->_com_address2;
        $this->_module_vars['company_time'] = $this->_com_time;
        $this->_module_vars['company_info'] = $this->_com_info;
        $this->_module_vars['company_skype'] = $this->_com_skype;
        $this->_module_vars['google_map'] = $this->_com_google_map;

        //banner_top_home
        $this->_module_vars['img_banner_top_home'] = $this->_img_banner_top_home;
        $this->_module_vars['link_banner_top_home'] = $this->_link_banner_top_home;
        $this->_module_vars['name_banner_top_home'] = $this->_name_banner_top_home;

        //static_page
        $this->_module_vars['static_page'] = $this->_static_page;

        //slide
//        if ($this->_top_slide_on_page) {
//            $this->_load_data_from('slider');
//            $this->_module_vars['slider'] = $this->_model->_get_limit(array('valid' => 1), array('id' => 'desc'), 1, 6);
//        }

        $this->_base_load_tmp($view);
    }

    function _error_page()
    {

    }

    /**
     * base ajax change pass
     */
    function _ajax_change_pass()
    {
        $tbl_user = $this->config->item('tbl_user');
        if ($tbl_user) {

            $this->_load_data_from($tbl_user);
            $current_pass = $this->input->post("cur_pass");
            $status = -1;
            $msg = "Vui lòng đăng nhập";
            if ($this->_sess_userid) {
                if ($this->_model->_count(
                    array(
                        'id' => $this->_sess_userid,
                        'password' => encryption_password($current_pass)
                    )
                )
                ) {
                    $new_pass = $this->input->post("new_pass");
                    $this->_model->_update(
                        array('password' => encryption_password($new_pass)), array("id" => $this->_sess_userid)
                    );
                    $status = 1;
                    $msg = "Bạn đã thay đổi mật khẩu thành công!";
                } else {
                    $status = 0;
                    $msg = "Mật khẩu hiện tại không chính xác!";
                }
            }

            echo json_encode(array(
                'status' => $status,
                'msg' => $msg
            ));
        }
    }

    /**
     * subscribe
     * - receive notification
     */
    function _ajax_subscribe()
    {
        $tbl_subscribe = $this->config->item('tbl_subscribe');
        if ($tbl_subscribe) {
            $email = $this->input->post('email');
            $status = -1;
            //$msg = "Vui lòng nhập vào email của bạn";
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $this->_load_data_from($tbl_subscribe);
                $this->_model->_insert(array('email' => $email));
                $status = 1;
                $msg = "<p class='text-success'>Đăng ký nhận thông báo thành công!</p>";
            } else {
                $msg = "<p class='text-danger'>Email đăng ký không hợp lệ, vui lòng kiểm tra lại!</p>";
            }

            echo json_encode(array(
                'status' => $status,
                'msg' => $msg
            ));
        }
    }

    /**
     * public account login
     */
    function ajax_login()
    {
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'required'
            )
        );

        $response = $this->_run_form_validation($rules);
        if ($response === true) {
            $this->_load_data_from('account');
            $user = $this->_model->_get_row(array(
                'email' => $email,
                'password' => encryption_password($password)
            ));
            if (!$user) {
                $status = -1;
                $msg = "Tài khoản hoặc mật khẩu không hợp lệ!";
            } else {
                $status = 1;
                $msg = "Đăng nhập thành công";
                $this->set_usersession(array(
                    'user_id' => $user->id,
                    'user_fullname' => $user->fullname,
                    'user_email' => $user->email
                ));
            }

            echo $this->_ajax_response_msg($status, $msg);
        } else
            echo $response;
    }

    function ajax_logout()
    {
        $this->desstroy_usersession();
        redirect("/");
    }

    /**
     * public account reg
     */
    function ajax_reg()
    {
        $this->_load_data_from('account');

        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $this->input->post('repassword', true);
        $phone = $this->input->post('phone', true);

        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_is_exists_email[email]',
                'errors' => array(
                    'is_exists' => 'Email này đã được sử dụng.',
                ),
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'required'
            ),
            array(
                'field' => 'repassword',
                'label' => 'Nhập lại mật khẩu',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'matches' => 'Nhập lại mật khẩu không chính xác.',
                ),
            ),
            array(
                'field' => 'phone',
                'label' => 'Số điện thoại',
                'rules' => 'numeric|min_length[9]|max_length[12]'
            )
        );

        $response = $this->_run_form_validation($rules);
        if ($response === true) {
            $data = array(
                'email' => $email,
                'password' => encryption_password($password),
                'phone' => $phone,
                'create_time' => date('Y-m-d H:i:s')
            );
            $this->_model->_insert($data);
        } else
            echo $response;
    }

    function is_exists_email($str)
    {
        return $this->_model->_count(array('email' => $str)) <= 0;
    }

    function ajax_reload_cart()
    {
        $user = $this->get_usersession();
        $emails = explode("@", $user['user_email']);
        $vars['user_id'] = $user['user_id'];
        $vars['user_fullname'] = $user['user_fullname'] ? $user['user_fullname'] : $emails[0];
        $this->load->vars($vars);
        $this->load->view("frontend/{$this->_theme}/ajax/cart");
    }
}
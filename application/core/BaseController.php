<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class BaseController
 * base class for controller (include backend & frontend)
 * @author quanghv
 */
class BaseController extends CI_Controller
{
    protected $_sess_userid, $_sess_group_id;
    protected $_module_vars, $_content, $_module, $_module_sub;
    protected $_page, $_limit, $_search, $_total, $_page_url;

    protected $_folder = 'frontend';
    protected $_folder_backend = 'backend';

    //custom theme
    protected $_theme, $_theme_tmp, $_theme_view;

    protected $session_conf;
    protected $_model;

    function __construct()
    {
        parent::__construct();
        $this->load->model("BaseModel", "model");
        $this->_model = new BaseModel();//$this->model;
    }

    /*
     * load data from table
     */
    function _load_data_from($table_name)
    {
        $this->_model->reset_all();
        $this->_model->table_name = $table_name;
    }

    /**
     * @param $query_arr
     */
    function _set_query($query_arr)
    {
        $this->_model->_query = isset($query_arr['query']) ? $query_arr['query'] : '';
        $this->_model->_search_keyword = isset($query_arr['keyword']) ? $query_arr['keyword'] : '';
        $this->_model->_search_row = isset($query_arr['search_row']) ? $query_arr['search_row'] : '';
    }

    /**
     * init page
     */
    function _base_init_page()
    {
        $this->_page = (int)$this->input->get("per_page", true);
        $this->_limit = (int)$this->input->get("per_size", true);
        $this->_search = str_replace('[removed]', '', trim($this->input->get("search", true)));
        $this->_page = $this->_page > 0 ? $this->_page : 1;
        $this->_limit = $this->_limit > 0 ? $this->_limit : $this->config->item('max_item');
    }

    /**
     * page info
     * @param $total
     * @return string
     */
    function _base_get_page_info($total)
    {
        $this->_page = $this->_page > 0 ? $this->_page : 1;
        $this->_total = $total;
        $item_from = (($this->_page - 1) * $this->_limit + 1) <= $total ? (($this->_page - 1) * $this->_limit + 1) : 0;
        $item_to = ($this->_page * $this->_limit) <= $total ? ($this->_page * $this->_limit) : $total;
        return "Hiển thị từ {$item_from} tới {$item_to} trong {$total} kết quả";
    }

    /**
     * load tmp
     * @param string $view
     */
    function _base_load_tmp($view = '')
    {
        $vars['theme'] = $this->_theme;
        $vars['theme_template'] = $this->_theme_tmp;
        $vars['theme_assets'] = $this->_theme_assets;
        $vars['theme_view'] = $this->_theme_view;

        $vars['module'] = $this->_module;
        $vars['module_sub'] = $this->_module_sub;
        $vars["content"] = $this->_folder . '/' . $this->_theme . '/' . $this->_content;
        //$vars['page_asset'] = $this->_page_asset = $this->_folder . "/{$this->_theme}/asset/" . $this->_content;

        $vars['sess_userid'] = $this->_sess_userid;

        if ($this->_module_vars) {
            $vars = array_merge($vars, $this->_module_vars);
        }

        $this->load->vars($vars);
        $view = $view ? $view : $this->_folder . '/base/base_tmp';

        $this->load->view($view);
    }
    /**
     * load tmp
     * @param string $view
     */
    function _base_load_tmp_backend($view = '')
    {
        $vars['theme'] = $this->_theme;
        $vars['theme_template'] = $this->_theme_tmp;
        $vars['theme_assets'] = $this->_theme_assets;
        $vars['theme_view'] = $this->_theme_view;

        $vars['module'] = $this->_module;
        $vars['module_sub'] = $this->_module_sub;
//        $vars["content"] = $this->_folder_backend . '/' . $this->_theme . '/' . $this->_content;
        $vars["content"] = $this->_folder_backend . '/alt/' . $this->_content;
        //$vars['page_asset'] = $this->_page_asset = $this->_folder . "/{$this->_theme}/asset/" . $this->_content;

        $vars['sess_userid'] = $this->_sess_userid;

        if ($this->_module_vars) {
            $vars = array_merge($vars, $this->_module_vars);
        }

        $this->load->vars($vars);
        $view = $view ? $view : $this->_folder_backend . '/base/base_tmp';

        $this->load->view($view);
    }

    /**
     * ajax base =======================================================================================================
     */

    /**
     * update or insert if not exists
     * @param $id
     * @param $data
     * @return $id
     */
    function _ajax_update($id, $data)
    {
        if ($id > 0) {
            $this->_model->_update($data, array('id' => $id));
        } else {
            $id = $this->_model->_insert($data);
        }

        return $id;
    }

    /**
     * delete record by id
     * @return int
     */
    function ajax_delete()
    {
        if ($this->_model) {
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    /**
     * update valid by id
     * @return int
     */
    function ajax_valid()
    {
        $id = $this->input->post("id", true);
        $valid = $this->input->post("valid", true);
        $valid = $valid == 1 ? 0 : 1;

        switch ($this->input->post('type')) {
            case 'is-home':
                $data = array('is_home' => $valid);
                break;
            default:
                $data = array('valid' => $valid);
                break;
        }

        if ($id > 0) {
            echo $this->_ajax_update($id, $data);
        }
    }

    function ajax_valid_old()
    {
        $id = $this->input->post("id");
        $valid = $this->input->post("valid");
        $valid = $valid == 1 ? 0 : 1;
        $data = array('valid' => $valid);
        if ($id > 0) {
            echo $this->_ajax_update($id, $data);
            die();
        }
    }
    /**
     * update valid by id
     * @return int
     */
    function ajax_hot()
    {
        $id = $this->input->post("id");
        $hot = $this->input->post("hot");
        $hot = $hot == 1 ? 0 : 1;
        $data = array('is_hot' => $hot);
        if ($id > 0) {
            echo $this->_ajax_update($id, $data);
            die();
        }
    }

    /**
     * form validation
     * @param array $config
     * @return mixed
     */
    function _validation($config = array())
    {
        $this->load->library('form_validation');

        foreach ($config as $item_config) {
            $this->form_validation->set_message($item_config["message_name"], $this->lang->line($item_config["message_name"]));
            $this->form_validation->set_rules($item_config["name"], $item_config["show_name"], $item_config["request"]);
        }

        return $this->form_validation->run();
    }

    function _last_query()
    {
        echo $this->db->last_query();
    }

    /**
     * @param $rules
     * @return bool|string
     */
    function _run_form_validation($rules, $delimiter_open = '', $delimiter_close = '')
    {
        $this->load->library('form_validation');
//        $this->form_validation->set_error_delimiters($delimiter_open, $delimiter_close);
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $status = -1;
            $msg = validation_errors($delimiter_open, $delimiter_close);
            return $this->_ajax_response_msg($status, $msg);
        } else
            return true;
    }

    /**
     * @param $status
     * @param $msg
     * @return string
     */
    function _ajax_response_msg($status, $msg)
    {
        return json_encode(array('status' => $status, 'msg' => $msg));
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class GoBackendController extends BaseController
{
    protected $_title, $_primary_name, $_default_name;

    protected $_sess_required = true;

    //custom theme
    protected $_theme, $_theme_tmp, $_theme_ajax;

    //permission
    protected $_permission_controller_name;
    protected $_permission_view = 'read', $_permission_edit = 'edit', $_permission_delete = 'delete', $_permission_category, $_permission_url;

    protected $_module_title, $_module_desc;

    protected $_search_enable = false;
    protected $_flag = false;

    function __construct()
    {
        // Construct our parent class
        parent::__construct();

        $this->load->helper('go_url_helper');

        $this->_folder = 'backend';

        $this->db->cache_off();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->lang->load('form_validation');

        //load theme config from config/maxcorp.php
        $theme_arr = $this->config->item('theme');
        $this->_theme = $theme_arr['backend'];

        //config
        $this->_module_vars['seo_title'] = "Quản trị hệ thống";

        $this->_theme_tmp = $this->_folder . '/' . $this->_theme . '/template/';
        $this->_theme_ajax = $this->_folder . '/' . $this->_theme . '/ajax/';
        $this->_theme_assets = "/assets/backend/{$this->_theme}/";
        $this->_theme_view = $this->_folder.'/'.$this->_theme;

        //session
        $session_arr = $this->config->item('session');
        $this->_sess_userid = $this->session->userdata($session_arr['backend_userid']);
        $this->_sess_group_id = $this->session->userdata($session_arr['backend_groupid']);

        if ($this->_sess_required && !$this->_sess_userid && !$this->_sess_group_id) {
            $this->session->set_flashdata("back_url", $_SERVER['REDIRECT_URL']);
            $this->session->sess_destroy();
            redirect($this->config->item('admin_login'));
            exit();
        }

        $this->_permission_category = $this->router->fetch_class();

        if ($this->config->item('permission_enable') && $this->_permission_controller_name) {
            $this->_permission_url = 'quan-tri/' . get_urltitle($this->_permission_controller_name);
            //set access permission (access controller - ex: news, product)
            $this->_set_access_permission($this->_permission_category);
            //check permission view
            if (!$this->check_permission($this->_permission_view, 'auto')) {
                $this->no_permission();
            } else {
                //echo "Có quyền truy cập";
            }
        }

        $this->load->helper('go_paging_helper');

    }

    private function no_permission()
    {
        //echo "";
        $data['msg'] = "<p style='color:red'>Bạn chưa được cấp quyền truy cập chức năng này!</p>";
        $data['url_redirect'] = base_url($this->config->item('admin_login'));
        echo $this->load->view("backend/base/thongbao", $data, true);
        exit();
    }

    private function _set_access_permission($category)
    {
//        $this->_permission_view = $category . "_read";
//        $this->_permission_edit = $category . "_edit";
//        $this->_permission_delete = $category . "_delete";

        //check permission
        $this->_load_data_from('permissions');
        $where = array('category' => $category);
        if ($this->_model->_count($where) != -3) {
//            $this->_model->_delete($where);
            $insert_read['name'] = "Xem";
            $insert_read['key'] = $this->_permission_view;
            $insert_read['category'] = $category;
            $insert_read['category_name'] = $this->_permission_controller_name;
            $this->_model->_insert_duplicate($insert_read);

            $insert_edit['name'] = "Sửa";
            $insert_edit['key'] = $this->_permission_edit;
            $insert_edit['category'] = $category;
            $insert_edit['category_name'] = $this->_permission_controller_name;
            $this->_model->_insert_duplicate($insert_edit);

            $insert_delete['name'] = "Xóa";
            $insert_delete['key'] = $this->_permission_delete;
            $insert_delete['category'] = $category;
            $insert_delete['category_name'] = $this->_permission_controller_name;
            $this->_model->_insert_duplicate($insert_delete);
        }

//        //check admin menu
//        $this->_load_data_from('admin_menu');
//        $menu_where = array(
//            'category' => $category
//        );
//
//        $new_menu = array(
//            'name' => $this->_permission_controller_name,
//            'icon' => 'fa fa-circle-o text-red',
//            'url' => $this->_permission_url
//        );
//        if ($this->_model->_count($menu_where) != 1) {
//            $this->_model->_delete($menu_where);
//            $new_menu['category'] = $category;
//            $this->_model->_insert($new_menu);
//        } else {
//            $this->_model->_update($new_menu, $menu_where);
//        }

        $this->update_routes();
    }

    private function update_routes()
    {
        $this->load->helper('file');
//        $this->_load_data_from('admin_menu');
//
//        $this->_model->delete_invalid_admin_menu();
//        $menu = $this->_model->_get(array('valid' => 1));
        $routes = "<?php \n";
//        foreach ($menu as $item) {
//            $routes .= "\$route['{$item->url}']='/backend/{$item->category}';\n";
//            $routes .= "\$route['{$item->url}/" . ROUTE_UPDATE . "']='/backend/{$item->category}/update';\n";
//            $routes .= "\$route['{$item->url}/" . ROUTE_VALID . "']='/backend/{$item->category}/ajax_valid';\n";
//            $routes .= "\$route['{$item->url}/" . ROUTE_DELETE . "']='/backend/{$item->category}/ajax_delete';\n";
//        }
        if (!write_file('file/routes_plus.php', $routes)) {
//            echo 'Unable to write the file';
        } else {
//            echo 'File written!';
        }
    }

    /**
     * set session for permission check
     * @param $user_id
     * @param $group_id
     */
    protected function _set_permission_sesson($user_id, $group_id)
    {
        $session_arr = $this->config->item('session');
        $sess_arr = array(
            $session_arr['backend_userid'] => $user_id,
            $session_arr['backend_groupid'] => $group_id
        );
        $this->session->set_userdata($sess_arr);

        $this->_sess_userid = $this->session->userdata($session_arr['backend_userid']);
        $this->_sess_group_id = $this->session->userdata($session_arr['backend_groupid']);
    }

    protected function check_permission($permission_code, $type = '')
    {
        if ($this->config->item('permission_enable')) {
            if ($permission_code && $this->_permission_controller_name) {
                //get user's permission
                $permission_arr = $this->_model->get_user_permission($this->_sess_group_id);
//                echo $this->_last_query() . '<br/>';
                foreach ($permission_arr as $item) {
                    $permission[] = $item->category . '_' . $item->key;
                }
                return in_array($this->_permission_category . '_' . $permission_code, $permission);
            } else {
                if ($type == 'auto') {
                    $this->no_permission();
                }
                return false;
            }
        } else
            return true;
    }

    function _init_page()
    {
        $this->_base_init_page();
        $this->_page_url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;
    }

    function _get_page_info($total)
    {
        $this->_base_get_page_info($total);
    }

    function _load_tmp($view = '')
    {
        $this->_module_vars['module_title'] = $this->_module_title;
        $this->_module_vars['module_desc'] = $this->_module_desc;
        $this->_module_vars['keyword'] = $this->_search;
        $this->_module_vars['page'] = $this->_page;
        $this->_module_vars['page_size'] = $this->_limit;
        $this->_module_vars['search_enable'] = $this->_search_enable;

        //admin menu
        $this->_module_vars['permission_url'] = $this->_permission_url;

        $this->_base_load_tmp_backend($view);
    }

    /**
     * @param $id
     * @param $data
     * @param $view
     * @return mixed
     */
    function _ajax_update($id, $data, $view = "", $data_more = "")
    {

        //user has permission edit
        if ($this->check_permission($this->_permission_edit)) {
            if ($id > 0) {
                $this->_model->_update($data, array('id' => $id));
            } else {
                $id = $this->_model->_insert($data);
            }

            if ($view) {
                $ls = $this->_model->_get_row(array('id' => $id));
                foreach ($data_more as $key => $val) {
                    $ls->$key = $val;
                }
                //array_merge($ls, $data_more);
                $this->_load_updated_views($ls, $view);
            } else
                return $id;

        }
    }

    function _load_updated_views($ls, $view)
    {
        $vars['p'] = $this->input->post('in_p');
        $vars['ls'] = $ls;
        $this->load->vars($vars);
//        print_r($ls);
        $this->load->view($this->_theme_ajax . $view);
    }

    function ajax_delete()
    {
        //user has permission delete
//        if ($this->check_permission($this->_permission_delete)) {
//            parent::ajax_delete();
//        }
        parent::ajax_delete();
    }

    /**
     *
     */
//    function ajax_ishome()
//    {
//        $id = $this->input->post("id");
//        $valid = $this->input->post("ishome");
//        $valid = $valid == 1 ? 0 : 1;
//        $this->_model->_update(array('is_home' => $valid), array('id' => $id));
//        echo $valid;
//    }
//
//    function ajax_ismenu()
//    {
//        $id = $this->input->post("id");
//        $valid = $this->input->post("ismenu");
//        $valid = $valid == 1 ? 0 : 1;
//        $this->_model->_update(array('is_menu' => $valid), array('id' => $id));
//        echo $valid;
//    }
//
//    function ajax_change_position()
//    {
//        $id = $this->input->post("id");
//        $position = $this->input->post("position");
//        $this->_model->_update(array('position' => $position), array('id' => $id));
//        echo 1;
//    }
    function _load_model($model_name)
    {
        $this->load->model($model_name, "model");
        $this->_model = $this->model;
    }

    public function _ValidateFormCustom($config = array())
    {
        $this->lang->load('form_validation');
        foreach ($config as $item_config) {
            $this->form_validation->set_message($item_config["message_name"], $this->lang->line($item_config["message_name"]));
            $this->form_validation->set_rules($item_config["name"], $item_config["show_name"], $item_config["request"]);
        }

        if ($this->form_validation->run() == TRUE) {
            $this->_flag = true;
        }
        return $this->_flag;
    }
}
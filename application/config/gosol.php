<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//general
$config['site_type'] = 'site';
$config['max_item'] = 10;
$config['dbprefix'] = 'tbl_';

//su dung jquery
$config['jquery'] = true;

//su dung material design lib
$config['material'] = false;

$config['material_icons'] = true;
$config['mdb_lib'] = false;

$config['font.roboto'] = true;
//load fontawsome
$config['font.awesome'] = true;
//load fontawsome
$config['font.material'] = false;

//$config['error_404'] = false;

//folder
$config['folder']['frontend'] = 'frontend';
$config['folder']['backend'] = 'backend';

//theme
$config['theme']['frontend'] = "hungphat";
$config['theme']['backend'] = "alt";


//config for using cache
$config['cache_on'] = '0';
$config['cache_time'] = 7 * 24 * 60;

//config for db
$config['tbl_user'] = '';
$config['tbl_menu'] = '';
$config['tbl_subscribe'] = '';
$config['tbl_site_setting'] = '';

//other
$config['custom_url_suffix'] = '.html';

//route
$config['admin_login'] = ADMIN_DANG_NHAP;

//session
$config['session']['backend_groupid'] = 'session_groupid_abcxyz';
$config['session']['backend_userid'] = 'session_id_abcxyz';

$config['session']['frontend_userid'] = 'session_id_public';
$config['session']['frontend_useremail'] = 'session_email_public';
$config['session']['frontend_userfullname'] = 'session_fullname_public';


/**
 * to using permission function, you need 3 tables in db
 *
 * 1. permission:
 *
 * CREATE TABLE `permissions` (
 * `id` int(11) NOT NULL AUTO_INCREMENT,
 * `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 * `key` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
 * `category` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
 * PRIMARY KEY (`id`),
 * UNIQUE KEY `key` (`key`)
 * ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 *
 * 2. permission group:
 *
 *
 * CREATE TABLE `permission_group` (
 * `id` int(11) NOT NULL AUTO_INCREMENT,
 * `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 * `valid` tinyint(1) DEFAULT 1,
 * PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 *
 * 3. permission mapping:
 *
 * CREATE TABLE `permission_map` (
 * `group_id` int(11) NOT NULL DEFAULT '0',
 * `permission_id` int(11) NOT NULL DEFAULT '0',
 * PRIMARY KEY (`group_id`,`permission_id`),
 * KEY `permission_id` (`permission_id`),
 * CONSTRAINT `permission_map_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `permission_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 * CONSTRAINT `permission_map_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
 * ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 *
 */
$config['permission_enable'] = false;
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @theme config
 * 
 * theme: thebox
 */

$config['body_class'] = "option6";
$config['body_id'] = "";
$config['menu_onload'] = true;
$config['menu_model'] = 'model_product_type';
$config['theme_style'] = 'option7_custom';
$config['show_other_script'] = false;
$config['product_code'] = 'GOFOOD-';
$config['account_cookie'] = 'ACC_COOKIE';
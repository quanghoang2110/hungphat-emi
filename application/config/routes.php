<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[QUAN_TRI] = 'backend/dashboard';
$route[ADMIN_DANG_NHAP] = 'backend/login';
$route[ADMIN_DANG_XUAT] = 'backend/login/logout';
$route[ADMIN_DOI_MATKHAU] = 'backend/login/change_pass';
$route[ADMIN_ROUTE_ADMIN_MENU] = 'backend/admin_menu';
$route[ADMIN_ROUTE_ADMIN_MENU . '/' . ROUTE_UPDATE] = 'backend/admin_menu/update';
//$route[ADMIN_ROUTE_ADMIN_MENU] = 'backend/permission_group/ajax_update_permission';

$route[ROUTE_FRONTEND_LIEN_HE] = 'lien-he';


$route[ROUTE_ADMIN_GALLERY_CONTENT_UPLOAD] = 'backend/gallery_content/ajax_upload_file';

$route[ROUTE_ADMIN_VIEW_PERMISSION] = 'backend/permission_group/ajax_view_permission';

$route['tin-tuc'] = 'frontend/news';
$route['tuyen-dung'] = 'frontend/news_recruitment';

$route[ROUTE_PERMISSION_UPDATE] = 'backend/permission_group/ajax_update_permission';

$route['default_controller'] = 'home';
$route['gioi-thieu'] = 'frontend/static_page/index/gioi-thieu';
$route[ROUTE_GALLERY] = 'frontend/gallery';
$route[ROUTE_GALLERY . '/(.+html)'] = 'frontend/gallery/detail/$1';
$route[ROUTE_GALLERY . '/(:any)'] = 'frontend/gallery/index/$1';

$this->config->load('gosol');
$theme_arr = $this->config->item('theme');
$theme = $theme_arr['frontend'];
$frontend_folder = 'frontend/' . $theme . '/';

//tim kiem san pham
$route[PUBLIC_ROUTE_SEARCH] = $frontend_folder . "product_type/search";

$route[PUBLIC_ROUTE_AJAX_LOGIN] = $frontend_folder . "product_type/ajax_login";
$route[PUBLIC_ROUTE_AJAX_LOGOUT] = $frontend_folder . "product_type/ajax_logout";
$route[PUBLIC_ROUTE_AJAX_REG] = $frontend_folder . "product_type/ajax_reg";
$route[PUBLIC_ROUTE_AJAX_REFRESH_CART] = $frontend_folder . "product_type/ajax_reload_cart";

$route['kiem-tra-dang-ky.html'] = $frontend_folder . "login/check_register_user";
$route['kiem-tra-dang-nhap.html'] = $frontend_folder . "login/index";
$route['lay-lai-mat-khau.html'] = $frontend_folder . "login/get_pass_before";
$route[ROUTE_FRONTEND_GET_PASS] = $frontend_folder.'login/get_token_change_pass';
$route[FRONTEND_LOGIN_FACEBOOK] = $frontend_folder.'login/login_facebook_a';
$route['dang-xuat.html'] = $frontend_folder . "login/logout";
//$route[ROUTE_STATIC_PAGE.'.html'] = $frontend_folder . "static_page2";
//$route[ROUTE_STATIC_PAGE] = "frontend/hungphat/static_page2/detail";
$route[ROUTE_CONTACT_PAGE] = "frontend/contact/contact_page";
$route[ROUTE_BUY_CART] = $frontend_folder . "product/buy_cart";
$route[ROUTE_SHOW_CART] = $frontend_folder . "product/get_product_cart";
$route[ROUTE_DELTE_ITEM_CART] = $frontend_folder . "product/delete_product_cart";
$route[ROUTE_ADD_NUMBER_CART] = $frontend_folder . "product/add_number_product_cart";
$route['xac-nhan-don-hang.html'] = $frontend_folder . "product/form_payment";
$route['dat-hang-thanh-cong.html'] = $frontend_folder . "product/process_payment";

$route[ROUTE_NEWS.'.html'] = $frontend_folder . "news";
$route[ROUTE_NEWS . '/(:any)'.'.html'] = $frontend_folder . "news/detail/$1";
$route[ROUTE_NEWS_RECRUITMENT.'.html'] = $frontend_folder . "news";
$route[ROUTE_NEWS_RECRUITMENT . '/(:any)'.'.html'] = $frontend_folder . "news/detail/$1";
$route[ROUTE_NEWS_SALE.'.html'] = $frontend_folder . "news";
$route[ROUTE_NEWS_SALE . '/(:any)'.'.html'] = $frontend_folder . "news/detail/$1";

$route[ROUTE_STATIC_PAGE . '/(:any)' . '.html'] = "frontend/hungphat/Static_page2/detail/$1";
$route['lien-he'] = 'frontend/contact';
$route[ROUTE_PRODUCT . '/(:any)'.'.html'] = $frontend_folder . 'product/index';
$route[ROUTE_PRODUCT_DETAIL . '/(:any)'.'.html'] = $frontend_folder . 'product/detail/$1';
$route[ROUTE_PRODUCT_HOT . '(:any)'.'.html'] = $frontend_folder . 'product/product_hot/$1';
$route[ROUTE_SEARCH] = $frontend_folder . 'product/search';

$route[ROUTE_INTRODUCE] = 'frontend/introduce';
$route[ROUTE_PRODUCT_PUBLISHER] = 'frontend/product_publisher';

$route[ROUTE_AJAX_INCREASE_VIEWED] = $frontend_folder . 'product/increase_viewed';




/*tuyen*/
//nha cung cap
$route[QUAN_TRI_NHA_CUNG_CAP] = 'backend/provider';
$route[QUAN_TRI_NHA_CUNG_CAP_FORM] = 'backend/provider/update';
$route[QUAN_TRI_NHA_CUNG_CAP_UPDATE] = 'backend/provider/ajax_update';
$route[QUAN_TRI_NHA_CUNG_CAP_DELETE] = 'backend/provider/ajax_delete';
$route[QUAN_TRI_NHA_CUNG_CAP_AJAX_VALID] = 'backend/provider/ajax_valid_old';

//loai san pham
$route[QUAN_TRI_LOAI_SAN_PHAM] = 'backend/product_type';
$route[QUAN_TRI_LOAI_SAN_PHAM_FORM] = 'backend/product_type/update';
$route[QUAN_TRI_LOAI_SAN_PHAM_UPDATE] = 'backend/product_type/ajax_update';
$route[QUAN_TRI_LOAI_SAN_PHAM_DELETE] = 'backend/product_type/ajax_delete';
$route[QUAN_TRI_LOAI_SAN_PHAM_AJAX_VALID] = 'backend/product_type/ajax_valid_old';

//san pham
$route[QUAN_TRI_SAN_PHAM] = 'backend/product';
$route[QUAN_TRI_SAN_PHAM_FORM] = 'backend/product/update';
$route[QUAN_TRI_SAN_PHAM_UPDATE] = 'backend/product/ajax_update';
$route[QUAN_TRI_SAN_PHAM_DELETE] = 'backend/product/ajax_delete';
$route[QUAN_TRI_SAN_PHAM_AJAX_VALID] = 'backend/product/ajax_valid_old';
$route[QUAN_TRI_SAN_PHAM_AJAX_HOT] = 'backend/product/ajax_hot';

//Don hang
$route[QUAN_TRI_DON_HANG] = 'backend/account_order';
$route[QUAN_TRI_DON_HANG_FORM] = 'backend/account_order/update';
$route[QUAN_TRI_DON_HANG_UPDATE] = 'backend/account_order/ajax_update';
$route[QUAN_TRI_DON_HANG_DELETE] = 'backend/account_order/ajax_delete';
$route[QUAN_TRI_DON_HANG_AJAX_VALID] = 'backend/account_order/ajax_valid_old';
$route[QUAN_TRI_DON_HANG_AJAX_HOT] = 'backend/account_order/ajax_hot';
//tin tuc
$route[QUAN_TRI_TIN_TUC] = 'backend/news';
$route[QUAN_TRI_TIN_TUC_FORM] = 'backend/news/update';
$route[QUAN_TRI_TIN_TUC_UPDATE] = 'backend/news/ajax_update';
$route[QUAN_TRI_TIN_TUC_DELETE] = 'backend/news/ajax_delete';
$route[QUAN_TRI_TIN_TUC_AJAX_VALID] = 'backend/news/ajax_valid_old';

//san pham
$route[QUAN_TRI_NHOM_TIN_TUC] = 'backend/news_type';
$route[QUAN_TRI_NHOM_TIN_TUC_FORM] = 'backend/news_type/update';
$route[QUAN_TRI_NHOM_TIN_TUC_UPDATE] = 'backend/news_type/ajax_update';
$route[QUAN_TRI_NHOM_TIN_TUC_DELETE] = 'backend/news_type/ajax_delete';
$route[QUAN_TRI_NHOM_TIN_TUC_AJAX_VALID] = 'backend/news_type/ajax_valid_old';

//san pham
$route[QUAN_TRI_LIEN_HE] = 'backend/contact';
$route[QUAN_TRI_LIEN_HE_FORM] = 'backend/contact/update';
$route[QUAN_TRI_LIEN_HE_UPDATE] = 'backend/contact/ajax_update';
$route[QUAN_TRI_LIEN_HE_DELETE] = 'backend/contact/ajax_delete';
$route[QUAN_TRI_LIEN_HE_AJAX_VALID] = 'backend/contact/ajax_valid_old';

//nhom tintuc
$route[QUAN_TRI_CAM_NHAN_KHACH_HANG] = 'backend/comment';
$route[QUAN_TRI_CAM_NHAN_KHACH_HANG_FORM] = 'backend/comment/update';
$route[QUAN_TRI_CAM_NHAN_KHACH_HANG_UPDATE] = 'backend/comment/ajax_update';
$route[QUAN_TRI_CAM_NHAN_KHACH_HANG_DELETE] = 'backend/comment/ajax_delete';
$route[QUAN_TRI_CAM_NHAN_KHACH_HANG_AJAX_VALID] = 'backend/comment/ajax_valid_old';

/*
 * site_setting
 * */
$route[ADMIN_ROUTE_SITE_SETTING] = 'backend/setting';
$route[ADMIN_ROUTE_SITE_SETTING_UPDATE] = 'backend/setting/update';

//slider
$route[QUAN_TRI_SLIDER] = 'backend/slide';
$route[QUAN_TRI_SLIDER_FORM] = 'backend/slide/update';
$route[QUAN_TRI_SLIDER_UPDATE] = 'backend/slide/submit_update';
$route[QUAN_TRI_SLIDER_DELETE] = 'backend/slide/ajax_delete';
$route[QUAN_TRI_SLIDER_AJAX_VALID] = 'backend/slide/ajax_valid_old';

//banner
$route[QUAN_TRI_BANNER_QUANG_CAO] = 'backend/banner';
$route[QUAN_TRI_BANNER_QUANG_CAO_FORM] = 'backend/banner/update';
$route[QUAN_TRI_BANNER_QUANG_CAO_UPDATE] = 'backend/banner/ajax_update';
$route[QUAN_TRI_BANNER_QUANG_CAO_DELETE] = 'backend/banner/ajax_delete';
$route[QUAN_TRI_BANNER_QUANG_CAO_AJAX_VALID] = 'backend/banner/ajax_valid_old';

//Trang gioi thieu
$route[QUAN_TRI_TRANG_GIOI_THIEU] = 'backend/page_introduce';
$route[QUAN_TRI_TRANG_GIOI_THIEU_UPDATE] = 'backend/page_introduce/update';
//$route[QUAN_TRI_NHA_CUNG_CAP] = 'backend/provider';
//$route[QUAN_TRI_NHA_CUNG_CAP] = 'backend/provider';
/*end tuyen*/

//trang tinh
//tin tuc
$route[QUAN_TRI_TRANG_TINH] = 'backend/static_page2';
$route[QUAN_TRI_TRANG_TINH_FORM] = 'backend/static_page2/update';
$route[QUAN_TRI_TRANG_TINH_UPDATE] = 'backend/static_page2/ajax_update';
//$route[QUAN_TRI_TRANG_TINH_DELETE] = 'backend/static_page/ajax_delete';
//$route[QUAN_TRI_TRANG_TINH_AJAX_VALID] = 'backend/static_page/ajax_valid_old';


$route['404_override'] = "error";
$route["404"] = "frontend/{$theme}/error";

include FCPATH . 'file/routes_plus.php';


$uri_str = $this->uri->uri_string;
if (!SETUP_PERMISSION) {
    $controller = $this->uri->segment(1);
    if ($uri_str && !array_key_exists($uri_str, $route)
        && !in_array($controller, array(ROUTE_NEWS, ROUTE_NEWS_SALE, ROUTE_NEWS_RECRUITMENT, ROUTE_GALLERY, ROUTE_PRODUCT, ROUTE_STATIC_PAGE,ROUTE_PRODUCT_HOT))
    ) {
        $segs = $this->uri->segment_array();
        //print_r($segs);
        $last = str_replace($this->config->item('custom_url_suffix'), '', $segs[count($segs)]);
        $last_arr = explode('-', $last);
        $id = $last_arr[count($last_arr) - 1];
        if ($id > 0) {
            // id -> is product
            //echo $id;
            $route[$this->uri->uri_string()] = "frontend/hungphat/product/detail/{$id}";
        }
//        else
//            $route[$this->uri->uri_string()] = "frontend/home/index";
    }
}
//print_r($route);

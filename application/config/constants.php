<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * * ===================================================================================================================
 */
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('SETUP_PERMISSION', FALSE);

define('URL_SUFFIX', '.html');
define('NO_SUFFIX', 'NONE');
define('IMG_NEWS_DEFAULT', '/assets/base/img/img_df.jpg');
define('IMG_SLIDER_DEFAULT', '/assets/base/img/img_df.jpg');
define('IMG_PACK_DEFAULT', '/assets/base/img/img_df.jpg');

define('CAPTCHA_WORD', 'captcha_sess');
define('ROUTE_UPDATE', 'cap-nhat');
define('ROUTE_VALID', 'kha-dung');
define('ROUTE_DELETE', 'xoa');

define('PHONE_CONTACT', '04.9981.666');

#
define('ROUTE_CAC_MAU_THIET_KE', 'cac-mau-thiet-ke');
define('ROUTE_CONTACT_NOW', 'lien-he');

define('ERROR_404', '404');


/**
 * custom define
 */
define('ROUTE_ADMIN_VIEW_PERMISSION', 'quan-tri/ajax-view-permission');
define('ROUTE_PERMISSION_UPDATE', 'quan-tri/ajax-update-permission');

define('ROUTE_ADMIN_GALLERY_CONTENT_UPLOAD', 'quan-tri/ajax-gallery-content-upload-file');

define('QUAN_TRI', 'quan-tri');
define('ADMIN_DANG_NHAP', 'quan-tri/dang-nhap');
define('ADMIN_DANG_XUAT', 'quan-tri/dang-xuat');
define('ADMIN_DOI_MATKHAU', 'quan-tri/doi-mat-khau');
define('ADMIN_ROUTE_TEST', 'abc-test');
define('ADMIN_ROUTE_ADMIN_MENU', 'quan-tri/admin-menu');

define('ROUTE_FRONTEND_LIEN_HE', 'lien-he');
//
define('PUBLIC_ROUTE_SEARCH', 'tim-kiem');
define('PUBLIC_ROUTE_AJAX_LOGIN', 'dang-nhap');
define('PUBLIC_ROUTE_AJAX_LOGOUT', 'dang-xuat');
define('PUBLIC_ROUTE_AJAX_REG', 'dang-ky');
define('PUBLIC_ROUTE_AJAX_REFRESH_CART', 'reload-cart');

define('ROUTE_GALLERY', 'goc-chia-se');
/*product*/
define('ROUTE_PRODUCT','san-pham');
define('ROUTE_NEWS', 'tin-tuc');
define('ROUTE_STATIC_PAGE', 'trang-tinh');
define('ROUTE_CONTACT_PAGE', 'gui-lien-he');
define('ROUTE_PRODUCT_DETAIL','chi-tiet-san-pham');
define('ROUTE_NEWS_RECRUITMENT', 'tuyen-dung');
define('ROUTE_NEWS_SALE', 'khuyen-mai');
define('ROUTE_INTRODUCE', 'gioi-thieu.html');
define('ROUTE_PRODUCT_PUBLISHER', 'download.html');
/*tuyen*/
define('DEFAULT_IMG', '/images/hungphat/default/image.png');
define('ADMIN_ROW_LIMIT', 10);
define('THEME_FRONTEND', 'frontend'); // change theme for frontend
define('THEME_BACKEND', 'backend/alt/');

define('THEME_BACKEND_TMP', THEME_BACKEND . 'template/');
/*nha cung cap*/
define('QUAN_TRI_NHA_CUNG_CAP', 'quan-tri/nha-cung-cap');
define('QUAN_TRI_NHA_CUNG_CAP_FORM', 'quan-tri/nha-cung-cap/cap-nhat');
define('QUAN_TRI_NHA_CUNG_CAP_UPDATE', 'quan-tri/nha-cung-cap/update');
define('QUAN_TRI_NHA_CUNG_CAP_AJAX_VALID', 'quan-tri/nha-cung-cap/valid');
define('QUAN_TRI_NHA_CUNG_CAP_DELETE', 'quan-tri/nha-cung-cap/delete');

/*loai-san-pham*/
define('QUAN_TRI_LOAI_SAN_PHAM', 'quan-tri/loai-san-pham');
define('QUAN_TRI_LOAI_SAN_PHAM_FORM', 'quan-tri/loai-san-pham/cap-nhat');
define('QUAN_TRI_LOAI_SAN_PHAM_UPDATE', 'quan-tri/loai-san-pham/update');
define('QUAN_TRI_LOAI_SAN_PHAM_AJAX_VALID', 'quan-tri/loai-san-pham/valid');
define('QUAN_TRI_LOAI_SAN_PHAM_DELETE', 'quan-tri/loai-san-pham/delete');

/*San pham*/
define('QUAN_TRI_SAN_PHAM', 'quan-tri/san-pham');
define('QUAN_TRI_SAN_PHAM_FORM', 'quan-tri/san-pham/cap-nhat');
define('QUAN_TRI_SAN_PHAM_UPDATE', 'quan-tri/san-pham/update');
define('QUAN_TRI_SAN_PHAM_AJAX_VALID', 'quan-tri/san-pham/valid');
define('QUAN_TRI_SAN_PHAM_AJAX_HOT', 'quan-tri/san-pham/hot');
define('QUAN_TRI_SAN_PHAM_DELETE', 'quan-tri/san-pham/delete');

/*Don hang*/
define('QUAN_TRI_DON_HANG', 'quan-tri/don-hang');
define('QUAN_TRI_DON_HANG_FORM', 'quan-tri/don-hang/cap-nhat');
define('QUAN_TRI_DON_HANG_UPDATE', 'quan-tri/don-hang/update');
define('QUAN_TRI_DON_HANG_AJAX_VALID', 'quan-tri/don-hang/valid');
define('QUAN_TRI_DON_HANG_AJAX_HOT', 'quan-tri/don-hang/hot');
define('QUAN_TRI_DON_HANG_DELETE', 'quan-tri/don-hang/delete');
/*Nhóm Tin tuc*/
define('QUAN_TRI_NHOM_TIN_TUC', 'quan-tri/nhom-tin-tuc');
define('QUAN_TRI_NHOM_TIN_TUC_FORM', 'quan-tri/nhom-tin-tuc/cap-nhat');
define('QUAN_TRI_NHOM_TIN_TUC_UPDATE', 'quan-tri/nhom-tin-tuc/update');
define('QUAN_TRI_NHOM_TIN_TUC_AJAX_VALID', 'quan-tri/nhom-tin-tuc/valid');
define('QUAN_TRI_NHOM_TIN_TUC_DELETE', 'quan-tri/nhom-tin-tuc/delete');
/*Tin tuc*/
define('QUAN_TRI_TIN_TUC', 'quan-tri/tin-tuc');
define('QUAN_TRI_TIN_TUC_FORM', 'quan-tri/tin-tuc/cap-nhat');
define('QUAN_TRI_TIN_TUC_UPDATE', 'quan-tri/tin-tuc/update');
define('QUAN_TRI_TIN_TUC_AJAX_VALID', 'quan-tri/tin-tuc/valid');
define('QUAN_TRI_TIN_TUC_DELETE', 'quan-tri/tin-tuc/delete');
/*Trang tinh*/
define('QUAN_TRI_TRANG_TINH', 'quan-tri/trang-tinh');
define('QUAN_TRI_TRANG_TINH_FORM', 'quan-tri/trang-tinh/cap-nhat');
define('QUAN_TRI_TRANG_TINH_UPDATE', 'quan-tri/trang-tinh/update');
//define('QUAN_TRI_TRANG_TINH_AJAX_VALID', 'quan-tri/trang-tinh/valid');
//define('QUAN_TRI_TRANG_TINH_DELETE', 'quan-tri/trang-tinh/delete');

/*Lien he*/
define('QUAN_TRI_LIEN_HE', 'quan-tri/lien-he');
define('QUAN_TRI_LIEN_HE_FORM', 'quan-tri/lien-he/cap-nhat');
define('QUAN_TRI_LIEN_HE_UPDATE', 'quan-tri/lien-he/update');
define('QUAN_TRI_LIEN_HE_AJAX_VALID', 'quan-tri/lien-he/valid');
define('QUAN_TRI_LIEN_HE_DELETE', 'quan-tri/lien-he/delete');

/*user_feel*/
define('QUAN_TRI_CAM_NHAN_KHACH_HANG', 'quan-tri/cam-nhan-khach-hang');
define('QUAN_TRI_CAM_NHAN_KHACH_HANG_FORM', 'quan-tri/cam-nhan-khach-hang/cap-nhat');
define('QUAN_TRI_CAM_NHAN_KHACH_HANG_UPDATE', 'quan-tri/cam-nhan-khach-hang/update');
define('QUAN_TRI_CAM_NHAN_KHACH_HANG_AJAX_VALID', 'quan-tri/cam-nhan-khach-hang/valid');
define('QUAN_TRI_CAM_NHAN_KHACH_HANG_DELETE', 'quan-tri/cam-nhan-khach-hang/delete');

/*site setting*/
define('ADMIN_ROUTE_SITE_SETTING', 'site-setting');
define('ADMIN_ROUTE_SITE_SETTING_UPDATE', 'site-setting/cap-nhat');

/*Slider*/
define('QUAN_TRI_SLIDER', 'quan-tri/slide');
define('QUAN_TRI_SLIDER_FORM', 'quan-tri/slide/cap-nhat');
define('QUAN_TRI_SLIDER_UPDATE', 'quan-tri/slide/update');
define('QUAN_TRI_SLIDER_AJAX_VALID', 'quan-tri/slide/valid');
define('QUAN_TRI_SLIDER_DELETE', 'quan-tri/slide/delete');

//banner
define('BANNER_QUANG_CAO_TRANG_CHU', 1);
define('BANNER_QUANG_CAO_TRANG_CHI_TIET_SAN_PHAM', 2);
define('BANNER_QUANG_CAO_TOP_TRANG_CHU', 3);
define('BANNER_QUANG_CAO_TRANG_TIN_TUC', 4);
define('BANNER_QUANG_CAO_TRANG_TUYEN_DUNG', 5);
define('BANNER_QUANG_CAO_TRANG_KHUYEN_MAI', 6);
define('QUAN_TRI_BANNER_QUANG_CAO', 'quan-tri/banner-quang-cao');
define('QUAN_TRI_BANNER_QUANG_CAO_FORM', 'quan-tri/banner-quang-cao/cap-nhat');
define('QUAN_TRI_BANNER_QUANG_CAO_UPDATE', 'quan-tri/banner-quang-cao/update');
define('QUAN_TRI_BANNER_QUANG_CAO_AJAX_VALID', 'quan-tri/banner-quang-cao/valid');
define('QUAN_TRI_BANNER_QUANG_CAO_DELETE', 'quan-tri/banner-quang-cao/delete');

//trang gioi thieu
define('QUAN_TRI_TRANG_GIOI_THIEU', 'quan-tri/trang-gioi-thieu');
define('QUAN_TRI_TRANG_GIOI_THIEU_UPDATE', 'quan-tri/trang-gioi-thieu/update');
//define('QUAN_TRI_NHOM_SAN_PHAM', 'ADMIN_MENU_PROVIDER/nhom-san-pham');
//define('QUAN_TRI_SAN_PHAM', 'quan-tri/san-pham');
/*end tuyen*/

//frontend: tin tuc
define('TIN_KHUYEN_MAI', 1);
define('TIN_TUC', 2);
define('TIN_TUYEN_DUNG', 3);
define('PUBLIC', 1);
define('CHUA_DIEN_THONG_TIN', -1);
define('CHUA_GIAO_HANG', 0);
define('HOAN_THANH', 1);
define('SUM_IMG_PRODUCT', 4);

define('BANNER_NGANG', 1);
define('BANNER_VUONG', 2);
define('BANNER_TOP', 3);

define('ROUTE_AJAX_INCREASE_VIEWED', 'increase-product-viewed');

//trang tinh
define('TRANG_HUONG_DAN', 1);
define('TRANG_CAM_KET_CUA_CHUNG_TOI', 2);
define('TRANG_THANH_TOAN_VA_MUA_HANG', 3);
define('TRANG_USER_LINK', 4);
define('TRANG_PHUONG_THUC_VAN_CHUYEN', 5);
define('TRANG_VAN_CHUYEN_VA_BAO_HANH', 6);
define('TRANG_DIEU_KIEN_BAO_HANH', 7);
define('TRANG_CHINH_SACH_VA_BAO_MAT', 8);

define('ROUTE_BUY_CART','dat-mua.html');
define('ROUTE_SHOW_CART','gio-hang.html');
define('ROUTE_DELTE_ITEM_CART','xoa-sp-gio-hang.html');
define('ROUTE_ADD_NUMBER_CART','so-luong-gio-hang.html');
define('USERNAME_FRONTEND_SESSION','username_session_dt');
define('ID_FRONTEND_SESSION','userid_session_dt')   ;
define('FRONTEND_LOGIN_FACEBOOK','dang-nhap-facebook');
define('ROUTE_SEARCH','san-pham-tim-kiem.html');
define('ROUTE_FRONTEND_GET_PASS','lay-lai-mat-khau-token.html');
define('DEFAULT_PASSWORD','123456');
define('ROUTE_PRODUCT_HOT','san-pham-');
